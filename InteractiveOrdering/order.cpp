#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Point_2.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include<iostream>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <unistd.h>
#include <vector>
#include <boost/lexical_cast.hpp>
#include<sstream>
#include<fstream>
using namespace cv;
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Point_2<K> point;
point pts[1000],org[1000];
int ptsi=0;
Mat image;
void drawpoly(int cx,int cy,Vec3b c)
{
    cv::Point rook_points[1][20];
    rook_points[0][0] = cv::Point(cx-2,cy-2);
    rook_points[0][1] = cv::Point(cx-2,cy+2);
    rook_points[0][2] = cv::Point(cx+2,cy+2);
    rook_points[0][3] = cv::Point(cx+2,cy-2);
    const Point* ppt[1] = { rook_points[0] };
    int npt[] = { 4 };
    fillPoly(image, ppt, npt,1,c,8);
}
double distance(float x1,float y1,float x2,float y2)
{
    return sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
}
point prev;
int first=1;
FILE *fp;
float minx=9999,maxx=-9999,miny=9999,maxy=-9999;
float scale;
void onmouse(int event, int x, int y, int flags, void* param)
{
    int ind;
    double min=9999;
    if(event==1&&y<900)
    {
        for(int i=0;i<ptsi;i++)
            if(distance(x,y,pts[i].x(),pts[i].y())<min)
            {
                min=distance(x,y,pts[i].x(),pts[i].y());
                ind=i;
            }
        if(first==0)
            line(image,Point(prev.x(),prev.y()),Point(pts[ind].x(),pts[ind].y()),Scalar(0,0,255),1,8);
        else
            first=0;
        prev=pts[ind];
        drawpoly(pts[ind][0],pts[ind][1],Vec3b(0,0,255));
        float p1=(pts[ind][0]-20)*(scale/650.0)+minx;
        float p2=(pts[ind][1]-20)*(scale/650.0)+miny;
        double min=9999;
        for(int i=0;i<ptsi;i++)
            if(distance(p1,p2,org[i].x(),org[i].y())<min)
            {
                min=distance(p1,p2,org[i].x(),org[i].y());
                ind=i;
            }
        fprintf(fp,"%f %f\n",org[ind][0],org[ind][1]);
    }
    cv::imshow("Display Image", image);
}

int main(int argc, char** argv )
{
    image = cv::imread("image.png", 1 );
    char inputname[100];
    strcpy(inputname,argv[1]);
    std::fstream myfile(inputname,std::ios::in | std::ios::out);
    inputname[strlen(inputname)-4]='\0';
    strcat(inputname,"out.txt");
    fp=fopen(inputname,"w");
    int number;
    int counter = 0;
    float a;
    int fl=0;
    float x1,y1;
    while (myfile >> a)
    {
        if(fl==0)
        {
            x1=a;
            fl=1;
            if(x1>maxx)
                maxx=x1;
            if(x1<minx)
                minx=x1;
        }
        else
        {
            y1=a;
            fl=0;
            if(y1>maxy)
                maxy=y1;
            if(y1<miny)
                miny=y1;
            pts[ptsi]=point(x1,y1);
            org[ptsi]=point(x1,y1);
            ptsi++;
        }
    }
    if(maxx-minx>maxy-miny)
        scale=maxx-minx;
    else
        scale=maxy-miny;
    for(int i=0;i<ptsi;i++)
        pts[i]=point((pts[i].x()+(0-minx))*(650/(scale))+20,(pts[i].y()+(0-miny))*(650/(scale))+20);
    for(int i=0;i<ptsi;i++)
        drawpoly(pts[i][0],pts[i][1],Vec3b(0,0,0));
    cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE );
    setMouseCallback("Display Image", onmouse, &image);
    cv::imshow("Display Image", image);
    cv::waitKey(0);
    fclose(fp);
    return 0;
}
