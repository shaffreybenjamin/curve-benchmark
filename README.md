# curve-benchmark

Benchmark to evaluate curve reconstruction algorithms


First, execute ./install-ubuntu20.04.sh on Ubuntu 20.04LTS system to download referenced source codes and required libraries.

Then, execute "make" in the created directory to build the binaries

Directory structure:
src/		Benchmark and interface source code
datasets/	Data sets for evaluation
other dir's 	Curve reconstruction algorithms

Please execute cd src; ./Replicate.sh for generating all the Tables/Figures shown in the paper. Note that this may take several hours on generates ~2GB of data.
