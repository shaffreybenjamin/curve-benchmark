#!/bin/sh

# The references versions which were available at the time of creating this benchmark are used (17.09.2018). The current versions can be replaced into the same directories and rebuilt (although with no guarantee of functionality).

#  current version available at: http://sourceforge.net/projects/connect2dlib/
wget -q -O connect2d-1.0.2.tar.gz http://sourceforge.net/projects/connect2dlib/files/connect2d-1.0.2.tar.gz/download
tar zxf connect2d-1.0.2.tar.gz
rm connect2d-1.0.2.tar.gz
cd connect2d-1.0.2; patch < ../connect2d.diff; cd ..

# for git projects, current version available with 'git -C <directory> checkout master'
git clone -q https://github.com/stefango74/hnn-crust-sgp16.git
git -C hnn-crust-sgp16 checkout -q ef67f835f0467da314d45544106a678933bcdb51
patch hnn-crust-sgp16/install.sh < hnn-crust-install.sh.diff
cd hnn-crust-sgp16; git apply ../hnn-src.diff; cd ..

git clone -q https://github.com/stefango74/fitconnect.git
git -C fitconnect checkout -q 0dde2526da58225058522bcd409578051cb53898
patch fitconnect/install.sh < fitconnect-install.sh.diff
cd fitconnect; git apply ../fitconnect-src.diff; cd ..

git clone -q https://github.com/stefango74/stretchdenoise.git
git -C stretchdenoise checkout -q 6aa1a56b8d3294855590828d3c4957843204e327
patch stretchdenoise/install.sh < stretchdenoise-install.sh.diff
cd stretchdenoise; git apply ../stretchdenoise-src.diff; cd ..

git clone -q https://github.com/reproducibilitystamp/Crawl_through_Neighbors.git
git -C Crawl_through_Neighbors checkout -q 01020483573fd4027ea47c8704b445d5347b7cd7
patch Crawl_through_Neighbors/script.sh < crawl-script.sh.diff

git clone -q https://github.com/amaldevp/Peeling_the_Longest.git
git -C Peeling_the_Longest checkout -q ba894372b417f604b3b639c09e21dd115a340e59
patch Peeling_the_Longest/Noise_Removal/script.sh < peeling-noise-script.sh.diff
patch Peeling_the_Longest/Reconstruct/script.sh < peeling-reconstruct-script.sh.diff


git clone -q https://github.com/jijup/VoronoiCurve.git
git -C VoronoiCurve checkout -q 6c046cb76b210c7aceddb0dc975c15322c3c39c3
patch VoronoiCurve/script.sh < voronoi-script.sh.diff

git clone -q https://github.com/jijup/Shapehull2D.git
git -C Shapehull2D checkout -q 27ad0bf7cd12c50c300fa3ad840266d4a5e7b6d9
patch Shapehull2D/script.sh < shapehull-script.sh.diff


#git clone -q https://github.com/an-ng/VICUR.git
#git -C VICUR checkout -q c74edbb2bf46a1e66d5d10242b9132bfb828d4ef
#patch VICUR/src/2DCRTool.csproj < 2DCRTool.csproj.diff
