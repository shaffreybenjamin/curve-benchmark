DIRS += connect2d-1.0.2
DIRS += hnn-crust-sgp16
DIRS += fitconnect
DIRS += stretchdenoise
DIRS += VICUR
DIRS += crust-family
DIRS += src

.PHONY: all clean

all:
	for dir in $(DIRS); do ($(MAKE) -C $$dir) || exit 1; done

clean:
	for dir in $(DIRS); do ($(MAKE) -C $$dir clean) || exit 1; done
