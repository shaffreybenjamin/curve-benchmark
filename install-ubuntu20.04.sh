#!/bin/sh

# first, download the references source codes:
echo DOWNLOADING SOURCE CODES:
sudo apt install -qqq --assume-yes git
sh ./download-sources.sh

# then, install required libraries:
echo INSTALLING REQUIRED LIBRARIES:

# enabling 'universe' repository (e.g. for server, live-install Ubuntu)
sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) universe"

# curve benchmark
sudo apt install -qqq --assume-yes libargtable2-dev

# connect2d:
sudo apt install -qqq --assume-yes libcgal-dev

cd hnn-crust-sgp16; ./install.sh; cd ..
cd fitconnect; ./install.sh; cd ..
cd stretchdenoise; ./install.sh; cd ..

# VICUR/DISCUR:
sudo apt install -qqq --assume-yes mono-complete

echo INSTALL FINISHED. NOW BUILD WITH 'make'
