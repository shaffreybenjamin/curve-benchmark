/*
 * TestConnect2D.cpp
 *
 *  Created on: 2011-05-17
 *      Author: Stefan Ohrhallinger
 */

#include <fstream>
#include <string>

#include "Connect2D.h"

using namespace std;
using namespace connect2d;

// constants to control SVG output can be changed here:
#define SVG_PIXEL_DIM 1000	// x and y resolution on screen
#define SVG_LINE_WIDTH 1.0	// stroke size of boundary edges
#define SVG_DOT_RADIUS 4.0	// radius of dots


class TestConnect2D
{
private:
	string filename;
	vector<pair<double, double> > pointVec;
	Connect2D *connectInstance;
	vector<int> *manifoldBoundary;

	/*
	 * outputs edges and vertices of boundary as SVG file
	 */
	void writeSVG(string name, vector<int> *manifoldBoundary)
	{
		string svgHeaderPart1="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
			"<!-- Created with Connect2D -->\n"
			"<svg\n"
			"	xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
			"	xmlns:cc=\"http://creativecommons.org/ns#\"\n"
			"	xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
			"	xmlns:svg=\"http://www.w3.org/2000/svg\"\n"
			"	xmlns=\"http://www.w3.org/2000/svg\"\n"
			"	xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"\n"
			"	xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"\n"
			"	width=\"";
		string svgHeaderPart2="\"\n"
			"	height=\"";
		string svgHeaderPart3="\"\n"
			"	id=\"svg2\"\n"
			"	version=\"1.1\"\n"
			"	inkscape:version=\"0.48.1 r9760\"\n"
			"	sodipodi:docname=\"";
		string svgHeaderPart4 = "\">\n"
			"<defs\n"
			"	id=\"defs4\" />\n"
			"<metadata\n"
			"	id=\"metadata7\">\n"
			"	<rdf:RDF>\n"
			"		<cc:Work\n"
			"			rdf:about=\"\">\n"
			"			<dc:format>image/svg+xml</dc:format>\n"
			"			<dc:type\n"
			"				rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\n"
			"			<dc:title></dc:title>\n"
			"		</cc:Work>\n"
			"	</rdf:RDF>\n"
			"</metadata>\n"
			"<g\n"
			"	inkscape:label=\"Layer 1\"\n"
			"	inkscape:groupmode=\"layer\"\n"
			"	id=\"layer1\">\n";
		string svgFooter = "	</g>\n"
			"</svg>";

		// determine maximum extension in either dimension
		double min[2] = { pointVec[0].first, pointVec[0].second };
		double max[2] = { pointVec[0].first, pointVec[0].second };

		for (vector<int>::iterator iter = manifoldBoundary->begin(); iter != manifoldBoundary->end(); iter++)
		{
			int index = *iter;

			if (pointVec[index].first < min[0])
				min[0] = pointVec[index].first;

			if (pointVec[index].first > max[0])
				max[0] = pointVec[index].first;

			if (pointVec[index].second < min[1])
				min[1] = pointVec[index].second;

			if (pointVec[index].second > max[1])
				max[1] = pointVec[index].second;
		}

		double dim[2] = { max[0] - min[0], max[1] - min[1] };
		double maxDim = (dim[0] > dim[1]) ? dim[0] : dim[1];
		double factor = (SVG_PIXEL_DIM - 2*SVG_DOT_RADIUS)/maxDim;

		ofstream svgFile;
		filename = name + ".svg";
		svgFile.open(filename.data());
		svgFile << svgHeaderPart1 << name << svgHeaderPart2 << SVG_PIXEL_DIM << svgHeaderPart3 << SVG_PIXEL_DIM << svgHeaderPart4;

		// output boundary as closed path of edges
		svgFile << "	<path style=\"fill:none;stroke:#000000;stroke-width:" << SVG_LINE_WIDTH << "px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\" d=\"M";

		for (vector<int>::iterator iter = manifoldBoundary->begin(); iter != manifoldBoundary->end(); iter++)
		{
			int index = *iter;
			svgFile << " " << ((pointVec[index].first - min[0])*factor + SVG_DOT_RADIUS) << "," << ((pointVec[index].second - min[1])*factor + SVG_DOT_RADIUS);
		}

		svgFile << " z\" id=\"boundary\" inkscape:connector-curvature=\"0\" />\n";

		// output dots for vertices
		for (vector<int>::iterator iter = manifoldBoundary->begin(); iter != manifoldBoundary->end(); iter++)
		{
			int index = *iter;
			svgFile << "	<circle cx=\"" << ((pointVec[index].first - min[0])*factor + SVG_DOT_RADIUS) << "\" cy=\""  << ((pointVec[index].second - min[1])*factor + SVG_DOT_RADIUS) << "\" r=\"" << SVG_DOT_RADIUS << "\" fill=\"#0\" id=\"circle" << index << "\"/>\n";
		}

		svgFile << svgFooter;
		svgFile.close();
	}

	/*
	 * output boundary as successive vertex indices
	 */
	void writeBoundary(string name, vector<int> *manifoldBoundary)
	{
		ofstream bndFile;
		filename = name + ".bnd";
		bndFile.open(filename.data());

		for (vector<int>::iterator iter = manifoldBoundary->begin(); iter != manifoldBoundary->end(); iter++)
			bndFile << *iter << " ";

		bndFile << "\n";
		bndFile.close();
	}

	/*
	 * load point set from file with name
	 */
	void loadPointSet(string name)
	{
		pointVec.clear();
		ifstream file(name.data());

		if (file)
		{
			while (!file.eof())
			{
				float x, y;
				file >> x >> y;

				if (!file.eof())
					pointVec.push_back(pair<double, double>(x, y));
			}
		}
		else
		{
			cerr << "ERROR: input file " << name << " could not be read." << endl;
			exit(2);
		}

		file.close();

		if (pointVec.size() < 3)
		{
			cerr << "ERROR: input file " << name << " contains less than 3 points." << endl;
			exit(3);
		}
	}

public:
	TestConnect2D(string filename)
	{
		loadPointSet(filename);
		connectInstance = new Connect2D(&pointVec);
		manifoldBoundary = connectInstance->getBoundary();

		cout << manifoldBoundary->size() << " of " << pointVec.size() << " points interpolated in boundary" << endl;

		writeBoundary(filename, manifoldBoundary);
		writeSVG(filename, manifoldBoundary);
	}
};

int main(int argc, char **argv)
{
	string filename;

	if (argc >= 2)
	{
		filename = string(argv[1]);
		TestConnect2D instance = TestConnect2D(filename);

		return 0;
	}
	else
	{
		cout << "Usage: " << argv[0] << " filename" << endl;
		return 1;
	}
}
