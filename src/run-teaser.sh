#!/bin/bash

evalname=teaser
_alglist="connect2d hnncrust fitconnect stretchdenoise discur vicur crawl peel crust nncrust ccrust gathan1 gathang lenz"
_filelist="bird28.png.txt"

env alglist="${_alglist}" filelist="${_filelist}" ./run-evalsub.sh ${evalname} ../testdata/manifold yes

gnuplot -c teaser.plot

