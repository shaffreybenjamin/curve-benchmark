set title "Algorithm Runtime"
set style data histograms
set style histogram cluster
set style fill solid border
set autoscale
set yrange [0.0001:5]
set logscale y
set xtic auto rotate by 45 right
set ytic auto
set xlabel "Algorithm" 
set ylabel "Average runtime in millisec (logscale)."
set term pdfcairo
set output 'manifold-runtime.pdf'
plot "manifold.dat" using 10:xticlabels(1) notitle
