#ifndef CRAWL_H_
#define CRAWL_H_

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_hierarchy_2.h>
#include "CurveBenchmarkDataTypes.h"
#include<string.h>
#include<fstream>
#define NENDS 2

namespace crawl
{
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<int, K> Vb;
typedef CGAL::Triangulation_face_base_2<K> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
typedef CGAL::Delaunay_triangulation_2<K,Tds> Delaunay;
typedef K::Point_2   Point;

class Crawl
{
public:
	void _reconstruct(std::vector<Point> p);
	std::vector<std::pair<Point,Point>> *getBoundary();
};
}

#endif /* CRAWL_H_ */
