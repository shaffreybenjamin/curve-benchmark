/*
 * LenzWrapper.h
 *
 *  Created on: Dec 6, 2018
 *      Author: stef
 */

#ifndef LENZWRAPPER_H_
#define LENZWRAPPER_H_

#include "CurveBenchmarkDataTypes.h"
//#include "../lenz/main.cpp"

using namespace std;

class LenzWrapper
{
public:
	void reconstruct(vector<CBPoint> &cbpoints, list<CBEdge> &cbedges);
};

#endif /* LENZWRAPPER_H_ */
