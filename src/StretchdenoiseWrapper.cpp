/*
 * StretchdenoiseWrapper.cpp
 *
 *  Created on: Nov 8, 2018
 *      Author: stef
 */

#include "StretchdenoiseWrapper.h"

using namespace stretchdenoise;

void StretchdenoiseWrapper::reconstruct(vector<CBPoint> &cbpoints, vector<float> noise, vector<CBPoint> &newcbpoints, list<CBEdge> &cbedges)
{
	int i;
	vector<Point> points(cbpoints.size());

	for (i = 0; i < (int)cbpoints.size(); i++)
	{
		points[i][0] = cbpoints[i].x;
		points[i][1] = cbpoints[i].y;
	}

	Reconstruct2D *instance;

	if (noise.size() == 0)
		instance = new Reconstruct2D(points, MODE_DENOISE);
	else
	if (noise.size() == 1)
		instance = new Reconstruct2D(points, noise[0], MODE_DENOISE);
	else
		instance = new Reconstruct2D(points, noise, MODE_DENOISE);

	instance->setMaxIter(-1);	// standard value, unlimited iterations
	instance->reconstructNoisy();
	map<pair<int, int>, EdgeEnum> edgeMap = instance->getEdgeMap();

	for (auto edgeItem:edgeMap)
	{
		CBEdge cbedge;
		cbedge.v[0] = edgeItem.first.first;
		cbedge.v[1] = edgeItem.first.second;
		cbedges.push_back(cbedge);
	}

	vector<Point> newpoints = instance->getDenoisedPoints();
	newcbpoints.resize(newpoints.size());

	for (i = 0; i < (int)newpoints.size(); i++)
	{
		newcbpoints[i].x = newpoints[i][0];
		newcbpoints[i].y = newpoints[i][1];
	}
}

