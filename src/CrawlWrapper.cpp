#include "CrawlWrapper.h"

using namespace crawl;

void CrawlWrapper::reconstruct(std::vector<CBPoint> &cbpoints, std::list<CBEdge> &cbedges)
{
	int i;
	std::vector<Point> points(cbpoints.size());
	std::map<Point, int> pointMap;

	for (i = 0; i < (int)cbpoints.size(); i++)
	{
		points[i] = Point(cbpoints[i].x, cbpoints[i].y);
		pointMap[points[i]] = i;
	}

	Crawl *crawl = new Crawl();
	crawl->_reconstruct(points);
	std::vector<std::pair<Point,Point>> *boundary = crawl->getBoundary();
	
	for (auto item:*boundary)
	{
		CBEdge cbedge;
		cbedge.v[0] = pointMap[item.first];
		cbedge.v[1] = pointMap[item.second];
		cbedges.push_back(cbedge);
	}
}

