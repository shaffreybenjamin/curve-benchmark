/*
 * OptimalTransportWrapper.cpp
 *
 *  Created on: Dec 24, 2018
 *      Author: Jiju
 */

#include "OptimalTransportWrapper.h"

void OptimalTransportWrapper::reconstruct(vector<CBPoint> &cbpoints, vector<CBPoint> &newcbpoints, list<CBEdge> &cbedges, int iterations)
{
	int i;

	vector<Point_> points(cbpoints.size());

	for (i = 0; i < (int)cbpoints.size(); i++)
	{
                Point_ p(cbpoints[i].x, cbpoints[i].y);
		points.push_back(p);
	}

	Otr_2 otr2(points);
        otr2.run(iterations);

        
        std::vector<Point_> newpoints;
        std::vector<std::size_t> isolated_vertices; //Ignored for now
        std::vector<std::pair<std::size_t,std::size_t> > edges;

        otr2.indexed_output(
        std::back_inserter(newpoints),
        std::back_inserter(isolated_vertices),
        std::back_inserter(edges));
        

  // points

  newcbpoints.resize(newpoints.size());

  i=0;
  std::vector<Point_>::iterator p_pit;
  for (p_pit = newpoints.begin(); p_pit != newpoints.end(); p_pit++) {
	newcbpoints[i].x = p_pit->x();
        newcbpoints[i].y = p_pit->y();
        i++;

	}
 

  // edges
  std::vector<std::pair<std::size_t, std::size_t> >::iterator eit;
  for (eit = edges.begin(); eit != edges.end(); eit++){
   CBEdge cbedge;
   cbedge.v[0] = (int)eit->first;
   cbedge.v[1] = (int)eit->second;
   cbedges.push_back(cbedge);
    
  }

// isolated vertices
  std::vector<std::size_t>::iterator vit;
  for (vit = isolated_vertices.begin(); vit != isolated_vertices.end(); vit++){
        CBEdge cbedge;
   cbedge.v[0] = (int)*vit;
   cbedge.v[1] = (int)*vit;
   cbedges.push_back(cbedge);
}

       
	
}




