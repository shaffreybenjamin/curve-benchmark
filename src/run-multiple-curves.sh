#!/bin/bash

evalname=multiple-curves
_alglist="hnncrust fitconnect discur vicur crawl peel crust nncrust ccrust gathan1 gathang"
_filelist="mc1.txt mc2.txt mc3.txt mc4.txt mc5.txt mc6.txt mc7.txt mc8.txt mc9.txt mc10.txt mc11.txt mc12.txt mc13.txt mc14.txt mc15.txt mc16.txt mc17.txt mc18.txt mc19.txt mc20.txt mc21.txt mc22.txt mc23.txt mc24.txt mc25.txt mc26.txt mc27.txt mc28.txt mc29.txt mc30.txt mc31.txt mc32.txt mc33.txt mc34.txt mc35.txt mc36.txt mc37.txt mc38.txt mc39.txt mc40.txt mc41.txt mc42.txt mc43.txt mc44.txt mc45.txt mc46.txt mc47.txt mc48.txt mc49.txt mc50.txt mc51.txt mc52.txt"

_imagelist="toilet075.png.txt toilet275.png.txt"

_filepath1=../testdata/multiple-curves/classic-data/

_filepath2=../testdata/multiple-curves/image-data/inputs/

_gt=../testdata/multiple-curves/image-data/groundtruths/


env alglist="${_alglist}" filelist="${_filelist}" imagelist="${_imagelist}" inputdir2=${_filepath2} gtdir=${_gt} ./run-evalsub-modified.sh ${evalname} ${_filepath1} yes 

gnuplot -c multiple-curves.plot



