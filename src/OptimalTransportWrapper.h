/*
 * OPtimalTransportWrapper.h
 *
 *  Created on: Dec 24, 2018
 *      Author: Jiju
 */

#ifndef OPTIMALTANSPORTWRAPPER_H_
#define OPTIMALTANSPORTWRAPPER_H_

#include "CurveBenchmarkDataTypes.h"
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Optimal_transportation_reconstruction_2.h>
#include <fstream>
#include <iostream>
#include <string>
#include <iterator>
#include <vector>
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::FT                                               FT;
typedef K::Point_2                                          Point_;
typedef K::Segment_2                                        Segment;
typedef CGAL::Optimal_transportation_reconstruction_2<K>    Otr_2;

using namespace std;

class OptimalTransportWrapper
{
public:
	void reconstruct(vector<CBPoint> &cbpoints,vector<CBPoint> &newcbpoints, list<CBEdge> &cbedges, int iterations);
};

#endif /* OPTIMALTANSPORTWRAPPER_H_ */



