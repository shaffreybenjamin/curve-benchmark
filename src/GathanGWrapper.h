#ifndef GATHANGWRAPPER_H_
#define GATHANGWRAPPER_H_

#include "../crust-family/recon2d.h"
#include "CurveBenchmarkDataTypes.h"

class GathanGWrapper
{
public:
void reconstruct(std::vector<CBPoint> &cbpoints, std::list<CBEdge> &cbedges, float min_corner_angle);
};

#endif /* GATHANGWRAPPER_H_ */
