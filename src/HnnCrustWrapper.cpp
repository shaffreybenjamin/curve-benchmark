/*
 * HnnCrustWrapper.cpp
 *
 *  Created on: Dec 5, 2018
 *      Author: stef
 */

#include "HnnCrustWrapper.h"

using namespace hnn;

void HnnCrustWrapper::reconstruct(vector<CBPoint> &cbpoints, list<CBEdge> &cbedges)
{
	int i;
	vector<Point> points(cbpoints.size());

	for (i = 0; i < (int)cbpoints.size(); i++)
	{
		points[i][0] = cbpoints[i].x;
		points[i][1] = cbpoints[i].y;
	}

	Reconstruct2D *instance = new Reconstruct2D(points, true);
	instance->reconstruct();
	map<pair<int, int>, EdgeEnum> edgeMap = instance->getEdgeMap();

	for (auto edgeItem:edgeMap)
	{
		CBEdge cbedge;
		cbedge.v[0] = edgeItem.first.first;
		cbedge.v[1] = edgeItem.first.second;
		cbedges.push_back(cbedge);
	}
}

