/*
 * CurveBenchmarkDataTypes.h
 *
 *  Created on: Nov 8, 2018
 *      Author: stef
 */

#ifndef CURVEBENCHMARKDATATYPES_H_
#define CURVEBENCHMARKDATATYPES_H_

#include <vector>
#include <list>

typedef struct
{
	double x, y;
} CBPoint;

typedef struct
{
	int v[2];
} CBEdge;

typedef struct
{
	bool isManifold;
	bool isClosed;
	bool isConnected;
	double timeElapsed;
} Result;

typedef struct
{
	bool isExact;
	float distMax;
	float distRMSE;
	float angleMax;
	float angleRMSE;
        float symDiffArea;
} Evaluation;

#endif /* CURVEBENCHMARKDATATYPES_H_ */
