evalname=outliers
_alglist="connect2d hnncrust fitconnect stretchdenoise crawl peel crust nncrust ccrust gathan1 gathang lenz discur vicur"
_filelist="mc10.txt  mc14.txt  mc20.txt  mc29.txt  mc45.txt  mc5.txt  mc9.txt
mc11.txt  mc15.txt  mc21.txt  mc32.txt  mc47.txt  mc6.txt
mc12.txt  mc16.txt  mc24.txt  mc3.txt   mc4.txt   mc7.txt
mc13.txt  mc19.txt  mc26.txt  mc44.txt  mc52.txt  mc8.txt"

for outliers in  5 10 20; do
 echo "Running with ${outliers} share of outliers:"
env alglist="${_alglist}" filelist="${_filelist}" ./run-evalsub.sh ${evalname}-${outliers} ../testdata/manifold/classic-data/ yes -u ${outliers};
done

gnuplot -c outliers.plot
