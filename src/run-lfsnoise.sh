#!/bin/bash

evalname=lfsnoise
_alglist="connect2d hnncrust fitconnect stretchdenoise crawl peel crust nncrust ccrust gathan1 gathang lenz discur vicur"
_filelist="bunny.svg"

for noise in 0.1 0.3333 0.5; do
  echo "Running with lfs noise ${noise}:"
  env alglist="${_alglist}" filelist="${_filelist}" ./run-evalsub.sh ${evalname}-${noise} ../testdata/sampling no -e0.3 -n${noise};
done

gnuplot -c lfsnoise.plot
