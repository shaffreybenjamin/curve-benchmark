#ifndef PEELWRAPPER_H_
#define PEELWRAPPER_H_

#include "Peel.h"

class PeelWrapper
{
public:
void reconstruct(std::vector<CBPoint> &cbpoints, std::list<CBEdge> &cbedges, float len);
};

#endif /* PEELWRAPPER_H_ */
