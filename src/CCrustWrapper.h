#ifndef CCRUSTWRAPPER_H_
#define CCRUSTWRAPPER_H_

#include "../crust-family/recon2d.h"
#include "CurveBenchmarkDataTypes.h"

class CCrustWrapper
{
public:
void reconstruct(std::vector<CBPoint> &cbpoints, std::list<CBEdge> &cbedges, float cc_parameter);
};

#endif /* CCRUSTWRAPPER_H_ */
