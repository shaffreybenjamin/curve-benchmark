#!/bin/bash

evalname=Non-manifold
_alglist="crust nncrust ccrust lenz peel"
_filelist="Figure10.txt   Figure16.txt   nm10out.txt  nm6out.txt   OTFig18g.txt Figure14a.txt  Figure19b.txt  nm1out.txt   nm7out.txt    OTFig18k.txt Figure14b.txt  Figure1.txt    nm2out.txt   nm8out.txt    OTFig19b.txt Figure14c.txt  Figure9.txt    nm3out.txt   nm9out.txt    OTFig1.txt Figure14d.txt  nm4out.txt   OTFig14.txt Figure14e.txt   nm5out.txt   OTFig18a.txt"

env alglist="${_alglist}" filelist="${_filelist}" ./run-evalsub.sh ${evalname} ../testdata/Non-manifold yes

gnuplot -c Non-manifold.plot


