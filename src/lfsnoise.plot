set title "Sampled with lfs-dependent noise"
set style data histograms
set style histogram cluster
set style fill solid border
set key over
set autoscale
set xtic auto rotate by 45 right
set yrange [0.00001:0.05]
#set logscale y
set xtic auto
set ytic auto
set xlabel "Algorithm"
set ylabel "RMS Error in terms of bounding box diagonal"
set term pdfcairo
set output 'lfsnoise.pdf'
plot "lfsnoise-0.1.dat" using 5:xticlabels(1) title "0.1", "lfsnoise-0.3333.dat" using 5:xticlabels(1) title "0.333", "lfsnoise-0.5.dat" using 5:xticlabels(1) title "0.5"
