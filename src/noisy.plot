set title "Algorithm RMS Error"
set style data histograms
set style histogram cluster
set style fill solid border
set key over
set autoscale
set xtic auto rotate by 45 right
set yrange [0.0001:0.1]
#set logscale y
set xtic auto
set ytic auto
set xlabel "Algorithm"
set ylabel "RMS Error in terms of bounding box diagonal"
set term pdfcairo
set output 'noisy.pdf'
plot "noisy-0.dat" using 5:xticlabels(1) title "0", "noisy-0.003.dat" using 5:xticlabels(1) title "0.003", "noisy-0.01.dat" using 5:xticlabels(1) title "0.01", "noisy-0.03.dat" using 5:xticlabels(1) title "0.03"
