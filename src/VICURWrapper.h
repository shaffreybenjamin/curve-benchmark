/*
 * VICURWrapper.h
 *
 *  Created on: Aug 02, 2019
 *      Author: Stef
 */

#ifndef VICURWRAPPER_H_
#define VICURWRAPPER_H_

#include "CurveBenchmarkDataTypes.h"

using namespace std;

class VICURWrapper
{
public:
	void reconstruct(vector<CBPoint> &cbpoints, list<CBEdge> &cbedges);
};

#endif /* VICURWRAPPER_H_ */



