#include "CCrustWrapper.h"

void CCrustWrapper::reconstruct(std::vector<CBPoint> &cbpoints, std::list<CBEdge> &cbedges, float cc_parameter)
{
	int i;
	REAL pointcoords[cbpoints.size()*2];
	int edgecount, *edgelist = NULL;

	for (i = 0; i < (int)cbpoints.size(); i++)
	{
		pointcoords[i*2] = cbpoints[i].x;
		pointcoords[i*2 + 1] = cbpoints[i].y;
	}

	conservative_crust2d(pointcoords, cbpoints.size(), cc_parameter, edgelist, edgecount);
	
	for (i = 0; i < edgecount; i++)
	{
		CBEdge cbedge;
		cbedge.v[0] = edgelist[i*2];
		cbedge.v[1] = edgelist[i*2 + 1];
		cbedges.push_back(cbedge);
	}
}

