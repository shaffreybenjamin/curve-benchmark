using System;
using System.Windows.Forms;
using System.Drawing;


namespace DrawTools
{
	/// <summary>
	/// Polygon tool
	/// </summary>
	public class ToolPolygon : DrawTools.ToolObject
	{
		public ToolPolygon()
		{
          
        }

        private int lastX;
        private int lastY;
        private DrawPolygon newPolygon;
        
        private const int minDistance = 15*15;
		//private Type type;
        private bool firstDraw = true;
        /// <summary>
        /// Left nouse button is pressed
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public override void OnMouseDown(PictureBox sender, MouseEventArgs e)
        {
            // Create new polygon, add it to the list
            // and keep reference to it
            if(firstDraw){
                newPolygon = new DrawPolygon();
				AddNewObject(sender, newPolygon);
				newPolygon.AddPoint(new Point(e.X,e.Y));
				((CRT.Mainform)sender.Parent).GraphicsList[0].Objtype="Polygon";
				firstDraw = false;
			}
            
           // lastX = e.X;
            //lastY = e.Y;
			
			if ( e.Button != MouseButtons.Right )
			{
				
					newPolygon.AddPoint(new Point(e.X,e.Y));
				
			}
			
			//newPolygon.MakeClosed();
			//drawArea.Refresh();
            
        }

        /// <summary>
        /// Mouse move - resize new polygon
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public override void OnMouseMove(PictureBox sender, MouseEventArgs e)
        {
            sender.Cursor = Cursor;

         //   if ( e.Button != MouseButtons.Left )
          //      return;

            if ( newPolygon == null )
                return;                 // precaution
            
            Point point = new Point(e.X, e.Y);
            int distance = (e.X - lastX)*(e.X - lastX) + (e.Y - lastY)*(e.Y - lastY);
			//int distance = (e.X - newPolygon.GetHandle(1).X)*(e.X - newPolygon.GetHandle(1).X) + (e.Y - newPolygon.GetHandle(1).Y)*(e.Y - newPolygon.GetHandle(1).Y);
			
            //if ( distance < minDistance )
            //{
                // Distance between last two points is less than minimum -
                // move last point
			
                newPolygon.MoveHandleTo(point, newPolygon.HandleCount);
			
               // newLine.MoveHandleTo(point, newLine.HandleCount);
          //  }
          //  else
          //  {
                // Add new point
               // newPolygon.AddPoint(point);
                lastX = e.X;
                lastY = e.Y;
           // }
             
            sender.Refresh();
        }

        public override void OnMouseUp(PictureBox sender, MouseEventArgs e)
        {
           // newPolygon = null;
           // base.OnMouseUp (drawArea, e);
			if ( e.Button == MouseButtons.Right )
			{  
				newPolygon.MakeClosed();
				if(!newPolygon.IsClosed) newPolygon.CutLastPoint();
				sender.Refresh();
				firstDraw=true;
				newPolygon = null;
				base.OnMouseUp (sender, e);}
			
        }


	}
}
