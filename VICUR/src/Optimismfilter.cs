using System;
using System.Collections;
namespace Vision
{
	/// <summary>
	/// 
	/// </summary>
	public class Optimismfilter
	{
		public Optimismfilter()
		{
			// 
			// TODO: Add constructor logic here
			//
		}
		private const double minimangle = Math.PI *3/4;
		public ArrayList SharpCornerFilter(ArrayList mycurve,bool fristtime)
		{
			int startpoint = 0;
			int endpoint = mycurve.Count - 1;
			int endcorner = mycurve.Count - 1;
			int topindx=0,startindx=0,endindx=0;
			int sma = 0, smb = 0, smc = 0,smx=0 ; // curve smooth weight
            int distributeweight;
			ArrayList CornerAreaa=new ArrayList();
			ArrayList CornerAreab=new ArrayList();
			ArrayList CornerAreac=new ArrayList();
			ArrayList CornerAread = new ArrayList();
			ArrayList CornerAreax = new ArrayList();
			ArrayList ResultCurve=new ArrayList();

			int i=0;
			startpoint = FindFirstPoint(mycurve);
			if (startpoint != -1)
			{
				endpoint = FindLastPoint(startpoint + 1, mycurve);
				for(i=startpoint;i<=endpoint;i++)
				{
					CornerAreaa.Add((Point2D)mycurve[i]);
					CornerAreab.Add((Point2D)mycurve[i]);
					CornerAreac.Add((Point2D)mycurve[i]);
					CornerAread.Add((Point2D)mycurve[i]);
				}
				//Point2D midpoint = new Point2D((((Point2D)mycurve[startpoint]).X +
				//((Point2D)mycurve[endpoint]).X) / 2,
				//	(((Point2D)mycurve[startpoint]).Y + ((Point2D)mycurve[endpoint]).Y) / 2);

				double k = 0;// midpoint.Distance((Point2D)mycurve[startpoint]);
				int g = startpoint; double h = 0;
				for (i = 0; i < CornerAreaa.Count; i++)
				{
					h = PointToLineDistance((Point2D)CornerAreaa[i],
						(Point2D)CornerAreaa[0], (Point2D)CornerAreaa[CornerAreaa.Count - 1]);
					if ( h> k) 
					{
						k = h;
						g = i;
					}
				}
				topindx=g;
				startindx=0;
				endindx=CornerAreaa.Count-1;
				sma = ReFineCorner(ref CornerAreaa, startindx,	endindx);
				if (startpoint==0) smb=ReFineCorner(ref CornerAreab,topindx,endindx);
				if(endpoint==mycurve.Count-1) 	smc=ReFineCorner(ref CornerAreac,startindx,topindx);
				distributeweight=WeightDistributeLevel(CornerAread);

				if(WeightDistributeLevel(CornerAreaa)>distributeweight)
					sma=0;
				if(WeightDistributeLevel(CornerAreab)>distributeweight)
					smb=0;
				if(WeightDistributeLevel(CornerAreac)>distributeweight)
					smc=0;

				if(Selfintersection(CornerAreaa))
					sma=0;
				if(Selfintersection(CornerAreab))
					smb=0;
				if(Selfintersection(CornerAreac))
					smc=0;
				if (!fristtime)
					smb=0;
				for (i = 0; i < startpoint; i++)
				{
					ResultCurve.Add((Point2D)mycurve[i]);
				}
				if (smb != 0 && smc != 0)
				{
					if (sma <= smb && sma <= smc&&sma!=0)
						//ResultCurve.AddRange(CornerAreaa);
					{
						CornerAreax = CornerAreaa;
						smx = sma;
					}
					else if (smb < sma && smb <= smc&&smb!=0)
						//ResultCurve.AddRange(CornerAreab);
					{
						CornerAreax = CornerAreab;
						smx = smb;
					}
					else if (smc < sma && smc < smb&&smc!=0)
						//ResultCurve.AddRange(CornerAreac);
					{
						CornerAreax = CornerAreac;
						smx = smc;
					}
				}
				if (smb == 0 && smc != 0)
				{
					if (sma < smc&&sma!=0) 
					{
						CornerAreax = CornerAreaa; smx = sma;
					}//ResultCurve.AddRange(CornerAreaa);
					else { CornerAreax = CornerAreac; smx = smc; }//ResultCurve.AddRange(CornerAreac);

				}
				if (smb != 0 && smc == 0)
				{
					if (sma < smc&&sma!=0) 
					{
						CornerAreax = CornerAreaa; smx = sma;
					}//ResultCurve.AddRange(CornerAreaa);
					else {CornerAreax = CornerAreab;smx=smb;}//	ResultCurve.AddRange(CornerAreab);
				}
				if (smb == 0 && smc == 0 && sma!=0)
				{ CornerAreax = CornerAreaa; smx = sma; }
				// ResultCurve.AddRange(CornerAreaa);
				if (smx >= WeightSmoothLevel(CornerAread)||smx==0)
					CornerAreax = CornerAread;
				ResultCurve.AddRange(CornerAreax);
				//add  other points
				ArrayList restcurve = new ArrayList();
				for (i = endpoint + 1; i < mycurve.Count ; i++)
				{
					restcurve.Add((Point2D)mycurve[i]);
				}
				startpoint = FindFirstPoint(restcurve);
				if (startpoint == -1)
				{
					ResultCurve.AddRange(restcurve);
				}
				else
					ResultCurve.AddRange(SharpCornerFilter(restcurve,false));

            
				return ResultCurve;
				
			}
			
			return mycurve;
			
		}

		public double CalAngle(Point2D firstpoint, Point2D secondpoint, Point2D
			thirdpoint)
		{

			double ang;
			ang=CalAngle(secondpoint,thirdpoint)-CalAngle(secondpoint,firstpoint);
			if (ang>2*Math.PI)
				ang=ang-2*Math.PI;
			else if (ang<0)
				ang=ang+2*Math.PI;
			return ang;

		}
		private int FindFirstPoint(ArrayList mycurve)
		{
			if (mycurve.Count >= 4)
			{
				int i = 0;
				double tempangle1 = 0, tempangle2 = 0;
				for (i = 0; i <= mycurve.Count - 4; i++)
				{
					tempangle1 = CalAngle((Point2D)mycurve[i + 0],
						(Point2D)mycurve[i + 1], (Point2D)mycurve[i + 2]);
					tempangle2 = CalAngle((Point2D)mycurve[i + 1],
						(Point2D)mycurve[i + 2], (Point2D)mycurve[i + 3]);
					if(RefineAngle(tempangle1)&&RefineAngle(tempangle2))
						return i;
                   
				}
			}
			return -1;

		}
		private int FindLastPoint(int startpoint, ArrayList mycurve)
		{
			if (mycurve.Count - startpoint >= 4)
			{
                
				int i = 0;
				double tempangle1 = 0, tempangle2 = 0;
				for (i = startpoint; i <= mycurve.Count - 4; i++)
				{
					tempangle1 = CalAngle((Point2D)mycurve[i + 0],
						(Point2D)mycurve[i + 1], (Point2D)mycurve[i + 2]);
					tempangle2 = CalAngle((Point2D)mycurve[i + 1],
						(Point2D)mycurve[i + 2], (Point2D)mycurve[i + 3]);
					if(!(RefineAngle(tempangle1)||RefineAngle(tempangle2)))
						return i+1;
                
				}

			}
			return mycurve.Count - 1;
		}
		public double CalAngle(Point2D firstpoint, Point2D secondpoint) 
		{
			double ang = 0;
			int tx = 0, ty = 0;
			tx=secondpoint.X - firstpoint.X;
			ty = secondpoint.Y - firstpoint.Y;
						
			if ((double)(tx) != 0)
				ang= Math.Atan(((double)(ty) / (double)(tx)));
			if (tx<0)
				ang=Math.PI-ang;
			else if (ty<0)
				ang=-ang;
			else
				ang=2*Math.PI-ang; /**< ??? */
			
			if ((double)(tx) == 0)
			{
				if (ty<0)
					ang= Math.PI / 2;
				else if (ty==0)
					ang=0;
				else
					ang= 3*Math.PI / 2;
			}
				
			return ang;
         
		}
		public ArrayList ReArrangArray(Point2D firstpoint, ArrayList myedge)
		{
			ArrayList myarray = new ArrayList();
			//myarray.Add(firstpoint);
			double d = 0;
			double sd = 0;
			double td=0;
                        
			while (myedge.Count > 0)
			{
				int i = 0, k = 0;
				sd = firstpoint.Distance((Point2D)myedge[0]);
				for (i = 0; i < myedge.Count ; i++)
				{
					td = firstpoint.Distance((Point2D)myedge[i]);
					if (td <= sd && td >= d)
					{
						k = i;
						sd = td;
					}
				}
				myarray.Add(myedge[k]);
				myedge.RemoveAt(k);
				d = sd;
			}
			return myarray;
		}
		public bool RefineAngle(double myangle)  //turn too sharp
		{
			if ((myangle < minimangle) || (myangle >2*Math.PI-minimangle))
			return true;
			else return false;
		}
		public int WeightSmoothLevel(ArrayList mycurve)
		{
			if (mycurve.Count >= 3)
			{
                
				int i = 0;
				double tempangle1 = 0;
				ArrayList myangle = new ArrayList();
				for (i = 0; i <= mycurve.Count - 3; i++)
				{
					tempangle1 = CalAngle((Point2D)mycurve[i + 0], (Point2D)mycurve[i + 1],
						(Point2D)mycurve[i + 2]);
					myangle.Add(tempangle1);
					
						
				}
               
				return (int) (Variance(myangle)*10000);

			}
			return 0;
		}
		public int WeightDistributeLevel(ArrayList mycurve)
		{
			if (mycurve.Count >= 3)
			{
                
				int i = 0;
				double tempseg = 0;
				double avg=0,var=0;
				ArrayList mycv = new ArrayList();
				for (i = 0; i <= mycurve.Count -2; i++)
				{
					tempseg = ((Point2D)mycurve[i + 0]).Distance((Point2D)mycurve[i + 1]);
					mycv.Add(tempseg);
					avg=avg+tempseg;
				}
				avg=avg/(mycurve.Count-1);
                var=Variance(mycv);
				if (avg!=0)
				var=var/avg;
				return (int) (var*10000);

			}
			return 0;
		}
		public int ReFineCorner(ref ArrayList myCornerArea, int myTop, int myStart,
			int myEnd)
		{
			//=======================================================================//
			double ang1 = 0, ang2 = 0,ang3=0;
			ArrayList leftedge = new ArrayList();
			ArrayList rightedge = new ArrayList();
			ArrayList ResultCurve = new ArrayList();
			
			Point2D midpoint = new Point2D((((Point2D)myCornerArea[myStart]).X +
				((Point2D)myCornerArea[myEnd]).X) / 2,
				(((Point2D)myCornerArea[myStart]).Y + ((Point2D)myCornerArea[myEnd]).Y) /
				2);
			Point2D toppoint=(Point2D)myCornerArea[myTop];
			ang1 = CalAngle(midpoint, toppoint);
			if (ang1<1/2*Math.PI) {ang2=ang1+Math.PI;}
			else{ang2=ang1;ang1=ang2-Math.PI;}
			for (int i = 0 ; i < myCornerArea.Count; i++)
			{
				ang3 = CalAngle((Point2D)midpoint, (Point2D)myCornerArea[i]);
				if (ang1<=ang3&&ang3<=ang2)
				{
					leftedge.Add(myCornerArea[i]);
				}
				else rightedge.Add(myCornerArea[i]);
			}
			ang3 = CalAngle((Point2D)midpoint, (Point2D)myCornerArea[myStart]);
			if  (ang1<ang3&&ang3<=ang2)
			{
			
				ResultCurve.AddRange(ReArrangArray((Point2D)myCornerArea[myStart],leftedge));
				if (ResultCurve.Contains(toppoint)) ResultCurve.Remove(toppoint);
				rightedge.Add(toppoint);
				ResultCurve.AddRange(ReArrangArray(toppoint, rightedge));

				
			}
			else
			{
				ResultCurve.AddRange(ReArrangArray((Point2D)myCornerArea[myStart],
					rightedge));
				if (ResultCurve.Contains(toppoint)) ResultCurve.Remove(toppoint);
				leftedge.Add(toppoint);
				ResultCurve.AddRange(ReArrangArray(toppoint,leftedge));
				
			}
			myCornerArea.Clear();
			myCornerArea.AddRange(ResultCurve);
			return WeightSmoothLevel(ResultCurve);
		}
		public int ReFineCorner(ref ArrayList myCornerArea, int myStart,
			int myEnd)
		{
			//=======================================================================//
			//Summery of the sharp corner correct algorithm			
			//step1. find the base triangle pa,pb and top point pc
			//step2. find the forth point pd. pd is the second nearest to ab
			//step3. let pab be the midpoint of pa-pb
			//		 let pca and pbc be the midpoint of pa-pc and pc-pc
			//		 let cdca and cdcb be the center point of triangle dca and dcb
			//		 calculate the angle of 	pab-pac-cdca and pab-pba-cdcb
			//stpe4. take the smooth one,and let acd or bcd be the base triangle.
			//goto step1 untill no point leaft.	
			
			Point2D Pab,Pac,Pbc;
			Point2D Pdca,Pdcb;
			double ang1,ang2;
			int Pa,Pb,Pc,Pd;
			if (myCornerArea.Count<4) 
				return -1;
			
			ArrayList leftedge = new ArrayList();
			ArrayList rightedge = new ArrayList();
			ArrayList ResultCurve = new ArrayList();
			ArrayList Excludelist= new ArrayList();

			Pa=myStart;
			Pb=myEnd;
			leftedge.Add(myCornerArea[Pa]);
			rightedge.Add(myCornerArea[Pb]);


			
			Pd=0;
			Excludelist.Add(Pa);
			Excludelist.Add(Pb);	
			
			int i;
			Pc=FindTheNearestPointInArea(myCornerArea,Excludelist,Pa,Pb);
			for(i=0;i<myCornerArea.Count-3;i++)
			{
				Pc=FindTheNearestPointInArea(myCornerArea,Excludelist,Pa,Pb);
				Excludelist.Add(Pc);

				Pab = new Point2D((((Point2D)myCornerArea[Pa]).X +
					((Point2D)myCornerArea[Pb]).X) / 2,
					(((Point2D)myCornerArea[Pa]).Y + ((Point2D)myCornerArea[Pb]).Y) /2);

				Pac = new Point2D((((Point2D)myCornerArea[Pa]).X +
					((Point2D)myCornerArea[Pc]).X) / 2,
					(((Point2D)myCornerArea[Pa]).Y + ((Point2D)myCornerArea[Pc]).Y) /2);

				Pbc = new Point2D((((Point2D)myCornerArea[Pb]).X +
					((Point2D)myCornerArea[Pc]).X) / 2,
					(((Point2D)myCornerArea[Pb]).Y + ((Point2D)myCornerArea[Pc]).Y) /2);

				Pd=	FindTheNearestPointInArea(myCornerArea,Excludelist,Pc,Pa,Pb);


				Pdca = new Point2D((((Point2D)myCornerArea[Pd]).X +
					((Point2D)myCornerArea[Pc]).X+
					+((Point2D)myCornerArea[Pa]).X) / 3,
					(((Point2D)myCornerArea[Pd]).Y 
					+ ((Point2D)myCornerArea[Pc]).Y
					+((Point2D)myCornerArea[Pa]).Y) /3);

				Pdcb = new Point2D((((Point2D)myCornerArea[Pd]).X +
					((Point2D)myCornerArea[Pc]).X+
					((Point2D)myCornerArea[Pb]).X) / 3,
					(((Point2D)myCornerArea[Pd]).Y 
					+ ((Point2D)myCornerArea[Pc]).Y
					+((Point2D)myCornerArea[Pb]).Y) /3);
				
				ang1=Math.Abs(CalAngle(Pab,Pac,Pdca)-Math.PI);
				ang2=Math.Abs(CalAngle(Pdcb,Pbc,Pab)-Math.PI);
				if (ang1>ang2)
				{
					leftedge.Add(myCornerArea[Pc]);
					Pa=Pc;

				}
				else
				{
					rightedge.Insert(0,myCornerArea[Pc]);
					Pb=Pc;
				}			
			}
			ang1=Math.Abs(CalAngle((Point2D)myCornerArea[Pa],(Point2D)myCornerArea[Pc],(Point2D)myCornerArea[Pd])-Math.PI);
			ang2=Math.Abs(CalAngle((Point2D)myCornerArea[Pd],(Point2D)myCornerArea[Pc],(Point2D)myCornerArea[Pb])-Math.PI);
			if (ang1<ang2)
			{
				leftedge.Add(myCornerArea[Pd]);
			}
			else
			{
				rightedge.Insert(0,myCornerArea[Pd]);
			}
			
			ResultCurve.AddRange(leftedge);						
			ResultCurve.AddRange(rightedge);
			myCornerArea.Clear();
			myCornerArea.AddRange(ResultCurve);
			if(leftedge.Count>3||rightedge.Count>3)
			return WeightSmoothLevel(leftedge)+WeightSmoothLevel(rightedge);
			else
			return WeightSmoothLevel(ResultCurve);
		}
		public double Variance(ArrayList myangle)
		{
			int i=0;
			double myVariance = 0;
			double simplemean = 0;
			if (myangle.Count<2)
			return 0;
			for (i = 0; i < myangle.Count; i++)
				simplemean = simplemean + (double)myangle[i];
			simplemean = simplemean / (double)myangle.Count;
			for (i = 0; i < myangle.Count; i++)
				myVariance = myVariance + ((double)myangle[i] - simplemean) *
					((double)myangle[i] - simplemean);

			myVariance = Math.Sqrt(myVariance / ((double)myangle.Count - 1));
            
			return myVariance;
		}
		public double Average(ArrayList segments)
		{
			int i=0;
			double myAverage = 0;
			for (i = 0; i < segments.Count; i++)
				myAverage = myAverage + (double)segments[i];
			myAverage=myAverage/segments.Count;
			return myAverage;
		}
		public double PointToLineDistance(Point2D mypoint,Point2D myhead,Point2D
			mytail)
		{
			double p2pdis = 0;
			p2pdis = myhead.Distance(mypoint);
			double myangle = 0;
			myangle = CalAngle(mypoint, myhead) - CalAngle(mytail, myhead);
			p2pdis = Math.Abs(p2pdis * Math.Sin(myangle));
			return p2pdis;
		}
		public void CurveFilter(ref Curve myCurve)
		{
			ArrayList al = SharpCornerFilter(myCurve,true);
			myCurve = new Curve(1, 1);
			for (int i = 0; i < al.Count; i++)
			{
				myCurve.Add((Point2D)al[i]);
			}
		}
		public ArrayList SharpCornerFilterB(ArrayList mycurve)
		{
			int startpoint = 0;
			int endpoint = mycurve.Count - 1;
			int endcorner = mycurve.Count - 1;
			int topindx = 0, startindx = 0, endindx = 0;
			int sma = 0, smc = 0, smx = 0; // curve smooth weight

			ArrayList CornerAreaa = new ArrayList();
            
			ArrayList CornerAreac = new ArrayList();
			ArrayList CornerAread = new ArrayList();
			ArrayList CornerAreax = new ArrayList();
			ArrayList ResultCurve = new ArrayList();
			int i = 0;
			startpoint = FindFirstPoint(mycurve);
			if (startpoint != -1)
			{
				endpoint = FindLastPoint(startpoint + 1, mycurve);
				for (i = startpoint; i <= endpoint; i++)
				{
					CornerAreaa.Add((Point2D)mycurve[i]);
                
					CornerAreac.Add((Point2D)mycurve[i]);
					CornerAread.Add((Point2D)mycurve[i]);
				}
				//Point2D midpoint = new
				//Point2D((((Point2D)mycurve[startpoint]).X + ((Point2D)mycurve[endpoint]).X) / 2,
				//	(((Point2D)mycurve[startpoint]).Y + ((Point2D)mycurve[endpoint]).Y) / 2);
				
				double k = 0;// midpoint.Distance((Point2D)mycurve[startpoint]);
				int g = startpoint; double h = 0;
				for (i = 0; i < CornerAreaa.Count; i++)
				{
					h = PointToLineDistance((Point2D)CornerAreaa[i],
						(Point2D)CornerAreaa[0], (Point2D)CornerAreaa[CornerAreaa.Count - 1]);
					if (h > k)
					{
						k = h;
						g = i;
					}
				}
				topindx = g;
				startindx = 0;
				endindx = CornerAreaa.Count - 1;
				sma = ReFineCorner(ref CornerAreaa, startindx,endindx);
				if (endpoint == mycurve.Count - 1) smc = ReFineCorner(ref CornerAreac,  startindx, topindx);
				for (i = 0; i < startpoint; i++)
				{
					ResultCurve.Add((Point2D)mycurve[i]);
				}
				if (smc != 0 )
				{
					if ( sma <= smc)
					{
						CornerAreax = CornerAreaa;
						smx = sma;
					}
					else if (smc < sma )
					{
						CornerAreax = CornerAreac;
						smx = smc;
					}
				}
				else
				{ CornerAreax = CornerAreaa; smx = sma; }
				if (smx >= WeightSmoothLevel(CornerAread))
					CornerAreax = CornerAread;
				ResultCurve.AddRange(CornerAreax);              
				ArrayList restcurve = new ArrayList();
				for (i = endpoint + 1; i < mycurve.Count; i++)
				{
					restcurve.Add((Point2D)mycurve[i]);
				}
				startpoint = FindFirstPoint(restcurve);
				if (startpoint == -1)
				{
					ResultCurve.AddRange(restcurve);
				}
				else
					ResultCurve.AddRange(SharpCornerFilterB(restcurve));


				return ResultCurve;

			}

			return mycurve;

		}

		internal int FindTheNearestPointInArea(ArrayList myArea, ArrayList myExcludeList, int myPa, int myPb)
		{
			int i,k,l;

			int shortdis;
			shortdis=999999999;
			k=-1;
			for(i=0;i<myArea.Count;i++)
			{
				
					
				if (!myExcludeList.Contains(i))	
				{
					l=(int)PointToLineDistance((Point2D)myArea[i],(Point2D)myArea[myPa],(Point2D)myArea[myPb]);
					if (l<=shortdis)
					{
						k=i;
						shortdis=l;
					}
				}

			}
			return k;
		}
		internal int FindTheNearestPointInArea(ArrayList myArea, ArrayList myExcludeList,int myPc, int myPa, int myPb)
		{
			int i,k,l;

			int shortdis;
			shortdis=999999999;
			int largedis;
			largedis=(int)PointToLineDistance((Point2D)myArea[myPc],(Point2D)myArea[myPa],(Point2D)myArea[myPb]);
			k=-1;
			for(i=0;i<myArea.Count;i++)
			{
				
					
				if (!myExcludeList.Contains(i))	
				{
					l=(int)PointToLineDistance((Point2D)myArea[i],(Point2D)myArea[myPa],(Point2D)myArea[myPb]);
					if (l<=shortdis&&l>=largedis)
					{
						k=i;
						shortdis=l;
					}
				}

			}
			return k;
		}
		internal int FindTheNearestPointInArea(ArrayList myArea, ArrayList myExcludeList, int myPc)
		{
			int i,k,m;
	
			int shortdis;
			shortdis=999999999;
			k=-1;
			for(i=0;i<myArea.Count;i++)
			{
				
				
				if (!myExcludeList.Contains(i))	
				{
					
					m=((Point2D)myArea[myPc]).DistanceSqr((Point2D)myArea[i]);

					if (m<=shortdis)
					{
						k=i;
						shortdis=m;
					}					
				}

			}
			return k;
		}
		public bool CurveConnector(ref StCurve curveA,ref StCurve curveB)
		{
		return true;
			int i,j,k;
			for (i=0;i<curveA.Count;i++)
				for(j=0;j<curveB.Count;j++)
				{
					
				}

		}
		public bool Selfintersection(ArrayList mycurve)
		{
			Point2D a;
			Point2D b;
			Point2D c;
			Point2D d;
			if (mycurve.Count>3)
				for(int i=0;i<mycurve.Count-3;i++)
				{
					a=(Point2D)mycurve[i];
					b=(Point2D)mycurve[i+1];
					for(int j=i+2;j<mycurve.Count-1;j++)
					{
						c=(Point2D)mycurve[j];
						d=(Point2D)mycurve[j+1];
						if(CheckCross(a,b,c,d))
							return true;
					}
				}
			return  false;
		}
		public bool CheckCross(Point2D a,Point2D b,Point2D c,Point2D d)
		{
			double ang,ang1,ang2,ang3,ang4;
			ang1=CalAngle(a,c,b)-Math.PI;
			ang2=CalAngle(c,b,d)-Math.PI;
			ang3=CalAngle(b,d,a)-Math.PI;
			ang4=CalAngle(d,a,c)-Math.PI;
			if((ang1>0 &&ang2>0&&ang3>0&&ang4>0)||(ang1<0&&ang2<0&&ang3<0&&ang4<0))	
			return true;
			/*ang=Math.Abs(ang1+ang2+ang3+ang4)+Math.PI;
			do
			{
				ang=ang-Math.PI;
			}
			while(ang>Math.PI);
			if (ang<0.000001)
				return false;*/

			return false;
		}
		public double ExternalLength(ArrayList seg,double l,double lo)
		{
			double h,havg,s,sig,et;
			
			System.Collections.ArrayList rt=seg;			
			if (rt.Count>1)
			{
				//lo=(double)rt[0];
				h=(l+lo)/2;
				s=(Math.Abs(l-lo))/(Math.Sqrt(2));
				//Optimismfilter op=new Optimismfilter();
				sig=this.Variance(rt);
				havg=this.Average(rt);
				et=havg/sig;
				if(s!=0)
				{
					et=havg*h/s*Math.Pow(et+1,sig/h);
					return (int)et;			
				}
				else
					return 10000000;
			}
			else
				return (int)(2.05*(double)rt[0]);

		}
		public virtual void  shortdistancedenauley(System.Drawing.Graphics g,Delaunay d)
		{
			System.Collections.ArrayList myedges=new System.Collections.ArrayList();
			if (d.edges.Count<3)
				return;
			for (int i = 0; i < d.edges.Count; i++)
			{
				myedges.Add(d.edges[i]);		
			
			}
			
			double shortdis=10000000;
			int shortedgeId=0;
			StCurveList cl=new 	Vision.StCurveList();
			Point2D head,tail;

			for(int i =0;i<myedges.Count;i++)
			{
				Edge edge = (Edge)myedges[i];
			
				head=new Point2D(edge.p1.x,edge.p1.y);
				tail=new Point2D(edge.p2.x,edge.p2.y);
				if (head.Distance(tail)<shortdis)
				{
					shortdis=head.Distance(tail);
					shortedgeId=i;
				}
				StCurve sc=new StCurve();
				sc.AddPoint(head);
				sc.AddPoint(tail);

			}

			//myedges[shortedgeId]
			




		}
	}
}
 
