using System;
// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   Delaunay.java


class Node
{
	
	public Node(int i, int j)
	{
		x = i;
		y = j;
		anEdge = null;
	}
	
	public Node(int i, int j, int k)
	{
		x = i;
		y = j;
		type = k;
		anEdge = null;
	}
	
	public virtual Edge GetEdge()
	{
		return anEdge;
	}
	
	public virtual double Distance(double d, double d1)
	{
		double d2 = d - (double) x;
		double d3 = d1 - (double) y;
		return System.Math.Sqrt(d2 * d2 + d3 * d3);
	}
	
	internal int x;
	internal int y;
	internal Edge anEdge;
	internal int type;
}