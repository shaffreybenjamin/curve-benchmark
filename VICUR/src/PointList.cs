using System;
using System.Collections;
using System.Drawing;

namespace Vision
{
	/// <summary>
	/// Summary description for PointsList.
	/// </summary>
	/// 
    
	/**
	 *
	 */ 
	[Serializable]
	public class PointList : ArrayList
	{
		public PointList()
		{
			//
			// TODO: Add constructor logic here
			//			
		}

		// Decide whether a point is in this PointList or not
		public bool InThis(Point2D ptone)
		{
			for(int i=0; i< this.Count; i++)
			{
				Point2D pttwo = (Point2D)this[i];
				if(ptone.Equals(pttwo))
				   return true;
			}
			return false;
		}

		// Return the nearest point to the specific point pt in this PointList
		public Point2D FindTheNearestPoint(Point2D ptIn)
		{
			int index = -1;
			Point2D ptOut;

			double shortDistance = 100000000;
			
			// Find the nearest point
			for(int i=0; i< this.Count; i++)
			{
				Point2D pt = (Point2D)this[i];		
					
				double pointDistance = ptIn.Distance(pt);
				
				if( shortDistance > pointDistance)
				{
					shortDistance = pointDistance;
					index = i;						
				}
			} 
			
			// If the point is found, return it, else return null 
			if(index != -1)
				ptOut = (Point2D)this[index];
			else
				ptOut = null;
			
			return ptOut;
		}

		/*
		// Test if three points are on the right angle, ptone
		public bool RightAngle(Point2D ptone, Point2D pttwo, Point2D pt3, double alphaV)
		{
			double a = 0, b = 0, c = 0, A = 0;

			a = ptone.Distance(pttwo);
			b = ptone.Distance(pt3);
			c = pttwo.Distance(pt3);

			A = Math.Acos( ( Math.Pow(c,2)-Math.Pow(a,2)-Math.Pow(b,2) ) /
				(2*a*b) );
			
			A *= 57; 

			if (A > alphaV)
				return true;
			else
				return false;
		}
		*/
	}
}
