using System;
using System.Collections;
using System.Drawing;

namespace Vision
{
	/// <summary>
	/// Summary description for PointsList.
	/// </summary>
	/// 

	[Serializable]

	public class Curve : ArrayList
	{
		public double sigmaTimes;
		public double deltaTimes;
		public bool closed;

		public Curve(double sigmaTimes, double deltaTimes)
		{			
			this.closed = false;
			this.sigmaTimes = sigmaTimes;
			this.deltaTimes = deltaTimes;
		}

		public void Close()
		{
			this.closed = true;			
		}

		public bool ShouldBeOpen()
		{
			if( this.Count > 2)
			{
				Point2D pthead = (Point2D)this[0];
				Point2D pttail = (Point2D)this[this.Count-1];

				double edgeDistance = pthead.Distance(pttail);

				double maxDistance = this.GetMaxDistance();
				double distanceMean = this.GetDistanceMean();
				double sigma = this.GetSigma();

				if (edgeDistance > maxDistance * sigma)
					return true;
				else 
					return false;
			}
			else
				return true;
		}
		public bool ShouldBeOpenEx()
		{
			if( this.Count > 2)
			{
				Point2D pthead = (Point2D)this[0];
				Point2D pttail = (Point2D)this[this.Count-1];


				double exedge,p2pedge,loA,loB;
				bool extendable=false;
				ArrayList segA=new ArrayList();
				int ii,jj;
				for(ii=0;ii<this.Count-1;ii++)
				{
					jj=(((Point2D)this[ii]).Y-((Point2D)this[ii+1]).Y)*(((Point2D)this[ii]).Y-((Point2D)this[ii+1]).Y)
						+(((Point2D)this[ii]).X-((Point2D)this[ii+1]).X)*(((Point2D)this[ii]).X-((Point2D)this[ii+1]).X);	
					segA.Add(Math.Sqrt((double)jj));
				}
				Optimismfilter op=new Optimismfilter();
			    p2pedge=pthead.Distance(pttail);
				loA=0;
				loA=(double)segA[0];
				loB=(double)segA[segA.Count-1];
				exedge=op.ExternalLength(segA,p2pedge,loA);
				if (exedge>=p2pedge)
				extendable=true;
				exedge=op.ExternalLength(segA,p2pedge,loB);
					if (exedge>=p2pedge)
				extendable=true;
						
				return !extendable;
				
			}
			else
				return true;
		}

		public double GetSigma()
		{
			double n = this.sigmaTimes;
			double m = this.deltaTimes;

			double sigma = 1;

			if(this.Count < 2)
				sigma = 1;
			else
			{
				double maxDistance = this.GetMaxDistance();
				double distanceMean = this.GetDistanceMean();
				double squreMeanDif = this.GetSqureMeanDif();
				
				sigma = n + m * squreMeanDif / distanceMean;
			}
			return sigma;
		}

		public double GetDistanceMean()
		{
			double sumDistance = 0.0;
			
			if(this.Count < 2)
				return 0.0;
			else
			{
				for(int i=0; i<(this.Count-1); i++)
				{
					Point2D ptone = (Point2D)this[i];
					Point2D pttwo = (Point2D)this[i+1];
					double edgeDistance = ptone.Distance(pttwo);
					sumDistance += edgeDistance;
				}
				
				return sumDistance / (this.Count - 1);
			}
		}
	
		public double GetSqureMeanDif()
		{
			if(this.Count < 3)
				return 0.0;

			double distanceMean = this.GetDistanceMean();
			double jfc = 0;
			for(int i=0; i<(this.Count-1); i++)
			{
				Point2D ptone = (Point2D)this[i];
				Point2D pttwo = (Point2D)this[i+1];
				double edgeDistance = ptone.Distance(pttwo);
				jfc += Math.Pow((edgeDistance - distanceMean), 2);
			}

			return Math.Sqrt(jfc) / (this.Count-1);
			//return Math.Sqrt(jfc);
		}	

		/**
		 * Get maximum distance between two **neighbor** points ???
		 */ 

		public double GetMaxDistance()
		{
			int i = 0;
			double maxdistance = 0, pointdistance = 0;
			Point2D ptone, pttwo;

			for(i = 0; i<this.Count -1; i++)
			{
				ptone = (Point2D)this[i];
				pttwo = (Point2D)this[i+1];
				pointdistance = ptone.Distance(pttwo);
				if(pointdistance > maxdistance)
				{
					maxdistance = pointdistance;
				}
			}
			return maxdistance;
		}

		// Return the relationship between two open curves
		public int CurveRelationship(Curve cl)
		{
			Point2D pt1, pt2, pt3, pt4;

			double dt1 = 0, dt2 = 0, dt3 = 0, dt4 = 0;

			pt1 = (Point2D)this[0];
			pt2 = (Point2D)this[this.Count-1];
			pt3 = (Point2D)cl[0];
			pt4 = (Point2D)cl[cl.Count-1];

			// head to head-1
			dt1 = pt1.Distance(pt3);
			
			// head to tail-2
			dt2 = pt1.Distance(pt4);

			// tail to head-3
			dt3 = pt2.Distance(pt3);

			// tail to tail-4
			dt4 = pt2.Distance(pt4);

			if( (dt1 <= dt2) && (dt1 <= dt3) && (dt1 <= dt4) )
			{
				return 1;
			}
			else if( (dt2 <= dt1) && (dt2 <= dt3) && (dt2 <= dt4) )
			{
				return 2;
			}
			else if( (dt3 <= dt1) && (dt3 <= dt2) && (dt3 <= dt4) )
			{
				return 3;
			}
			else 
			{
				return 4;
			}
		}

		// Return the shortest distance between two curves (check distances between all the points)
		public double GetShortestCurveDistance(Curve cl)
		{
			double shortDis = 100000000;

			for(int i=0; i<this.Count; i++)
			{
				Point2D ptone = (Point2D)this[i];

				for(int j=0; j<cl.Count; j++)
				{
					Point2D pttwo = (Point2D)cl[j];
					
					double pointDis = ptone.Distance(pttwo); /**< distance between two points */

					if(pointDis < shortDis)
						shortDis = pointDis;
				}
			}
			return shortDis;
		}

		// Return a arraylist containing distances between all the points in two closed curves
		public ArrayList GetAllDistancesBetweenTwoCurves(Curve cl)
		{
			ArrayList pointDisArr = new ArrayList();

			for(int i=0; i<this.Count; i++)
			{
				Point2D ptone = (Point2D)this[i];

				for(int j=0; j<cl.Count; j++)
				{
					Point2D pttwo = (Point2D)cl[j];
					double pointDis = ptone.Distance(pttwo);

					DistanceComparable pd = new DistanceComparable(i, j, pointDis);
					pointDisArr.Add(pd);
				}
			}

			return pointDisArr;
		}
		
		// Return the short distance between two curves (head and tail)
		public double GetCurveDistance(Curve cl)
		{
			double shortDistance = 100000000, pointDistance = 0;

			Point2D ptOne = (Point2D)this[0];
			Point2D ptTwo = (Point2D)this[this.Count-1];			
			Point2D ptThree = (Point2D)cl[0];
			Point2D ptFour = (Point2D)cl[cl.Count-1];

			if(ptOne.Distance(ptThree) <= ptOne.Distance(ptFour))
			{
				pointDistance = ptOne.Distance(ptThree);				
			}
			else
			{
				pointDistance = ptOne.Distance(ptFour);				
			}

			if(ptTwo.Distance(ptThree) <= ptTwo.Distance(ptFour))
			{
				shortDistance = ptTwo.Distance(ptThree);
			}	
			else
			{
				shortDistance = ptTwo.Distance(ptFour);
			}

			if( pointDistance <= shortDistance)
			{
				shortDistance = pointDistance;
			}
			
			return shortDistance;
		}


		// Return distance arraylist between two curves (distance from this[0] and this[count-1] to cl all points)
		/*
		public ArrayList GetActualCurveDistance(Curve cl)
		{
			ArrayList pointDisArr = new ArrayList();

			Point2D ptone = (Point2D)this[0];
			
			for(int j=0; j<cl.Count; j++)
			{
				Point2D pttwo = (Point2D)cl[j];
				double pointDis = ptone.Distance(pttwo);
				DistanceComparable pd = new DistanceComparable(0, j, pointDis);
				pointDisArr.Add(pd);
			}

			if(this.Count > 0)
			{
				ptone = (Point2D)this[this.Count-1];

				for(int j=0; j<cl.Count; j++)
				{
					Point2D pttwo = (Point2D)cl[j];
					double pointDis = ptone.Distance(pttwo);
					DistanceComparable pd = new DistanceComparable(this.Count-1, j, pointDis);
					pointDisArr.Add(pd);
				}
			}
		
			return pointDisArr;
		}
		*/

	}
}