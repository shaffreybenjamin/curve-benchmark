using System;
using System.Windows.Forms;
using System.Drawing;

namespace DrawTools
{
	/// <summary>
	/// Line tool
	/// </summary>
	public class ToolLine : DrawTools.ToolObject
	{
        
		public ToolLine()
        {
           
        }

        public override void OnMouseDown(PictureBox sender, MouseEventArgs e)
        {
            AddNewObject(sender, new DrawLine(e.X, e.Y, e.X + 1, e.Y + 1));
			((CRT.Mainform)sender.Parent).GraphicsList[0].Objtype="Line";
        }

        public override void OnMouseMove(PictureBox sender, MouseEventArgs e)
        {
            sender.Cursor = Cursor;

            if ( e.Button == MouseButtons.Left )
            {
			   
                Point point = new Point(e.X, e.Y);
                ((CRT.Mainform)sender.Parent).GraphicsList[0].MoveHandleTo(point, 2);
				   sender.Refresh();
            }
        }
    }
}
