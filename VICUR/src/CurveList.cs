using System;
using System.Collections;
using System.Drawing;

namespace Vision
{
	/// <summary>
	/// Summary description for PointsList.
	/// </summary>
	/// 

	[Serializable]
	public class CurveList : ArrayList
	{
		private int curveslimite=0;
		private int curvesconnectcount=0;
		public CurveList()
		{		
		}

		public void CloseCurves()
		{
			for(int i=0; i<this.Count; i++)
			{
				Curve cl = (Curve)this[i];
				if(!cl.ShouldBeOpenEx())
					cl.Close();
			}
		}
		public void SetConnectCount(int c)
		{
			curvesconnectcount=0;
			curveslimite=c;
		}
		public double GetSigma()
		{
			double n = 1;
			double m = 2;
			
			double sigma = 1;
			double jfc = 0, edgeDistance = 0, sumDistance = 0, distanceMean = 0;

			int edgeNumber = 0;

			distanceMean = this.GetDistanceMean();

			for(int i=0; i<this.Count; i++)
			{
				Curve cl = (Curve)this[i];

				for(int j=0; j<cl.Count-1; j++)
				{
					Point2D ptOne = (Point2D)cl[j];
					Point2D ptTwo = (Point2D)cl[j+1];
					edgeDistance = ptOne.Distance(ptTwo);
					sumDistance += edgeDistance;
					jfc += Math.Pow((edgeDistance - distanceMean), 2);
				}

				edgeNumber += (cl.Count - 1);
			}
			
			if(distanceMean != 0)
				sigma = n + m * (Math.Sqrt(jfc) / sumDistance);				
			else				
				sigma = 1.1;		

			return sigma; 
		}

		public double GetDistanceMean()
		{
			int edgeNumber = 0;
			double distanceMean = 0, sumDistance = 0;
                
			foreach(Curve cl in this)
			{
				distanceMean = cl.GetDistanceMean();
				sumDistance += distanceMean * (cl.Count - 1);

				edgeNumber += cl.Count - 1;
			}
			
			if(edgeNumber != 0)
				distanceMean = sumDistance / edgeNumber;
			else
				distanceMean = 0;

			return distanceMean; 
		}

		public double GetMaxDistance()
		{
			double maxDistance = 0;
			
			for(int i = 0; i<this.Count; i++)
			{
				Curve clone = (Curve)this[i];

				for(int j = 0; j<clone.Count-1; j++)
				{
					Point2D ptone = (Point2D)clone[j];
					Point2D pttwo = (Point2D)clone[j+1];
					double edgeDistance = ptone.Distance(pttwo);
					if(edgeDistance > maxDistance)
					{
						maxDistance = edgeDistance;
					}
				}											
			}
			return maxDistance;
		}

		public int GetPointCurveNumber()
		{
			int i = 0;
			foreach(Curve cl in this)
			{
				if(cl.Count == 1)
					i++;
			}
			return i;
		}

		public Curve ConnetTwoCurves(ref Curve clOne, ref Curve clTwo)
		{
			curvesconnectcount++;
			int relationship = 0;

			relationship = clOne.CurveRelationship(clTwo);

			if(relationship == 1)
			{
				foreach(Point2D pt in clTwo)
				{
					clOne.Insert(0, pt);
				}
				return clOne;				
			}

			else if(relationship == 2)
			{
				foreach(Point2D pt in clOne)
				{
					clTwo.Add(pt);
				}
				return clTwo;
			}
			
			else if(relationship == 3)
			{
				foreach(Point2D pt in clTwo)
				{
					clOne.Add(pt);
				}
				return clOne;
			}

			else 
			{
				for(int i=clTwo.Count-1; i > -1; i--)
				{
					Point2D pt = (Point2D)clTwo[i];
					clOne.Add(pt);
				}
				return clOne;
			}			
		}		
		
		// Get the shortest curve distance of certain curve
		public double GetShortestCurveDistance(Curve curveOne, out int index)
		{
			double shortDis = 100000000;
			index = -1;

			for(int i=0; i<this.Count; i++)
			{
				Curve curveTwo = (Curve)this[i];
				
				if(curveOne.Equals(curveTwo))
					continue;

				double curveDis = curveOne.GetCurveDistance(curveTwo);

				if(curveDis < shortDis)
				{
					shortDis = curveDis;
					index = i;
				}
			}
			return shortDis;
		}

		// Extend a curve
		public void CurveExtend(Curve curveOne)
		{
			int indexTwo = -1;
			double maxDistance = curveOne.GetMaxDistance();
			double distanceMean = curveOne.GetDistanceMean();
			double squreMeanDif = curveOne.GetSqureMeanDif();
			double sigma = curveOne.GetSigma();

			for( ; ; )
			{
				double shortestCurveDistance = this.GetShortestCurveDistance(curveOne, out indexTwo);
				
				// Test
				double newSigma = Math.Pow( ( 1 + distanceMean / squreMeanDif ), ( squreMeanDif / distanceMean) )  ;
				if( (shortestCurveDistance <= distanceMean * newSigma) )
				 
				/*
				// Try to extend the curve
				if( (indexTwo != -1) &&
					(shortestCurveDistance <= distanceMean * sigma) )
				 */
				{
					// Connet the two curves
					Curve curveTwo = (Curve)this[indexTwo];
					
					this.Remove(curveTwo);
				
					Curve curveNew = this.ConnetTwoCurves(ref curveOne, ref curveTwo);

				}
				else break;
			}
		}

		public void CurveReconstructions(double leftCurveNumber)
		{
			// This ArrayList store the distances between all the curves(heads and tails)
			ArrayList curveDisArr = new ArrayList();

			for(int i=0; i<this.Count; i++)
			{
				Curve clOne = (Curve)this[i];
				
				if(clOne.closed == true)
					continue;

				for(int j=i+1; j<this.Count; j++)
				{
					Curve clTwo = (Curve)this[j];
					
					if(clTwo.closed == true)
						continue;

					double curveDis = clOne.GetCurveDistance(clTwo);

					DistanceComparable cd = new DistanceComparable(i, j, curveDis);
					curveDisArr.Add(cd);
					cd = null;
				}
			}

			if(curveDisArr.Count == 0)
				return;

			// Sort the distances ??????
			curveDisArr.Sort();

			// If no distance less than DistanceMean * sigma, connect the shortest distance directly
			bool connect = false;

			// If the shortest distance is not qualified to be connected, than try the second short distance, continue like this
			for(int i=0; i<curveDisArr.Count; i++)
			{
				DistanceComparable cd = (DistanceComparable)curveDisArr[i];
				Curve clOne = (Curve)this[cd.IndexOne];
				Curve clTwo = (Curve)this[cd.IndexTwo];
				double curveDis = cd.Distance;

				double maxDistance = this.GetMaxDistance();
				double distanceMean = this.GetDistanceMean();
				double sigma = this.GetSigma();

				double maxDistanceOne = clOne.GetMaxDistance();
				double distanceMeanOne = clOne.GetDistanceMean();
				double squreMeanDifOne = clOne.GetSqureMeanDif();
				double sigmaOne = clOne.GetSigma();
				
				double maxDistanceTwo = clTwo.GetMaxDistance();
				double distanceMeanTwo = clTwo.GetDistanceMean();
				double squreMeanDifTwo = clTwo.GetSqureMeanDif();
				double sigmaTwo = clTwo.GetSigma();

				int pointCurveNum = this.GetPointCurveNumber();

				// Connect two point curves with shortest distance directly
				if( (i == 0) &&  
					(clOne.Count == 1) && (clTwo.Count == 1) )
				{
					// Connet the two curves
					this.Remove(clOne);
					this.Remove(clTwo);
				
					Curve clNew = this.ConnetTwoCurves(ref clOne, ref clTwo);

					this.Add(clNew);

					//this.CurveExtend(clNew);

					break;
				}
				// clOne has one point and clTwo has two points
				else if( (clOne.Count == 1) && (clTwo.Count == 2) &&
					     (i == 0) &&
						 // Test a new fumulation, must delete the previous line---i == 0
					      ( curveDis <= distanceMeanTwo * 2.05) )
				{
					// Connet the two curves, should add new conditions here
					this.Remove(clOne);
					this.Remove(clTwo);
				
					Curve clNew = this.ConnetTwoCurves(ref clOne, ref clTwo);

					this.Add(clNew);

					//this.CurveExtend(clNew);

					break;
				}
				// clOne has two points and clTwo has one point
				else if( (clOne.Count == 2) && (clTwo.Count == 1) &&
					     (i == 0) &&
					     // Test a new fumulation, must delete the previous line---i == 0
					      ( curveDis <= distanceMeanTwo * 2.05) )
				{
					// Connet the two curves, should add new conditions here
					this.Remove(clOne);
					this.Remove(clTwo);
				
					Curve clNew = this.ConnetTwoCurves(ref clOne, ref clTwo);

					this.Add(clNew);

					//this.CurveExtend(clNew);

					break;
				}
				else if( (clOne.Count == 2) && (clTwo.Count == 2) &&
					(i == 0) &&
					// Test a new fumulation, must delete the previous line---i == 0
					( ( curveDis <= distanceMeanOne * 2.05) ||
					  ( curveDis <= distanceMeanTwo * 2.05) ) )
				{
					// Connet the two curves, should add new conditions here
					this.Remove(clOne);
					this.Remove(clTwo);
				
					Curve clNew = this.ConnetTwoCurves(ref clOne, ref clTwo);

					this.Add(clNew);

					//this.CurveExtend(clNew);

					break;
				}
				else 
				{
					// If the curves contain more than three points, try to close them
					if( (clOne.Count >= 3) && (i == 0))
					{
						Point2D ptHeadOne = (Point2D)clOne[0];
						Point2D ptTailOne = (Point2D)clOne[clOne.Count-1];
						double chordDistanceOne = ptHeadOne.Distance(ptTailOne);
						
						if( (chordDistanceOne < curveDis) && (!clOne.ShouldBeOpen()) )
							//(chordDistanceOne < maxDistanceOne) )							
							clOne.Close();
					}
				
					if( clTwo.Count >= 3 && (i == 0))
					{
						Point2D ptHeadTwo = (Point2D)clTwo[0];
						Point2D ptTailTwo = (Point2D)clTwo[clTwo.Count-1];
						double chordDistanceTwo = ptHeadTwo.Distance(ptTailTwo);
						
						if( (chordDistanceTwo < curveDis) && (!clTwo.ShouldBeOpen()) )
							//(chordDistanceTwo < maxDistanceTwo) )
							clTwo.Close();
					}
					
					// If either of two curves is closed, do not connect them, recall reconstruction
					if( clOne.closed == true || clTwo.closed == true )
						break;

					// No distance is less than DistanceMean * sigma, try to connect the shortest distance 
					// If the reconstruction is going to end, do not do this
					//if( (connect == true) && (this.Count > leftCurveNumber) )
						//curveDis /= 2;
					

					// Test a new fumulation one 
					double edgeDisOne = 0;
					double edgeDisTwo = 0;

					if(clOne.Count > 1)
					{
						Point2D ptOne = (Point2D)clOne[clOne.Count-2];
						Point2D ptTwo = (Point2D)clOne[clOne.Count-1];
						edgeDisOne = ptOne.Distance(ptTwo);
						ptOne = (Point2D)clTwo[0];
						ptTwo = (Point2D)clTwo[1];
						double edgeDisTmp = ptOne.Distance(ptTwo);
						if(edgeDisTmp > edgeDisTwo)
							edgeDisTwo = edgeDisTmp;
					}
					
					if(clTwo.Count > 1)
					{
						Point2D ptThree = (Point2D)clTwo[clTwo.Count-2];
						Point2D ptFour = (Point2D)clTwo[clTwo.Count-1];
						edgeDisTwo = ptThree.Distance(ptFour);
						ptThree = (Point2D)clTwo[0];
						ptFour = (Point2D)clTwo[1];
						double edgeDisTmp = ptThree.Distance(ptFour);
						if(edgeDisTmp > edgeDisTwo)
							edgeDisTwo = edgeDisTmp;
					}
					
					// Test Two
					double test3 = ( 1 + distanceMeanTwo / squreMeanDifTwo );
					double test4 = squreMeanDifTwo / distanceMeanTwo;
					double newSigmaOne = 1;
					double newSigmaTwo = 1;
					
					if( (squreMeanDifOne != 0) && (distanceMeanOne != 0) )
					{
						double test1 = ( 1 + distanceMeanOne / squreMeanDifOne );
						double test2 = squreMeanDifOne / distanceMeanOne;
						newSigmaOne = Math.Pow( ( 1 + distanceMeanOne / squreMeanDifOne ), ( squreMeanDifOne / distanceMeanOne) )  ;
					}
					
					if( (squreMeanDifTwo != 0) && (distanceMeanTwo != 0) )
					{
						double test1 = ( 1 + distanceMeanTwo / squreMeanDifTwo );
						double test2 = squreMeanDifTwo / distanceMeanTwo;
						newSigmaTwo = Math.Pow( ( 1 + distanceMeanTwo / squreMeanDifTwo ), ( squreMeanDifTwo / distanceMeanTwo) )  ;
					}

					/* Test One
					// If the curveDis is satisfied, connect them
					if( (curveDis <= edgeDisOne * 2.05 )		       ||
						(curveDis <= edgeDisTwo * 2.05) )
					   Test one */
					
					/*
					// Test Two
					if( (curveDis <= edgeDisOne * newSigmaOne) ||
						(curveDis <= edgeDisTwo * newSigmaTwo) )

					 */
					
					// Test Three
					if( (curveDis <= distanceMeanOne * newSigmaOne) ||
						(curveDis <= distanceMeanTwo * newSigmaTwo) ||
						(connect == true) )

					/* Original
					// If the curveDis is satisfied, connect them
					if( //(curveDis <= (distanceMean * sigma))     ||
						(curveDis <= maxDistanceOne)			       ||
						(curveDis <= maxDistanceTwo)				   ||
						(curveDis <= (distanceMeanOne * sigmaOne ))    ||
						(curveDis <= (distanceMeanTwo * sigmaTwo))  )
					 */
					{
						// Connect the two curves
						this.Remove(clOne);
						this.Remove(clTwo);
				
						Curve clNew = this.ConnetTwoCurves(ref clOne, ref clTwo);

						this.Add(clNew);
						
						// Try to extend the curve
						//this.CurveExtend(clNew);

						if(connect == true)
							connect = false;

						break;
					}					
				} // else end

				//If the shortest distance is too large(Even have been divided by 2), quit the reconstruction
				if(connect == true)
					return;
				
				// If no curve can be connected, try to connect the shortest distance
				if(i == (curveDisArr.Count - 1) )
				{
					connect = true;
					i = -1;
				}
				
			} // for end
	
			// Recall CurveReconstruction
			this.CurveReconstructions(leftCurveNumber);
		}

		// Treat special situation one - hat with more than three points
		public void SpecialTreatOne()
		{
			for(int i=0; i<this.Count; i++)
			{
				Curve clone = (Curve)this[i];

				double maxDisOne = clone.GetMaxDistance();
				double distanceMeanOne = clone.GetDistanceMean();
				double sigmaOne = clone.GetSigma();

				for(int j=0; j<this.Count; j++)
				{
					if(j == i)
						continue;

					Curve cltwo = (Curve)this[j];

					if(cltwo.Count < 2)
						continue;

					double curveDis = clone.GetShortestCurveDistance(cltwo);
					
					double maxDisTwo = cltwo.GetMaxDistance();
					double distanceMeanTwo = cltwo.GetDistanceMean();
					double sigmaTwo = cltwo.GetSigma();					 

					// If two curves are close, try to connect them
					if( (curveDis < distanceMeanOne * sigmaOne) ||
						(curveDis < distanceMeanTwo * sigmaTwo) ||
						(curveDis < maxDisOne)                  ||
						(curveDis < maxDisTwo)                    )						
					{
						ArrayList pointDisArr = clone.GetAllDistancesBetweenTwoCurves(cltwo);
						pointDisArr.Sort();

						DistanceComparable pdOne = (DistanceComparable)pointDisArr[0];
						DistanceComparable pdTwo = (DistanceComparable)pointDisArr[1];

						// Two shortest distance are very close, there exist some relationship between two curves
						if( ( (pdTwo.Distance / pdOne.Distance) < 1.08) &&
							(pdOne.IndexOne != pdTwo.IndexOne) &&
							(pdOne.IndexTwo != pdTwo.IndexTwo) &&
							
							( (Math.Abs( pdOne.IndexOne - pdTwo.IndexOne) == 1) || 
							  ((pdOne.IndexOne == 0) && (pdTwo.IndexOne == clone.Count - 1)) ||
							  ((pdTwo.IndexOne == 0) && (pdOne.IndexOne == clone.Count - 1)) ) &&

							( (Math.Abs( pdOne.IndexTwo - pdTwo.IndexTwo) == 1) || 
							((pdOne.IndexTwo == 0) && (pdTwo.IndexTwo == cltwo.Count - 1)) ||
							((pdTwo.IndexTwo == 0) && (pdOne.IndexTwo == cltwo.Count - 1)) )    )
						{
							if(pdOne.IndexOne > pdTwo.IndexOne)
							{
								if(pdOne.IndexTwo > pdTwo.IndexTwo)
								{
									for(int k=pdTwo.IndexOne; k>-1; k--)
									{
										Point2D pt = (Point2D)clone[k];
										cltwo.Insert(pdOne.IndexTwo+pdTwo.IndexOne-k,pt);
									}
									for(int k=clone.Count-1; k>pdOne.IndexOne-1; k--)
									{
										Point2D pt = (Point2D)clone[k];
										cltwo.Insert(pdOne.IndexTwo+pdTwo.IndexOne+1+clone.Count-1-k,pt);
									}
								}
								else // pdOne.IndexTwo < pdTwo.IndexTwo
								{
									for(int k=pdOne.IndexOne; k<clone.Count; k++)
									{
										Point2D pt = (Point2D)clone[k];
										cltwo.Insert(pdTwo.IndexTwo+k-pdOne.IndexOne,pt);
									}
									for(int k=0; k < pdTwo.IndexOne + 1; k++)
									{
										Point2D pt = (Point2D)clone[k];
										cltwo.Insert(pdTwo.IndexTwo+clone.Count-pdOne.IndexOne+k,pt);
									}
								}
							}
							else // pdOne.IndexOne < pdOne.IndexOne
							{
								if(pdOne.IndexTwo > pdTwo.IndexTwo)
								{
									for(int k=pdTwo.IndexOne; k<clone.Count; k++)
									{
										Point2D pt = (Point2D)clone[k];
										cltwo.Insert(pdOne.IndexTwo+k-pdTwo.IndexOne,pt);
									}
									for(int k=0; k < pdOne.IndexOne + 1; k++)
									{
										Point2D pt = (Point2D)clone[k];
										cltwo.Insert(pdOne.IndexTwo+clone.Count-pdTwo.IndexOne+k,pt);
									}
								}
								else // pdOne.IndexTwo < pdTwo.IndexTwo
								{
									for(int k=pdOne.IndexOne; k>-1; k--)
									{
										Point2D pt = (Point2D)clone[k];
										cltwo.Insert(pdTwo.IndexTwo+pdOne.IndexOne-k,pt);
									}
									for(int k=clone.Count-1; k>pdTwo.IndexOne-1; k--)
									{
										Point2D pt = (Point2D)clone[k];
										cltwo.Insert(pdTwo.IndexTwo+pdOne.IndexOne+1+clone.Count-1-k,pt);
									}
								}
							}

							this.Remove(clone);
							i--;
							break;
						}
					}
				} // for j end
			}// for i end
		}

		// Treat special situation two - sharp angle(one point)
		public void SpecialTreatTwo()
		{
			for(int i=0; i<this.Count; i++)
			{
				Curve clone = (Curve)this[i];

				if(clone.Count > 1)
					continue;

				double maxDisOne = clone.GetMaxDistance();
				double distanceMeanOne = clone.GetDistanceMean();
				double sigmaOne = clone.GetSigma();

				for(int j=0; j<this.Count; j++)
				{
					if(j == i)
						continue;

					Curve cltwo = (Curve)this[j];

					if(cltwo.Count == 1)
						continue;

					double curveDis = clone.GetShortestCurveDistance(cltwo);
					
					double maxDisTwo = cltwo.GetMaxDistance();
					double distanceMeanTwo = cltwo.GetDistanceMean();
					double sigmaTwo = cltwo.GetSigma();					 

					// If two curves are close, try to connect them
					if( (curveDis < distanceMeanOne * sigmaOne) ||
						(curveDis < distanceMeanTwo * sigmaTwo) ||
						(curveDis < maxDisOne)                  ||
						(curveDis < maxDisTwo)                    )						
					{
						ArrayList pointDisArr = clone.GetAllDistancesBetweenTwoCurves(cltwo);
						pointDisArr.Sort();

						DistanceComparable pdOne = (DistanceComparable)pointDisArr[0];
						DistanceComparable pdTwo = (DistanceComparable)pointDisArr[1];

						// Two continue points
						if( ( (pdTwo.Distance / pdOne.Distance) < 1.08) &&
							(Math.Abs( pdOne.IndexTwo - pdTwo.IndexTwo) == 1) )
						{
							Point2D ptone = (Point2D)clone[0];

							if(pdOne.IndexTwo > pdTwo.IndexTwo)
								cltwo.Insert(pdTwo.IndexTwo+1,ptone);
							else
								cltwo.Insert(pdOne.IndexTwo+1,ptone);

							this.Remove(clone);
							i--;
							break;
						}
					}
				} // for j end
			}// for i end
		}

		// Try to connect two curves if they are very close( open curve to other curves )
		public void SpecialTreatThree()
		{
			for(int i=0; i<this.Count; i++)
			{
				Curve clone = (Curve)this[i];

				if(clone.closed)
					continue;

				double maxDisOne = clone.GetMaxDistance();
				double distanceMeanOne = clone.GetDistanceMean();
				double sigmaOne = clone.GetSigma();

				for(int j=0; j<this.Count; j++)
				{
					if(j == i)
						continue;

					Curve cltwo = (Curve)this[j];

					if(cltwo.Count < 4)
						continue;

					double curveDis = clone.GetShortestCurveDistance(cltwo);
					
					double maxDisTwo = cltwo.GetMaxDistance();
					double distanceMeanTwo = cltwo.GetDistanceMean();
					double sigmaTwo = cltwo.GetSigma();					 

					// If two curves are close, try to connect them
					if( (curveDis < distanceMeanOne * sigmaOne) ||
						(curveDis < distanceMeanTwo * sigmaTwo) ||
						(curveDis < maxDisOne)                  ||
						(curveDis < maxDisTwo)                    )						
					{
						ArrayList pointDisArr = clone.GetAllDistancesBetweenTwoCurves(cltwo);
						pointDisArr.Sort();

						DistanceComparable pdOne = (DistanceComparable)pointDisArr[0];
						DistanceComparable pdTwo = (DistanceComparable)pointDisArr[1];

						// If the shortest distance is from the end(head or tail) to the other curve
						if(pdOne.IndexOne == 0)
						{
							Point2D ptone = (Point2D)cltwo[pdOne.IndexTwo];
							clone.Insert(0, ptone);
						}
						else if(pdOne.IndexOne == clone.Count - 1)
						{
							Point2D ptone = (Point2D)cltwo[pdOne.IndexTwo];
							clone.Add(ptone);
						}

						// Two distances are very close, connect both of them
						if( ((pdTwo.Distance / pdOne.Distance) < 1.08) &&
							(pdOne.IndexOne != pdTwo.IndexOne) )
						{
							if(pdTwo.IndexOne == 0)
							{
								Point2D ptone = (Point2D)cltwo[pdTwo.IndexTwo];
								clone.Insert(0, ptone);
							}
							else if(pdOne.IndexOne == clone.Count - 1)
							{
								Point2D ptone = (Point2D)cltwo[pdOne.IndexTwo];
								clone.Add(ptone);
							}
						}					
					}
				} // for j end
			}// for i end
		}
		public void CurveReconstructionsEx()
		{
			// This ArrayList store the distances between all the curves(heads and tails)
			ArrayList curveDisArr = new ArrayList();

			for(int i=0; i<this.Count; i++)
			{
				Curve clOne = (Curve)this[i];
				
				if(clOne.closed == true)
					continue;

				for(int j=i+1; j<this.Count; j++)
				{
					Curve clTwo = (Curve)this[j];
					
					if(clTwo.closed == true)
						continue;

					double curveDis = clOne.GetCurveDistance(clTwo);

					DistanceComparable cd = new DistanceComparable(i, j, curveDis);
					curveDisArr.Add(cd);
					cd = null;
				}
			}

			if(curveDisArr.Count == 0)
			{
				if (this.Count>0)
				{
					Curve clOne = (Curve)this[0];
					if( (clOne.Count >= 3) )
					{
						Point2D ptHeadOne = (Point2D)clOne[0];
						Point2D ptTailOne = (Point2D)clOne[clOne.Count-1];
						double chordDistanceOne = ptHeadOne.Distance(ptTailOne);
						
						if (!clOne.ShouldBeOpenEx())		
							clOne.Close();
					}
				}
				return;

			}
			// Sort the distances ??????
			curveDisArr.Sort();

			// If no distance less than DistanceMean * sigma, connect the shortest distance directly
			bool connect = false;

			// If the shortest distance is not qualified to be connected, than try the second short distance, continue like this
			for(int i=0; i<curveDisArr.Count; i++)
			{
				DistanceComparable cd = (DistanceComparable)curveDisArr[i];
				Curve clOne = (Curve)this[cd.IndexOne];
				Curve clTwo = (Curve)this[cd.IndexTwo];
				double curveDis = cd.Distance;
				
				double distanceMeanOne = clOne.GetDistanceMean();
				
				
				double distanceMeanTwo = clTwo.GetDistanceMean();
				

				int pointCurveNum = this.GetPointCurveNumber();

				// Connect two point curves with shortest distance directly
				if( (clOne.Count == 1) && (clTwo.Count == 1) )
				{
					// Connet the two curves
					this.Remove(clOne);
					this.Remove(clTwo);
				
					Curve clNew = this.ConnetTwoCurves(ref clOne, ref clTwo);

					this.Add(clNew);

					//this.CurveExtend(clNew);

					break;
				}
					// clOne has one point and clTwo has two points
				else if( (clOne.Count == 1) && (clTwo.Count == 2)  &&
					// Test a new fumulation, must delete the previous line---i == 0
					( curveDis <= distanceMeanTwo * 2.05) &&(curveDis >= distanceMeanTwo /2.05))
				{
					// Connet the two curves, should add new conditions here
					this.Remove(clOne);
					this.Remove(clTwo);
				
					Curve clNew = this.ConnetTwoCurves(ref clOne, ref clTwo);

					this.Add(clNew);

					//this.CurveExtend(clNew);

					break;
				}
					// clOne has two points and clTwo has one point
				else if( (clOne.Count == 2) && (clTwo.Count == 1)  &&
					// Test a new fumulation, must delete the previous line---i == 0
					( curveDis <= distanceMeanOne * 2.05)&&(curveDis >= distanceMeanOne /2.05) )
				{
					// Connet the two curves, should add new conditions here
					this.Remove(clOne);
					this.Remove(clTwo);
				
					Curve clNew = this.ConnetTwoCurves(ref clOne, ref clTwo);

					this.Add(clNew);

					//this.CurveExtend(clNew);

					break;
				}
				else if( (clOne.Count == 2) && (clTwo.Count == 2)  &&
					// Test a new fumulation, must delete the previous line---i == 0
					( ( curveDis <= distanceMeanOne * 2.05) ||
					( curveDis <= distanceMeanTwo * 2.05) ) )
				{
		
					// Connet the two curves, should add new conditions here
					this.Remove(clOne);
					this.Remove(clTwo);
				
					Curve clNew = this.ConnetTwoCurves(ref clOne, ref clTwo);

					this.Add(clNew);

					//this.CurveExtend(clNew);

					break;
				}
				else if(( clOne.Count > 2) || (clTwo.Count > 2))
				{
					// If the curves contain more than three points, try to close them
					if( (clOne.Count >= 3) )
					{
						Point2D ptHeadOne = (Point2D)clOne[0];
						Point2D ptTailOne = (Point2D)clOne[clOne.Count-1];
						double chordDistanceOne = ptHeadOne.Distance(ptTailOne);
						
						if( (chordDistanceOne < curveDis) && (!clOne.ShouldBeOpenEx()) )
							//(chordDistanceOne < maxDistanceOne) )							
							clOne.Close();
					}
				
					if( clTwo.Count >= 3 )
					{
						Point2D ptHeadTwo = (Point2D)clTwo[0];
						Point2D ptTailTwo = (Point2D)clTwo[clTwo.Count-1];
						double chordDistanceTwo = ptHeadTwo.Distance(ptTailTwo);
						
						if( (chordDistanceTwo < curveDis) && (!clTwo.ShouldBeOpenEx()) )
							//(chordDistanceTwo < maxDistanceTwo) )
							clTwo.Close();
					}
					
					// If either of two curves is closed, do not connect them, recall reconstruction
					if( clOne.closed == true || clTwo.closed == true )
						break;

					

					
					
					Point2D ptA,ptB,ptC,ptD;
					ptA=(Point2D)clOne[0];
					ptB=(Point2D)clOne[clOne.Count-1];
					ptC=(Point2D)clTwo[0];
					ptD=(Point2D)clTwo[clTwo.Count-1];
					
					int p2pdis;
					
					double exedge,p2pedge,loA,loB,h2tA=0,h2tB=0;
					ArrayList segA=new ArrayList();
					ArrayList segB=new ArrayList();
					int ii,jj;
					for(ii=0;ii<clOne.Count-1;ii++)
					{
						//jj=(((Point2D)clOne[ii]).Y-((Point2D)clOne[ii+1]).Y)*(((Point2D)clOne[ii]).Y-((Point2D)clOne[ii+1]).Y)
						//	+(((Point2D)clOne[ii]).X-((Point2D)clOne[ii+1]).X)*(((Point2D)clOne[ii]).X-((Point2D)clOne[ii+1]).X);	
						//segA.Add(Math.Sqrt((double)jj));
						segA.Add(((Point2D)clOne[ii]).Distance((Point2D)clOne[ii+1]));
					}
					for(ii=0;ii<clTwo.Count-1;ii++)
					{
						//jj=(((Point2D)clTwo[ii]).Y-((Point2D)clTwo[ii+1]).Y)*(((Point2D)clTwo[ii]).Y-((Point2D)clTwo[ii+1]).Y)
						//	+(((Point2D)clTwo[ii]).X-((Point2D)clTwo[ii+1]).X)*(((Point2D)clTwo[ii]).X-((Point2D)clTwo[ii+1]).X);	
						//segB.Add(Math.Sqrt((double)jj));
						segB.Add(((Point2D)clTwo[ii]).Distance((Point2D)clTwo[ii+1]));
					}


					Optimismfilter op=new Optimismfilter();
			
					p2pdis=ptA.DistanceSqr(ptC);
					p2pedge=ptA.Distance(ptC);
					loA=0;
					loB=0;
					if (segA.Count!=0)
					{
						loA=(double)segA[0];
						//jj=(((Point2D)clOne[0]).Y-((Point2D)clOne[clOne.Count-1]).Y)*(((Point2D)clOne[0]).Y-((Point2D)clOne[clOne.Count-1]).Y)
						//	+(((Point2D)clOne[0]).X-((Point2D)clOne[clOne.Count-1]).X)*(((Point2D)clOne[0]).X-((Point2D)clOne[clOne.Count-1]).X);	
						//h2tA=Math.Sqrt((double)jj);
						h2tA=((Point2D)clOne[0]).Distance((Point2D)clOne[clOne.Count-1]);
						
					}
					if(segB.Count!=0)
					{
						loB=(double)segB[0];
						//jj=(((Point2D)clTwo[0]).Y-((Point2D)clTwo[clTwo.Count-1]).Y)*(((Point2D)clTwo[0]).Y-((Point2D)clTwo[clTwo.Count-1]).Y)
						//	+(((Point2D)clTwo[0]).X-((Point2D)clTwo[clTwo.Count-1]).X)*(((Point2D)clTwo[0]).X-((Point2D)clTwo[clTwo.Count-1]).X);	
						//h2tB=Math.Sqrt((double)jj);
						h2tB=((Point2D)clTwo[0]).Distance((Point2D)clTwo[clTwo.Count-1]);
					}
					if (ptA.DistanceSqr(ptD)<p2pdis)
					{
						p2pedge=ptA.Distance(ptD);
						p2pdis=ptA.DistanceSqr(ptD);
						if(segB.Count!=0)
							loB=(double)segB[segB.Count-1];
					}
					if (ptB.DistanceSqr(ptC)<p2pdis)
					{
						p2pedge=ptB.Distance(ptC);
						p2pdis=ptB.DistanceSqr(ptC);
						if (segA.Count!=0)
							loA=(double)segA[segA.Count-1];
					}
					if (ptB.DistanceSqr(ptD)<p2pdis)
					{
						p2pedge=ptB.Distance(ptD);
						
						if (segA.Count!=0)
							loA=(double)segA[segA.Count-1];
						if(segB.Count!=0)
							loB=(double)segB[segB.Count-1];
					}
					bool extendableA=false,extendableB=false;
					if (segA.Count!=0&&segB.Count==0)
					{
						exedge=op.ExternalLength(segA,p2pedge,loA);
						if (exedge>=p2pedge)
							extendableA=true;
					}
					if(segB.Count!=0&&segA.Count==0)
					{
						exedge=op.ExternalLength(segB,p2pedge,loB);
						if (exedge>=p2pedge)
							extendableB=true;
					}
					if((segA.Count!=0&&segB.Count!=0))
					{
						
							exedge=op.ExternalLength(segA,p2pedge,loA);
							if (exedge>=p2pedge&&p2pedge<h2tB&&segB.Count>1||exedge>=p2pedge&&segB.Count<2)
								extendableA=true;
						
							exedge=op.ExternalLength(segB,p2pedge,loB);
							if (exedge>=p2pedge&&p2pedge<h2tA&&segA.Count>1||exedge>=p2pedge&&segA.Count<2)
								extendableB=true;

												
					}
					
					// Test Three
					if( extendableA||extendableB)
					{
						// Connect the two curves
						this.Remove(clOne);
						this.Remove(clTwo);
				
						Curve clNew = this.ConnetTwoCurves(ref clOne, ref clTwo);

						this.Add(clNew);

						if(connect == true)
							connect = false;

						break;
					}					
				} // else end
								
				// If no curve can be connected, try to connect the shortest distance
				if(i == (curveDisArr.Count - 1) )
				{
					return;
				}
				
			} // for end
	
			// Recall CurveReconstruction
			if(curvesconnectcount<curveslimite||curveslimite==0)
			this.CurveReconstructionsEx();			
		}

		/* This method can not deal with popular situations, leave it unused
		// Build up connections between reconstructed curves
		public void ConnectCurves(ArrayList curveConnectionArr)
		{
			for(int i=0; i<this.Count; i++)
			{
				Curve clone = (Curve)this[i];

				if(clone.closed == true)
					continue;

				for(int j=0; j<this.Count; j++)
				{
					if(j == i)
						continue;

					Curve cltwo = (Curve)this[j];
					
					ArrayList pdArr = clone.GetActualCurveDistance(cltwo);
					
					pdArr.Sort();

					DistanceComparable pd = (DistanceComparable)pdArr[0];
					DistanceComparable pd1 = (DistanceComparable)pdArr[1];

					Point2D ptone = (Point2D)clone[pd.IndexOne];
					Point2D pttwo = (Point2D)cltwo[pd.IndexTwo];
					
					Point2D ptthree = (Point2D)clone[pd1.IndexOne];
					Point2D ptfour = (Point2D)cltwo[pd1.IndexTwo];
					
					double curveDis = ptone.Distance(pttwo);
					double curveDis1 = ptthree.Distance(ptfour);

					double maxDistance1 = clone.GetMaxDistance();
					double distanceMean1 = clone.GetDistanceMean();
					double sigma1 = clone.GetSigma();
				
					double maxDistance2 = cltwo.GetMaxDistance();
					double distanceMean2 = cltwo.GetDistanceMean();
					double sigma2 = cltwo.GetSigma();

					if( (curveDis < (distanceMean1 * sigma1 * 1.8)) ||
						(curveDis < (distanceMean2 * sigma2 * 1.8)) )
					{
						Point2D ptonepre = new Point2D(0, 0);
						Point2D pttwopre = new Point2D(0, 0);
						Point2D pttwonext = new Point2D(0, 0);

						if( (pd.IndexOne == 0) && (clone.Count > 0) )
							ptonepre = (Point2D)clone[1];
						else if(pd.IndexOne == clone.Count - 1) 
							ptonepre = (Point2D)clone[clone.Count-2];

						if(!pttwo.OnLine(ptonepre, ptone))
						{
							if(pd.IndexTwo > 0)
								pttwopre = (Point2D)cltwo[pd.IndexTwo-1];

							if(pttwopre.OnLine(ptonepre, ptone))
							{
								CurveActualDistance cdtwopre = new CurveActualDistance(i,pd.IndexOne,j,pd.IndexTwo-1,curveDis);
								curveConnectionArr.Add(cdtwopre);
							}
							else if(pd.IndexTwo < cltwo.Count - 1)
							{
								pttwonext = (Point2D)cltwo[pd.IndexTwo+1];
								if(pttwonext.OnLine(ptonepre, ptone))
								{
									CurveActualDistance cdtwonext = new CurveActualDistance(i,pd.IndexOne,j,pd.IndexTwo+1,curveDis);
									curveConnectionArr.Add(cdtwonext);
								}
							}
						}
						else
						{
							CurveActualDistance cd = new CurveActualDistance(i,pd.IndexOne,j,pd.IndexTwo,curveDis);
							curveConnectionArr.Add(cd);
						}

						if( (!ptone.Equals(ptthree)) &&
							( (curveDis1 / curveDis < 1.2) || (curveDis / curveDis1 < 1.2) ) )
						{
							Point2D ptthreepre = new Point2D(0, 0);
							Point2D ptfourpre = new Point2D(0, 0);
							Point2D ptfournext = new Point2D(0, 0);

							if( (pd1.IndexOne == 0) && (clone.Count > 0) )
								ptthreepre = (Point2D)clone[1];
							else if(pd1.IndexOne == clone.Count - 1) 
								ptthreepre = (Point2D)clone[clone.Count-2];

							if(!ptfour.OnLine(ptthreepre, ptthree))
							{
								if(pd1.IndexTwo > 0)
									ptfourpre = (Point2D)cltwo[pd1.IndexTwo-1];

								if(ptfourpre.OnLine(ptthreepre, ptthree))
								{
									CurveActualDistance cdfourpre = new CurveActualDistance(i,pd1.IndexOne,j,pd1.IndexTwo-1,curveDis);
									curveConnectionArr.Add(cdfourpre);
								}
								else if(pd1.IndexTwo < cltwo.Count - 1)
								{
									ptfournext = (Point2D)cltwo[pd1.IndexTwo+1];
									if(ptfournext.OnLine(ptthreepre, ptthree))
									{
										CurveActualDistance cdfournext = new CurveActualDistance(i,pd1.IndexOne,j,pd1.IndexTwo+1,curveDis);
										curveConnectionArr.Add(cdfournext);
									}
								}
							}
							else
							{
								CurveActualDistance cd1 = new CurveActualDistance(i,pd1.IndexOne,j,pd1.IndexTwo,curveDis1);
								curveConnectionArr.Add(cd1);
							}							
						}
					}
				}
			}			
		}
		*/

		/* This method will not be used anymore, leave it unused
		public void FindTwoCurves(out Curve clone, ref int indexOne, out Curve cltwo, ref int indexTwo, out double curveDistance)
		{
			double shortdistance = 100000000, curvedistance = 0.0;

			int i = 0, j = 0, iii = -1, jjj = -1;
						
			for(i=0; i<this.Count; i++)
			{
				if( (i == indexOne) || (i == indexTwo))
					continue;

				clone = (Curve)this[i];
				if (clone.closed == false)
				{
					for(j=i+1; j<this.Count; j++)
					{
						cltwo = (Curve)this[j];
						if (cltwo.closed == true)
						{	//???
								if(jjj == -1)
								iii = i;							
						}
						else
						{
							curvedistance = clone.GetCurveDistance(cltwo);

							if( (curvedistance < shortdistance ) )
							{
								shortdistance = curvedistance;
								iii = i;
								jjj = j;
							}
						}
					}
				}
			}

			indexOne = iii;
			indexTwo = jjj;

			if (iii == -1 && jjj == -1)
			{
				clone = null;
				cltwo = null;
				curveDistance = 0.0;
			}
			else if (jjj == -1) 
			{	// ???
				clone = (Curve)this[iii];;
				cltwo = null;
				Point2D pthead = (Point2D)clone[0];
				Point2D pttail = (Point2D)clone[clone.Count-1];
				curveDistance = pthead.Distance (pttail);
			}
			else
			{
				clone = (Curve)this[iii];;
				cltwo = (Curve)this[jjj];
				curveDistance = shortdistance;
			}
		}
		*/
		
	}
}
