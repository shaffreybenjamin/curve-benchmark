using System;
using System.Windows.Forms;

namespace DrawTools
{
	/// <summary>
	/// Ellipse tool
	/// </summary>
	public class ToolEllipse : DrawTools.ToolRectangle
	{
		public ToolEllipse()
		{
         
		}

        public override void OnMouseDown(PictureBox sender, MouseEventArgs e)
        {
            AddNewObject(sender, new DrawEllipse(e.X, e.Y, 50, 50));
			((CRT.Mainform)sender.Parent).GraphicsList[0].Objtype="Ellipse";
        }
		
	}
}
