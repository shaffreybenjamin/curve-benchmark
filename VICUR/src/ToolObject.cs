using System;
using System.Windows.Forms;
using System.Drawing;


namespace DrawTools
{
	/// <summary>
	/// Base class for all tools which create new graphic object
	/// </summary>
	public abstract class ToolObject : DrawTools.Tool
	{
        private Cursor cursor;

        /// <summary>
        /// Tool cursor.
        /// </summary>
        protected Cursor Cursor
        {
            get
            {
                return cursor;
            }
            set
            {
                cursor = value;
            }
        }


        /// <summary>
        /// Left mouse is released.
        /// New object is created and resized.
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public override void OnMouseUp(PictureBox sender, MouseEventArgs e)
        {
            ((CRT.Mainform)sender.Parent).GraphicsList[0].Normalize();
            ((CRT.Mainform)sender.Parent).ActiveTool = CRT.Mainform.DrawToolType.Pointer;

            sender.Capture = false;
            sender.Refresh();
        }

        /// <summary>
        /// Add new object to draw area.
        /// Function is called when user left-clicks draw area,
        /// and one of ToolObject-derived tools is active.
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="o"></param>
        protected void AddNewObject(PictureBox sender, DrawObject o)
        {
            ((CRT.Mainform)sender.Parent).GraphicsList.UnselectAll();

            o.Selected = true;
            ((CRT.Mainform)sender.Parent).GraphicsList.Add(o);
            
            sender.Capture = true;
            sender.Refresh();
            
        }
	}
}
