using System;
using System.Windows.Forms;
using System.Drawing;


namespace DrawTools
{
	/// <summary>
	/// Rectangle tool
	/// </summary>
	public class ToolRectangle : DrawTools.ToolObject
	{

		public ToolRectangle()
		{
        
			
		}         

        public override void OnMouseDown(PictureBox sender, MouseEventArgs e)
        {
            AddNewObject(sender, new DrawRectangle(e.X, e.Y, 1, 1));
			((CRT.Mainform)sender.Parent).GraphicsList[0].Objtype="Rect";
        }

        public override void OnMouseMove(PictureBox sender, MouseEventArgs e)
        {
            sender.Cursor = Cursor;

            if ( e.Button == MouseButtons.Left )
            {
                Point point = new Point(e.X, e.Y);
                ((CRT.Mainform)sender.Parent).GraphicsList[0].MoveHandleTo(point, 5);
                sender.Refresh();
            }
        }
	}
}
