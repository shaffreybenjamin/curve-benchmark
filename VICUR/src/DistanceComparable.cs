using System;

namespace Vision
{
	/// <summary>
	/// Summary description for PointDistance.
	/// </summary>
	
	// Store all the distance between a open curve and another curve
	public class DistanceComparable : IComparable
	{
		private int indexOne;
		private int indexTwo;
		private double distance; 

		public int IndexOne
		{
			get{return this.indexOne;}
			set{this.indexOne = value;}
		}

		public int IndexTwo
		{
			get{return this.indexTwo;}
			set{this.indexTwo = value;}
		}

		public double Distance
		{
			get{return this.distance;}
			set{this.distance = value;}
		}

		public DistanceComparable(int indexOne, int indexTwo, double distance)
		{
			this.indexOne = indexOne;
			this.indexTwo = indexTwo;
			this.distance = distance;
			//
			// TODO: Add constructor logic here
			//
		}

		int IComparable.CompareTo(object obj)
		{
			DistanceComparable mc = (DistanceComparable)obj;

			if (this.distance > mc.distance)
				return 1;
			if (this.distance < mc.distance)
				return -1;
			return 0;
		}
	}
	
}
