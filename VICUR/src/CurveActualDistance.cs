using System;

namespace Vision
{
	/// <summary>
	/// Summary description for CurveActualDistance.
	/// </summary>

	// Store the connections between all the open curves to other curves
	public class CurveActualDistance
	{
		private int indexOne;
		private int indexOneOne;
		private int indexTwo;
		private int indexTwoTwo;
		private double curveDis; 

		public int IndexOne
		{
			get{return this.indexOne;}
			set{this.indexOne = value;}
		}

		public int IndexOneOne
		{
			get{return this.indexOneOne;}
			set{this.indexOneOne = value;}
		}

		public int IndexTwo
		{
			get{return this.indexTwo;}
			set{this.indexTwo = value;}
		}

		public int IndexTwoTwo
		{
			get{return this.indexTwoTwo;}
			set{this.indexTwoTwo = value;}
		}

		public double CurveDis
		{
			get{return this.curveDis;}
			set{this.curveDis = value;}
		}

		public CurveActualDistance(int indexOne, int indexOneOne, int indexTwo, int indexTwoTwo, double curveDis)
		{
			this.indexOne = indexOne;
			this.indexOneOne = indexOneOne;
			this.indexTwo = indexTwo;
			this.IndexTwoTwo = indexTwoTwo;
			this.curveDis = curveDis;
			//
			// TODO: Add constructor logic here
			//
		}
	}
	
}
