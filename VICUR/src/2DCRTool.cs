using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
namespace CRT
{
	/// <summary>
	/// Summary description for Dey.
	/// </summary>
	public class Mainform : System.Windows.Forms.Form
	{
		static private System.Int32 state;
		private System.ComponentModel.IContainer components;
		internal System.Drawing.Bitmap buffImg;
		internal System.Drawing.Bitmap bgpicture;
		internal System.Drawing.Graphics buffGr;
		internal int pt;
		internal Vertex vert; /**< contains all the sampling points */
		internal ArrayList vertselected;
		internal bool leftbt;
		internal bool NN;
		internal bool crust;
		internal bool points;
		internal bool voronoi;
		internal bool delaunay;
		internal bool circum;
		internal bool grid;
		internal bool backgroundpicture;
		internal bool mouselocation;
		internal bool onsampling;
		//internal bool shortdistancebeta;
		internal bool distanceBasedNew;
		internal bool shortdistancebetaPlus;
		internal bool shortdistancealpha;
		internal bool shortdistanceDelauney;
		internal bool angledistanceDelauney; /**< angle distance delauney */
		internal bool angledistance2Delauney;
		internal bool angledistance3Delauney;
		internal bool experiment;
		internal bool distanceBased;
		internal bool interStep;
		internal bool gathan;
		internal bool statistic;
		internal int X;
		internal int Y;
		internal bool timetosample;
		internal System.Drawing.Color pointcolor;
		private Sampling sampling;
		private string curvestr="";

		private Cursor MyNoDropCursor=null;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.Panel Controlpanel;
		private System.Windows.Forms.ToolBar toolBar;
		private System.Windows.Forms.StatusBar statusBar;
		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage tabPageAcce;
		private System.Windows.Forms.TabPage tabPageSamp;
		private System.Windows.Forms.TabPage tabPageRecon;
		private System.Windows.Forms.TabPage tabPageStat;
		private System.Windows.Forms.CheckBox checkBoxGrid;
		private System.Windows.Forms.CheckBox checkBoxPoint;
		private System.Windows.Forms.NumericUpDown numericUpDownGrid;
		private System.Windows.Forms.CheckBox checkBoxVoronoi;
		private System.Windows.Forms.CheckBox checkBoxDelauney;
		private System.Windows.Forms.CheckBox checkBoxCircumCircle;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.CheckBox checkBoxCrust;
		private System.Windows.Forms.CheckBox checkBoxNNCrust;
		private System.Windows.Forms.CheckBox checkBoxMouseLoc;
		private System.Windows.Forms.NumericUpDown numericUpDownTimeSample;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RadioButton radioButtonDistSample;
		private System.Windows.Forms.NumericUpDown numericUpDownDistanceSample;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.RadioButton radioButtonScanningSample;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Button buttonScan;
		private System.Windows.Forms.Button buttonShake;
		private System.Windows.Forms.NumericUpDown numericUpDownShake;
		private System.Windows.Forms.NumericUpDown numericUpDownScan;
		private System.Windows.Forms.RadioButton radioButtonTimeSample;
		private System.Windows.Forms.ToolBar toolBarDraw;
		private System.Windows.Forms.ToolBarButton tbPointer;
		private System.Windows.Forms.ToolBarButton tbRectangle;
		private System.Windows.Forms.ToolBarButton tbEllipse;
		private System.Windows.Forms.ToolBarButton tbLine;
		private System.Windows.Forms.ToolBarButton tbPolygon;
		private System.Windows.Forms.ToolBarButton tbArc;
		private Cursor MyNormalCursor = null;
		
		public enum DrawToolType
		{
			Pointer,
			Rectangle,
			Ellipse,
			Line,
			Polygon,
			Arc
		};
		public DrawToolType ActiveTool;
		public DrawTools.Tool[] tools;
		private System.Windows.Forms.ToolBarButton toolBarButtonClear;                 /// array of tools
		public  DrawTools.GraphicsList GraphicsList;
		private System.Windows.Forms.CheckBox checkBoxBeta2;
		private System.Windows.Forms.CheckBox checkBoxAlpha;
		private System.Windows.Forms.CheckBox checkBoxAngl;
		private System.Windows.Forms.CheckBox checkBoxAngleChart;
		private System.Windows.Forms.CheckBox checkBoxSegChart;
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Button buttonStatClear;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.NumericUpDown numericUpDown2;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Button buttonAddpoint;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.CheckBox checkBox3;
		private System.Windows.Forms.ToolBarButton toolBarButtonOpen;
		private System.Windows.Forms.ToolBarButton toolBarButtonSave;    /// list of draw objects
		private System.Windows.Forms.ToolBarButton toolBarButtonSaveAsGTN;
		/// group selection rectangle
		private System.Windows.Forms.CheckBox checkBoxbeta;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.NumericUpDown numericUpDown3;
		private System.Windows.Forms.Button buttonAdd;
		private System.Windows.Forms.Button buttonReset;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Button buttonGoldVariance;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.CheckBox checkBox3D;
		private System.Windows.Forms.ToolBarButton toolBarButtonSaveCurve;
		private System.Windows.Forms.RichTextBox textBoxResult;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.CheckBox checkBoxAngle;
		private System.Windows.Forms.CheckBox checkBoxAngleDistance2;
		private System.Windows.Forms.CheckBox checkboxDISCUR;
		private System.Windows.Forms.CheckBox checkbox_DelEx;


		//Delaunay Ex
		private bool drawDEx = false;
		private System.Windows.Forms.CheckBox checkBox4;
		private System.Windows.Forms.Panel Utensils;
		private System.Windows.Forms.PictureBox SketchPad;
		private System.Windows.Forms.CheckBox chboxAngle;
		private System.Windows.Forms.CheckBox checkBoxVICUR;
		private System.Windows.Forms.CheckBox chkboxExperiment;
		private System.Windows.Forms.CheckBox chkboxInter;
		private System.Windows.Forms.NumericUpDown numericUpDown4;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.RadioButton radioBtnCountPoint;
		private System.Windows.Forms.TextBox txtBxPoints;
		private System.Windows.Forms.RadioButton radioBtnGenPoint;
        private MenuItem menuItem4;
        private MenuItem menuItemImport;
        private MenuItem menuItemExport;

		// Curve File Name
		private string curFileName = null;



		public Mainform()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			///////////////SKETCH PAD///////////////////////////
			pt = - 1;
			vert = new Vertex();
			vertselected= new ArrayList();
			NN = false;
			crust = false;
			points = true;
			voronoi = false;
			delaunay = false;
			circum = false;
			backgroundpicture=false;
			BackColor = System.Drawing.Color.White;
			sampling= new Sampling(vert);
			pointcolor=System.Drawing.Color.Black;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			/////////////Utensils/////////////////////////////////


			//MyNormalCursor = new Cursor("3dwarro.cur");
			//MyNoDropCursor = new Cursor("3dwno.cur");
			tools = new DrawTools.Tool[7];
			tools[(int)DrawToolType.Pointer] = new DrawTools.ToolPointer();
			tools[(int)DrawToolType.Rectangle] = new DrawTools.ToolRectangle();
			tools[(int)DrawToolType.Ellipse] = new DrawTools.ToolEllipse();
			tools[(int)DrawToolType.Line] = new DrawTools.ToolLine();
			tools[(int)DrawToolType.Polygon] = new DrawTools.ToolPolygon();
			tools[(int)DrawToolType.Arc] = new DrawTools.ToolArc();
			InitializeListView();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
					// Dispose of the cursors since they are no longer needed.
					if (MyNormalCursor != null)
						MyNormalCursor.Dispose();

					if (MyNoDropCursor != null)
						MyNoDropCursor.Dispose();

				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mainform));
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItemImport = new System.Windows.Forms.MenuItem();
            this.menuItemExport = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.Controlpanel = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageRecon = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.chkboxInter = new System.Windows.Forms.CheckBox();
            this.chkboxExperiment = new System.Windows.Forms.CheckBox();
            this.checkBoxVICUR = new System.Windows.Forms.CheckBox();
            this.textBoxResult = new System.Windows.Forms.RichTextBox();
            this.checkBox3D = new System.Windows.Forms.CheckBox();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBoxAlpha = new System.Windows.Forms.CheckBox();
            this.checkBoxBeta2 = new System.Windows.Forms.CheckBox();
            this.checkBoxbeta = new System.Windows.Forms.CheckBox();
            this.checkBoxNNCrust = new System.Windows.Forms.CheckBox();
            this.checkBoxCrust = new System.Windows.Forms.CheckBox();
            this.checkBoxAngle = new System.Windows.Forms.CheckBox();
            this.checkBoxAngleDistance2 = new System.Windows.Forms.CheckBox();
            this.checkboxDISCUR = new System.Windows.Forms.CheckBox();
            this.tabPageAcce = new System.Windows.Forms.TabPage();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkbox_DelEx = new System.Windows.Forms.CheckBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.buttonGoldVariance = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.buttonAddpoint = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.checkBoxMouseLoc = new System.Windows.Forms.CheckBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.checkBoxCircumCircle = new System.Windows.Forms.CheckBox();
            this.checkBoxDelauney = new System.Windows.Forms.CheckBox();
            this.checkBoxVoronoi = new System.Windows.Forms.CheckBox();
            this.numericUpDownGrid = new System.Windows.Forms.NumericUpDown();
            this.checkBoxPoint = new System.Windows.Forms.CheckBox();
            this.checkBoxGrid = new System.Windows.Forms.CheckBox();
            this.tabPageSamp = new System.Windows.Forms.TabPage();
            this.toolBarDraw = new System.Windows.Forms.ToolBar();
            this.tbPointer = new System.Windows.Forms.ToolBarButton();
            this.tbRectangle = new System.Windows.Forms.ToolBarButton();
            this.tbEllipse = new System.Windows.Forms.ToolBarButton();
            this.tbLine = new System.Windows.Forms.ToolBarButton();
            this.tbPolygon = new System.Windows.Forms.ToolBarButton();
            this.tbArc = new System.Windows.Forms.ToolBarButton();
            this.buttonShake = new System.Windows.Forms.Button();
            this.buttonScan = new System.Windows.Forms.Button();
            this.numericUpDownShake = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownScan = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButtonScanningSample = new System.Windows.Forms.RadioButton();
            this.numericUpDownDistanceSample = new System.Windows.Forms.NumericUpDown();
            this.radioButtonDistSample = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownTimeSample = new System.Windows.Forms.NumericUpDown();
            this.radioButtonTimeSample = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPageStat = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtBxPoints = new System.Windows.Forms.TextBox();
            this.radioBtnCountPoint = new System.Windows.Forms.RadioButton();
            this.radioBtnGenPoint = new System.Windows.Forms.RadioButton();
            this.chboxAngle = new System.Windows.Forms.CheckBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonStatClear = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.checkBoxSegChart = new System.Windows.Forms.CheckBox();
            this.checkBoxAngleChart = new System.Windows.Forms.CheckBox();
            this.checkBoxAngl = new System.Windows.Forms.CheckBox();
            this.toolBar = new System.Windows.Forms.ToolBar();
            this.toolBarButtonClear = new System.Windows.Forms.ToolBarButton();
            this.toolBarButtonOpen = new System.Windows.Forms.ToolBarButton();
            this.toolBarButtonSave = new System.Windows.Forms.ToolBarButton();
            this.toolBarButtonSaveAsGTN = new System.Windows.Forms.ToolBarButton();
            this.toolBarButtonSaveCurve = new System.Windows.Forms.ToolBarButton();
            this.statusBar = new System.Windows.Forms.StatusBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Utensils = new System.Windows.Forms.Panel();
            this.SketchPad = new System.Windows.Forms.PictureBox();
            this.Controlpanel.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPageRecon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            this.tabPageAcce.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGrid)).BeginInit();
            this.tabPageSamp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownShake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDistanceSample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimeSample)).BeginInit();
            this.tabPageStat.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SketchPad)).BeginInit();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem4});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem3,
            this.menuItemImport,
            this.menuItemExport,
            this.menuItem2});
            this.menuItem1.Text = "File";
            // 
            // menuItem3
            // 
            this.menuItem3.Enabled = false;
            this.menuItem3.Index = 0;
            this.menuItem3.Text = "LoadImage...";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // menuItemImport
            // 
            this.menuItemImport.Enabled = false;
            this.menuItemImport.Index = 1;
            this.menuItemImport.Text = "Import (txt)...";
            this.menuItemImport.Click += new System.EventHandler(this.menuItemImport_Click);
            // 
            // menuItemExport
            // 
            this.menuItemExport.Enabled = false;
            this.menuItemExport.Index = 2;
            this.menuItemExport.Text = "Export (txt)...";
            this.menuItemExport.Click += new System.EventHandler(this.menuItemExport_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 3;
            this.menuItem2.Text = "Exit";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click_1);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 1;
            this.menuItem4.Text = "About";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // Controlpanel
            // 
            this.Controlpanel.BackColor = System.Drawing.SystemColors.Highlight;
            this.Controlpanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controlpanel.Controls.Add(this.tabControl);
            this.Controlpanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Controlpanel.Location = new System.Drawing.Point(0, 40);
            this.Controlpanel.Name = "Controlpanel";
            this.Controlpanel.Size = new System.Drawing.Size(784, 856);
            this.Controlpanel.TabIndex = 2;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageRecon);
            this.tabControl.Controls.Add(this.tabPageAcce);
            this.tabControl.Controls.Add(this.tabPageSamp);
            this.tabControl.Controls.Add(this.tabPageStat);
            this.tabControl.Location = new System.Drawing.Point(8, 8);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(656, 840);
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabPageRecon
            // 
            this.tabPageRecon.Controls.Add(this.label9);
            this.tabPageRecon.Controls.Add(this.numericUpDown4);
            this.tabPageRecon.Controls.Add(this.chkboxInter);
            this.tabPageRecon.Controls.Add(this.chkboxExperiment);
            this.tabPageRecon.Controls.Add(this.checkBoxVICUR);
            this.tabPageRecon.Controls.Add(this.textBoxResult);
            this.tabPageRecon.Controls.Add(this.checkBox3D);
            this.tabPageRecon.Controls.Add(this.buttonReset);
            this.tabPageRecon.Controls.Add(this.buttonAdd);
            this.tabPageRecon.Controls.Add(this.numericUpDown3);
            this.tabPageRecon.Controls.Add(this.label21);
            this.tabPageRecon.Controls.Add(this.checkBox3);
            this.tabPageRecon.Controls.Add(this.checkBox2);
            this.tabPageRecon.Controls.Add(this.checkBox1);
            this.tabPageRecon.Controls.Add(this.checkBoxAlpha);
            this.tabPageRecon.Controls.Add(this.checkBoxBeta2);
            this.tabPageRecon.Controls.Add(this.checkBoxbeta);
            this.tabPageRecon.Controls.Add(this.checkBoxNNCrust);
            this.tabPageRecon.Controls.Add(this.checkBoxCrust);
            this.tabPageRecon.Controls.Add(this.checkBoxAngle);
            this.tabPageRecon.Controls.Add(this.checkBoxAngleDistance2);
            this.tabPageRecon.Controls.Add(this.checkboxDISCUR);
            this.tabPageRecon.Location = new System.Drawing.Point(4, 22);
            this.tabPageRecon.Name = "tabPageRecon";
            this.tabPageRecon.Size = new System.Drawing.Size(648, 814);
            this.tabPageRecon.TabIndex = 2;
            this.tabPageRecon.Text = "Reconstruction";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(42, 380);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 16);
            this.label9.TabIndex = 20;
            this.label9.Text = "smooth";
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.DecimalPlaces = 1;
            this.numericUpDown4.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown4.Location = new System.Drawing.Point(96, 376);
            this.numericUpDown4.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown4.TabIndex = 19;
            this.numericUpDown4.Value = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numericUpDown4.ValueChanged += new System.EventHandler(this.numericUpDown4_ValueChanged);
            this.numericUpDown4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // chkboxInter
            // 
            this.chkboxInter.Enabled = false;
            this.chkboxInter.Location = new System.Drawing.Point(24, 80);
            this.chkboxInter.Name = "chkboxInter";
            this.chkboxInter.Size = new System.Drawing.Size(104, 24);
            this.chkboxInter.TabIndex = 18;
            this.chkboxInter.Text = "inter step";
            this.chkboxInter.CheckedChanged += new System.EventHandler(this.chkboxInter_CheckedChanged);
            this.chkboxInter.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // chkboxExperiment
            // 
            this.chkboxExperiment.Enabled = false;
            this.chkboxExperiment.Location = new System.Drawing.Point(8, 400);
            this.chkboxExperiment.Name = "chkboxExperiment";
            this.chkboxExperiment.Size = new System.Drawing.Size(104, 24);
            this.chkboxExperiment.TabIndex = 17;
            this.chkboxExperiment.Text = "Experiment";
            this.chkboxExperiment.CheckedChanged += new System.EventHandler(this.chkboxExperiment_CheckedChanged);
            this.chkboxExperiment.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // checkBoxVICUR
            // 
            this.checkBoxVICUR.Location = new System.Drawing.Point(8, 352);
            this.checkBoxVICUR.Name = "checkBoxVICUR";
            this.checkBoxVICUR.Size = new System.Drawing.Size(168, 16);
            this.checkBoxVICUR.TabIndex = 16;
            this.checkBoxVICUR.Text = "VICUR";
            this.checkBoxVICUR.CheckedChanged += new System.EventHandler(this.checkBoxAngleDistance3_CheckedChanged);
            this.checkBoxVICUR.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // textBoxResult
            // 
            this.textBoxResult.Location = new System.Drawing.Point(8, 496);
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.Size = new System.Drawing.Size(256, 312);
            this.textBoxResult.TabIndex = 15;
            this.textBoxResult.Text = "richTextBox1";
            // 
            // checkBox3D
            // 
            this.checkBox3D.Enabled = false;
            this.checkBox3D.Location = new System.Drawing.Point(24, 152);
            this.checkBox3D.Name = "checkBox3D";
            this.checkBox3D.Size = new System.Drawing.Size(104, 24);
            this.checkBox3D.TabIndex = 13;
            this.checkBox3D.Text = "3D Points";
            this.checkBox3D.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(192, 456);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 12;
            this.buttonReset.Text = "Reset";
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(112, 456);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonAdd.TabIndex = 11;
            this.buttonAdd.Text = "Add";
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Location = new System.Drawing.Point(8, 456);
            this.numericUpDown3.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(96, 20);
            this.numericUpDown3.TabIndex = 10;
            this.numericUpDown3.ValueChanged += new System.EventHandler(this.numericUpDown3_ValueChanged);
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(8, 432);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(168, 23);
            this.label21.TabIndex = 9;
            this.label21.Text = "Curve Connection Count";
            // 
            // checkBox3
            // 
            this.checkBox3.Enabled = false;
            this.checkBox3.Location = new System.Drawing.Point(8, 232);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(104, 24);
            this.checkBox3.TabIndex = 8;
            this.checkBox3.Text = "GATHAN";
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.Enabled = false;
            this.checkBox2.Location = new System.Drawing.Point(8, 280);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(160, 24);
            this.checkBox2.TabIndex = 7;
            this.checkBox2.Text = "Short Distance (Delauney)";
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            this.checkBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // checkBox1
            // 
            this.checkBox1.Enabled = false;
            this.checkBox1.Location = new System.Drawing.Point(8, 128);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(152, 24);
            this.checkBox1.TabIndex = 6;
            this.checkBox1.Text = "Short Distance (Beta+)";
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            this.checkBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // checkBoxAlpha
            // 
            this.checkBoxAlpha.Enabled = false;
            this.checkBoxAlpha.Location = new System.Drawing.Point(8, 176);
            this.checkBoxAlpha.Name = "checkBoxAlpha";
            this.checkBoxAlpha.Size = new System.Drawing.Size(152, 24);
            this.checkBoxAlpha.TabIndex = 5;
            this.checkBoxAlpha.Text = "Short Distance(alpha)";
            this.checkBoxAlpha.CheckedChanged += new System.EventHandler(this.checkBoxAlpha_CheckedChanged);
            this.checkBoxAlpha.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // checkBoxBeta2
            // 
            this.checkBoxBeta2.Enabled = false;
            this.checkBoxBeta2.Location = new System.Drawing.Point(24, 208);
            this.checkBoxBeta2.Name = "checkBoxBeta2";
            this.checkBoxBeta2.Size = new System.Drawing.Size(160, 24);
            this.checkBoxBeta2.TabIndex = 4;
            this.checkBoxBeta2.Text = "S.D. with sharp corner filter";
            this.checkBoxBeta2.CheckedChanged += new System.EventHandler(this.checkBoxBeta2_CheckedChanged);
            this.checkBoxBeta2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // checkBoxbeta
            // 
            this.checkBoxbeta.Enabled = false;
            this.checkBoxbeta.Location = new System.Drawing.Point(8, 56);
            this.checkBoxbeta.Name = "checkBoxbeta";
            this.checkBoxbeta.Size = new System.Drawing.Size(136, 24);
            this.checkBoxbeta.TabIndex = 2;
            this.checkBoxbeta.Text = "Distance-based (New)";
            this.checkBoxbeta.CheckedChanged += new System.EventHandler(this.checkBoxbeta_CheckedChanged);
            this.checkBoxbeta.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // checkBoxNNCrust
            // 
            this.checkBoxNNCrust.Location = new System.Drawing.Point(8, 32);
            this.checkBoxNNCrust.Name = "checkBoxNNCrust";
            this.checkBoxNNCrust.Size = new System.Drawing.Size(144, 24);
            this.checkBoxNNCrust.TabIndex = 1;
            this.checkBoxNNCrust.Text = "Dey\'s Nearest Neighbor ";
            this.checkBoxNNCrust.CheckedChanged += new System.EventHandler(this.checkBoxNNCrust_CheckStateChanged);
            this.checkBoxNNCrust.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // checkBoxCrust
            // 
            this.checkBoxCrust.Location = new System.Drawing.Point(8, 8);
            this.checkBoxCrust.Name = "checkBoxCrust";
            this.checkBoxCrust.Size = new System.Drawing.Size(104, 24);
            this.checkBoxCrust.TabIndex = 0;
            this.checkBoxCrust.Text = "Crust";
            this.checkBoxCrust.CheckedChanged += new System.EventHandler(this.checkBoxCrust_CheckStateChanged);
            this.checkBoxCrust.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // checkBoxAngle
            // 
            this.checkBoxAngle.Enabled = false;
            this.checkBoxAngle.Location = new System.Drawing.Point(8, 304);
            this.checkBoxAngle.Name = "checkBoxAngle";
            this.checkBoxAngle.Size = new System.Drawing.Size(160, 16);
            this.checkBoxAngle.TabIndex = 7;
            this.checkBoxAngle.Text = "Angle Distance (Delauney)";
            this.checkBoxAngle.CheckedChanged += new System.EventHandler(this.checkBoxAngle_CheckedChanged);
            this.checkBoxAngle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // checkBoxAngleDistance2
            // 
            this.checkBoxAngleDistance2.Enabled = false;
            this.checkBoxAngleDistance2.Location = new System.Drawing.Point(8, 328);
            this.checkBoxAngleDistance2.Name = "checkBoxAngleDistance2";
            this.checkBoxAngleDistance2.Size = new System.Drawing.Size(160, 16);
            this.checkBoxAngleDistance2.TabIndex = 7;
            this.checkBoxAngleDistance2.Text = "Angle Distance2 (Delauney)";
            this.checkBoxAngleDistance2.CheckedChanged += new System.EventHandler(this.checkBoxAngleDistance2_CheckedChanged);
            this.checkBoxAngleDistance2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // checkboxDISCUR
            // 
            this.checkboxDISCUR.Location = new System.Drawing.Point(8, 256);
            this.checkboxDISCUR.Name = "checkboxDISCUR";
            this.checkboxDISCUR.Size = new System.Drawing.Size(104, 24);
            this.checkboxDISCUR.TabIndex = 5;
            this.checkboxDISCUR.Text = "DISCUR";
            this.checkboxDISCUR.CheckedChanged += new System.EventHandler(this.checkboxDistance_CheckedChanged);
            this.checkboxDISCUR.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // tabPageAcce
            // 
            this.tabPageAcce.Controls.Add(this.checkBox4);
            this.tabPageAcce.Controls.Add(this.checkbox_DelEx);
            this.tabPageAcce.Controls.Add(this.radioButton2);
            this.tabPageAcce.Controls.Add(this.radioButton1);
            this.tabPageAcce.Controls.Add(this.buttonGoldVariance);
            this.tabPageAcce.Controls.Add(this.listBox1);
            this.tabPageAcce.Controls.Add(this.buttonAddpoint);
            this.tabPageAcce.Controls.Add(this.label20);
            this.tabPageAcce.Controls.Add(this.label19);
            this.tabPageAcce.Controls.Add(this.numericUpDown2);
            this.tabPageAcce.Controls.Add(this.numericUpDown1);
            this.tabPageAcce.Controls.Add(this.checkBoxMouseLoc);
            this.tabPageAcce.Controls.Add(this.buttonClear);
            this.tabPageAcce.Controls.Add(this.checkBoxCircumCircle);
            this.tabPageAcce.Controls.Add(this.checkBoxDelauney);
            this.tabPageAcce.Controls.Add(this.checkBoxVoronoi);
            this.tabPageAcce.Controls.Add(this.numericUpDownGrid);
            this.tabPageAcce.Controls.Add(this.checkBoxPoint);
            this.tabPageAcce.Controls.Add(this.checkBoxGrid);
            this.tabPageAcce.Location = new System.Drawing.Point(4, 22);
            this.tabPageAcce.Name = "tabPageAcce";
            this.tabPageAcce.Size = new System.Drawing.Size(648, 814);
            this.tabPageAcce.TabIndex = 0;
            this.tabPageAcce.Text = "Accesories";
            // 
            // checkBox4
            // 
            this.checkBox4.Location = new System.Drawing.Point(8, 160);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(128, 24);
            this.checkBox4.TabIndex = 21;
            this.checkBox4.Text = "Background Picture";
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged_1);
            // 
            // checkbox_DelEx
            // 
            this.checkbox_DelEx.Location = new System.Drawing.Point(8, 184);
            this.checkbox_DelEx.Name = "checkbox_DelEx";
            this.checkbox_DelEx.Size = new System.Drawing.Size(104, 24);
            this.checkbox_DelEx.TabIndex = 20;
            this.checkbox_DelEx.Text = "Delaunay_Ex";
            this.checkbox_DelEx.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            this.checkbox_DelEx.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // radioButton2
            // 
            this.radioButton2.Location = new System.Drawing.Point(128, 56);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(48, 24);
            this.radioButton2.TabIndex = 19;
            this.radioButton2.Text = "Red";
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(72, 56);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(104, 24);
            this.radioButton1.TabIndex = 18;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Black";
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // buttonGoldVariance
            // 
            this.buttonGoldVariance.Location = new System.Drawing.Point(8, 296);
            this.buttonGoldVariance.Name = "buttonGoldVariance";
            this.buttonGoldVariance.Size = new System.Drawing.Size(88, 23);
            this.buttonGoldVariance.TabIndex = 16;
            this.buttonGoldVariance.Text = "GoldVariance";
            this.buttonGoldVariance.Click += new System.EventHandler(this.buttonGoldVariance_Click);
            // 
            // listBox1
            // 
            this.listBox1.Location = new System.Drawing.Point(8, 328);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(256, 251);
            this.listBox1.TabIndex = 15;
            // 
            // buttonAddpoint
            // 
            this.buttonAddpoint.Location = new System.Drawing.Point(8, 256);
            this.buttonAddpoint.Name = "buttonAddpoint";
            this.buttonAddpoint.Size = new System.Drawing.Size(75, 23);
            this.buttonAddpoint.TabIndex = 12;
            this.buttonAddpoint.Text = "Add Point";
            this.buttonAddpoint.Click += new System.EventHandler(this.buttonAddpoint_Click);
            this.buttonAddpoint.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(128, 224);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(24, 23);
            this.label20.TabIndex = 11;
            this.label20.Text = "Y=";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(8, 224);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(24, 23);
            this.label19.TabIndex = 10;
            this.label19.Text = "X=";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(160, 224);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(80, 20);
            this.numericUpDown2.TabIndex = 9;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(40, 224);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(80, 20);
            this.numericUpDown1.TabIndex = 8;
            // 
            // checkBoxMouseLoc
            // 
            this.checkBoxMouseLoc.Location = new System.Drawing.Point(8, 140);
            this.checkBoxMouseLoc.Name = "checkBoxMouseLoc";
            this.checkBoxMouseLoc.Size = new System.Drawing.Size(104, 24);
            this.checkBoxMouseLoc.TabIndex = 7;
            this.checkBoxMouseLoc.Text = "MouseLocation";
            this.checkBoxMouseLoc.CheckedChanged += new System.EventHandler(this.checkBoxMouseLoc_CheckStateChanged);
            this.checkBoxMouseLoc.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(8, 728);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 6;
            this.buttonClear.Text = "Clear";
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // checkBoxCircumCircle
            // 
            this.checkBoxCircumCircle.Location = new System.Drawing.Point(8, 120);
            this.checkBoxCircumCircle.Name = "checkBoxCircumCircle";
            this.checkBoxCircumCircle.Size = new System.Drawing.Size(104, 24);
            this.checkBoxCircumCircle.TabIndex = 5;
            this.checkBoxCircumCircle.Text = "Circumcircle";
            this.checkBoxCircumCircle.CheckedChanged += new System.EventHandler(this.checkBoxCircumCircle_CheckStateChanged);
            this.checkBoxCircumCircle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // checkBoxDelauney
            // 
            this.checkBoxDelauney.Location = new System.Drawing.Point(8, 98);
            this.checkBoxDelauney.Name = "checkBoxDelauney";
            this.checkBoxDelauney.Size = new System.Drawing.Size(104, 24);
            this.checkBoxDelauney.TabIndex = 4;
            this.checkBoxDelauney.Text = "Delauney";
            this.checkBoxDelauney.CheckedChanged += new System.EventHandler(this.checkBoxDelauney_CheckStateChanged);
            this.checkBoxDelauney.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // checkBoxVoronoi
            // 
            this.checkBoxVoronoi.Location = new System.Drawing.Point(8, 80);
            this.checkBoxVoronoi.Name = "checkBoxVoronoi";
            this.checkBoxVoronoi.Size = new System.Drawing.Size(104, 24);
            this.checkBoxVoronoi.TabIndex = 3;
            this.checkBoxVoronoi.Text = "Voronoi";
            this.checkBoxVoronoi.CheckedChanged += new System.EventHandler(this.checkBoxVoronoi_CheckStateChanged);
            this.checkBoxVoronoi.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // numericUpDownGrid
            // 
            this.numericUpDownGrid.Location = new System.Drawing.Point(8, 8);
            this.numericUpDownGrid.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownGrid.Name = "numericUpDownGrid";
            this.numericUpDownGrid.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownGrid.TabIndex = 2;
            this.numericUpDownGrid.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownGrid.ValueChanged += new System.EventHandler(this.numericUpDownGrid_ValueChanged);
            // 
            // checkBoxPoint
            // 
            this.checkBoxPoint.Checked = true;
            this.checkBoxPoint.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPoint.Location = new System.Drawing.Point(8, 56);
            this.checkBoxPoint.Name = "checkBoxPoint";
            this.checkBoxPoint.Size = new System.Drawing.Size(104, 24);
            this.checkBoxPoint.TabIndex = 1;
            this.checkBoxPoint.Text = "Points";
            this.checkBoxPoint.CheckedChanged += new System.EventHandler(this.checkBoxPoint_CheckStateChanged);
            this.checkBoxPoint.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // checkBoxGrid
            // 
            this.checkBoxGrid.Location = new System.Drawing.Point(8, 32);
            this.checkBoxGrid.Name = "checkBoxGrid";
            this.checkBoxGrid.Size = new System.Drawing.Size(56, 24);
            this.checkBoxGrid.TabIndex = 0;
            this.checkBoxGrid.Text = "Grids";
            this.checkBoxGrid.CheckedChanged += new System.EventHandler(this.checkBoxGrid_CheckStateChanged);
            this.checkBoxGrid.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // tabPageSamp
            // 
            this.tabPageSamp.Controls.Add(this.toolBarDraw);
            this.tabPageSamp.Controls.Add(this.buttonShake);
            this.tabPageSamp.Controls.Add(this.buttonScan);
            this.tabPageSamp.Controls.Add(this.numericUpDownShake);
            this.tabPageSamp.Controls.Add(this.numericUpDownScan);
            this.tabPageSamp.Controls.Add(this.label3);
            this.tabPageSamp.Controls.Add(this.radioButtonScanningSample);
            this.tabPageSamp.Controls.Add(this.numericUpDownDistanceSample);
            this.tabPageSamp.Controls.Add(this.radioButtonDistSample);
            this.tabPageSamp.Controls.Add(this.label1);
            this.tabPageSamp.Controls.Add(this.numericUpDownTimeSample);
            this.tabPageSamp.Controls.Add(this.radioButtonTimeSample);
            this.tabPageSamp.Controls.Add(this.label2);
            this.tabPageSamp.Controls.Add(this.label4);
            this.tabPageSamp.Controls.Add(this.label5);
            this.tabPageSamp.Controls.Add(this.label6);
            this.tabPageSamp.Controls.Add(this.label7);
            this.tabPageSamp.Controls.Add(this.label8);
            this.tabPageSamp.Location = new System.Drawing.Point(4, 22);
            this.tabPageSamp.Name = "tabPageSamp";
            this.tabPageSamp.Size = new System.Drawing.Size(648, 814);
            this.tabPageSamp.TabIndex = 1;
            this.tabPageSamp.Text = "Sampling";
            // 
            // toolBarDraw
            // 
            this.toolBarDraw.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.toolBarDraw.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.tbPointer,
            this.tbRectangle,
            this.tbEllipse,
            this.tbLine,
            this.tbPolygon,
            this.tbArc});
            this.toolBarDraw.ButtonSize = new System.Drawing.Size(45, 45);
            this.toolBarDraw.Dock = System.Windows.Forms.DockStyle.None;
            this.toolBarDraw.DropDownArrows = true;
            this.toolBarDraw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolBarDraw.Location = new System.Drawing.Point(8, 440);
            this.toolBarDraw.Name = "toolBarDraw";
            this.toolBarDraw.ShowToolTips = true;
            this.toolBarDraw.Size = new System.Drawing.Size(140, 116);
            this.toolBarDraw.TabIndex = 18;
            this.toolBarDraw.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBarDraw_ButtonClick);
            this.toolBarDraw.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // tbPointer
            // 
            this.tbPointer.ImageIndex = 3;
            this.tbPointer.Name = "tbPointer";
            this.tbPointer.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.tbPointer.Text = "Pointer";
            this.tbPointer.ToolTipText = "Pointer";
            // 
            // tbRectangle
            // 
            this.tbRectangle.ImageIndex = 4;
            this.tbRectangle.Name = "tbRectangle";
            this.tbRectangle.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.tbRectangle.Text = "Rectangle";
            this.tbRectangle.ToolTipText = "Recatngle";
            // 
            // tbEllipse
            // 
            this.tbEllipse.ImageIndex = 5;
            this.tbEllipse.Name = "tbEllipse";
            this.tbEllipse.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.tbEllipse.Text = "Ellipse";
            this.tbEllipse.ToolTipText = "Ellipse";
            // 
            // tbLine
            // 
            this.tbLine.ImageIndex = 6;
            this.tbLine.Name = "tbLine";
            this.tbLine.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.tbLine.Text = "Line";
            this.tbLine.ToolTipText = "Line";
            // 
            // tbPolygon
            // 
            this.tbPolygon.ImageIndex = 7;
            this.tbPolygon.Name = "tbPolygon";
            this.tbPolygon.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.tbPolygon.Text = "Polygon";
            this.tbPolygon.ToolTipText = "Polygon";
            // 
            // tbArc
            // 
            this.tbArc.ImageIndex = 9;
            this.tbArc.Name = "tbArc";
            this.tbArc.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.tbArc.Text = "Arc";
            this.tbArc.ToolTipText = "Arc";
            // 
            // buttonShake
            // 
            this.buttonShake.Location = new System.Drawing.Point(16, 392);
            this.buttonShake.Name = "buttonShake";
            this.buttonShake.Size = new System.Drawing.Size(120, 23);
            this.buttonShake.TabIndex = 16;
            this.buttonShake.Text = "Shake";
            this.buttonShake.Click += new System.EventHandler(this.buttonShake_Click);
            this.buttonShake.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // buttonScan
            // 
            this.buttonScan.Location = new System.Drawing.Point(16, 264);
            this.buttonScan.Name = "buttonScan";
            this.buttonScan.Size = new System.Drawing.Size(120, 23);
            this.buttonScan.TabIndex = 15;
            this.buttonScan.Text = "Scan";
            this.buttonScan.Click += new System.EventHandler(this.buttonScan_Click);
            this.buttonScan.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // numericUpDownShake
            // 
            this.numericUpDownShake.Location = new System.Drawing.Point(16, 360);
            this.numericUpDownShake.Name = "numericUpDownShake";
            this.numericUpDownShake.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownShake.TabIndex = 12;
            this.numericUpDownShake.Value = new decimal(new int[] {
            14,
            0,
            0,
            0});
            // 
            // numericUpDownScan
            // 
            this.numericUpDownScan.Location = new System.Drawing.Point(16, 232);
            this.numericUpDownScan.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownScan.Name = "numericUpDownScan";
            this.numericUpDownScan.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownScan.TabIndex = 8;
            this.numericUpDownScan.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDownScan.ValueChanged += new System.EventHandler(this.radioButtonScanningSample_Click);
            this.numericUpDownScan.Click += new System.EventHandler(this.radioButtonScanningSample_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 216);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "Step Distance (pix)";
            // 
            // radioButtonScanningSample
            // 
            this.radioButtonScanningSample.Location = new System.Drawing.Point(16, 200);
            this.radioButtonScanningSample.Name = "radioButtonScanningSample";
            this.radioButtonScanningSample.Size = new System.Drawing.Size(120, 16);
            this.radioButtonScanningSample.TabIndex = 6;
            this.radioButtonScanningSample.Text = "Scanning Sampling";
            this.radioButtonScanningSample.CheckedChanged += new System.EventHandler(this.radioButtonScanningSample_Click);
            this.radioButtonScanningSample.Click += new System.EventHandler(this.radioButtonScanningSample_Click);
            // 
            // numericUpDownDistanceSample
            // 
            this.numericUpDownDistanceSample.Location = new System.Drawing.Point(16, 144);
            this.numericUpDownDistanceSample.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownDistanceSample.Name = "numericUpDownDistanceSample";
            this.numericUpDownDistanceSample.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownDistanceSample.TabIndex = 4;
            this.numericUpDownDistanceSample.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownDistanceSample.ValueChanged += new System.EventHandler(this.radioButtonDistSample_Click);
            this.numericUpDownDistanceSample.Click += new System.EventHandler(this.radioButtonDistSample_Click);
            // 
            // radioButtonDistSample
            // 
            this.radioButtonDistSample.Location = new System.Drawing.Point(16, 112);
            this.radioButtonDistSample.Name = "radioButtonDistSample";
            this.radioButtonDistSample.Size = new System.Drawing.Size(120, 16);
            this.radioButtonDistSample.TabIndex = 3;
            this.radioButtonDistSample.Text = "Distance Sampling ";
            this.radioButtonDistSample.CheckedChanged += new System.EventHandler(this.radioButtonDistSample_Click);
            this.radioButtonDistSample.Click += new System.EventHandler(this.radioButtonDistSample_Click);
            this.radioButtonDistSample.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sampling Interval (ms)";
            // 
            // numericUpDownTimeSample
            // 
            this.numericUpDownTimeSample.Location = new System.Drawing.Point(16, 56);
            this.numericUpDownTimeSample.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownTimeSample.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTimeSample.Name = "numericUpDownTimeSample";
            this.numericUpDownTimeSample.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownTimeSample.TabIndex = 1;
            this.numericUpDownTimeSample.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDownTimeSample.ValueChanged += new System.EventHandler(this.numericUpDownTimeSample_Click);
            this.numericUpDownTimeSample.Click += new System.EventHandler(this.numericUpDownTimeSample_Click);
            // 
            // radioButtonTimeSample
            // 
            this.radioButtonTimeSample.Location = new System.Drawing.Point(16, 24);
            this.radioButtonTimeSample.Name = "radioButtonTimeSample";
            this.radioButtonTimeSample.Size = new System.Drawing.Size(104, 16);
            this.radioButtonTimeSample.TabIndex = 0;
            this.radioButtonTimeSample.Text = "Time Sampling";
            this.radioButtonTimeSample.CheckedChanged += new System.EventHandler(this.radioButtonTimeSample_CheckedChanged);
            this.radioButtonTimeSample.Click += new System.EventHandler(this.radioButtonTimeSample_Click);
            this.radioButtonTimeSample.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 23);
            this.label2.TabIndex = 5;
            this.label2.Text = "Step Distance (pix)";
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(8, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 70);
            this.label4.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(8, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 70);
            this.label5.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(8, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 104);
            this.label6.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(16, 328);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 23);
            this.label7.TabIndex = 13;
            this.label7.Text = "Max Shake ";
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Location = new System.Drawing.Point(8, 320);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 104);
            this.label8.TabIndex = 14;
            // 
            // tabPageStat
            // 
            this.tabPageStat.Controls.Add(this.panel1);
            this.tabPageStat.Controls.Add(this.chboxAngle);
            this.tabPageStat.Controls.Add(this.label25);
            this.tabPageStat.Controls.Add(this.label24);
            this.tabPageStat.Controls.Add(this.label23);
            this.tabPageStat.Controls.Add(this.label22);
            this.tabPageStat.Controls.Add(this.label18);
            this.tabPageStat.Controls.Add(this.label17);
            this.tabPageStat.Controls.Add(this.label16);
            this.tabPageStat.Controls.Add(this.label15);
            this.tabPageStat.Controls.Add(this.label14);
            this.tabPageStat.Controls.Add(this.label13);
            this.tabPageStat.Controls.Add(this.label12);
            this.tabPageStat.Controls.Add(this.buttonStatClear);
            this.tabPageStat.Controls.Add(this.label11);
            this.tabPageStat.Controls.Add(this.label10);
            this.tabPageStat.Controls.Add(this.listView1);
            this.tabPageStat.Controls.Add(this.checkBoxSegChart);
            this.tabPageStat.Controls.Add(this.checkBoxAngleChart);
            this.tabPageStat.Controls.Add(this.checkBoxAngl);
            this.tabPageStat.Location = new System.Drawing.Point(4, 22);
            this.tabPageStat.Name = "tabPageStat";
            this.tabPageStat.Size = new System.Drawing.Size(648, 814);
            this.tabPageStat.TabIndex = 3;
            this.tabPageStat.Text = "Statistic Result";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtBxPoints);
            this.panel1.Controls.Add(this.radioBtnCountPoint);
            this.panel1.Controls.Add(this.radioBtnGenPoint);
            this.panel1.Location = new System.Drawing.Point(8, 160);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(144, 88);
            this.panel1.TabIndex = 24;
            // 
            // txtBxPoints
            // 
            this.txtBxPoints.Location = new System.Drawing.Point(8, 8);
            this.txtBxPoints.Name = "txtBxPoints";
            this.txtBxPoints.Size = new System.Drawing.Size(88, 20);
            this.txtBxPoints.TabIndex = 1;
            // 
            // radioBtnCountPoint
            // 
            this.radioBtnCountPoint.Location = new System.Drawing.Point(8, 32);
            this.radioBtnCountPoint.Name = "radioBtnCountPoint";
            this.radioBtnCountPoint.Size = new System.Drawing.Size(120, 24);
            this.radioBtnCountPoint.TabIndex = 0;
            this.radioBtnCountPoint.Text = "Number of points";
            this.radioBtnCountPoint.CheckedChanged += new System.EventHandler(this.radioBtnPoint_CheckedChanged);
            // 
            // radioBtnGenPoint
            // 
            this.radioBtnGenPoint.Location = new System.Drawing.Point(8, 56);
            this.radioBtnGenPoint.Name = "radioBtnGenPoint";
            this.radioBtnGenPoint.Size = new System.Drawing.Size(112, 24);
            this.radioBtnGenPoint.TabIndex = 0;
            this.radioBtnGenPoint.Text = "Generation points";
            this.radioBtnGenPoint.CheckedChanged += new System.EventHandler(this.radioBtnPoint_CheckedChanged);
            // 
            // chboxAngle
            // 
            this.chboxAngle.Location = new System.Drawing.Point(144, 16);
            this.chboxAngle.Name = "chboxAngle";
            this.chboxAngle.Size = new System.Drawing.Size(72, 24);
            this.chboxAngle.TabIndex = 23;
            this.chboxAngle.Text = "Angle";
            this.chboxAngle.CheckedChanged += new System.EventHandler(this.chboxAngle_CheckedChanged);
            // 
            // label25
            // 
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Location = new System.Drawing.Point(136, 496);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(144, 23);
            this.label25.TabIndex = 19;
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(8, 496);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(100, 23);
            this.label24.TabIndex = 18;
            this.label24.Text = "Convex";
            // 
            // label23
            // 
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Location = new System.Drawing.Point(136, 424);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(144, 23);
            this.label23.TabIndex = 17;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(8, 424);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(100, 23);
            this.label22.TabIndex = 16;
            this.label22.Text = "Extend Rate";
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(8, 464);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 23);
            this.label18.TabIndex = 15;
            this.label18.Text = "SelfIntersection";
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(8, 392);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(128, 23);
            this.label17.TabIndex = 14;
            this.label17.Text = "Center Average Length";
            // 
            // label16
            // 
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label16.Location = new System.Drawing.Point(136, 392);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(144, 23);
            this.label16.TabIndex = 13;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(8, 360);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 23);
            this.label15.TabIndex = 12;
            this.label15.Text = "Tail External Length";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(8, 328);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(120, 23);
            this.label14.TabIndex = 11;
            this.label14.Text = "Head External Length";
            // 
            // label13
            // 
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Location = new System.Drawing.Point(136, 360);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(144, 23);
            this.label13.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Location = new System.Drawing.Point(136, 328);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(144, 23);
            this.label12.TabIndex = 9;
            // 
            // buttonStatClear
            // 
            this.buttonStatClear.Location = new System.Drawing.Point(8, 544);
            this.buttonStatClear.Name = "buttonStatClear";
            this.buttonStatClear.Size = new System.Drawing.Size(128, 24);
            this.buttonStatClear.TabIndex = 8;
            this.buttonStatClear.Text = "Clear Statistic Result";
            this.buttonStatClear.Click += new System.EventHandler(this.buttonStatClear_Click);
            this.buttonStatClear.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(8, 296);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 23);
            this.label11.TabIndex = 7;
            this.label11.Text = "Point To Line Distance:";
            // 
            // label10
            // 
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Location = new System.Drawing.Point(136, 296);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(144, 23);
            this.label10.TabIndex = 6;
            // 
            // listView1
            // 
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(8, 48);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(272, 96);
            this.listView1.TabIndex = 5;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Mainform_KeyDown);
            // 
            // checkBoxSegChart
            // 
            this.checkBoxSegChart.Location = new System.Drawing.Point(8, 608);
            this.checkBoxSegChart.Name = "checkBoxSegChart";
            this.checkBoxSegChart.Size = new System.Drawing.Size(160, 24);
            this.checkBoxSegChart.TabIndex = 4;
            this.checkBoxSegChart.Text = "Segment Statistic Chart";
            // 
            // checkBoxAngleChart
            // 
            this.checkBoxAngleChart.Location = new System.Drawing.Point(8, 576);
            this.checkBoxAngleChart.Name = "checkBoxAngleChart";
            this.checkBoxAngleChart.Size = new System.Drawing.Size(152, 24);
            this.checkBoxAngleChart.TabIndex = 3;
            this.checkBoxAngleChart.Text = "Angle Statistic Chart";
            // 
            // checkBoxAngl
            // 
            this.checkBoxAngl.Location = new System.Drawing.Point(16, 16);
            this.checkBoxAngl.Name = "checkBoxAngl";
            this.checkBoxAngl.Size = new System.Drawing.Size(104, 24);
            this.checkBoxAngl.TabIndex = 0;
            this.checkBoxAngl.Text = "CurveStatistic";
            this.checkBoxAngl.CheckedChanged += new System.EventHandler(this.checkBoxAngl_CheckedChanged);
            this.checkBoxAngl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxPoint_MouseUp);
            // 
            // toolBar
            // 
            this.toolBar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.toolBarButtonClear,
            this.toolBarButtonOpen,
            this.toolBarButtonSave,
            this.toolBarButtonSaveAsGTN,
            this.toolBarButtonSaveCurve});
            this.toolBar.DropDownArrows = true;
            this.toolBar.Location = new System.Drawing.Point(0, 0);
            this.toolBar.Name = "toolBar";
            this.toolBar.ShowToolTips = true;
            this.toolBar.Size = new System.Drawing.Size(1028, 42);
            this.toolBar.TabIndex = 3;
            this.toolBar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar_ButtonClick);
            // 
            // toolBarButtonClear
            // 
            this.toolBarButtonClear.Name = "toolBarButtonClear";
            this.toolBarButtonClear.Text = "Clear";
            // 
            // toolBarButtonOpen
            // 
            this.toolBarButtonOpen.Name = "toolBarButtonOpen";
            this.toolBarButtonOpen.Text = "Open Points File";
            // 
            // toolBarButtonSave
            // 
            this.toolBarButtonSave.Name = "toolBarButtonSave";
            this.toolBarButtonSave.Text = "Save Points File";
            // 
            // toolBarButtonSaveAsGTN
            // 
            this.toolBarButtonSaveAsGTN.Enabled = false;
            this.toolBarButtonSaveAsGTN.Name = "toolBarButtonSaveAsGTN";
            this.toolBarButtonSaveAsGTN.Text = "Save Points File (GTN)";
            // 
            // toolBarButtonSaveCurve
            // 
            this.toolBarButtonSaveCurve.Name = "toolBarButtonSaveCurve";
            this.toolBarButtonSaveCurve.Text = "Save Curves File";
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 689);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1028, 16);
            this.statusBar.TabIndex = 4;
            this.statusBar.Text = "statusBar1";
            // 
            // Utensils
            // 
            this.Utensils.AutoScroll = true;
            this.Utensils.BackColor = System.Drawing.SystemColors.Info;
            this.Utensils.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Utensils.Location = new System.Drawing.Point(64, 736);
            this.Utensils.Name = "Utensils";
            this.Utensils.Size = new System.Drawing.Size(872, 200);
            this.Utensils.TabIndex = 1;
            // 
            // SketchPad
            // 
            this.SketchPad.Location = new System.Drawing.Point(216, 168);
            this.SketchPad.Name = "SketchPad";
            this.SketchPad.Size = new System.Drawing.Size(656, 416);
            this.SketchPad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.SketchPad.TabIndex = 0;
            this.SketchPad.TabStop = false;
            this.SketchPad.Click += new System.EventHandler(this.SketchPad_Click);
            this.SketchPad.Paint += new System.Windows.Forms.PaintEventHandler(this.SketchPad_Paint);
            this.SketchPad.DoubleClick += new System.EventHandler(this.SketchPad_DoubleClick);
            this.SketchPad.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SketchPad_MouseDown);
            this.SketchPad.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SketchPad_MouseMove);
            this.SketchPad.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SketchPad_MouseUp);
            // 
            // Mainform
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1028, 705);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.toolBar);
            this.Controls.Add(this.Controlpanel);
            this.Controls.Add(this.Utensils);
            this.Controls.Add(this.SketchPad);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Menu = this.mainMenu1;
            this.Name = "Mainform";
            this.Text = "2DCurveReconstructionTool";
            this.Load += new System.EventHandler(this.Mainform_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Dey_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Mainform_KeyDown);
            this.Resize += new System.EventHandler(this.Dey_Resize);
            this.Controlpanel.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPageRecon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            this.tabPageAcce.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGrid)).EndInit();
            this.tabPageSamp.ResumeLayout(false);
            this.tabPageSamp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownShake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDistanceSample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimeSample)).EndInit();
            this.tabPageStat.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SketchPad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void SketchPad_Click(object sender, System.EventArgs e)
		{
			if (!sampling.SampleByScanning)
				if (pt==-1&&leftbt )
				{
					vert.add(X,Y);
					Refresh();
				}
			if(pt!=-1&&!leftbt)
				if(!vertselected.Contains(pt))
				{
					vertselected.Add(pt) ;
					System.Windows.Forms.ListViewItem myItem=new System.Windows.Forms.ListViewItem();
					myItem.Text=pt.ToString();
					myItem.SubItems.Add(vert.get_Renamed(pt).X.ToString());
					myItem.SubItems.Add(vert.get_Renamed(pt).Y.ToString());
					myItem.SubItems.Add("0");
					myItem.SubItems.Add("0");
					myItem.SubItems.Add("0");
					myItem.SubItems.Add("0");
					listView1.Items.Insert(listView1.Items.Count,myItem);

				}
			
		}

		private void SketchPad_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			int i;
			i=0;

			X=e.X;
			Y=e.Y;

			if (pt != - 1&&leftbt)
			
			{
				
				vert.set_Renamed(pt, e.X, e.Y);
				i++;
				
			}

			if (mouselocation)
			{
				
				i++;
				buffGr.FillRegion(new System.Drawing.SolidBrush(SupportClass.GraphicsManager.manager.GetBackColor(buffGr)), new System.Drawing.Region(new System.Drawing.Rectangle(0, 0, Size.Width, Size.Height)));
				drawMouseLocation(buffGr);
				
			}
			
			if (pt == -1 )
			{
				i++;

			}
			if (onsampling )
			{
				if ((timetosample) && (e.Button== MouseButtons.Left)	)
					if (sampling.DoSample(X,Y))
						i++;

				
				if(sampling.SampleByScanning)
				{
					if ( e.Button == MouseButtons.Left  )
						tools[(int)ActiveTool].OnMouseMove((PictureBox)sender, e);
					else if ( e.Button == MouseButtons.None )
						tools[(int)ActiveTool].OnMouseMove((PictureBox)sender, e);
					else
						this.Cursor = Cursors.Default;
				}
				else if (e.Button== MouseButtons.Left)	
					if (sampling.DoSample(X,Y))
						i++;

			}
			if(pt==-1&&!leftbt)
			{
				pt = findPt(e.X, e.Y);
				if (pt!=-1)
				{
					if(!vertselected.Contains(pt))
					{
						vertselected.Add(pt) ;
						System.Windows.Forms.ListViewItem myItem=new System.Windows.Forms.ListViewItem();
						myItem.Text=pt.ToString();
						myItem.SubItems.Add(vert.get_Renamed(pt).X.ToString());
						myItem.SubItems.Add(vert.get_Renamed(pt).Y.ToString());
						myItem.SubItems.Add("0");
						myItem.SubItems.Add("0");
						myItem.SubItems.Add("0");
						myItem.SubItems.Add("0");
						listView1.Items.Insert(listView1.Items.Count,myItem);

					}
				}
				
				pt=-1;
			}
			if (i!=0)
			{
				Refresh();
			}
		}

		
		
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Mainform());
		}

		private void SketchPad_DoubleClick(object sender, System.EventArgs e)
		{
			if (pt != - 1&&leftbt)
			{
			
				vert.remove(pt);
				vertselected.Remove(pt);
				Refresh();
				return ;
			}
			if(pt!=-1&&!leftbt)
			{
				vertselected.Remove(pt);
				Refresh();
				return;

			}
		}

		private void SketchPad_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
				leftbt=false;
			else
				leftbt=true;
		
			if (onsampling && sampling.SampleByScanning)
			{
				if ( e.Button == MouseButtons.Left )
					tools[(int)ActiveTool].OnMouseDown((PictureBox)sender, e);
				if ( e.Button == MouseButtons.Right )
					GraphicsList.UnselectAll();
				//this.SketchPad.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SketchPad_MouseUp);
			}
			else
			{
				pt = findPt(e.X, e.Y);
				state = ((int) e.Button | (int) System.Windows.Forms.Control.ModifierKeys);
			}
		}
		virtual public bool Crust
		{
			set
			{
				crust = value;
				Refresh();
			}
		
		}
		virtual public bool Points
		{
			set
			{
				points = value;
				Refresh();
			}
		
		}
		virtual public bool Voronoi
		{
			set
			{
				voronoi = value;
				Refresh();
			}
		
		}
		virtual public bool Delaun
		{
			set
			{
				delaunay = value;
				Refresh();
			}
		
		}
		virtual public bool Circum
		{
			set
			{
				circum = value;
				Refresh();
			}
		
		}
		virtual public bool Grid
		{
			set
			{
				grid = value;
				Refresh();
			}
		
		}
		virtual public bool MouseLocation
		{
			set
			{
				mouselocation = value;
				Refresh();
			}
		
		}
		internal virtual int findPt(int i, int j)
		{
			for (int k = 0; k < vert.size(); k++)
				if (System.Math.Abs(i - vert.get_Renamed(k).X) <= 6 && System.Math.Abs(j - vert.get_Renamed(k).Y) <= 6)
					return k;
		
			return -1;
		}

		static internal int findPt(ref Vertex vert, int i, int j)
		{
			for (int k = 0; k < vert.size(); k++)
				if (System.Math.Abs(i - vert.get_Renamed(k).X) <= 6 && System.Math.Abs(j - vert.get_Renamed(k).Y) <= 6)
					return k;
		
			return -1;
		}
	
		//UPGRADE_TODO: The equivalent of method 'java.awt.Component.update' is not an override method. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1143"'
		public void  update(System.Drawing.Graphics g)
		{
			if (buffImg == null)
			{
				//UPGRADE_ISSUE: Method 'java.awt.Component.createImage' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javaawtComponentcreateImage_int_int"'
				//buffImg = new System.Drawing.Image(Size.Width, Size.Height);
				//buffImg =new  System.Drawing.Bitmap(Size.Width, Size.Height);
				buffImg =new  System.Drawing.Bitmap(1600,1200 );
				buffGr = System.Drawing.Graphics.FromImage(buffImg);
			}
			
			//UPGRADE_TODO: The equivalent in .NET for method 'java.awt.Graphics.drawImage' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
			//UPGRADE_WARNING: Constructor 'java.awt.Graphics.drawImage' was converted to 'System.Drawing.Graphics.drawImage' which may throw an exception. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1101"'
			g.DrawImage(buffImg, 0, 0);
		}
	
		internal virtual void  drawNN(System.Drawing.Graphics g)
		{
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
			double[][] ad = new double[vert.size()][];
			for (int i = 0; i < vert.size(); i++)
			{
				ad[i] = new double[vert.size()];
			}
			for (int i = 0; i < vert.size(); i++)
			{
				ad[i][i] = 0.0D;
				for (int j = i + 1; j < vert.size(); j++)
				{
					double d = vert.get_Renamed(j).X - vert.get_Renamed(i).X;
					double d2 = vert.get_Renamed(j).Y - vert.get_Renamed(i).Y;
					ad[j][i] = d * d + d2 * d2;
					ad[i][j] = ad[j][i];
				}
			}
		
			vert.newNeighbor();
			for (int k = 0; k < vert.size(); k++)
			{
				double d1 = 99999D;
				int j1 = k;
				for (int l1 = 0; l1 < vert.size(); l1++)
					if (k != l1 && ad[k][l1] < d1)
					{
						j1 = l1;
						d1 = ad[k][l1];
					}
			
				vert.addNeighbor(k, j1, d1);
			}
		
			for (int l = 0; l < vert.size(); l++)
				if (vert.neighbors(l) == 1)
				{
					System.Collections.IEnumerator enumeration = vert.listNeighbors(l);
					enumeration.MoveNext();
					//UPGRADE_TODO: Method 'java.util.Enumeration.nextElement' was converted to 'System.Collections.IEnumerator.Current' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					int k1 = ((System.Int32) enumeration.Current);
					int[] ai = new int[]{vert.get_Renamed(k1).X - vert.get_Renamed(l).X, vert.get_Renamed(k1).Y - vert.get_Renamed(l).Y};
					double d3 = 99999D;
					int k2 = l;
					for (int l2 = 0; l2 < vert.size(); l2++)
						if (l != l2)
						{
							int[] ai1 = new int[]{vert.get_Renamed(l2).X - vert.get_Renamed(l).X, vert.get_Renamed(l2).Y - vert.get_Renamed(l).Y};
							int i3 = ai[0] * ai1[0] + ai[1] * ai1[1];
							if (i3 < 0 && ad[l][l2] < d3)
							{
								k2 = l2;
								d3 = ad[l][l2];
							}
						}
				
					vert.addNeighbor(l, k2, d3);
				}
		
			vert.cleanUp();
			for (int i1 = 0; i1 < vert.size(); i1++)
			{
				System.Collections.IEnumerator enumeration1 = vert.listNeighbors(i1);
				//UPGRADE_TODO: Method 'java.util.Enumeration.nextElement' was converted to 'System.Collections.IEnumerator.Current' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				if(enumeration1.MoveNext())
				{
					int i2 = ((System.Int32) enumeration1.Current);
					g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i1).X, vert.get_Renamed(i1).Y, vert.get_Renamed(i2).X, vert.get_Renamed(i2).Y);
				}
				//UPGRADE_TODO: Method 'java.util.Enumeration.hasMoreElements' was converted to 'System.Collections.IEnumerator.MoveNext' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				if (enumeration1.MoveNext())
				{
					//UPGRADE_TODO: Method 'java.util.Enumeration.nextElement' was converted to 'System.Collections.IEnumerator.Current' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					int j2 = ((System.Int32) enumeration1.Current);
					g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i1).X, vert.get_Renamed(i1).Y, vert.get_Renamed(j2).X, vert.get_Renamed(j2).Y);
				}
			}
		}
	
		internal virtual void  drawDelaunay(System.Drawing.Graphics g)
		{
			Delaunay delaunay1 = new Delaunay(vert.size());
			for (int i = 0; i < vert.size(); i++)
				delaunay1.Insert(vert.get_Renamed(i).X, vert.get_Renamed(i).Y, 0);
		
			if (voronoi)
				delaunay1.DrawVoronoiDiagram(g, System.Drawing.Color.Cyan);
			if (delaunay)
				delaunay1.DrawTriangles(g, System.Drawing.Color.Magenta);
			if (circum)
				delaunay1.DrawCircles(g, System.Drawing.Color.Green);
			if (crust)
			{
				System.Drawing.Point point;
				//UPGRADE_TODO: Method 'java.util.Enumeration.hasMoreElements' was converted to 'System.Collections.IEnumerator.MoveNext' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				for (System.Collections.IEnumerator enumeration = delaunay1.voronoi().Keys.GetEnumerator(); enumeration.MoveNext(); delaunay1.Insert(point.X, point.Y, 1))
				{
					//UPGRADE_TODO: Method 'java.util.Enumeration.nextElement' was converted to 'System.Collections.IEnumerator.Current' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					point = (System.Drawing.Point) enumeration.Current;
				}
			
				SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
				delaunay1.crust(g);
			}
		}
	
		//UPGRADE_TODO: The equivalent of method 'java.awt.Canvas.paint' is not an override method. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1143"'
		public void  paint(System.Drawing.Graphics g)
		{
			if(backgroundpicture)
			{
				g.DrawImage(bgpicture,0,0);
			}
			if (vert.size() > 1)
			{
				if (crust || voronoi || delaunay || circum)
					drawDelaunay(g);
				if (NN)
					drawNN(g);
			//	if(shortdistancebeta)
				if ((interStep)&&(distanceBasedNew))	
					drawDistanceBasedNew(g);
				else if (distanceBasedNew)
					//drawShortDistanceBeta(g);
					drawDistanceBasedNew(g);

				if(shortdistancebetaPlus)
					drawShortDistanceBetaPlus(g);	
				if(shortdistancealpha)
					drawShortDistanceAlpha(g);
				if(shortdistanceDelauney)
					drawShortDistanceDelauneyEx(g);
				if(gathan)
					drawGATHAN(g);			
				if(angledistanceDelauney)
					drawAngleDistanceDelauney(g);
				if(angledistance2Delauney)
					drawAngleDistance2Delauney(g);	
				if(angledistance3Delauney)
					drawAngleDistance3Delauney(g);				
				if(experiment)
					drawExperiment(g);
				 if(distanceBased)
					//drawDistanceBased(g);	
					drawDistanceBasedEx(g);	
			}
			/////////////////////////////////////////
			///////Main Paint Routine  //////////////
			/////////////////////////////////////////
			if (grid)
			{
				drawGrid(g);
			}
			if(mouselocation)
			{
				drawMouseLocation(g);

			}

			if (points)
			{
				SupportClass.GraphicsManager.manager.SetColor(g,pointcolor);
				for (int i = 0; i < vert.size(); i++)
					// point size
					g.FillRectangle(SupportClass.GraphicsManager.manager.GetBrush(g), vert.get_Renamed(i).X - 2, vert.get_Renamed(i).Y - 2, 4, 4);
				//g.FillRectangle(SupportClass.GraphicsManager.manager.GetBrush(g), vert.get_Renamed(i).X - 3, vert.get_Renamed(i).Y - 3, 6, 6);
				//	g.FillRectangle(SupportClass.GraphicsManager.manager.GetBrush(g), vert.get_Renamed(i).X - 1, vert.get_Renamed(i).Y - 1, 1, 1);
					//g.FillEllipse(SupportClass.GraphicsManager.manager.GetBrush(g), vert.get_Renamed(i).X - 3, vert.get_Renamed(i).Y - 3, 6, 6);
			}
			if(onsampling && sampling.SampleByScanning)
			{
				SolidBrush brush = new SolidBrush(Color.FromArgb(255, 255, 255));
				g.FillRectangle(brush, 
					this.ClientRectangle);
				if ( GraphicsList != null )
				{
					GraphicsList.Draw(g);
				}
				brush.Dispose();
			}
			if (statistic)
			{
				DoStatistic();//fill the table and calculation.
				drawStatistic(g);
			}

			if (calAngle)
			{
				DoCalAngle();
				drawStatistic(g);
			}
		}
	
		public virtual void  clear()
		{
			vert.clear();
			vertselected.Clear();
			listView1.Items.Clear();
			//buffImg =new  System.Drawing.Bitmap(1600,1200 );
			buffGr = System.Drawing.Graphics.FromImage(buffImg);
			buffGr.Clear(System.Drawing.Color.White);
			Refresh();
		}
	
		public virtual void  setNN(bool flag)
		{
			
			NN = flag;
			Refresh();
		}
		public virtual void  setDistanceBasedNew(bool flag)
		{
			distanceBasedNew = flag;
		//	shortdistancebeta = flag;
			Refresh();
		}
		public virtual void  setShortShortDistanceAlpha(bool flag)
		{
			
			shortdistancealpha = flag;
			Refresh();
		}
		public virtual void  setShortShortDistanceBetaPlus(bool flag)
		{
			
			shortdistancebetaPlus = flag;
			Refresh();
		}
		
		public virtual void  setShortShortDistanceDelauney(bool flag)
		{
			
			shortdistanceDelauney = flag;
			Refresh();
		}

		/** angle distance delauney */
		public virtual void  setAngleDistanceDelauney(bool flag)
		{			
			angledistanceDelauney = flag;
			Refresh();
		}

		public virtual void  setAngleDistance2Delauney(bool flag)
		{			
			angledistance2Delauney = flag;
			Refresh();
		}
		//An: algorithm 3
		public virtual void  setAngleDistance3Delauney(bool flag)
		{			
			angledistance3Delauney = flag;
			Refresh();
		}
		//An: 
		public virtual void  setExperiment(bool flag)
		{			
			experiment = flag;
			Refresh();
		}
		public virtual void  setDistanceBased(bool flag)
		{			
			distanceBased = flag;
			Refresh();
		}
		public virtual void setInterStep(bool flag)
		{
			interStep = flag;
			Refresh();
		}
		public virtual void  setGathan(bool flag)
		{
			
			gathan = flag;
			Refresh();
		}

		///////////////////////Utensils///////////////////////////////////////////////////////////////////
		///
		
		private void Dey_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			//update();
		}

		private void SketchPad_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			update(e.Graphics);
		}

		private void SketchPad_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			//if (pt != - 1)
			//{
			//	vert.set_Renamed(pt, e.X, e.Y);
			//	Refresh();
			//}
		}

		private void SketchPad_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
			
		}

		private void SketchPad_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			pt=-1;
			//Cursor.Current = MyNormalCursor;
			if (onsampling && sampling.SampleByScanning)
			{
				if ( e.Button == MouseButtons.Left )
					tools[(int)ActiveTool].OnMouseUp((PictureBox) sender, e);
				if ( e.Button == MouseButtons.Right )
					tools[(int)ActiveTool].OnMouseUp((PictureBox) sender, e);
				SetStateOfControls();
			}
			leftbt=true;

			drawAll();
		}

		private void SketchPad_DragLeave(object sender, System.EventArgs e)
		{

		}

		private void SketchPad_GiveFeedback(object sender, System.Windows.Forms.GiveFeedbackEventArgs e)
		{
		
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			
		}

		private void Dey_Resize(object sender, System.EventArgs e)
		{
			Controlpanel.Top=toolBar.Height;
			Controlpanel.Left=0;
			Controlpanel.Width=295;
			Utensils.Height=10;
			Controlpanel.Height=this.ClientSize.Height-Utensils.Height-toolBar.Height-statusBar.Height;
			SketchPad.Top=Controlpanel.Top;
			SketchPad.Left=Controlpanel.Width;
			SketchPad.Width=this.ClientSize.Width -Controlpanel.Width;
			SketchPad.Height=this.ClientSize.Height-Utensils.Height;
			Utensils.Top=Controlpanel.Height+Controlpanel.Top;
			Utensils.Width=this.ClientSize.Width;
			Utensils.Left=0;
			tabControl.Width=Controlpanel.Width-5;
			tabControl.Height=Controlpanel.Height;
			tabControl.Top=0;
			tabControl.Left=0;
			this.Refresh();
		}

		private void checkBoxVoronoi_CheckStateChanged(object sender, System.EventArgs e)
		{
			if (checkBoxVoronoi.Checked == true)
			{
				Voronoi = true;
				return ;
			}
			else
			{
				Voronoi = false;
				return ;
			}
		}

		private void checkBoxDelauney_CheckStateChanged(object sender, System.EventArgs e)
		{
			if (checkBoxDelauney.Checked == true)
			{
				Delaun = true;
				return ;
			}
			else
			{
				Delaun = false;
				return ;
			}
		}

		private void checkBoxCircumCircle_CheckStateChanged(object sender, System.EventArgs e)
		{
			if (checkBoxCircumCircle.Checked == true)
			{
				Circum = true;
				return ;
			}
			Circum = false;
		}

		private void checkBoxPoint_CheckStateChanged(object sender, System.EventArgs e)
		{
			if (checkBoxPoint.Checked == true)
			{
				Points = true;
				return ;
			}
			else
			{
				Points = false;
				return ;
			}
		}

		private void checkBoxCrust_CheckStateChanged(object sender, System.EventArgs e)
		{
			if (checkBoxCrust.Checked == true)
			{
				Crust = true;
				return ;
			}
			else
			{
				Crust = false;
				return ;
			}
		}

		private void checkBoxNNCrust_CheckStateChanged(object sender, System.EventArgs e)
		{
			if (checkBoxNNCrust.Checked == true)
			{
				setNN(true);
				return ;
			}
			else
			{
				setNN(false);
				return ;
			}
		}

		internal virtual void drawGrid(System.Drawing.Graphics g)
		{
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.IndianRed);
			int i,j;
			if (numericUpDownGrid.Value>1000) 
				numericUpDownGrid.Value=10;
			
			for(i=0;i<1600;i=i+(int)numericUpDownGrid.Value)
				for(j=0;j<1200;j=j+(int)numericUpDownGrid.Value)
				{
					g.FillRectangle(SupportClass.GraphicsManager.manager.GetBrush(g),i,j,1,1);
				}
		}

		private void checkBoxGrid_CheckStateChanged(object sender, System.EventArgs e)
		{
			if (checkBoxGrid.Checked == true)
			{
				Grid = true;
				return ;
			}
			else
			{
				Grid = false;
				return ;
			}
		}

		private void numericUpDownGrid_ValueChanged(object sender, System.EventArgs e)
		{
			Refresh();
		}

		private void checkBoxMouseLoc_CheckStateChanged(object sender, System.EventArgs e)
		{
			if (checkBoxMouseLoc.Checked == true)
			{
				MouseLocation = true;
				return ;
			}
			else
			{
				MouseLocation = false;
				return ;
			}
		
		}
		internal virtual void drawMouseLocation(System.Drawing.Graphics g)
		{
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
			int i,j;
			i=SketchPad.Width-100;
			j=0;
			g.FillRectangle(SupportClass.GraphicsManager.manager.GetBrush(g),i,j,100,24);
			string s1;
			s1="X: "+Convert.ToString(X)+" Y: "+Convert.ToString(Y);
			g.DrawString(s1,new Font("Verdana",10),new SolidBrush(Color.GreenYellow),i,2);

		}

		private void buttonClear_Click(object sender, System.EventArgs e)
		{
			clear();
		}

		private void tabControl_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (tabControl.SelectedTab==tabPageSamp)
			{onsampling=true;}
			else
			{onsampling=false;}
		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			timetosample=true;
		}

		private void radioButtonTimeSample_Click(object sender, System.EventArgs e)
		{
			
			timer1.Interval=(int)numericUpDownTimeSample.Value;
			sampling.SampleByTime=radioButtonTimeSample.Checked;
			timer1.Enabled=radioButtonTimeSample.Checked;
			timetosample=false;

		}

		private void numericUpDownTimeSample_Click(object sender, System.EventArgs e)
		{
			
			timer1.Interval=(int)numericUpDownTimeSample.Value;
			sampling.SampleByTime=radioButtonTimeSample.Checked;
			timer1.Enabled=radioButtonTimeSample.Checked;
			timetosample=false;
		}

		private void radioButtonTimeSample_CheckedChanged(object sender, System.EventArgs e)
		{
			
			timer1.Interval=(int)numericUpDownTimeSample.Value;
			sampling.SampleByTime=radioButtonTimeSample.Checked;
			timer1.Enabled=radioButtonTimeSample.Checked;
			timetosample=false;
		}

		private void radioButtonDistSample_Click(object sender, System.EventArgs e)
		{
			sampling.SampleByDistance=radioButtonDistSample.Checked;
			sampling.DistanceSamplingStep=(int)numericUpDownDistanceSample.Value;
			
		}

		private void radioButtonScanningSample_Click(object sender, System.EventArgs e)
		{
			sampling.SampleByScanning=radioButtonScanningSample.Checked;
			sampling.ScanningSamplingStep=(int)numericUpDownScan.Value;
			
		}

		private void buttonScan_Click(object sender, System.EventArgs e)
		{
			sampling.ScanningSamplingStep=(int)numericUpDownScan.Value;
			sampling.DoScan(buffImg);
			
		}

		private void toolBarDraw_ButtonClick(object sender, System.Windows.Forms.ToolBarButtonClickEventArgs e)
		{

			if ( e.Button == tbPointer )
				ActiveTool = DrawToolType.Pointer;
			else if ( e.Button == tbRectangle)
				ActiveTool = DrawToolType.Rectangle;
			else if ( e.Button == tbEllipse)
				ActiveTool = DrawToolType.Ellipse;
			else if ( e.Button == tbLine)
				ActiveTool = DrawToolType.Line;
			else if ( e.Button == tbPolygon)
				ActiveTool = DrawToolType.Polygon;
			else if (e.Button == tbArc )
				ActiveTool = DrawToolType.Arc;
			
			SetStateOfControls();
		
		}
		public void SetStateOfControls()
		{
			// Select active tool
			tbPointer.Pushed = (this.ActiveTool == DrawToolType.Pointer);
			tbRectangle.Pushed = (this.ActiveTool == DrawToolType.Rectangle);
			tbEllipse.Pushed  = (this.ActiveTool == DrawToolType.Ellipse);
			tbLine.Pushed = (this.ActiveTool == DrawToolType.Line);
			tbPolygon.Pushed = (this.ActiveTool == DrawToolType.Polygon);
			tbArc.Pushed =  (this.ActiveTool == DrawToolType.Arc);
			
		}

		private void toolBar_ButtonClick(object sender, System.Windows.Forms.ToolBarButtonClickEventArgs e)
		{
			if (e.Button == toolBarButtonClear)
			{
				clear();
				this.GraphicsList.Clear();
			}
			if(e.Button == toolBarButtonOpen)
			{
				// Create OpenFileDialog
				OpenFileDialog opnDlg = new OpenFileDialog();
				//opnDlg.InitialDirectory="G:/";
				// Set a filter for images
				opnDlg.Filter = "Txt files|*.txt";

				opnDlg.Title = "TxtFileViewer: Open Text File";
				opnDlg.ShowHelp = true;

				if(opnDlg.ShowDialog() == DialogResult.OK)
				{
					// Read current selected file name
					curFileName = opnDlg.FileName;

					// Creat the Image object using
					// Image.FromFile;
					try
					{
						FileStream fs = new FileStream(curFileName, FileMode.Open);
						BinaryFormatter bf = new BinaryFormatter();
						bf.AssemblyFormat=System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
						vert = (Vertex)bf.Deserialize(fs);
						fs.Close();
					}
					catch(Exception exp)
					{
						MessageBox.Show(exp.Message);
					}
				}
				drawAll();
				Refresh();
			}
			else if(e.Button == toolBarButtonSave)
			{
				// Create OpenFileDialog
				SaveFileDialog savDlg = new SaveFileDialog();

				// Set a filter for images
				savDlg.Filter = "Txt files|*.txt";

				savDlg.Title = "TxtFileViewer: Save Text File";
				savDlg.ShowHelp = true;

				if(savDlg.ShowDialog() == DialogResult.OK)
				{
					// Read current selected file name
					curFileName = savDlg.FileName;

					// Creat the Image object using
					// Image.FromFile;
					try
					{
						// DirectoryInfo di = new DirectoryInfo("C:\\Project\\Vision");
						// di.Create();
						FileStream fs = File.Create(curFileName);
			
						if(File.Exists(curFileName))
						{
							Console.WriteLine("File Created!");
						}
						else
						{
							Console.WriteLine("File Not Created!");
						}
			
						FileInfo f = new FileInfo(curFileName);
						BinaryFormatter bf = new BinaryFormatter();
						bf.AssemblyFormat=System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
						bf.Serialize(fs,vert);
			
						fs.Close();	
					}
					catch(Exception exp)
					{
						MessageBox.Show(exp.Message);
					}
				}
				
			}
			else if(e.Button == toolBarButtonSaveAsGTN)
			{
				//Save point file for Gathan program
				// Create OpenFileDialog
				SaveFileDialog savDlg = new SaveFileDialog();

				// Set a filter for images
				savDlg.Filter = "GTN file|*.gtn";

				savDlg.Title = "TxtFileViewer: Save Gathan File";
				savDlg.ShowHelp = true;

				if(savDlg.ShowDialog() == DialogResult.OK)
				{
					// Read current selected file name
					curFileName = savDlg.FileName;

					try 
					{

						if(File.Exists(curFileName))
						{
							Console.WriteLine("File Created!");
						}
						else
						{
							Console.WriteLine("File Not Created!");
						}

						//Pass the filepath and filename to the StreamWriter Constructor
						StreamWriter sw = new StreamWriter(curFileName);
						
						//Write first line, format is <#of point> 2 0 0

						sw.WriteLine(vert.size() + " 2 0 0" );
						//Write a second line of text
						//sw.WriteLine("From the StreamWriter class");
					
						for(int i=0;i<vert.size();i++)
						{
							System.Drawing.Point point;
							point=vert.get_Renamed(i);

							sw.WriteLine(i+ " "+ String.Format("{0:00.000000}",point.X) + " " + String.Format("{0:00.000000}",point.Y));
						}
					

						//Close the file
						sw.Close();

					}
					catch(Exception exp)
					{
						MessageBox.Show(exp.Message);
					}
					finally 
					{
						Console.WriteLine("Executing finally block.");
					}
				}
				
			}
			else if(e.Button == toolBarButtonSaveCurve)
			{
				// Create OpenFileDialog
				SaveFileDialog savDlg = new SaveFileDialog();

				// Set a filter for images
				savDlg.Filter = "Txt files|*.txt";

				savDlg.Title = "TxtFileViewer: Save Text File";
				savDlg.ShowHelp = true;

				if(savDlg.ShowDialog() == DialogResult.OK)
				{
					// Read current selected file name
					curFileName = savDlg.FileName;
					textBoxResult.SaveFile(curFileName,System.Windows.Forms.RichTextBoxStreamType.PlainText);
				}
				
			}
		}

		private void Mainform_Load(object sender, System.EventArgs e)
		{
			ActiveTool = DrawToolType.Pointer;

			// create list of graphic objects
			GraphicsList = new DrawTools.GraphicsList();
			//new created closed graphic objects
			tools = new DrawTools.Tool[6];
			tools[(int)DrawToolType.Pointer] = new DrawTools.ToolPointer();
			tools[(int)DrawToolType.Rectangle] = new DrawTools.ToolRectangle();
			tools[(int)DrawToolType.Ellipse] = new DrawTools.ToolEllipse();
			tools[(int)DrawToolType.Line] = new DrawTools.ToolLine();
			tools[(int)DrawToolType.Polygon] = new DrawTools.ToolPolygon();
			tools[(int)DrawToolType.Arc] = new DrawTools.ToolArc();
			
		}

		private void buttonShake_Click(object sender, System.EventArgs e)
		{
			int i,j,k,m,px,py;
			m=(int)numericUpDownShake.Value;
			Random random = new Random();
			for(i=0;i<vert.size()-1;i++)
			{
				j=random.Next(m);
				k=random.Next(m);
				System.Drawing.Point tpt;
				tpt=vert.get_Renamed(i);
				px=tpt.X+j-m/2;
				py=tpt.Y+k-m/2;
				if(findPt(px,py)==-1)
					vert.set_Renamed(i,px,py );
			}
			Refresh();
		}

		private void checkBoxbeta_CheckedChanged(object sender, System.EventArgs e)
		{
			if (checkBoxbeta.Checked == true)
			{
				if (chkboxInter.Checked==true)
				{
					setInterStep(true); //intermediate step is on
					setDistanceBasedNew(true);}

				else 
				{
					setInterStep(false); //intermediate step is on
					setDistanceBasedNew(true);
				}
				return ;
			}
			else
			{
				setDistanceBasedNew(false);
				return ;
			}
			
		}
		#region drawShortDistanceBeta

//		internal virtual void  drawShortDistanceBeta(System.Drawing.Graphics g)
//		{
//			/** Points list */
//			Vision.PointList OPL = new Vision.PointList();
//
//			// Curve List
//			Vision.CurveList CVL = new Vision.CurveList();
//
//			// ArrayList represent the connections between curves
//			ArrayList curveConnectionArr = new ArrayList();
//
//			int i,j;
//			for(i=0;i<vert.size();i++)
//			{
//				System.Drawing.Point tpt;
//				tpt=vert.get_Renamed(i);
//				OPL.Add(new Vision.Point2D(tpt.X,tpt.Y));
//			}
//			//Algorithim Beta
//
//			// Clear the curvelist
//			CVL.Clear();
//
//			if(curveConnectionArr.Count > 0)
//				curveConnectionArr.Clear();
//
//			double sigmaTimes = Convert.ToDouble(txtSigma.Text);
//			double deltaTimes = Convert.ToDouble(txtDelta.Text);
//
//			// treat each point as a curve
//			for(i=0; i<OPL.Count; i++)
//			{
//				Vision.Point2D ptone = (Vision.Point2D)OPL[i];
//				Vision.Curve CL = new Vision.Curve(sigmaTimes, deltaTimes);
//				CL.Add(ptone);
//				CVL.Add(CL);				
//			}
//
//			int leftCurveNumber = Convert.ToInt16(txtBoxLeftCurveNumber.Text);
//			
//			// CurveReconstruction
//			CVL.CurveReconstructions(leftCurveNumber);
//
//			if(checkBoxBeta2.Checked)
//			{
//				
//				Vision.Optimismfilter of1 = new Vision.Optimismfilter(); 
//				for (i = 0; i < CVL.Count; i++) 
//				{ 
//					Vision.Curve cl=new Vision.Curve(1,1); 
//					cl=(Vision.Curve)CVL[i]; 
//					of1.CurveFilter(ref cl); 
//					CVL[i] = cl; 
//				}
//			}
//
//
//			// If there is any curves should be closed left, close it
//			//CVL.CloseCurves();
//			
//			// Deal with the situation of hat - more than three points
//			//CVL.SpecialTreatOne();
//			// Deal with the situation of sharp angle-single point
//			//CVL.SpecialTreatTwo();
//
//			// Connect a open curve to another very close curve
//			//CVL.SpecialTreatThree();
//
//			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Green);
//			// Draw curves
//			for(i=0; i<CVL.Count; i++)
//			{
//				Vision.Curve CL = (Vision.Curve)CVL[i];
//			
//				for(j=0; j<(CL.Count-1); j++)
//				{
//					Vision.Point2D ptone = (Vision.Point2D)CL[j];
//					Vision.Point2D pttwo = (Vision.Point2D)CL[j+1];
//					g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ptone.X, ptone.Y, pttwo.X, pttwo.Y);
//				}					
//
//				if(CL.closed == true)
//				{
//					Vision.Point2D ptone = (Vision.Point2D)CL[0];
//					Vision.Point2D pttwo = (Vision.Point2D)CL[CL.Count-1];
//					g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ptone.X, ptone.Y, pttwo.X, pttwo.Y);
//				}
//					
//				
//			} 
//		}
		#endregion drawShortDistanceBeta


		internal virtual void  drawShortDistanceBetaPlus(System.Drawing.Graphics g)
		{
			// Points list
			Vision.PointList OPL = new Vision.PointList();

			// Curve List
			Vision.CurveList CVL = new Vision.CurveList();

			// ArrayList represent the connections between curves
			ArrayList curveConnectionArr = new ArrayList();

			int i,j;
			for(i=0;i<vert.size();i++)
			{
				System.Drawing.Point tpt;
				tpt=vert.get_Renamed(i);
				if(checkBox3D.Checked)
				//3D Points . Z coordinate inputs here.
				OPL.Add(new Vision.Point2D(tpt.X,tpt.Y,i*60));
				else
				OPL.Add(new Vision.Point2D(tpt.X,tpt.Y));
			}
			//Algorithim Beta

			// Clear the curvelist
			CVL.Clear();

			if(curveConnectionArr.Count > 0)
				curveConnectionArr.Clear();
			// treat each point as a curve
			for(i=0; i<OPL.Count; i++)
			{
				Vision.Point2D ptone = (Vision.Point2D)OPL[i];
				Vision.Curve CL = new Vision.Curve(1, 1);
				CL.Add(ptone);
				CVL.Add(CL);				
			}
		
			// CurveReconstruction
			CVL.SetConnectCount((int)numericUpDown3.Value);
			CVL.CurveReconstructionsEx();

			if(checkBoxBeta2.Checked)
			{
				
				Vision.Optimismfilter of1 = new Vision.Optimismfilter(); 
				for (i = 0; i < CVL.Count; i++) 
				{ 
					Vision.Curve cl=new Vision.Curve(1,1); 
					cl=(Vision.Curve)CVL[i]; 
					of1.CurveFilter(ref cl); 
					CVL[i] = cl; 
				}
				//CVL.CurveReconstructionsEx();
			}


			// If there is any curves should be closed left, close it
			if(numericUpDown3.Value==0)
			CVL.CloseCurves();
			
			// Deal with the situation of hat - more than three points
			//CVL.SpecialTreatOne();
			// Deal with the situation of sharp angle-single point
			//CVL.SpecialTreatTwo();

			// Connect a open curve to another very close curve
			//CVL.SpecialTreatThree();

			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Green);
			curvestr="";
			curvestr=curvestr+"Curves Count="+CVL.Count.ToString()+'\x0D'+'\x0A';
			// Draw curves
			for(i=0; i<CVL.Count; i++)
			{
				Vision.Curve CL = (Vision.Curve)CVL[i];
				curvestr=curvestr+'['+i.ToString()+']'+'\x0D'+'\x0A';
				
				for(j=0; j<(CL.Count-1); j++)
				{
					Vision.Point2D ptone = (Vision.Point2D)CL[j];
					Vision.Point2D pttwo = (Vision.Point2D)CL[j+1];
					g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ptone.X, ptone.Y, pttwo.X, pttwo.Y);
					curvestr=curvestr+((Vision.Point2D)CL[j]).X.ToString()
						+','+((Vision.Point2D)CL[j]).Y.ToString()
						+','+((Vision.Point2D)CL[j]).Z.ToString();
					curvestr=curvestr+'\x0D'+'\x0A';
				}					
				j=CL.Count-1;
				curvestr=curvestr+((Vision.Point2D)CL[j]).X.ToString()
					+','+((Vision.Point2D)CL[j]).Y.ToString()
					+','+((Vision.Point2D)CL[j]).Z.ToString();
				curvestr=curvestr+'\x0D'+'\x0A';

				if(CL.closed == true)
				{
					Vision.Point2D ptone = (Vision.Point2D)CL[0];
					Vision.Point2D pttwo = (Vision.Point2D)CL[CL.Count-1];
					g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ptone.X, ptone.Y, pttwo.X, pttwo.Y);
					j=0;
					curvestr=curvestr+((Vision.Point2D)CL[j]).X.ToString()
						+','+((Vision.Point2D)CL[j]).Y.ToString()
						+','+((Vision.Point2D)CL[j]).Z.ToString();
					curvestr=curvestr+'\x0D'+'\x0A';
				}
							
				
			} 
			textBoxResult.Text=curvestr;
			
		}


		/**
		 * ShortDistance (alpha) algorithm.
		 */ 
		internal virtual void  drawShortDistanceAlpha(System.Drawing.Graphics g)
		{
			// Points list
			Vision.StPointList spl=new Vision.StPointList();

			// Curve List contains curves to be connected
			Vision.StCurveList scl=new Vision.StCurveList();

			// ArrayList represent the connections between curves
			ArrayList curveConnectionArr = new ArrayList();

			int i,j;
			for(i=0;i<vert.size();i++)
			{
				System.Drawing.Point tpt;
				tpt=vert.get_Renamed(i);
				
				Vision.Point2D ptone = new Vision.Point2D(tpt.X,tpt.Y);
				ptone.Valid=0;
				spl.AddPoint(ptone);
			}

			/// CurveReconstruction
			scl.CurveReconstruction(spl,checkBoxBeta2.Checked);
			
			/// Curve List contains curves to be drawed
			Vision.CurveList CVL = new Vision.CurveList();
			CVL.Clear();
			
			//  
			for(i=0; i<scl.Count; i++)
			{
				Vision.Curve cl=new Vision.Curve(1,1);
				cl.Clear();
				for(j=0;j<((Vision.StCurve)scl[i]).Count;j++)
					cl.Add((Vision.Point2D)((Vision.StCurve)scl[i])[j]);
				CVL.Add(cl);
			}
			
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Purple);
			// Draw curves
			for(i=0; i<CVL.Count; i++)
			{
				Vision.Curve CL = (Vision.Curve)CVL[i];
			
				for(j=0; j<(CL.Count-1); j++)
				{
					Vision.Point2D ptone = (Vision.Point2D)CL[j];
					Vision.Point2D pttwo = (Vision.Point2D)CL[j+1];
					g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ptone.X, ptone.Y, pttwo.X, pttwo.Y);
				}					

				if(CL.closed == true)
				{
					Vision.Point2D ptone = (Vision.Point2D)CL[0];
					Vision.Point2D pttwo = (Vision.Point2D)CL[CL.Count-1];
					g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ptone.X, ptone.Y, pttwo.X, pttwo.Y);
				}
					
				
			} 
		}

		private void checkBoxBeta2_CheckedChanged(object sender, System.EventArgs e)
		{
			Refresh();
		}

		/**
		 * Connect point using short distance Algorithm.
		 * @see drawShortDistanceAlpha(g)
		 */
		private void checkBoxAlpha_CheckedChanged(object sender, System.EventArgs e)
		{
			if (checkBoxAlpha.Checked == true)
			{
				setShortShortDistanceAlpha(true);
				return ;
			}
			else
			{
				setShortShortDistanceAlpha(false);
				return ;
			}
		}

		private void checkBoxAngl_CheckedChanged(object sender, System.EventArgs e)
		{
			if (checkBoxAngl.Checked==true)
			{ 
				statistic=true;
			}
			else
			{
				statistic=false;
			}
		}
		internal virtual void drawStatistic(System.Drawing.Graphics g)
		{
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.LightBlue);
			int mx,my,i;
			for(i=0;i<listView1.Items.Count;i++)
			{
				mx= vert.get_Renamed((int)vertselected[i]).X;
				my= vert.get_Renamed((int)vertselected[i]).Y;
			// point size	
				g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g),mx-3,my-3,6,6);
												
				if (i>0)
					g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g),mx,my,vert.get_Renamed((int)vertselected[i-1]).X,vert.get_Renamed((int)vertselected[i-1]).Y);
			}
			
			
		}

	
		public void InitializeListView()
		{
			ColumnHeader header1 = this.listView1.Columns.Add( "Point", 30,HorizontalAlignment.Center);
			ColumnHeader header2 = this.listView1.Columns.Add( "X ", 40, HorizontalAlignment.Center);
			ColumnHeader header3 = this.listView1.Columns.Add( "Y ", 40, HorizontalAlignment.Center);
			ColumnHeader header4 = this.listView1.Columns.Add( "Segment Length ", 60, HorizontalAlignment.Center);
			ColumnHeader header5 = this.listView1.Columns.Add( "Angle ", 60, HorizontalAlignment.Center);
			ColumnHeader header6 = this.listView1.Columns.Add( "Segment Variance", 60, HorizontalAlignment.Center);
			ColumnHeader header7 = this.listView1.Columns.Add( "Angle Variance", 60, HorizontalAlignment.Center);

			//An: OUTLIER added
			ColumnHeader header8 = this.listView1.Columns.Add( "Outliers", 60, HorizontalAlignment.Center);


		}

	
		public void DoStatistic()
		{
			int i;
			int vk=0, am_X=0, bm_Y=0, am_Y=0, bm_X=0;
			Vision.Optimismfilter op=new Vision.Optimismfilter();
			Vision.Point2D pt1=new Vision.Point2D(0,0);
			Vision.Point2D pt2=new Vision.Point2D(0,0);
			Vision.Point2D pt3=new Vision.Point2D(0,0);
			double tempangle1 = 0;
			ArrayList myangle = new ArrayList();
			double seg;
			ArrayList myseg = new ArrayList();	
			double mysegv,myangv;
			listView1.Items.Clear();
			for(i=0;i<vertselected.Count;i++)
			{
				System.Windows.Forms.ListViewItem myItem=new System.Windows.Forms.ListViewItem();
				myItem.Text=vertselected[i].ToString();
				myItem.SubItems.Add(vert.get_Renamed((int)vertselected[i]).X.ToString());
				myItem.SubItems.Add(vert.get_Renamed((int)vertselected[i]).Y.ToString());
				myItem.SubItems.Add("0");
				myItem.SubItems.Add("0");
				myItem.SubItems.Add("0");
				myItem.SubItems.Add("0");				
				listView1.Items.Insert(listView1.Items.Count,myItem);
			}
			if (vertselected.Count>=2)
			{
				pt1.X=vert.get_Renamed((int)vertselected[0]).X;
				pt1.Y=vert.get_Renamed((int)vertselected[0]).Y;
				pt2.X=vert.get_Renamed((int)vertselected[1]).X;
				pt2.Y=vert.get_Renamed((int)vertselected[1]).Y;
				seg=pt2.Distance(pt1);
				listView1.Items[1].SubItems[3].Text= seg.ToString();
				tempangle1=op.CalAngle(pt1,pt2);
				listView1.Items[1].SubItems[4].Text= tempangle1.ToString();
			}
		
			if (vertselected.Count>=3)
			{
			
				for(i=2;i<vertselected.Count;i++)
				{
					pt1.X=vert.get_Renamed((int)vertselected[i-2]).X;
					pt1.Y=vert.get_Renamed((int)vertselected[i-2]).Y;
					pt2.X=vert.get_Renamed((int)vertselected[i-1]).X;
					pt2.Y=vert.get_Renamed((int)vertselected[i-1]).Y;
					pt3.X=vert.get_Renamed((int)vertselected[i]).X;
					pt3.Y=vert.get_Renamed((int)vertselected[i]).Y;
					seg=pt2.Distance(pt3);
					listView1.Items[i].SubItems[3].Text= seg.ToString();
					myseg.Add(seg);
					tempangle1=op.CalAngle(pt1,pt2,pt3);
					listView1.Items[i].SubItems[4].Text= tempangle1.ToString();
					myangle.Add(tempangle1);
					mysegv=op.Variance(myseg);
					listView1.Items[i].SubItems[5].Text= mysegv.ToString();
					myangv=op.Variance(myangle);
					listView1.Items[i].SubItems[6].Text= myangv.ToString();

				}
			}
			if (vertselected.Count==3)
			{
				i=2;
				pt1.X=vert.get_Renamed((int)vertselected[i-2]).X;
				pt1.Y=vert.get_Renamed((int)vertselected[i-2]).Y;
				pt2.X=vert.get_Renamed((int)vertselected[i-1]).X;
				pt2.Y=vert.get_Renamed((int)vertselected[i-1]).Y;
				pt3.X=vert.get_Renamed((int)vertselected[i]).X;
				pt3.Y=vert.get_Renamed((int)vertselected[i]).Y;
				label10.Text=op.PointToLineDistance(pt3,pt1,pt2).ToString();
			}
			if (vertselected.Count>=4)
			{
				Vision.StCurve sc=new Vision.StCurve();
				for(i=1;i<vertselected.Count-1;i++)
				{
					sc.AddPoint(vert.get_Renamed((int)vertselected[i]).X,vert.get_Renamed((int)vertselected[i]).Y);
				}
				double l;
				Vision.Point2D pt2d1,pt2d2,pt2d3,pt2d4;
				i=0;
				pt2d1= new Vision.Point2D(vert.get_Renamed((int)vertselected[i]).X,vert.get_Renamed((int)vertselected[i]).Y);
				i=1;
				pt2d2= new Vision.Point2D(vert.get_Renamed((int)vertselected[i]).X,vert.get_Renamed((int)vertselected[i]).Y);
				i=vertselected.Count-2;
				pt2d3= new Vision.Point2D(vert.get_Renamed((int)vertselected[i]).X,vert.get_Renamed((int)vertselected[i]).Y);
				i=vertselected.Count-1;
				pt2d4= new Vision.Point2D(vert.get_Renamed((int)vertselected[i]).X,vert.get_Renamed((int)vertselected[i]).Y);
				l=pt2d1.Distance(pt2d2);
				label12.Text=sc.ExternalLengthEx(0,l).ToString();
				l=pt2d3.Distance(pt2d4);
				label13.Text=sc.ExternalLengthEx(-1,l).ToString();
				
				System.Collections.ArrayList rt;
				rt=sc.ReadSegments(0,sc.Count-1);
				label16.Text=op.Average(rt).ToString();
				label23.Text=((sc.ExternalLengthEx(-1,l))/(op.Average(rt))).ToString();
				if(op.Selfintersection(sc))
					label18.Text="true";
				else
					label18.Text="false";		
			}
			if (vertselected.Count==3)
			{
				i=2;
				pt1.X=vert.get_Renamed((int)vertselected[i-2]).X;
				pt1.Y=vert.get_Renamed((int)vertselected[i-2]).Y;
				pt2.X=vert.get_Renamed((int)vertselected[i-1]).X;
				pt2.Y=vert.get_Renamed((int)vertselected[i-1]).Y;
				pt3.X=vert.get_Renamed((int)vertselected[i]).X;
				pt3.Y=vert.get_Renamed((int)vertselected[i]).Y;
				am_X=pt2.X-pt1.X;
				bm_Y=pt3.Y-pt2.Y;
				am_Y=pt2.Y-pt1.Y;
				bm_X=pt3.X-pt2.X;
				vk = am_X * bm_Y - am_Y * bm_X;
				if(vk>0)
					label25.Text="true";
				else
					label25.Text="false";		
			}
		
		}

//An:Angle checkbox
		public void DoCalAngle()
		{
			int i;
			int vk=0, am_X=0, bm_Y=0, am_Y=0, bm_X=0;
			Vision.Optimismfilter op=new Vision.Optimismfilter();
			Vision.Point2D pt1=new Vision.Point2D(0,0);
			Vision.Point2D pt2=new Vision.Point2D(0,0);
			Vision.Point2D pt3=new Vision.Point2D(0,0);
			double tempangle1 = 0;
			ArrayList myangle = new ArrayList();
			double seg;
			ArrayList myseg = new ArrayList();	
			double mysegv,myangv;
			listView1.Items.Clear();
			for(i=0;i<vertselected.Count;i++)
			{
				System.Windows.Forms.ListViewItem myItem=new System.Windows.Forms.ListViewItem();
				myItem.Text=vertselected[i].ToString();
				myItem.SubItems.Add(vert.get_Renamed((int)vertselected[i]).X.ToString());
				myItem.SubItems.Add(vert.get_Renamed((int)vertselected[i]).Y.ToString());
				myItem.SubItems.Add("0");
				myItem.SubItems.Add("0");
				myItem.SubItems.Add("0");
				myItem.SubItems.Add("0");
				myItem.SubItems.Add("");
				listView1.Items.Insert(listView1.Items.Count,myItem);
			}
			if (vertselected.Count>=2)
			{
				
				pt1.X=vert.get_Renamed((int)vertselected[0]).X;
				pt1.Y=vert.get_Renamed((int)vertselected[0]).Y;
				pt2.X=vert.get_Renamed((int)vertselected[1]).X;
				pt2.Y=vert.get_Renamed((int)vertselected[1]).Y;
				seg=pt2.Distance(pt1);
				listView1.Items[1].SubItems[3].Text= seg.ToString();
		//		tempangle1=op.CalAngle(pt1,pt2);
		//		tempangle1 = Math.Atan2(pt2.Y-pt1.Y, pt2.X-pt1.X)*180/Math.PI;

				listView1.Items[1].SubItems[4].Text= tempangle1.ToString();
			}
		
			//An(temporary)
		//	ArrayList arrayPoint = new ArrayList();
			if (vertselected.Count>=3)
			{
			
				for(i=2;i<vertselected.Count;i++)
				{
					
					pt1.X=vert.get_Renamed((int)vertselected[i-2]).X;
					pt1.Y=vert.get_Renamed((int)vertselected[i-2]).Y;
					pt2.X=vert.get_Renamed((int)vertselected[i-1]).X;
					pt2.Y=vert.get_Renamed((int)vertselected[i-1]).Y;
					pt3.X=vert.get_Renamed((int)vertselected[i]).X;
					pt3.Y=vert.get_Renamed((int)vertselected[i]).Y;
					seg=pt2.Distance(pt3);
					listView1.Items[i].SubItems[3].Text= seg.ToString();
					myseg.Add(seg);
			//		tempangle1=op.CalAngle(pt1,pt2,pt3);

					//An
                    tempangle1=CalAngle(pt1, pt2, pt3);
					listView1.Items[i].SubItems[4].Text= tempangle1.ToString();
					
					myangle.Add(tempangle1);
				//	arrayPoint.Add(pt2);

					mysegv=op.Variance(myseg);
					listView1.Items[i].SubItems[5].Text= mysegv.ToString();
					myangv=op.Variance(myangle);
					listView1.Items[i].SubItems[6].Text= myangv.ToString();															
				//	if ((tempangle1 <135)||(tempangle1>225))
				//		listView1.Items[i].SubItems[7].Text= "yes";	
				}

				//identify outliers: LMS method
//				if (myangle.Count >=3)
//				{
//					ArrayList outliers = outlier(myangle);
//					int k=0;
//				
//					foreach (double s in outliers)
//					{
//						if (s == (double)myangle[k])						
//						{
//							//((Vision.Point2D)arrayPoint[k]).outlier = true;						
//							listView1.Items[i].SubItems[7].Text= "yes";
//						}
//						k++;
//					}
//				}

				//An: identify oulier: customized method
				//remove range that has starting angle value <135 or > 225
				for (int k=0; k<myangle.Count; k++)
				{
					
					if (((double)myangle[k] <135)||((double)myangle[k]>225))
					{
						myangle.RemoveRange(k, myangle.Count-k);
						listView1.Items[k+2].SubItems[7].Text+="yes";
						break;
					}					
				}
				
				//identify outlier: based on mean and median
//				if (myangle.Count>=3)
//				{
//					ArrayList arraySorted = new ArrayList();
//					//sort the array
//					arraySorted = MergeSort((ArrayList)myangle.Clone());
//					
//					//find the shortest half
//					double temp = 0;
//					double shortest = Double.MaxValue;
//					int h = arraySorted.Count/2;
//					double median = 0;
//					double ave = 0;
//
//					for (int k=0; k<Math.Ceiling(arraySorted.Count/2); k++)						
//					{
//						temp = (double)arraySorted[k+h] - (double)arraySorted[k];
//						if (temp<shortest)
//						{
//							shortest = temp;															
//						}
//					}
//
//					//get the median value
//					if (arraySorted.Count % 2 ==0)
//					{
//						median = (double)arraySorted[arraySorted.Count/2]+(double)arraySorted[arraySorted.Count/2-1];
//						median = median/2;
//					}
//					else
//						median = (double)arraySorted[arraySorted.Count/2];
//
//					//get the mean value
//					foreach (double s in arraySorted)
//					{
//						ave = ave+s;
//					}
//					ave = ave/arraySorted.Count;
//
//					//compute difference between mean and average value
//					temp = ave - median;
//					if (Math.Abs(temp)>shortest/2)  //outlier
//					{
//						int index =0;
//						if (temp <0) //the smallest point is outlier
//						{
//							index = myangle.IndexOf((double)arraySorted[0], 0, myangle.Count);														
//						}
//						else //the biggest point is outlier
//						{
//							index = myangle.IndexOf((double)arraySorted[arraySorted.Count-1], 0, myangle.Count);														
//						}
//						//remove range starting from index value
//						listView1.Items[index+2].SubItems[7].Text+="r-del";
//									
//						
//						myangle.RemoveRange(index, myangle.Count-index);
//					}			
//					
//
//				}
						
				//				if (myangle.Count>=3)
//				{
//					ArrayList arraySorted = new ArrayList();
//					//sort the array
//					arraySorted = MergeSort((ArrayList)myangle.Clone());
//					
//					//find the shortest half
//					double temp = 0;
//					double shortest = Double.MaxValue;
//					int h = arraySorted.Count/2;
//					double median = 0;
//					double ave = 0;
//
//					for (int k=0; k<Math.Ceiling(arraySorted.Count/2); k++)						
//					{
//						temp = (double)arraySorted[k+h] - (double)arraySorted[k];
//						if (temp<shortest)
//						{
//							shortest = temp;															
//						}
//					}
//
//					//get the median value
//					if (arraySorted.Count % 2 ==0)
//					{
//						median = (double)arraySorted[arraySorted.Count/2]+(double)arraySorted[arraySorted.Count/2-1];
//						median = median/2;
//					}
//					else
//						median = (double)arraySorted[arraySorted.Count/2];
//
//					//get the mean value
//					foreach (double s in arraySorted)
//					{
//						ave = ave+s;
//					}
//					ave = ave/arraySorted.Count;
//
//					//compute difference between median and average value
//					temp = ave - median;
//					if (Math.Abs(temp)>shortest/2)  //outlier
//					{
//						int index =0;
//						if (temp <0) //the smallest point is outlier
//						{
//							index = myangle.IndexOf((double)arraySorted[0], 0, myangle.Count);														
//						}
//						else //the biggest point is outlier
//						{
//							index = myangle.IndexOf((double)arraySorted[arraySorted.Count-1], 0, myangle.Count);														
//						}
//						//remove range starting from index value
//						listView1.Items[index+2].SubItems[7].Text+="r-del";
//									
//						
//						myangle.RemoveRange(index, myangle.Count-index);
//					}			
//					
//
//				}

				//identify outlier: based on difference between shortest half, median and endpoint
				if (myangle.Count>=3)
				{				
					DetectOutlier(myangle);
					for (int k =0; k<myangle.Count; k++)
					{
						listView1.Items[k+2].SubItems[7].Text+="none";
					}
				}
			}
			if (vertselected.Count==3)
			{
				i=2;
				pt1.X=vert.get_Renamed((int)vertselected[i-2]).X;
				pt1.Y=vert.get_Renamed((int)vertselected[i-2]).Y;
				pt2.X=vert.get_Renamed((int)vertselected[i-1]).X;
				pt2.Y=vert.get_Renamed((int)vertselected[i-1]).Y;
				pt3.X=vert.get_Renamed((int)vertselected[i]).X;
				pt3.Y=vert.get_Renamed((int)vertselected[i]).Y;
				label10.Text=op.PointToLineDistance(pt3,pt1,pt2).ToString();
			}
			if (vertselected.Count>=4)
			{
				Vision.StCurve sc=new Vision.StCurve();
				for(i=1;i<vertselected.Count-1;i++)
				{
					sc.AddPoint(vert.get_Renamed((int)vertselected[i]).X,vert.get_Renamed((int)vertselected[i]).Y);
				}
				double l;
				Vision.Point2D pt2d1,pt2d2,pt2d3,pt2d4;
				i=0;
				pt2d1= new Vision.Point2D(vert.get_Renamed((int)vertselected[i]).X,vert.get_Renamed((int)vertselected[i]).Y);
				i=1;
				pt2d2= new Vision.Point2D(vert.get_Renamed((int)vertselected[i]).X,vert.get_Renamed((int)vertselected[i]).Y);
				i=vertselected.Count-2;
				pt2d3= new Vision.Point2D(vert.get_Renamed((int)vertselected[i]).X,vert.get_Renamed((int)vertselected[i]).Y);
				i=vertselected.Count-1;
				pt2d4= new Vision.Point2D(vert.get_Renamed((int)vertselected[i]).X,vert.get_Renamed((int)vertselected[i]).Y);
				l=pt2d1.Distance(pt2d2);
				label12.Text=sc.ExternalLengthEx(0,l).ToString();
				l=pt2d3.Distance(pt2d4);
				label13.Text=sc.ExternalLengthEx(-1,l).ToString();
				
				System.Collections.ArrayList rt;
				rt=sc.ReadSegments(0,sc.Count-1);
				label16.Text=op.Average(rt).ToString();
				label23.Text=((sc.ExternalLengthEx(-1,l))/(op.Average(rt))).ToString();
				if(op.Selfintersection(sc))
					label18.Text="true";
				else
					label18.Text="false";		
			}
			if (vertselected.Count==3)
			{
				i=2;
				pt1.X=vert.get_Renamed((int)vertselected[i-2]).X;
				pt1.Y=vert.get_Renamed((int)vertselected[i-2]).Y;
				pt2.X=vert.get_Renamed((int)vertselected[i-1]).X;
				pt2.Y=vert.get_Renamed((int)vertselected[i-1]).Y;
				pt3.X=vert.get_Renamed((int)vertselected[i]).X;
				pt3.Y=vert.get_Renamed((int)vertselected[i]).Y;
				am_X=pt2.X-pt1.X;
				bm_Y=pt3.Y-pt2.Y;
				am_Y=pt2.Y-pt1.Y;
				bm_X=pt3.X-pt2.X;
				vk = am_X * bm_Y - am_Y * bm_X;
				if(vk>0)
					label25.Text="true";
				else
					label25.Text="false";		
			}
		
		}
//		//An: calAngle
		private static double CalAngle(Vision.Point2D pt1, Vision.Point2D pt2, Vision.Point2D pt3)
		{
			double angle1=0, angle2=0;
			double angle =0;
			angle1 = Math.Atan2(pt2.Y-pt1.Y, pt2.X-pt1.X);
			angle2 = Math.Atan2(pt3.Y-pt2.Y, pt3.X-pt2.X);

			angle = angle2 - angle1;

			if (angle1*angle2 >=0)
			{
				angle = 180 - angle*180/Math.PI; //angle in degree
			}
			else
			{
				if (angle<-Math.PI)
					angle = Math.Abs(angle)*180/Math.PI - 180;		
				else if (angle<Math.PI)					
					angle = 180 - angle*180/Math.PI;			
				else if (angle>Math.PI)
					angle = 540 - angle*180/Math.PI;									
			}
		
			return angle;
		}

		#region identify outlier using customized method		
		private static ArrayList DetectOutlier(ArrayList myangle)
		{					
			
//			//sorting the array
//			ArrayList arraySorted = new ArrayList();
//			arraySorted = MergeSort((ArrayList)myangle.Clone());
//	
//			//find the shortest half
//			double temp = 0;
//			double shortest = Double.MaxValue;
//			int h = arraySorted.Count/2;
//			double median = 0;
//			double intervalpoint1=0, intervalpoint2=0;
//			int index =0;
//
//			for (int k=0; k<Math.Ceiling(arraySorted.Count/2); k++)						
//			{
//				temp = (double)arraySorted[k+h] - (double)arraySorted[k];
//				if (temp<shortest)
//				{
//					shortest = temp;															
//				}
//			}
//
//			//get the median value
//			if (arraySorted.Count % 2 ==0)
//			{
//				median = (double)arraySorted[arraySorted.Count/2]+(double)arraySorted[arraySorted.Count/2-1];
//				median = median/2;
//			}
//			else
//				median = (double)arraySorted[arraySorted.Count/2];
//
//			//compute difference between median and 2 endpoints
//			intervalpoint1 = median - (double) arraySorted[0];
//			intervalpoint2 = (double)arraySorted[arraySorted.Count-1] - median;
//
//			if ((intervalpoint1>intervalpoint2)&&(intervalpoint1>45))  //outlier
//			{			
//				index = myangle.IndexOf((double)arraySorted[0], 0, myangle.Count);																									
//			}			
//			else if ((intervalpoint2>intervalpoint1)&&(intervalpoint2>45))
//			{
//				index = myangle.IndexOf((double)arraySorted[arraySorted.Count-1], 0, myangle.Count);																																		
//			}
//			else
//			{
//				return myangle;
//			}
//			myangle.RemoveRange(index, myangle.Count-index);	
//
//		//	if (myangle.Count>=3)
//		//	{
//		//		DetectOutlier(myangle);
//		//	}

			
			for (int k=0; k<myangle.Count; k++)						
			{
				
				if (((double)myangle[k]<135)&&((double)myangle[k]>225))
				{
					myangle.RemoveRange(k,myangle.Count-k);
				}
			}
			return myangle;
			//remove range starting from index value
			//listView1.Items[index+2].SubItems[7].Text+="r-del";
		}
		#endregion identify outlier using customized method
	//	#region identify outlier using Least median of squares method
		//An: estimate LMS
		//@param arrayAngle is unsorted array
		/*
		private double LMS(ArrayList arrayAngle)
		{
			int h = arrayAngle.Count/2;  //half of the observation data 
			//double[] arrayLMS = new double[ (int) Math.Ceiling(arrayAngle.Count/2)]; //array contains LMS in case there are more than 1 shortest half
			
			ArrayList arrayLMS = new ArrayList();			//array contains LMS in case there are more than 1 shortest half
			double shortest = Double.MaxValue;
			double temp = 0;
			//int count = 0; //numbers of shortest half
			double LMSvalue = 0;
			//rearrange the points in increasing order	
			//mergesort
			arrayAngle = MergeSort(arrayAngle);

			//find the shortest half
			for (int i=0; i<Math.Ceiling(arrayAngle.Count/2); i++)
		//	for (int i=0; i<arrayAngle.Count/2; i++)
			{
				temp = (double)arrayAngle[i+h] - (double)arrayAngle[i];
				if (temp<shortest)
				{
					shortest = temp;															
				}
			}

			//compute LMS
			for (int i=0; i<Math.Ceiling(arrayAngle.Count/2); i++)
		//		for (int i=0; i<arrayAngle.Count/2; i++)
			{

				temp = (double)arrayAngle[i+h] - (double)arrayAngle[i];
				if (temp==shortest)
				{
					arrayLMS.Add(temp/2 + (double)arrayAngle[i]); //midpoint of the shortest half															
				}
			}

			//LMS = average of midpoint if more than one shortest half present
			foreach (double s in arrayLMS)
			{
				LMSvalue = LMSvalue + s;	
			}
			return LMSvalue/arrayLMS.Count;

		}
*/
		//An: mergeSort pseudocode (from wiki)
		//		function mergesort(m)
		//      var list left, right, result
		//		if length(m) \A1\DC 1
		//			return m
		//		else
		//			middle = length(m) / 2
		//			for each x in m up to middle
		//				add x to left
		//			for each x in m after middle
		//				add x to right
		//			left = mergesort(left)
		//			right = mergesort(right)
		//			result = merge(left, right)
		//			return result

		//@param sortingList is the array needs to be sorted
		private ArrayList MergeSort(ArrayList sortingList)
		{
			ArrayList left =  new ArrayList();
			ArrayList right  = new ArrayList(); 			
			ArrayList result =   new ArrayList();
			int middle;
			if (sortingList.Count <=1)
			{
				return sortingList;
			}
			else{
				middle= sortingList.Count/2;
				for (int j=0; j<middle; j++){
					left.Add(sortingList[j]);
				}
				for (int j=middle; j<sortingList.Count; j++)
				{
					right.Add(sortingList[j]);
				}
				left = MergeSort(left);
				right = MergeSort(right);
				result = Merge(left, right);
				return result;
			}
		}
		//An: merge pseudocode (from wiki)
		//		function merge(left,right)
		//			var list result
		//		while length(left) > 0 and length(right) > 0
		//			if first(left) \A1\DC first(right)
		//				append first(left) to result
		//				left = rest(left)
		//			else
		//				append first(right) to result
		//				right = rest(right)
		//		if length(left) > 0 
		//			append rest(left) to result
		//		if length(right) > 0 
		//			append rest(right) to result
		//		return result
		
		private ArrayList Merge(ArrayList left, ArrayList right)
		{
			ArrayList result  = new ArrayList();
			while ((left.Count>0)&&(right.Count>0))
			{
				if ((double)left[0] <= (double)right[0])
				{
					result.Add(left[0]);
					left.RemoveAt(0);
				}
				else
				{
					result.Add(right[0]);
					right.RemoveAt(0);
				}
			}
			if (left.Count>0)
			{
				foreach (double s in left)
				{
					result.Add(s);
				}
			}

			if (right.Count>0)
			{
				foreach (double s in right)
				{
					result.Add(s);
				}
			}

			return result;
						
		}

		//An: find the list of outliers
		//@param arrayAngle 
		/*
		private ArrayList outlier(ArrayList arrayAngle)
		{
			
			ArrayList arraySorted = new ArrayList();
			ArrayList outlier = new ArrayList();
			ArrayList arrayClone = (ArrayList)arrayAngle.Clone();

			double lms = LMS(arrayClone); //compute lms

			double median = 0;
			double scale = 0;

			foreach (double s in arrayAngle){
				arraySorted.Add((s - lms)*(s-lms));  //square of residual
			}

			arraySorted = MergeSort(arraySorted); //list of residuals
			if (arraySorted.Count % 2 ==0)
			{
				median = (double)arraySorted[arraySorted.Count/2]+(double)arraySorted[arraySorted.Count/2-1];
				median = median/2;
			}
			else
				median = (double)arraySorted[arraySorted.Count/2];
									
			median = Math.Sqrt(median);
			scale = 1.4826 * (1+5/(arraySorted.Count-1));
			scale = scale * median;

			foreach (double s in arrayAngle)
			{
				if ((s-lms)/scale >2.5)
				{
					outlier.Add(s);
				}
			}
			
			return outlier;
		}

		#endregion identify outlier using Least median of squares method
			*/
		private void Mainform_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			int nKeyValue = (int)e.KeyData;//.KeyData.ToInt32();

			if (nKeyValue == 46)
			{
				for (int i = listView1.SelectedItems.Count - 1; i >= 0; i--)
				{
					System.Windows.Forms.ListViewItem li = listView1.SelectedItems[i];
					vertselected.Remove(Convert.ToInt32(li.Text));
					listView1.Items.Remove(li);
					
				}
			}
		}

		private void buttonStatClear_Click(object sender, System.EventArgs e)
		{
			for (int i = listView1.Items.Count - 1; i >= 0; i--)
			{
				System.Windows.Forms.ListViewItem li = listView1.Items[i];
				vertselected.Remove(Convert.ToInt32(li.Text));
				listView1.Items.Remove(li);
				label10.Text="";
					
			}
			Refresh();
		}

		private void checkBox1_CheckedChanged(object sender, System.EventArgs e)
		{
			if (checkBox1.Checked == true)
			{
				setShortShortDistanceBetaPlus(true);
				return ;
			}
			else
			{
				setShortShortDistanceBetaPlus(false);
				return ;
			}
			
		}

		private void buttonAddpoint_Click(object sender, System.EventArgs e)
		{
			int i,j;
			i=(int)numericUpDown1.Value;
			j=(int)numericUpDown2.Value;
			vert.add(i,j);
			Refresh();
		}

		private void checkBox2_CheckedChanged(object sender, System.EventArgs e)
		{
			if (checkBox2.Checked == true)
			{
				setShortShortDistanceDelauney(true);
				return ;
			}
			else
			{
				setShortShortDistanceDelauney(false);
				return ;
			}
			
		}

		private void checkBox3_CheckedChanged(object sender, System.EventArgs e)
		{
			if (checkBox3.Checked == true)
			{
				setGathan(true);
				return ;
			}
			else
			{
				setGathan(false);
				return ;
			}
			
		}
		internal virtual void  drawGATHAN(System.Drawing.Graphics g)
		{
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Blue);
		}

		internal virtual void drawDistanceBasedNew(System.Drawing.Graphics g)
		{
			//compute Delaunay 

			int bestconnectivepoint=-1;
			int freedomdegree=0;

			#region calculate the delaunay  trianglation
			int GCurveID=0;
			int maxcr=(int)numericUpDown3.Value;
			int crcount=0;
			
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
			Delaunay delaunay1 = new Delaunay(vert.size());

			/**
				 *  op is not used in so An comments it out
				 * 	Vision.Optimismfilter op=new Vision.Optimismfilter();
				 */

			Vision.StCurveList CVL= new Vision.StCurveList();
			int [] ExtendablePoints={0,0};
			int i=0,j=0,k;
			int head = 0;
			int tail=0;

			/**
				 * Insert all the point into the delaunay array for delaunay triangulation
				 */ 
			for (i = 0; i < vert.size(); i++)
				delaunay1.Insert(vert.get_Renamed(i).X, vert.get_Renamed(i).Y, 0);
			System.Collections.ArrayList nodes=delaunay1.nodes;
			System.Collections.ArrayList edges=delaunay1.edges;
			System.Collections.ArrayList curveedge=new ArrayList();
			System.Collections.ArrayList curvepoint=new ArrayList();
			if (nodes.Count == 1)
				/**each point is a rectangle of size 1*/
				g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
			if (nodes.Count == 2)
				g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, ((Node) nodes[1]).x, ((Node) nodes[1]).y);
			#endregion


			#region initialize the data
			double[][] EdgeMap = new double[vert.size()][];
			int [][] EdgeStatus= new int[vert.size()][];
			int [] PointStatus=new int[vert.size()];
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i] = new double[vert.size()];
				EdgeStatus[i]= new int[vert.size()];
				PointStatus[i]=0;
			}
			/**
			 * Initialize all EdgeMap and EdgeSatus to value -1
			 */ 
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i][i]= -1;
				EdgeStatus[i][i]=-1;
				for (j = i + 1; j < vert.size(); j++)
				{
					EdgeMap[i][j]= -1;
					EdgeStatus[i][j]=-1;
					EdgeMap[j][i]= -1;
					EdgeStatus[j][i]=-1;
				}
			}
			
			/**
			 * find 2 points that around the two end points of the Delaunay edge
			 * Calculate the distance between these 2 points
			 */ 
			for (k = 0; k < edges.Count; k++)
			{
				Edge myedge=(Edge)edges[k];
				i=findPt(myedge.p1.x,myedge.p1.y);
				j=findPt(myedge.p2.x,myedge.p2.y);
				double d = vert.get_Renamed(j).X - vert.get_Renamed(i).X;
				double d2 = vert.get_Renamed(j).Y - vert.get_Renamed(i).Y;
				EdgeMap[i][j]=Math.Sqrt( d * d + d2 * d2);
				EdgeStatus[i][j]=0;
				EdgeMap[j][i]= EdgeMap[i][j];
				EdgeStatus[j][i]=0;
			}

			/**
			 * add all the sampling points to pointlist
			 * initialize curveid = -1
			 */ 
			Vision.PointList PL=new Vision.PointList();
			for (i=0 ; i< vert.size(); i++)
			{
				Vision.Point2D pt = new Vision.Point2D(0,0);
				pt.CurveID=-1;
				pt.X= vert.get_Renamed(i).X;
				pt.Y=vert.get_Renamed(i).Y;
				PL.Add(pt);
			}
			#endregion //end region for initialize data

			//Mapping the edge distance. Status=0 means the dalaunay edge.
			bool SearchOver=false;
			//int edgepriority=0;  // The priority of edges in some degree.
			do
			{
				#region steps control
				if (crcount>=maxcr&&maxcr!=0)
				{
					SearchOver=true;
					break;
				}
				#endregion
				//Step1 find the shortest distance
				//Comments: To find the shortest distance, we rank the different situations in different priority.
				//Thatis:
				//       1: if the freedom degree is 2, we connect these edges firstly.
				//       2: if the freedom degree is 1, we connnect these edges sencondly.
				//       3: The last step is to connnect the 0-degree edges.
				
				bool edgesearchover=false; // All the adges of edgepriorty are searched over.
				bool edgefilter=false;// Filter of edges in different degree;
				double d1 = 99999D;
				//bool ignoreEdge = true; //exempt ignored Delaunay edge from search shortest edge
				SearchOver=false;
				head = 0;
				tail=0;
				edgesearchover=true;	
				
				#region find the shortest edge
				
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						edgefilter=false;

						if((PointStatus[k])<2&&(PointStatus[l1]<2))
							edgefilter=true;

						if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
						{
							tail = l1;
							head = k;
							d1 = EdgeMap[k][l1]; //the Shortest edge
							edgesearchover=false;

						}		
					}
					
				}
				
				if (edgesearchover) /** Cannot find the shortest edge??*/
				{
				//	edgepriority++;
						SearchOver=true;
				}
				//if (edgepriority>2)
				//{	
			//		SearchOver=true; /** get out, not search again */				
			//	}
				#endregion				
				
					

				/////TO DO : USE DISTANCECOMPARABLE TO IMPROVE RUNNING SPEED.
				if (SearchOver)
				{
					break;  //search over

				}
				
				if (!edgesearchover)                // Shortest points pair will be connected directly if their degree is 2.  Totally free.				
				{
					SearchOver=false;
					ExtendablePoints[0]=-1;
					ExtendablePoints[1]=-1;
					Vision.Point2D pt= (Vision.Point2D)PL[head];
					ExtendablePoints[0]=pt.CurveID;
					pt= (Vision.Point2D)PL[tail];
					ExtendablePoints[1]=pt.CurveID;

					//if not interior point, put into T
					if ((PointStatus[head]<2)&&(PointStatus[tail]<2))
					{
						//put into T
						EdgeStatus[head][tail] = 2;
						EdgeStatus[tail][head] = 2;
						//connect into a curve

						#region free edge
						if (ExtendablePoints[0]+ExtendablePoints[1]==-2)
						{   
							GCurveID++;
							((Vision.Point2D)PL[head]).CurveID=GCurveID;
							((Vision.Point2D)PL[tail]).CurveID=GCurveID;
							Vision.StCurve cr=new Vision.StCurve();
							cr.AddPoint((Vision.Point2D)PL[head]);
							cr.AddPoint((Vision.Point2D)PL[tail]);
							cr.curveID=GCurveID;
							CVL.AddCurve(cr);
                            //((int)PointStatus[head])++;
                            //((int)PointStatus[tail])++;
                            PointStatus[head] = ((int)PointStatus[head])+1;
                            PointStatus[tail]= ((int)PointStatus[tail])+1;
							if (interStep)
							crcount++;
						}			

					
						#endregion free edge		
					
				
						//Step2 Extend the curve
						//freedom=0 connection
						int extindex=0;	
						int extendpoint=0;
						#region  if any side has curve, we figure the situation.
						if ((ExtendablePoints[0]!=-1)||(ExtendablePoints[1]!=-1))        
						{

							freedomdegree=0;

							extindex=0;
							extendpoint=0;
							for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
							{
								#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
								
								if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
								{
									freedomdegree++;

									Vision.Point2D hp=new Vision.Point2D(0,0);
									Vision.Point2D mp=new Vision.Point2D(0,0);
									Vision.Point2D ep=new Vision.Point2D(0,0);
									
									if (extendablePointsi==0)
									{
										extendpoint=head;
										bestconnectivepoint = tail;
									}
									else
									{
										extendpoint=tail;
										bestconnectivepoint = head;
									}	
								}
								#endregion
							}
							#region Update the curves
							//UPdate the curve
							#region one side is no free
							if (freedomdegree==1)
							{
								((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;
								Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
								extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
								if (extindex==0)
									cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
								else
									cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint])+1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint])+1;
								if (interStep)
									crcount++; /** curve count*/
							}
							#endregion one side is not free														
							#region both sides are not free
							if (freedomdegree==2)
							{
								//Join the two curve
								
								Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[head]).CurveID);
								Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[tail]).CurveID);
						
								int hid=crh.curveID;
								int tid=crt.curveID;
					
									if(hid!=tid)
									{
										GCurveID++;
										for(j=0;j<PL.Count;j++)
										{
											if (((Vision.Point2D)PL[j]).CurveID==hid)
												((Vision.Point2D)PL[j]).CurveID=GCurveID;
											if (((Vision.Point2D)PL[j]).CurveID==tid)
												((Vision.Point2D)PL[j]).CurveID=GCurveID;

										}
										//Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
										Vision.StCurve cr=CVL.ConnetTwoCurves2(ref crh,ref crt,(Vision.Point2D)PL[head],(Vision.Point2D)PL[tail]);
										cr.curveID=GCurveID;

										/* why remove???*/
										CVL.Remove(crh);
										CVL.Remove(crt);
										CVL.AddCurve(cr);						
									}
									else
										/**same curveID so connection will make a closed curve*/
										crh.Closed=1;				
			
                                    //((int)PointStatus[tail])++;
                                    //((int)PointStatus[head])++;
                                    PointStatus[tail] = ((int)PointStatus[tail])+1;
                                    PointStatus[head] = ((int)PointStatus[head])+1;
									if (interStep)
										crcount++;
								
							}
							#endregion both sides are not free

							//						}
							#endregion update the curve		
						
						}
						#endregion if any side has curve, we figure the situation.//endif any side has curve

					}//endif not interior point found
				}//endif shortest edge is found
				
			}while(!SearchOver);

			double [] connectivevalue=new double[2];
								
			if (!interStep)
			{
				do
				{	
					#region steps control
					if (crcount>=maxcr&&maxcr!=0)
					{
						SearchOver=true;
						break;
					}
					#endregion

					//find shortest edge 
					bool edgesearchover=false; // All the adges of edgepriorty are searched over.
					bool edgefilter=false;// Filter of edges in different degree;
					double d1 = 99999D;
					//bool ignoreEdge = true; //exempt ignored Delaunay edge from search shortest edge
					SearchOver=false;
					head = 0;
					tail=0;
					edgesearchover=true;	
				
					#region find the shortest edge
				
					for ( k = 0; k < vert.size(); k++)
					{
						for (int l1 = k+1; l1 < vert.size(); l1++)
						{
						//	edgefilter=false;

						//	if((PointStatus[k])<2&&(PointStatus[l1]<2))
						//		edgefilter=true;

							if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==2)
							{
								tail = l1;
								head = k;
								d1 = EdgeMap[k][l1]; //the Shortest edge
								edgesearchover=false;

							}		
						}
					
					}
				
					if (edgesearchover) /** Cannot find the shortest edge??*/
					{
						//	edgepriority++;
						SearchOver=true;
					}
					//if (edgepriority>2)
					//{	
					//		SearchOver=true; /** get out, not search again */				
					//	}
					#endregion find shortest edge
		

					connectivevalue[0]=-100d;
					connectivevalue[1]=-100d;

					for (i=0; i<2; i++)
					{

						int extendpoint = -1;
						if (i==1)
						{
							bestconnectivepoint = head;
							extendpoint = tail;
						}
						else 
						{
							bestconnectivepoint = tail;
							extendpoint = head;
						}
						Vision.StCurve mycr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);

						//only 2 points 
						if (mycr.Count <= 2)
						{
							connectivevalue[i] = 1;
							break;
						}

						int extPointIndex = mycr.IndexOf((Vision.Point2D)PL[extendpoint], 0, mycr.Count);
						int bestPointIndex = mycr.IndexOf((Vision.Point2D)PL[bestconnectivepoint], 0, mycr.Count);
						int neighborIndex = -1;


						if (extPointIndex > bestPointIndex)
						{
							if ((extPointIndex == mycr.Count-1)&&(bestPointIndex==0))
							{
								neighborIndex = extPointIndex - 1;
							}
							else 
							{
								neighborIndex  = extPointIndex+1; 

								if (neighborIndex>mycr.Count-1)
								{
									if (mycr.Closed !=1)
									{
										continue;									
									}
									neighborIndex = 0;
								}						
							}
						}
						else
						{
							if ((extPointIndex==0)&&(bestPointIndex==mycr.Count-1))
							{
								neighborIndex = extPointIndex+1;									
							}
							else
							{
								neighborIndex = extPointIndex - 1;
								
								if (neighborIndex<0)
								{
									if (mycr.Closed !=1)
									{
										continue;									
									}
									neighborIndex = mycr.Count-1;
								}																
							}
						}

						//compute connectivity value
						/////////////////////
						Vision.Point2D neighborPoint = (Vision.Point2D)mycr[neighborIndex];												
						double l= ((Vision.Point2D)PL[extendpoint]).Distance((Vision.Point2D)PL[bestconnectivepoint]);
						double lo= ((Vision.Point2D)PL[extendpoint]).Distance(neighborPoint);
						double disMean = 0;
						double std = 0;					
						//double [] dis;
						ArrayList dis = new ArrayList();

						//if the curve is open only 1 part of the curve is considered
						if (mycr.Closed!=1)
						{
							int pcount=0;
							if (neighborIndex > extPointIndex)
							{
								//dis = new double[mycr.Count-1-extPointIndex];
								for (k=extPointIndex; k<mycr.Count-1; k++)
								{
									dis.Add(((Vision.Point2D)mycr[k]).Distance((Vision.Point2D)mycr[k+1]));
									disMean = disMean + ((Vision.Point2D)mycr[k]).Distance((Vision.Point2D)mycr[k+1]);
									pcount++;
								}
							}
							else
							{
								//dis = new double[extPointIndex];
								for (k=extPointIndex; k>0; k--)
								{
									dis.Add(((Vision.Point2D)mycr[k]).Distance((Vision.Point2D)mycr[k-1]));
									disMean = disMean + ((Vision.Point2D)mycr[k]).Distance((Vision.Point2D)mycr[k-1]);
									pcount++;
								}
							}
							//distanceMean
							disMean = disMean/pcount;
						}
						else //if curve is closed
						{							
							disMean = ((mycr.DistanceMeanClosedCurve()*mycr.Count)-l)/(mycr.Count-1);						
						}

						
						//if the curve is closed
						if (mycr.Closed==1)
						{
							//dis = new double[mycr.Count];

							for (k=0; k<mycr.Count-1; k++)
							{
								dis.Add(((Vision.Point2D)mycr[k]).Distance((Vision.Point2D)mycr[k+1]));
							}

							dis.Add(((Vision.Point2D)mycr[mycr.Count-1]).Distance((Vision.Point2D)mycr[0]));
						

							for (k=0; k<mycr.Count; k++)
							{
								if ((double)dis[k]==l)
								{
									dis.RemoveAt(k);
									break;
								}
							}
						}

						//std deviation
						for (k=0; k<dis.Count; k++)
						{
							std = std + ((double)dis[k]-disMean)*((double)dis[k]-disMean);
						}

						std = std/(dis.Count-1);
						std = Math.Sqrt(std);
					
						if (std==0)
						{
							std = 1;
						}
						else
						{	std = disMean/std;
							std = Math.Pow((std+1),1/std);
						}							



						connectivevalue[i] = std*disMean;
						connectivevalue[i] = connectivevalue[i]*(l+lo)/Math.Sqrt(2);
						connectivevalue[i] = connectivevalue[i]/Math.Abs(l-lo);
						
						connectivevalue[i] = connectivevalue[i] - l;

					}
								
					
					if ((connectivevalue[0]>0)||(connectivevalue[1]>0))
					{
						EdgeStatus[head][tail]=1;
						EdgeStatus[tail][head]=1;
						crcount++;
					}
					else
					{
						EdgeStatus[head][tail]=-1;
						EdgeStatus[tail][head]=-1;
					}

				}while (!SearchOver);
			}//endif checkboxInter is checked


			#region Draw curves
			for (i = 0; i < vert.size(); i++)
			{
				for (j = i + 1; j < vert.size(); j++)
				{
					if (!interStep)
					{
						if (EdgeStatus[i][j]==1)
						{
							SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
							g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
						}
					}
					else
					{
						if (EdgeStatus[i][j]==2)
						{							
						//	SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Green);
								SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
							g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
						}
					}


				}
			}
			#endregion 
			/////

			}			

		internal virtual void  drawShortDistanceDelauneyEx(System.Drawing.Graphics g)
		{
			//Prepare : If we have an edge , we can identify the two point.
			//			If we have a point , we can identify all the connective adges.
			//			if we have a point , we can identify the curve.
			//Construction Delauney Trianglation.		
			//Find the shortest edge.
			// if the two end points are free, connect them  directly.
			// if one of the enf point is not free, try to extend them one by one.
			double [,] connectivevalue=new double[2,vert.size()];
			int bestconnectivepoint=-1;
			int bestextenalpoint=-1;
			double bestconnectivevalue=-20;
			int freedomdegree=0;

			#region calculate the delaunay  trianglation
			int GCurveID=0;
			int maxcr=(int)numericUpDown3.Value;
			int crcount=0;
			
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
			Delaunay delaunay1 = new Delaunay(vert.size());

		/**
		 *  op is not used in so An comments it out
		 * 	Vision.Optimismfilter op=new Vision.Optimismfilter();
		 */

			Vision.StCurveList CVL= new Vision.StCurveList();
			int [] ExtendablePoints={0,0};
			int i=0,j=0,k;
			int head = 0;
			int tail=0;

            /**
             * Insert all the point into the delaunay array for delaunay triangulation
             */ 
             for (i = 0; i < vert.size(); i++)
				delaunay1.Insert(vert.get_Renamed(i).X, vert.get_Renamed(i).Y, 0);
			System.Collections.ArrayList nodes=delaunay1.nodes;
			System.Collections.ArrayList edges=delaunay1.edges;
			System.Collections.ArrayList curveedge=new ArrayList();
			System.Collections.ArrayList curvepoint=new ArrayList();
			if (nodes.Count == 1)
                /**each point is a rectangle of size 1*/
				g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
			if (nodes.Count == 2)
				g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, ((Node) nodes[1]).x, ((Node) nodes[1]).y);
			#endregion

			#region initialize the data
			double[][] EdgeMap = new double[vert.size()][];
			int [][] EdgeStatus= new int[vert.size()][];
			int [] PointStatus=new int[vert.size()];
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i] = new double[vert.size()];
				EdgeStatus[i]= new int[vert.size()];
				PointStatus[i]=0;
			}
			/**
			 * Initialize all EdgeMap and EdgeSatus to value -1
			 */ 
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i][i]= -1;
				EdgeStatus[i][i]=-1;
				for (j = i + 1; j < vert.size(); j++)
				{
					EdgeMap[i][j]= -1;
					EdgeStatus[i][j]=-1;
					EdgeMap[j][i]= -1;
					EdgeStatus[j][i]=-1;
				}
			}
			
            /**
             * find 2 points that around the two end points of the Delaunay edge
             * Calculate the distance between these 2 points
             */ 
			for (k = 0; k < edges.Count; k++)
			{
				Edge myedge=(Edge)edges[k];
				i=findPt(myedge.p1.x,myedge.p1.y);
				j=findPt(myedge.p2.x,myedge.p2.y);
				double d = vert.get_Renamed(j).X - vert.get_Renamed(i).X;
				double d2 = vert.get_Renamed(j).Y - vert.get_Renamed(i).Y;
				EdgeMap[i][j]=Math.Sqrt( d * d + d2 * d2);
				EdgeStatus[i][j]=0;
				EdgeMap[j][i]= EdgeMap[i][j];
				EdgeStatus[j][i]=0;
			}

            /**
             * add all the sampling points to pointlist
             * initialize curveid = -1
             */ 
			Vision.PointList PL=new Vision.PointList();
			for (i=0 ; i< vert.size(); i++)
			{
				Vision.Point2D pt = new Vision.Point2D(0,0);
				pt.CurveID=-1;
				pt.X= vert.get_Renamed(i).X;
				pt.Y=vert.get_Renamed(i).Y;
			    PL.Add(pt);
			}
			#endregion //end region for initialize data

			//Mapping the edge distance. Status=0 means the dalaunay edge.
			bool SearchOver=false;
			int edgepriority=0;  // The priority of edges in some degree.
			do
			{
				#region steps control
				if (crcount>=maxcr&&maxcr!=0)
				{
					SearchOver=true;
					break;
				}
				#endregion
				//Step1 find the shortest distance
				//Comments: To find the shortest distance, we rank the different situations in different priority.
				//Thatis:
				//       1: if the freedom degree is 2, we connect these edges firstly.
				//       2: if the freedom degree is 1, we connnect these edges sencondly.
				//       3: The last step is to connnect the 0-degree edges.
				
				bool edgesearchover=false; // All the adges of edgepriorty are searched over.
				bool edgefilter=false;// Filter of edges in different degree;
				double d1 = 99999D;
				//bool ignoreEdge = true; //exempt ignored Delaunay edge from search shortest edge
				SearchOver=false;
				head = 0;
				tail=0;
				edgesearchover=true;	
				
				#region find the shortest edge
				
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						edgefilter=false;

							if((PointStatus[k])<2&&(PointStatus[l1]<2))
								edgefilter=true;

						if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
						{
							tail = l1;
							head = k;
							d1 = EdgeMap[k][l1]; //the Shortest edge
							edgesearchover=false;

						}		
					}
					
				}

				

				if (edgesearchover) /** Cannot find the shortest edge??*/
				{
					edgepriority++;
				}
				if (edgepriority>2)
				{	
					SearchOver=true; /** get out, not search again */
					//ignoreEdge = false; //include ingored Delaunay Edge in searching
				}
				#endregion				
				
					

				/////TO DO : USE DISTANCECOMPARABLE TO IMPROVE RUNNING SPEED.
				if (SearchOver)
				{
					break;  //search over

				}
				
				if (!edgesearchover)                // Shortest points pair will be connected directly if their degree is 2.  Totally free.				
				{
					SearchOver=false;
					ExtendablePoints[0]=-1;
					ExtendablePoints[1]=-1;
					Vision.Point2D pt= (Vision.Point2D)PL[head];
					ExtendablePoints[0]=pt.CurveID;
					pt= (Vision.Point2D)PL[tail];
					ExtendablePoints[1]=pt.CurveID;
					#region free edge
					if (ExtendablePoints[0]+ExtendablePoints[1]==-2)
					{   
						GCurveID++;
						((Vision.Point2D)PL[head]).CurveID=GCurveID;
						((Vision.Point2D)PL[tail]).CurveID=GCurveID;
						Vision.StCurve cr=new Vision.StCurve();
						cr.AddPoint((Vision.Point2D)PL[head]);
						cr.AddPoint((Vision.Point2D)PL[tail]);
						cr.curveID=GCurveID;
						CVL.AddCurve(cr);
						EdgeStatus[head][tail]=1;
						EdgeStatus[tail][head]=1;
                        //((int)PointStatus[head])++;
                        //((int)PointStatus[tail])++;
                        PointStatus[head] = ((int)PointStatus[head]) + 1;
                        PointStatus[tail] = ((int)PointStatus[tail]) + 1;
						crcount++;
					}			
					
					#endregion
					

					//Step2 Extend the curve
					//freedom=0 connection
					int extindex=0;	
					int extendpoint=0;
						if ((ExtendablePoints[0]!=-1)||(ExtendablePoints[1]!=-1))             //if any side has curve, we figure the situation.
						{
						#region clear connective value
						for(i=0;i<vert.size();i++)
						{
							connectivevalue[0,i]=-100d;
							connectivevalue[1,i]=-100d;
						}
						#endregion
						
						freedomdegree=0;

							extindex=0;
							extendpoint=0;
							for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
							{
								#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
								
								if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
								{
									freedomdegree++;

									Vision.Point2D hp=new Vision.Point2D(0,0);
									Vision.Point2D mp=new Vision.Point2D(0,0);
									Vision.Point2D ep=new Vision.Point2D(0,0);
									
									if (extendablePointsi==0)
										extendpoint=head;
									else
										extendpoint=tail;
									

									int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
									Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
									Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
								
									extindex=mycr.CatchPointEx(newpt);

									//?????? 
									if (extindex>0)
										extindex=-1; //the order is reverse from back to front of the curve

									//////////////////////////
									for (j = 0; j < vert.size(); j++)
									{

										if (EdgeStatus[extendpoint][j]==0) 
										{							
											// calcu the entendable length of each edge.
											connectivevalue[extendablePointsi,j]=mycr.ConnectiveValue(extindex,Convert.ToDouble(vert.get_Renamed(j).X),Convert.ToDouble(vert.get_Renamed(j).Y));
										}
									}
									/////////////////////////////////					
								}
								#endregion
							}

                            /**
                             * Find the best point to connect to the existing curve?
                             */ 	                
					#region find the best point
					bestconnectivevalue=-50d;
					bestextenalpoint=-1; /**< 0 for head, 1 for tai?? */
					bestconnectivepoint=-1; 
					for(i=0;i<2;i++)
						for(j=0;j<vert.size();j++)
						{
							if((connectivevalue[i,j]>bestconnectivevalue)&&(PointStatus[j]<2))
							{
								bestconnectivevalue=connectivevalue[i,j];
								bestextenalpoint=i;
								bestconnectivepoint=j;
							}

						}
					#endregion
					#region Update the curves
					if(bestconnectivevalue<7.5d) /** not be able to connect */
					{
						if (bestextenalpoint==0)
							extendpoint=head;
						else
							extendpoint=tail;

						//((int)PointStatus[extendpoint])++;
                        PointStatus[extendpoint]= ((int)PointStatus[extendpoint])+1;
					}
					else if(bestconnectivevalue>7.5d)
					{				
						if (bestextenalpoint==0)
							extendpoint=head;
						else
							extendpoint=tail;

						//UPdate the curve
						#region one side is no free
						if (freedomdegree==1)
						{
							((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;
							Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
							extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
							if (extindex==0)
								cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
							else
								cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);
							EdgeStatus[extendpoint][bestconnectivepoint]=1;
							EdgeStatus[bestconnectivepoint][extendpoint]=1;
                            //((int)PointStatus[extendpoint])++;
                            //((int)PointStatus[bestconnectivepoint])++;
                            PointStatus[extendpoint] = ((int)PointStatus[extendpoint])+1;
                            PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint])+1;

                            crcount++; /** curve count*/
						}
						#endregion
							
						//Update the curve
						#region both sides are not free
						if (freedomdegree==2)
						{
							//Join the two curve
								
							Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[head]).CurveID);
							Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[tail]).CurveID);
						
							int hid=crh.curveID;
							int tid=crt.curveID;
							//									if ((extendablePointsi==0)&&(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0))
							//										fzconnection++;
							if(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0)
							{
								if(hid!=tid)
								{
									GCurveID++;
									for(j=0;j<PL.Count;j++)
									{
										if (((Vision.Point2D)PL[j]).CurveID==hid)
											((Vision.Point2D)PL[j]).CurveID=GCurveID;
										if (((Vision.Point2D)PL[j]).CurveID==tid)
											((Vision.Point2D)PL[j]).CurveID=GCurveID;

									}
									Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
									cr.curveID=GCurveID;
									//										for(j=0;j<cr.Count;j++)		
									//										{
									//											((Vision.Point2D)cr[j]).CurveID=GCurveID;
									//										}

                                    /* why remove???*/
									CVL.Remove(crh);
									CVL.Remove(crt);
									CVL.AddCurve(cr);						
								}
								else
                                    /**same curveID so connection will make a closed curve*/
								    crh.Closed=1;							

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
								crcount++;
							}
						}
						#endregion

					}
					#endregion		
						}						
					}
				
			}while(!SearchOver);
			#region Draw curves
			for (i = 0; i < vert.size(); i++)
			{
				for (j = i + 1; j < vert.size(); j++)
				{
					if (EdgeStatus[i][j]==1)
					{
						g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
					}

				}
			}
			#endregion 
			/////
		}
		
		int p=1; //1-> head is currently considered, 2-> tail
		internal virtual void  drawAngleDistanceDelauney(System.Drawing.Graphics g)
		{
			// * Construct Delauney Trianglation.		
			// * Find the shortest edge.
			double [,] connectivevalue=new double[2,vert.size()];
			int bestconnectivepoint=-1;
			int bestextenalpoint=-1;
			double bestconnectivevalue=-20;
			int freedomdegree=0;
			
			#region calculate the delaunay  trianglation
			int GCurveID=0;
			int maxcr=(int)numericUpDown3.Value;
			int crcount=0;
			
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
			Delaunay delaunay1 = new Delaunay(vert.size());

			/**
			 *  op is not used 
			 * 	Vision.Optimismfilter op=new Vision.Optimismfilter();
			 */

			Vision.StCurveList CVL= new Vision.StCurveList();
			int [] ExtendablePoints={0,0};
			int i=0,j=0,k;
			int head = 0;
			int tail=0;

			/**
			 * Insert all the point into the delaunay array for delaunay triangulation
			 */ 
			for (i = 0; i < vert.size(); i++)
				delaunay1.Insert(vert.get_Renamed(i).X, vert.get_Renamed(i).Y, 0);
			System.Collections.ArrayList nodes=delaunay1.nodes;
			System.Collections.ArrayList edges=delaunay1.edges;
			System.Collections.ArrayList curveedge=new ArrayList();
			System.Collections.ArrayList curvepoint=new ArrayList();
			if (nodes.Count == 1)
				//**each point is a rectangle of size 1*/
				g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
			if (nodes.Count == 2)
				g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, ((Node) nodes[1]).x, ((Node) nodes[1]).y);
			#endregion

			#region initialize the data
			/** 	
			 * edgemap contains distance between 2 points if applicable
			 */
			double[][] EdgeMap = new double[vert.size()][];
			/*
			 * edgestatus = -1 -> not delaunay edge
			 * edgestatus = 0 -> delaunay edge
			 * edgestatus = 1 -> connected
			 */
			int [][] EdgeStatus= new int[vert.size()][];
			int [] PointStatus=new int[vert.size()];
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i] = new double[vert.size()];
				EdgeStatus[i]= new int[vert.size()];
				PointStatus[i]=0;
			}
			/**
			 * Initialize all EdgeMap and EdgeSatus to value -1
			 */ 
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i][i]= -1;
				EdgeStatus[i][i]=-1;
				for (j = i + 1; j < vert.size(); j++)
				{
					EdgeMap[i][j]= -1;
					EdgeStatus[i][j]=-1;
					EdgeMap[j][i]= -1;
					EdgeStatus[j][i]=-1;
				}
			}
			
			/**
			 * find 2 points that around the two end points of the Delaunay edge
			 * Calculate the distance between these 2 points
			 */ 
			for (k = 0; k < edges.Count; k++)
			{
				Edge myedge=(Edge)edges[k];
				i=findPt(myedge.p1.x,myedge.p1.y);
				j=findPt(myedge.p2.x,myedge.p2.y);
				double d = vert.get_Renamed(j).X - vert.get_Renamed(i).X;
				double d2 = vert.get_Renamed(j).Y - vert.get_Renamed(i).Y;
				EdgeMap[i][j]=Math.Sqrt( d * d + d2 * d2);
				EdgeStatus[i][j]=0;
				EdgeMap[j][i]= EdgeMap[i][j];
				EdgeStatus[j][i]=0;
			}

			/**
			 * add all the sampling points to pointlist
			 * initialize curveid = -1
			 */ 
			Vision.PointList PL=new Vision.PointList();
			for (i=0 ; i< vert.size(); i++)
			{
				Vision.Point2D pt = new Vision.Point2D(0,0);
				pt.CurveID=-1;
				pt.X= vert.get_Renamed(i).X;
				pt.Y=vert.get_Renamed(i).Y;
				PL.Add(pt);
			}
			#endregion //end region for initialize data

			//Mapping the edge distance. Status=0 means the dalaunay edge.
			bool SearchOver=false;
			int edgepriority=0;  // The priority of edges in some degree.
			int searchStage = 0; //0-> initial search, 1-> include ignored edge
			do
			{
				#region steps control
				if (crcount>=maxcr&&maxcr!=0)
				{
					SearchOver=true;
					break;
				}
				#endregion
				//Step1 find the shortest distance
				//Comments: To find the shortest distance, we rank the different situations in different priority.
				//That is:
				//       1: if the freedom degree is 2, we connect these edges firstly.
				//       2: if the freedom degree is 1, we connnect these edges sencondly.
				//       3: The last step is to connnect the 0-degree edges.
				
				bool edgesearchover=false; // All the adges of edgepriorty are searched over.
				bool edgefilter=false;// Filter of edges in different degree;
				//bool includeignoredEdge = false; //false -> exempt ignored Delaunay edge from searching				
				double d1 = 99999D;
				SearchOver=false;
				head = 0;
				tail=0;
				edgesearchover=true; /* = true -> search again */
				
				#region find the shortest edge				
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						edgefilter=false;

						if((PointStatus[k])<2&&(PointStatus[l1]<2))
							edgefilter=true; //** point does not lead to intersection situation
					
						/** this is delaunay edge, the point is end point*/
						if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
						{
							tail = l1;
							head = k;
							d1 = EdgeMap[k][l1]; //the Shortest edge
							edgesearchover=false; //shortest delaunay edge is found
						}		
					}					
				}
								
				if (edgesearchover) /** Cannot find the shortest edge??*/
				{
					edgepriority++;

					if (edgepriority>2)
					{
						//SearchOver=true; /** get out, not search again */
						if (searchStage ==0)
						{
							for ( k = 0; k < vert.size(); k++)
							{
								for (int l1 = k+1; l1 < vert.size(); l1++)
								{		
									if (EdgeStatus[k][l1]==2)
									{
										EdgeStatus[k][l1]=0;
										EdgeStatus[l1][k]=0;
									}
								}
							}						 
						}

						
						if (searchStage==2)
							SearchOver=true;

						searchStage++;

					}
				}


				#endregion				
				
					
				/////TO DO : USE DISTANCECOMPARABLE TO IMPROVE RUNNING SPEED.
				if (SearchOver)
				{
					break;  //search over
				}
				
				/** if shortest edge is found */
				if (!edgesearchover)                // Shortest points pair will be connected directly if their degree is 2.  Totally free.				
				{
					SearchOver=false;  /** < continue to search for next shortest edge  */
					
					// for initialize purpose 
					ExtendablePoints[0]=-1;
					ExtendablePoints[1]=-1;

					//first end point				
					Vision.Point2D pt= (Vision.Point2D)PL[head];										
					ExtendablePoints[0]=pt.CurveID;

					// second end point
					pt= (Vision.Point2D)PL[tail];
					ExtendablePoints[1]=pt.CurveID;

					/** if these are two free points 
					 *  check the angle 
					 */  
					#region free edge

					if (ExtendablePoints[0]+ExtendablePoints[1]==-2)
					{ 						
						p=1; //for connecting method 1-> head is currently considered, 2-> tail
// ... code here
						if (connecting(EdgeStatus, PointStatus, EdgeMap, head, tail, PL))
						{
							GCurveID++;
							((Vision.Point2D)PL[head]).CurveID=GCurveID;
							((Vision.Point2D)PL[tail]).CurveID=GCurveID;
							Vision.StCurve cr=new Vision.StCurve();
							cr.AddPoint((Vision.Point2D)PL[head]);
							cr.AddPoint((Vision.Point2D)PL[tail]);
							cr.curveID=GCurveID;
							CVL.AddCurve(cr);
							EdgeStatus[head][tail]=1;
							EdgeStatus[tail][head]=1;
                            //((int)PointStatus[head])++;
                            //((int)PointStatus[tail])++;
                            PointStatus[head] = ((int)PointStatus[head])+1;
                            PointStatus[tail] = ((int)PointStatus[tail])+1;
							crcount++; /** the number of edge for iteration view?? */					
							
						}
						else {					
							EdgeStatus[head][tail]=2; //ignored from searching shortest edge
							EdgeStatus[tail][head]=2; //ignored from searching shortest edge
							}
				     	}			
					
					#endregion
					

					//Step2 Extend the curve
					//freedom=0 connection
					int extindex=0;	
					int extendpoint=0;
					if ((ExtendablePoints[0]!=-1)||(ExtendablePoints[1]!=-1))             //if any side has curve, we figure the situation.
					{
						#region clear connective value
						for(i=0;i<vert.size();i++)
						{
							connectivevalue[0,i]=-100d;
							connectivevalue[1,i]=-100d;
						}
						#endregion
						
						freedomdegree=0;

						extindex=0;
						extendpoint=0;
						for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
						{
							#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
								
							if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
							{
								freedomdegree++;

								Vision.Point2D hp=new Vision.Point2D(0,0);
								Vision.Point2D mp=new Vision.Point2D(0,0);
								Vision.Point2D ep=new Vision.Point2D(0,0);
									
								if (extendablePointsi==0)
									extendpoint=head;
								else
									extendpoint=tail;
									

								int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
								Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
								Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
								
								extindex=mycr.CatchPointEx(newpt);

								if (extindex>0)
									extindex=-1;
								//////////////////////////
								for (j = 0; j < vert.size(); j++)
								{

									//if (EdgeStatus[extendpoint][j]==0||EdgeStatus[extendpoint][j]==2) //Omit EdgeStatus==1 ,cause intersection
									if (EdgeStatus[extendpoint][j]==0)
										// That is one point has more than one connection.
										// if Intersection is not allowed, we have to check that 
										// All the EdgeStatus[extendpoint][j] doesn't equal to 1
									{							
										// calcu the entendable length of each edge.
										connectivevalue[extendablePointsi,j]=mycr.ConnectiveValue(extindex,Convert.ToDouble(vert.get_Renamed(j).X),Convert.ToDouble(vert.get_Renamed(j).Y));
									}
								}
								/////////////////////////////////					
							}
							#endregion
						}

						/**
							 * Find the best point to connect to the existing curve?
							 */ 	                
						#region find the best point
						bestconnectivevalue=-50d;
						bestextenalpoint=-1; /**< 0 for head, 1 for tai?? */
						bestconnectivepoint=-1; 
						for(i=0;i<2;i++)
							for(j=0;j<vert.size();j++)
							{
								if((connectivevalue[i,j]>bestconnectivevalue)&&(PointStatus[j]<2))
								{
									bestconnectivevalue=connectivevalue[i,j];
									bestextenalpoint=i	;
									bestconnectivepoint=j;
								}
							}
						#endregion
						#region Update the curves
						if(bestconnectivevalue<7.5d) /** not be able to connect */
						{
							if (bestextenalpoint==0)
								extendpoint=head;
							else
								extendpoint=tail;

                            PointStatus[extendpoint] = ((int)PointStatus[extendpoint])+1;
						}
						else if(bestconnectivevalue>7.5d)
						{				
							if (bestextenalpoint==0)
								extendpoint=head;
							else
								extendpoint=tail;
							#region one side is no free
							//An: defined tempID
							int tempId = 	((Vision.Point2D)PL[bestconnectivepoint]).CurveID;
							//if (freedomdegree==1)
							if (tempId == -1)
							{
								((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;
								Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
								extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
								if (extindex==0)
									cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
								else
									cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint])+1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint])+1;
								crcount++; /** curve count*/

								//delaunay edge
								/*for ( k = 0; k < vert.size(); k++)
								{																		
									if (EdgeStatus[bestconnectivepoint][k]==2)
									{
										EdgeStatus[k][bestconnectivepoint]=0;
										EdgeStatus[bestconnectivepoint][k]=0;
									}
								}	*/
							}
							#endregion
							
							#region both sides are not free
							

							//if (freedomdegree==2)
							if (tempId != -1)
							{
								//Join the two curve
								
							//	Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[head]).CurveID);
							//	Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[tail]).CurveID);
								Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
								Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[bestconnectivepoint]).CurveID);
						
								int hid=crh.curveID;
								int tid=crt.curveID;
								//if ((extendablePointsi==0)&&(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0))
								//fzconnection++;
								if(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0)
								{
									if(hid!=tid)
									{
										GCurveID++;
										for(j=0;j<PL.Count;j++)
										{
											if (((Vision.Point2D)PL[j]).CurveID==hid)
												((Vision.Point2D)PL[j]).CurveID=GCurveID;
											if (((Vision.Point2D)PL[j]).CurveID==tid)
												((Vision.Point2D)PL[j]).CurveID=GCurveID;

										}
										Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
										cr.curveID=GCurveID;							

										/* why remove???*/
										CVL.Remove(crh);
										CVL.Remove(crt);
										CVL.AddCurve(cr);						
									}
									else
										/**same curveID so connection will make a closed curve*/
										crh.Closed=1;							

									EdgeStatus[extendpoint][bestconnectivepoint]=1;
									EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                    //((int)PointStatus[extendpoint])++;
                                    //((int)PointStatus[bestconnectivepoint])++;
                                    PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                    PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
									crcount++;
								}
							}
							#endregion

						}
						#endregion		
					}						
				}
				
			}while(!SearchOver);

			//Draw curves
			#region Draw curves
			for (i = 0; i < vert.size(); i++)
			{
				for (j = i + 1; j < vert.size(); j++)
				{
					if (EdgeStatus[i][j]==1)
					{
						g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
					}

				}
			}
			#endregion 
			/////			
		}

		/** Compute the angle using law of cosine in triangle */
		private static double computeAngle(Vision.Point2D p1, Vision.Point2D p2, Vision.Point2D p3)
		{
			double a=0, b=0, cSqr=0, cosine=0, aSqr = 0, bSqr = 0;
			
			a = p1.Distance(p2);
			b = p1.Distance(p3);
			cSqr = p2.DistanceSqr(p3);
			aSqr = p1.DistanceSqr(p2);
			bSqr = p1.DistanceSqr(p3);

			cosine = (aSqr + bSqr - cSqr)/(2*a*b);
			return Math.Acos(cosine);									
		}
		
		/** 
		 * Checking the angle condition before connecting 2 free points
		 */
		private bool connecting(int[][] arrEdgeStatus,int[] arrPointStatus, double[][] arrEdgeMap, 
			int head, int tail, Vision.PointList pl )
		{
			
			bool connected = true;

			//if maxangle = -1 after the second for loop, the circle is empty 
			//if maxangle = 0, 
			double maxangle=0, alpha=0;
			bool edgefilter;
			

			int t1=-1, t2 = -1;
            double range =  1.849 ; //define the area within which points are considered

			//get distance between two points
		//	double distance = arrEdgeMap[head][tail];
			double distance = double.MaxValue;			
			double distance1 = double.MaxValue;			
			for ( int k = 0; k < vert.size(); k++)
			{
				
				//	edgefilter=false;

				//	if((PointStatus[k])<2&&(PointStatus[l1]<2))
				//		edgefilter=true; //** point does not lead to intersection situation
					
					/** this is delaunay edge, the point is end point*/
			//		if (arrEdgeMap[head][k] < distance && arrEdgeStatus[head][k]!=-1 && (arrPointStatus[k])<2)
						if (arrEdgeMap[head][k] < distance && arrEdgeStatus[head][k]!=-1)
					{						
						distance = arrEdgeMap[head][k]; //the Shortest edge					
						
					}										
			}

			distance1 = distance;
			distance = double.MaxValue;
			for ( int k = 0; k < vert.size(); k++)
			{
				
				//	edgefilter=false;

				//	if((PointStatus[k])<2&&(PointStatus[l1]<2))
				//		edgefilter=true; //** point does not lead to intersection situation
					
				/** this is delaunay edge, the point is end point*/
				//		if (arrEdgeMap[head][k] < distance && arrEdgeStatus[head][k]!=-1 && (arrPointStatus[k])<2)
				if (arrEdgeMap[head][k] < distance && arrEdgeStatus[head][k]!=-1 && arrEdgeMap[head][k] > distance1)
				{						
					distance = arrEdgeMap[head][k]; //the Shortest edge					
						
				}										
			}
			//edgefilter = false;

			distance = (distance1+distance)/2;
			int firstpointcheck = 0; //to check if there is any point in area
			int secondpointcheck = 0; 
			/** Calculate all possible angle */
			for ( int k = 0; k < vert.size(); k++)
			{
				//three points considered must be distinct
				//1st point
				if ((k==head)||(arrEdgeMap[head][k]>distance*range)||
					//(arrEdgeStatus[head][k]==1)||(arrEdgeStatus[head][k]==-1))				
					(arrEdgeStatus[head][k]!=0))				
					continue; //skip if the third point is not distinct nor it is within a certain range

				if (k==tail)
					firstpointcheck++;		
						
				for (int l1 = k+1; l1 < vert.size(); l1++)
				{
					//three points considered must be distinct
					//2nd point
					if ((l1 ==head)||(arrEdgeMap[head][l1]>distance*range)||
						//(arrEdgeStatus[head][l1]==1)||(arrEdgeStatus[head][l1]==-1))
						(arrEdgeStatus[head][l1]!=0))
						continue;

				//	connected=false; //this is to control the case when there is only one point in the area
				//	secondpointcheck++;

					edgefilter=false;

					if((arrPointStatus[k])<2&&(arrPointStatus[l1]<2))
						edgefilter=true; //** point does not lead to intersection situation
						
//					if((arrPointStatus[k])<2&&(arrPointStatus[l1]<2))
//					{
//						if ((k!=tail)&&(l1!=tail))
//						{		
//							Vision.Point2D p1= (Vision.Point2D)pl[head];
//							Vision.Point2D p2= (Vision.Point2D)pl[l1];
//							Vision.Point2D p3= (Vision.Point2D)pl[k];
//							Vision.Point2D p4= (Vision.Point2D)pl[tail];
//						
//							if ((computeAngle(p1, p4, p3)>(44*Math.PI/180))&&(computeAngle(p1,p4,p2)>(44*Math.PI/180)))
//							{
//								edgefilter=true; //** point does not lead to intersection situation
//							}
//						}
//						else{edgefilter=true;}
//					}



					/** this is delaunay edge, the point is end point*/
					//if ((arrEdgeStatus[head][k]==0||arrEdgeStatus[head][k]==2)&&
					//	((arrEdgeStatus[head][l1]==0)||arrEdgeStatus[head][l1]==2)&&edgefilter)
					//if ((arrEdgeStatus[head][k]==0)&&
					//	(arrEdgeStatus[head][l1]==0)&&edgefilter)
					if (edgefilter)
					{
						Vision.Point2D p1= (Vision.Point2D)pl[head];
						Vision.Point2D p2= (Vision.Point2D)pl[l1];
						Vision.Point2D p3= (Vision.Point2D)pl[k];
						alpha = computeAngle(p1, p2, p3);
						if (maxangle < alpha)
						{
							maxangle = alpha;
							t1 = k;
							t2 = l1;
						}	
					}		
				}					
			} //end for loop
							
		//	if (((t1 == tail)||(t2==tail))&&(connected==false))		
	
	//			if ((t1 == tail)||(t2==tail)||(maxangle==0))  //maxangle ==0 -> there is no other point
			if ((t1 == tail)||(t2==tail)) 
			{
				connected = true;
				if (p==1) 
				{
					p++;					
				//	connecting(arrEdgeStatus, arrPointStatus, arrEdgeMap, tail, head, pl);
					if (connecting(arrEdgeStatus, arrPointStatus, arrEdgeMap, tail, head, pl)==false)
						connected=false;
				}
				//recursively check connective value for both endpoint of the current delaunay edge
				
			}
				//maxangle = 0 -> there are no points within an area, 
		//	else if (maxangle==0 && distance1==arrEdgeMap[head][tail])
			//else if (maxangle==0 && (distance1==arrEdgeMap[head][tail] || distance==arrEdgeMap[head][tail]))
			else if (maxangle==0 && (firstpointcheck>0))
			{
				connected = true;
				if (p==1) 
				{
					p++;					
					if (connecting(arrEdgeStatus, arrPointStatus, arrEdgeMap, tail, head, pl)==false)
						connected=false;
				}				
			}
			else	//if the value is not satisfied for one endpoint, no need to check other endpoint
				connected = false;
					
			return connected;
		}


		/* Checking the angle condition before connecting 2 free points
															   */
		/** 
		 * Checking the angle condition before connecting 2 free points
		 */
		private static bool connecting(ref Vertex vert, int[][] arrEdgeStatus,int[] arrPointStatus, double[][] arrEdgeMap, 
			int head, int tail, Vision.PointList pl, int p)
		{
			
			bool connected = true;

			//if maxangle = -1 after the second for loop, the circle is empty 
			//if maxangle = 0, 
			double maxangle=0, alpha=0;
			bool edgefilter;
			

			int t1=-1, t2 = -1;
            double range =  1.849 ; //define the area within which points are considered

			//get distance between two points
		//	double distance = arrEdgeMap[head][tail];
			double distance = double.MaxValue;			
			double distance1 = double.MaxValue;			
			for ( int k = 0; k < vert.size(); k++)
			{
				
				//	edgefilter=false;

				//	if((PointStatus[k])<2&&(PointStatus[l1]<2))
				//		edgefilter=true; //** point does not lead to intersection situation
					
					/** this is delaunay edge, the point is end point*/
			//		if (arrEdgeMap[head][k] < distance && arrEdgeStatus[head][k]!=-1 && (arrPointStatus[k])<2)
						if (arrEdgeMap[head][k] < distance && arrEdgeStatus[head][k]!=-1)
					{						
						distance = arrEdgeMap[head][k]; //the Shortest edge					
						
					}										
			}

			distance1 = distance;
			distance = double.MaxValue;
			for ( int k = 0; k < vert.size(); k++)
			{
				
				//	edgefilter=false;

				//	if((PointStatus[k])<2&&(PointStatus[l1]<2))
				//		edgefilter=true; //** point does not lead to intersection situation
					
				/** this is delaunay edge, the point is end point*/
				//		if (arrEdgeMap[head][k] < distance && arrEdgeStatus[head][k]!=-1 && (arrPointStatus[k])<2)
				if (arrEdgeMap[head][k] < distance && arrEdgeStatus[head][k]!=-1 && arrEdgeMap[head][k] > distance1)
				{						
					distance = arrEdgeMap[head][k]; //the Shortest edge					
						
				}										
			}
			//edgefilter = false;

			distance = (distance1+distance)/2;
			int firstpointcheck = 0; //to check if there is any point in area
			int secondpointcheck = 0; 
			/** Calculate all possible angle */
			for ( int k = 0; k < vert.size(); k++)
			{
				//three points considered must be distinct
				//1st point
				if ((k==head)||(arrEdgeMap[head][k]>distance*range)||
					//(arrEdgeStatus[head][k]==1)||(arrEdgeStatus[head][k]==-1))				
					(arrEdgeStatus[head][k]!=0))				
					continue; //skip if the third point is not distinct nor it is within a certain range

				if (k==tail)
					firstpointcheck++;		
						
				for (int l1 = k+1; l1 < vert.size(); l1++)
				{
					//three points considered must be distinct
					//2nd point
					if ((l1 ==head)||(arrEdgeMap[head][l1]>distance*range)||
						//(arrEdgeStatus[head][l1]==1)||(arrEdgeStatus[head][l1]==-1))
						(arrEdgeStatus[head][l1]!=0))
						continue;

				//	connected=false; //this is to control the case when there is only one point in the area
				//	secondpointcheck++;

					edgefilter=false;

					if((arrPointStatus[k])<2&&(arrPointStatus[l1]<2))
						edgefilter=true; //** point does not lead to intersection situation
						
//					if((arrPointStatus[k])<2&&(arrPointStatus[l1]<2))
//					{
//						if ((k!=tail)&&(l1!=tail))
//						{		
//							Vision.Point2D p1= (Vision.Point2D)pl[head];
//							Vision.Point2D p2= (Vision.Point2D)pl[l1];
//							Vision.Point2D p3= (Vision.Point2D)pl[k];
//							Vision.Point2D p4= (Vision.Point2D)pl[tail];
//						
//							if ((computeAngle(p1, p4, p3)>(44*Math.PI/180))&&(computeAngle(p1,p4,p2)>(44*Math.PI/180)))
//							{
//								edgefilter=true; //** point does not lead to intersection situation
//							}
//						}
//						else{edgefilter=true;}
//					}



					/** this is delaunay edge, the point is end point*/
					//if ((arrEdgeStatus[head][k]==0||arrEdgeStatus[head][k]==2)&&
					//	((arrEdgeStatus[head][l1]==0)||arrEdgeStatus[head][l1]==2)&&edgefilter)
					//if ((arrEdgeStatus[head][k]==0)&&

					//	(arrEdgeStatus[head][l1]==0)&&edgefilter)
					if (edgefilter)
					{
						Vision.Point2D p1= (Vision.Point2D)pl[head];
						Vision.Point2D p2= (Vision.Point2D)pl[l1];
						Vision.Point2D p3= (Vision.Point2D)pl[k];
						alpha = computeAngle(p1, p2, p3);
						if (maxangle < alpha)
						{
							maxangle = alpha;
							t1 = k;
							t2 = l1;
						}	
					}		
				}					
			} //end for loop
							
		//	if (((t1 == tail)||(t2==tail))&&(connected==false))		
	
	//			if ((t1 == tail)||(t2==tail)||(maxangle==0))  //maxangle ==0 -> there is no other point
			if ((t1 == tail)||(t2==tail)) 
			{
				connected = true;
				if (p==1) 
				{
					p++;					
				//	connecting(arrEdgeStatus, arrPointStatus, arrEdgeMap, tail, head, pl);
					if (connecting(ref vert, arrEdgeStatus, arrPointStatus, arrEdgeMap, tail, head, pl, p)==false)
						connected=false;
				}
				//recursively check connective value for both endpoint of the current delaunay edge
				
			}
				//maxangle = 0 -> there are no points within an area, 
		//	else if (maxangle==0 && distance1==arrEdgeMap[head][tail])
			//else if (maxangle==0 && (distance1==arrEdgeMap[head][tail] || distance==arrEdgeMap[head][tail]))
			else if (maxangle==0 && (firstpointcheck>0))
			{
				connected = true;
				if (p==1) 
				{
					p++;					
					if (connecting(ref vert, arrEdgeStatus, arrPointStatus, arrEdgeMap, tail, head, pl, p)==false)
						connected=false;
				}				
			}
			else	//if the value is not satisfied for one endpoint, no need to check other endpoint
				connected = false;
					
			return connected;
		}


		private bool connecting1(int[][] arrEdgeStatus,int[] arrPointStatus, double[][] arrEdgeMap, 
			int head, int tail, Vision.PointList pl )
		{
			
			bool connected = true;

			//if maxangle = -1 after the second for loop, the circle is empty 
			//if maxangle = 0, 
			double maxangle=0, alpha=0;
			bool edgefilter;
			

			int t1=-1, t2 = -1;
			double range =  1.859 ; //define the area within which points are considered

			//get distance between two points
			//	double distance = arrEdgeMap[head][tail];
			double distance = double.MaxValue;			
			double distance1 = double.MaxValue;			
			for ( int k = 0; k < vert.size(); k++)
			{
				
				//	edgefilter=false;

				//	if((PointStatus[k])<2&&(PointStatus[l1]<2))
				//		edgefilter=true; //** point does not lead to intersection situation
					
				/** this is delaunay edge, the point is end point*/
				//		if (arrEdgeMap[head][k] < distance && arrEdgeStatus[head][k]!=-1 && (arrPointStatus[k])<2)
				if (arrEdgeMap[head][k] < distance && arrEdgeStatus[head][k]!=-1)
				{						
					distance = arrEdgeMap[head][k]; //the Shortest edge					
						
				}										
			}

			distance1 = distance;
			distance = double.MaxValue;
			for ( int k = 0; k < vert.size(); k++)
			{
				
				//	edgefilter=false;

				//	if((PointStatus[k])<2&&(PointStatus[l1]<2))
				//		edgefilter=true; //** point does not lead to intersection situation
					
				/** this is delaunay edge, the point is end point*/
				//		if (arrEdgeMap[head][k] < distance && arrEdgeStatus[head][k]!=-1 && (arrPointStatus[k])<2)
				if (arrEdgeMap[head][k] < distance && arrEdgeStatus[head][k]!=-1 && arrEdgeMap[head][k] > distance1)
				{						
					distance = arrEdgeMap[head][k]; //the Shortest edge					
						
				}										
			}
			//edgefilter = false;

			distance = (distance1+distance)/2;
			int firstpointcheck = 0; //to check if there is any point in area
			int secondpointcheck = 0; 

			//compute the maximum angle having shortest edge as the angle's leg.
			for (int k=0; k<vert.size(); k++)
			{
				if ((k==head)||(k==tail))
				{
					continue;
				}
				
				if ((arrEdgeMap[k][head]> distance*range)||(arrEdgeStatus[head][k]!=0))
					continue;

				firstpointcheck++;

				Vision.Point2D p1= (Vision.Point2D)pl[head];
				Vision.Point2D p2= (Vision.Point2D)pl[tail];
				Vision.Point2D p3= (Vision.Point2D)pl[k];
				alpha = computeAngle(p1, p2, p3);
				if (maxangle < alpha)
				{
					maxangle = alpha;
					t1 = k;					
				}	
			}

			//if there exists such angle
			if (firstpointcheck!=0)
			{
				firstpointcheck=0;
				//find other angle that is larger than maxangle,
				//without having shortest edge as angle's leg
				for (int k=0; k<vert.size(); k++)
				{
					
					if ((k==tail)||(k==head)||(k==t1))
						continue;

					if ((arrEdgeMap[k][head]> distance*range)||(arrEdgeStatus[head][k]!=0))
						continue;
					
					firstpointcheck++;

					Vision.Point2D p1= (Vision.Point2D)pl[head];
					Vision.Point2D p2= (Vision.Point2D)pl[t1];
					Vision.Point2D p3= (Vision.Point2D)pl[k];
					alpha = computeAngle(p1, p2, p3);

					if (maxangle < alpha)
					{
						secondpointcheck++;
							break;
					}
				}

				if ((secondpointcheck!=0)&&(firstpointcheck!=0))
				{
					connected=true;
					if (p==1) 
					{
						p++;					
					
						if (connecting1(arrEdgeStatus, arrPointStatus, arrEdgeMap, tail, head, pl)==false)
							connected=false;
					}
				}
				else
					connected = false;

			
			}			//endif
				return connected;
		}


		static int[][] compute(ref Vertex vert, ref Delaunay delaunay1, int maxcr)
		{

				//Construction Delauney Trianglation.		
				//Find the shortest edge.
				// if the two end points are free, connect them  directly.			
				//double [,] connectivevalue=new double[2,vert.size()];
				int bestconnectivepoint=-1;
				//int bestextenalpoint=-1;
				//double bestconnectivevalue=-20;
				int freedomdegree=0;

				#region calculate the delaunay  trianglation
				int GCurveID=0;
				int crcount=0;
			

				/**
				 *  op is not used in so An comments it out
				 * 	Vision.Optimismfilter op=new Vision.Optimismfilter();
				 */

				Vision.StCurveList CVL= new Vision.StCurveList();
				int [] ExtendablePoints={0,0};
				int i=0,j=0,k;
				int head = 0;
				int tail=0;

				/**
				 * Insert all the point into the delaunay array for delaunay triangulation
				 */ 
				for (i = 0; i < vert.size(); i++)
					delaunay1.Insert(vert.get_Renamed(i).X, vert.get_Renamed(i).Y, 0);
				System.Collections.ArrayList edges=delaunay1.edges;
				System.Collections.ArrayList curveedge=new ArrayList();
				System.Collections.ArrayList curvepoint=new ArrayList();
				#endregion

				#region initialize the data
				double[][] EdgeMap = new double[vert.size()][];
				int [][] EdgeStatus= new int[vert.size()][];
				int [] PointStatus=new int[vert.size()];
				for (i = 0; i < vert.size(); i++)
				{
					EdgeMap[i] = new double[vert.size()];
					EdgeStatus[i]= new int[vert.size()];
					PointStatus[i]=0;
				}
				/**
				 * Initialize all EdgeMap and EdgeSatus to value -1
				 */ 
				for (i = 0; i < vert.size(); i++)
				{
					EdgeMap[i][i]= -1;
					EdgeStatus[i][i]=-1;
					for (j = i + 1; j < vert.size(); j++)
					{
						EdgeMap[i][j]= -1;
						EdgeStatus[i][j]=-1;
						EdgeMap[j][i]= -1;
						EdgeStatus[j][i]=-1;
					}
				}
			
				/**
				 * find 2 points that around the two end points of the Delaunay edge
				 * Calculate the distance between these 2 points
				 */ 
				for (k = 0; k < edges.Count; k++)
				{
					Edge myedge=(Edge)edges[k];
					i=findPt(ref vert,myedge.p1.x,myedge.p1.y);
					j=findPt(ref vert,myedge.p2.x,myedge.p2.y);
					double d = vert.get_Renamed(j).X - vert.get_Renamed(i).X;
					double d2 = vert.get_Renamed(j).Y - vert.get_Renamed(i).Y;
					EdgeMap[i][j]=Math.Sqrt( d * d + d2 * d2);
					EdgeStatus[i][j]=0;
					EdgeMap[j][i]= EdgeMap[i][j];
					EdgeStatus[j][i]=0;
				}

				/**
				 * add all the sampling points to pointlist
				 * initialize curveid = -1
				 */ 
				Vision.PointList PL=new Vision.PointList();
				for (i=0 ; i< vert.size(); i++)
				{
					Vision.Point2D pt = new Vision.Point2D(0,0);
					pt.CurveID=-1;
					pt.X= vert.get_Renamed(i).X;
					pt.Y=vert.get_Renamed(i).Y;
					PL.Add(pt);
				}
				#endregion //end region for initialize data

				//Mapping the edge distance. Status=0 means the dalaunay edge.
				bool SearchOver=false;
				int edgepriority=0;  // The priority of edges in some degree.
				do
				{
					#region steps control
					if (crcount>=maxcr&&maxcr!=0)
					{
						SearchOver=true;
						break;
					}
					#endregion
					//Step1 find the shortest distance
					//Comments: To find the shortest distance, we rank the different situations in different priority.
					//Thatis:
					//       1: if the freedom degree is 2, we connect these edges firstly.
					//       2: if the freedom degree is 1, we connnect these edges sencondly.
					//       3: The last step is to connnect the 0-degree edges.
				
					bool edgesearchover=false; // All the adges of edgepriorty are searched over.
					bool edgefilter=false;// Filter of edges in different degree;
					double d1 = 99999D;					
					SearchOver=false;
					head = 0;
					tail=0;
					edgesearchover=true;	
				
					#region find the shortest edge
				
					for ( k = 0; k < vert.size(); k++)
					{
						for (int l1 = k+1; l1 < vert.size(); l1++)
						{
							edgefilter=false;

							if((PointStatus[k])<2&&(PointStatus[l1]<2))
								edgefilter=true;

							if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
							{
								tail = l1;
								head = k;
								d1 = EdgeMap[k][l1]; //the Shortest edge
								edgesearchover=false;

							}		
						}
					
					}
		
					if (edgesearchover) /** Cannot find the shortest edge??*/
					{
						edgepriority++;
					}
					if (edgepriority>2)
					{	
						SearchOver=true; /** get out, not search again */						
					}
					#endregion				
				
					

					/////TO DO : USE DISTANCECOMPARABLE TO IMPROVE RUNNING SPEED.
					if (SearchOver)
					{
						break;  //search over

					}
				
					if (!edgesearchover)                // Shortest points pair will be connected directly if their degree is 2.  Totally free.				
					{
						SearchOver=false;
						ExtendablePoints[0]=-1;
						ExtendablePoints[1]=-1;
						Vision.Point2D pt= (Vision.Point2D)PL[head];
						ExtendablePoints[0]=pt.CurveID;
						pt= (Vision.Point2D)PL[tail];
						ExtendablePoints[1]=pt.CurveID;
						#region free edge
						if (ExtendablePoints[0]+ExtendablePoints[1]==-2)
						{   
							GCurveID++;
							((Vision.Point2D)PL[head]).CurveID=GCurveID;
							((Vision.Point2D)PL[tail]).CurveID=GCurveID;
							Vision.StCurve cr=new Vision.StCurve();
							cr.AddPoint((Vision.Point2D)PL[head]);
							cr.AddPoint((Vision.Point2D)PL[tail]);
							cr.curveID=GCurveID;
							CVL.AddCurve(cr);
							EdgeStatus[head][tail]=1;
							EdgeStatus[tail][head]=1;
                            PointStatus[head] = ((int)PointStatus[head])+1;
                            PointStatus[tail] = ((int)PointStatus[tail])+1;
							crcount++;
						}			
					
						#endregion free edge					
						//Step2 Extend the curve
						//freedom=0 connection
						int extindex=0;	
						int extendpoint=0;
						
						double[] connectivevalue = new double[2];

						if ((ExtendablePoints[0]!=-1)||(ExtendablePoints[1]!=-1))             //if any side has curve, we figure the situation.
						{
							#region clear connective value
							//	for(i=0;i<2;i++) //0--> distance, 1-> connectiveValue
							//	{
							//		connectivevalue[0,i]=-100d;  
							//		connectivevalue[1,i]=-100d;
							//	}
							connectivevalue[0]=-100d;  
							connectivevalue[1]=-100d;
							#endregion
						
							freedomdegree=0;
							extindex=0;
							extendpoint=0;
							for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
							{
								#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
								
								if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
								{
									freedomdegree++;

									Vision.Point2D hp=new Vision.Point2D(0,0);
									Vision.Point2D mp=new Vision.Point2D(0,0);
									Vision.Point2D ep=new Vision.Point2D(0,0);
									
									if (extendablePointsi==0)
									{
										extendpoint=head;
										bestconnectivepoint = tail;
									}
									else
									{
										extendpoint=tail;
										bestconnectivepoint = head;
									}

									int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
									Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
									Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
								
									extindex=mycr.CatchPointEx(newpt);

									//?????? 
									if (extindex>0)
										extindex=-1; //the order is reverse from back to front of the curve

									//////////////////////////
									
									if (extendpoint==head)
									{
										connectivevalue[extendablePointsi]=mycr.DistanceBasedConnectiveValue(extindex,((Vision.Point2D)PL[tail]));										
										
									}
									else
									{
										connectivevalue[extendablePointsi]=mycr.DistanceBasedConnectiveValue(extindex,((Vision.Point2D)PL[head]));										
									}
									connectivevalue[extendablePointsi] = connectivevalue[extendablePointsi] - EdgeMap[head][tail];
									/////////////////////////////////					
								}
								#endregion
							}														
							/**
							 * Find the best point to connect to the existing curve? Minimum
							 */ 	                			
							if ((connectivevalue[0]>0)||(connectivevalue[1]>0))
							{
								#region one side is no free
								if (freedomdegree==1)
								{
									((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;
									Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
									extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
									if (extindex==0)
										cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
									else
										cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);
									EdgeStatus[extendpoint][bestconnectivepoint]=1;
									EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                    //((int)PointStatus[extendpoint])++;
                                    //((int)PointStatus[bestconnectivepoint])++;
                                    PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                    PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
									crcount++; /** curve count*/
								}
								#endregion one side is free

								#region both sides are not free
								if (freedomdegree==2)
								{
									//Join the two curve
								
									Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[head]).CurveID);
									Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[tail]).CurveID);
									int hid=crh.curveID;
									int tid=crt.curveID;
									if(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0)
									{
										if(hid!=tid)
										{
											GCurveID++;
											for(j=0;j<PL.Count;j++)
											{
												if (((Vision.Point2D)PL[j]).CurveID==hid)
													((Vision.Point2D)PL[j]).CurveID=GCurveID;
												if (((Vision.Point2D)PL[j]).CurveID==tid)
													((Vision.Point2D)PL[j]).CurveID=GCurveID;

											}
											Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
											cr.curveID=GCurveID;
										
											/* why remove???*/
											CVL.Remove(crh);
											CVL.Remove(crt);
											CVL.AddCurve(cr);						
										}
										else
											/**same curveID so connection will make a closed curve*/
											crh.Closed=1;							

										EdgeStatus[extendpoint][bestconnectivepoint]=1;
										EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                        //((int)PointStatus[extendpoint])++;
                                        //((int)PointStatus[bestconnectivepoint])++;
                                        PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                        PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
										crcount++;
									}
									#endregion both sides are not free
								}
						
							}

							else
							{
								EdgeStatus[head][tail] = -1;
								EdgeStatus[tail][head] = -1;
							}													
						}
					}						
				}while(!SearchOver);

			return EdgeStatus;
		}

		//Consider distance-based (paper 1)
		internal virtual void  drawDistanceBased(System.Drawing.Graphics g)
		{

				SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
				Delaunay delaunay1 = new Delaunay(vert.size());
				System.Collections.ArrayList nodes=delaunay1.nodes;
				if (nodes.Count == 1)
					/**each point is a rectangle of size 1*/
					g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
					//g.DrawEllipse(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
				if (nodes.Count == 2)
					g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, ((Node) nodes[1]).x, ((Node) nodes[1]).y);

				int maxcr=(int)numericUpDown3.Value;
					int[][] EdgeStatus = compute(ref vert, ref delaunay1, maxcr);

					#region Draw curves
					int i, j;
					for (i = 0; i < vert.size(); i++)
					{
						for (j = i + 1; j < vert.size(); j++)
						{
							if (EdgeStatus[i][j]==1)
							{

								g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
							}

						}
					}
					#endregion draw curves
			
		}
		
		static int[][] computeDISCUR(ref Vertex vert, ref Delaunay delaunay1, int maxcr)
		{
			/**
			 * Construct Delauney Trianglation.		
			 * Find the shortest edge.
			 * if the two end points are free, connect them
			 * if one of the end point is not free, compute connective value
			 */ 
			int bestconnectivepoint=-1;
			int freedomdegree=0;		
			

			#region calculate the delaunay  trianglation
			int GCurveID=0;  /**< specify curveID, each connected point belongs to a curveID. */
			int crcount=0;
			

			Vision.StCurveList CVL= new Vision.StCurveList(); /**< curve list contains connected lines*/
			int [] ExtendablePoints={0,0};
			int i=0,j=0,k;
			int head = 0;
			int tail=0;

			/**
			 * Insert all the point into the delaunay array for delaunay triangulation
			 */ 
			for (i = 0; i < vert.size(); i++)
				delaunay1.Insert(vert.get_Renamed(i).X, vert.get_Renamed(i).Y, 0);

			System.Collections.ArrayList edges=delaunay1.edges;
			System.Collections.ArrayList curveedge=new ArrayList();
			System.Collections.ArrayList curvepoint=new ArrayList();
			#endregion

			#region initialize the data
			/** 	
			 * edgemap contains distance between 2 points if applicable
			 */
			double[][] EdgeMap = new double[vert.size()][];
			/*
			 * edgestatus = -1 -> not delaunay edge
			 * edgestatus = 0 -> delaunay edge
			 * edgestatus = 1 -> connected
			 */
			int [][] EdgeStatus= new int[vert.size()][];
			int [] PointStatus=new int[vert.size()];
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i] = new double[vert.size()];
				EdgeStatus[i]= new int[vert.size()];
				PointStatus[i]=0;
			}
			/**
			 * Initialize all EdgeMap and EdgeSatus to value -1
			 */ 
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i][i]= -1;
				EdgeStatus[i][i]=-1;
				for (j = i + 1; j < vert.size(); j++)
				{
					EdgeMap[i][j]= -1;
					EdgeStatus[i][j]=-1;
					EdgeMap[j][i]= -1;
					EdgeStatus[j][i]=-1;
				}
			}
			
			/**
			 * find 2 points that around the two end points of the Delaunay edge
			 * Calculate the distance between these 2 points
			 */ 
			for (k = 0; k < edges.Count; k++)
			{
				Edge myedge=(Edge)edges[k];
				i=findPt(ref vert, myedge.p1.x,myedge.p1.y);
				j=findPt(ref vert, myedge.p2.x,myedge.p2.y);
				double d = vert.get_Renamed(j).X - vert.get_Renamed(i).X;
				double d2 = vert.get_Renamed(j).Y - vert.get_Renamed(i).Y;
				EdgeMap[i][j]=Math.Sqrt( d * d + d2 * d2);
				EdgeStatus[i][j]=0;
				EdgeMap[j][i]= EdgeMap[i][j];
				EdgeStatus[j][i]=0;
			}

			/**
			 * add all the sampling points to pointlist
			 * initialize curveid = -1
			 */ 
			Vision.PointList PL=new Vision.PointList();
			for (i=0 ; i< vert.size(); i++)
			{
				Vision.Point2D pt = new Vision.Point2D(0,0);
				pt.CurveID=-1;
				pt.X= vert.get_Renamed(i).X;
				pt.Y=vert.get_Renamed(i).Y;
				PL.Add(pt);
			}
			#endregion //end region for initialize data

			//Mapping the edge distance. Status=0 means the dalaunay edge.
			bool SearchOver=false;
			int edgepriority=0;  // The priority of edges in some degree.
			int searchStage = 0; //0-> initial search, 1-> include ignored edge
			do
			{
				#region steps control
				if (crcount>=maxcr&&maxcr!=0)
				{
					SearchOver=true;
					break;
				}
				#endregion
				//Step1 find the shortest distance
				
				bool edgesearchover=false; // All the adges of edgepriorty are searched over.
				bool edgefilter=false;// Filter of edges in different degree;				
				double d1 = 99999D;
				SearchOver=false;
				head = 0;
				tail=0;
				edgesearchover=true; /* = true -> search again */
				
				
				#region find the shortest edge				
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						edgefilter=false;

						if((PointStatus[k])<2&&(PointStatus[l1]<2))
							edgefilter=true; //** point does not lead to intersection situation
					
						/** this is delaunay edge, the point is end point*/
						if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
						{
							tail = l1;
							head = k;
							d1 = EdgeMap[k][l1]; //the Shortest edge
							edgesearchover=false; //shortest delaunay edge is found
						}		
					}					
				}
				#endregion				
				
												
				if (edgesearchover) /** Cannot find the shortest edge??*/
				{
//					edgepriority++;
//
//					if (edgepriority>2)
//					{
//						//SearchOver=true; /** get out, not search again */
//						if (searchStage ==0)
//						{
//							//add back ignored Delaunay edge
//							for ( k = 0; k < vert.size(); k++)
//							{
//								for (int l1 = k+1; l1 < vert.size(); l1++)
//								{		
//									if (EdgeStatus[k][l1]==2)
//									{
//										EdgeStatus[k][l1]=0;
//										EdgeStatus[l1][k]=0;
//									}
//								}
//							}						 
//						}
//
//						
//						if (searchStage==1)
//							SearchOver=true;
//
//						searchStage++;
//
//					}
					SearchOver = true;
				}

				/////TO DO : USE DISTANCECOMPARABLE TO IMPROVE RUNNING SPEED.
				//no shortest edge is found
				if (SearchOver)
				{					
					break;  //search over
				}

				/** if shortest edge is found */
				if (!edgesearchover)                // Shortest points pair will be connected directly if their degree is 2.  Totally free.				
				{
					SearchOver=false;  /** < continue to search for next shortest edge  */
					
					// for initialize purpose 
					ExtendablePoints[0]=-1;
					ExtendablePoints[1]=-1;

					//first end point				
					Vision.Point2D pt= (Vision.Point2D)PL[head];										
					ExtendablePoints[0]=pt.CurveID;

					// second end point
					pt= (Vision.Point2D)PL[tail];
					ExtendablePoints[1]=pt.CurveID;

					/** if these are two free points 
					 *  check the angle 
					 */  
					#region free edge

					if (ExtendablePoints[0]+ExtendablePoints[1]==-2)
					{ 				

							GCurveID++;
							((Vision.Point2D)PL[head]).CurveID=GCurveID;
							((Vision.Point2D)PL[tail]).CurveID=GCurveID;
							Vision.StCurve cr=new Vision.StCurve();
							cr.AddPoint((Vision.Point2D)PL[head]);
							cr.AddPoint((Vision.Point2D)PL[tail]);
							cr.curveID=GCurveID;
							CVL.AddCurve(cr);
							EdgeStatus[head][tail]=1;
							EdgeStatus[tail][head]=1;
                            //((int)PointStatus[head])++;
                            //((int)PointStatus[tail])++;
                            PointStatus[head] = ((int)PointStatus[head])+1;
                            PointStatus[tail]=((int)PointStatus[tail])+1;
							crcount++; /** the number of edge for iteration view?? */					
							
					}			
					
					#endregion
					

					//Step2 Extend the curve
					//freedom=0 connection
					int extindex=0;		
					int extendpoint=0;
					
					double[] connectivevalue = new double[2];

					if ((ExtendablePoints[0]!=-1)||(ExtendablePoints[1]!=-1))             //if any side has curve, we figure the situation.
					{
						#region clear connective value

						connectivevalue[0]=-100d;  
						connectivevalue[1]=-100d;
						#endregion
						
						freedomdegree=0;
						extindex=0;
						extendpoint=0;
						for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
						{
							#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
								
							if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
							{
								freedomdegree++;

								Vision.Point2D hp=new Vision.Point2D(0,0);
								Vision.Point2D mp=new Vision.Point2D(0,0);
								Vision.Point2D ep=new Vision.Point2D(0,0);
									
								if (extendablePointsi==0)
								{
									extendpoint=head;
									bestconnectivepoint = tail;
								}
								else
								{
									extendpoint=tail;
									bestconnectivepoint = head;
								}
								
//								//remove all Delaunay edge associated with this curve endpoint
//								for (i = 0; i< vert.size(); i++)
//								{
//									if ((EdgeStatus[extendpoint][i] == 0)&&(i!=bestconnectivepoint))
//									{
//										EdgeStatus[extendpoint][i] = -1;
//										EdgeStatus[i][extendpoint] = -1;
//									}
//								}

								int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
								Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
								Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
								

								extindex=mycr.CatchPointEx(newpt);


								//?????? 
								if (extindex>0)
									extindex=-1; //the order is reverse from back to front of the curve

								//////////////////////////
									
								if (extendpoint==head)
								{
									connectivevalue[extendablePointsi]=mycr.DistanceBasedConnectiveValue(extindex,((Vision.Point2D)PL[tail]));										
										
								}
								else
								{
									connectivevalue[extendablePointsi]=mycr.DistanceBasedConnectiveValue(extindex,((Vision.Point2D)PL[head]));										
								}
								connectivevalue[extendablePointsi] = connectivevalue[extendablePointsi] - EdgeMap[head][tail];
								/////////////////////////////////					
							}
							#endregion
						}														
						/**
							 * Find the best point to connect to the existing curve? Minimum
							 */ 	                			
						if ((connectivevalue[0]>0)||(connectivevalue[1]>0))
						{
							#region one side is no free
							if (freedomdegree==1)
							{
								((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;
								Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
								extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
								if (extindex==0)
									cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
								else
									cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);
								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
								crcount++; /** curve count*/
															
							}
							#endregion one side is no free

							#region both sides are not free
							if (freedomdegree==2)
							{
								//Join the two curve
								
								Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[head]).CurveID);
								Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[tail]).CurveID);
								int hid=crh.curveID;
								int tid=crt.curveID;
								if(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0)
								{
									if(hid!=tid)
									{
										GCurveID++;
										for(j=0;j<PL.Count;j++)
										{
											if (((Vision.Point2D)PL[j]).CurveID==hid)
												((Vision.Point2D)PL[j]).CurveID=GCurveID;
											if (((Vision.Point2D)PL[j]).CurveID==tid)
												((Vision.Point2D)PL[j]).CurveID=GCurveID;

										}
									Vision.StCurve cr=CVL.ConnetTwoCurves2(ref crh,ref crt, (Vision.Point2D)PL[head],(Vision.Point2D)PL[tail]);
										cr.curveID=GCurveID;
										
										/* why remove???*/
										CVL.Remove(crh);
										CVL.Remove(crt);
										CVL.AddCurve(cr);						
									}
									else
										/**same curveID so connection will make a closed curve*/
										crh.Closed=1;							

									EdgeStatus[extendpoint][bestconnectivepoint]=1;
									EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                    //((int)PointStatus[extendpoint])++;
                                    //((int)PointStatus[bestconnectivepoint])++;
                                    PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                    PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
									crcount++;
								}
								#endregion both sides are not free
							}
						
						}
						else
						{
							//marked edge for later reconsideration
							EdgeStatus[head][tail] = 2;
							EdgeStatus[tail][head] = 2;
						}		
						
					}

					//remove delaunay edges
					for (int t=1;t<=2; t++)
					{
						if (t==1)
						{
							bestconnectivepoint = head;
							extendpoint = tail;
						}
						else
						{
							bestconnectivepoint = tail;
							extendpoint = head;
						}

						
						ArrayList temp = ((Vision.Point2D)PL[bestconnectivepoint]).potentialConnectedPoint;
						

						temp.Add(extendpoint);
						
						if (temp.Count==2)
						{
							for ( k = 0; k < vert.size(); k++)
							{								
								//remove other delaunay edges
								if ((EdgeStatus[k][bestconnectivepoint]==0)&&(k!=(int)temp[1])&&(k!=(int)temp[0]))
								{
									EdgeStatus[k][bestconnectivepoint]=-1;
									EdgeStatus[bestconnectivepoint][k]=-1;
								}									
							}	 
						}
					}
				}						
			}while(!SearchOver);

			#region draft1
			//add back ignored edges
//			for ( k = 0; k < vert.size(); k++)
//			{
//				for (int l1 = k+1; l1 < vert.size(); l1++)
//				{		
//					if ((EdgeStatus[k][l1]==2)&&(EdgeStatus[l1][k]==2))
//					{
//						EdgeStatus[k][l1]=0;
//						EdgeStatus[l1][k]=0;
//					}
//				}
//			}	
//
//			//assign edgeEndPoint
//			for ( k = 0; k < vert.size(); k++)
//			{
//				for (int l1 = k+1; l1 < vert.size(); l1++)
//				{		
//					if ((EdgeStatus[k][l1]==0)&&(EdgeStatus[l1][k]==0))
//					{
//						((Vision.Point2D)PL[k]).edgeEndPoint.Add((Vision.Point2D)PL[l1]);
//						((Vision.Point2D)PL[l1]).edgeEndPoint.Add((Vision.Point2D)PL[k]);
//					}
//				}
//			}			
//
//			do{
//				SearchOver=true;
//				head = 0;
//				tail=0;
//				double d1 = 99999D;
//				int counter = -1; 
//
//				
//				#region find the shortest edge				
//				for ( k = 0; k < vert.size(); k++)
//				{
//					for (int l1 = k+1; l1 < vert.size(); l1++)
//					{
//						if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0)
//						{
//							tail = l1;
//							head = k;
//							d1 = EdgeMap[k][l1]; //the Shortest edge
//							SearchOver=false; //shortest delaunay edge is found
//						}		
//					}					
//				}
//				#endregion		
//
//				//if shortest edge not found
//				if (SearchOver)
//					break;
//				else
//				//if (!SearchOver) //if found
//				{
//					Vision.Point2D p1=null;
//					Vision.Point2D p2=null;
//					do
//					{
//						#region steps control
//						if (crcount>=maxcr&&maxcr!=0)
//						{
//							SearchOver=true;
//							break;
//						}
//						#endregion
//
//						//an
//						if ((p1!=null)&&(p2!=null))
//						{
//							head = PL.IndexOf((Vision.Point2D)p1,0,PL.Count);
//							tail = PL.IndexOf((Vision.Point2D)p2,0,PL.Count);
//						}
//
//
//						// for initialize purpose 
//						ExtendablePoints[0]=-1;
//						ExtendablePoints[1]=-1;
//
//						//first end point				
//						Vision.Point2D pt= (Vision.Point2D)PL[head];										
//						ExtendablePoints[0]=pt.CurveID;
//
//						// second end point
//						pt= (Vision.Point2D)PL[tail];
//						ExtendablePoints[1]=pt.CurveID;
//
//						//Step2 Extend the curve
//						//freedom=0 connection
//						int extindex=0;		
//						int extendpoint=0;
//				
//						double[] connectivevalue = new double[2];
//						//	Vision.Point2D oldCurvePoint;
//
//
//						if ((ExtendablePoints[0]!=-1)||(ExtendablePoints[1]!=-1))             //if any side has curve, we figure the situation.
//						{
//							#region clear connective value
//
//							connectivevalue[0]=-100d;  
//							connectivevalue[1]=-100d;
//							#endregion
//					
//							freedomdegree=0;
//							extindex=0;
//							extendpoint=0;
//							for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
//							{
//								#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
//							
//								if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
//								{
//									freedomdegree++;
//
//									Vision.Point2D hp=new Vision.Point2D(0,0);
//									Vision.Point2D mp=new Vision.Point2D(0,0);
//									Vision.Point2D ep=new Vision.Point2D(0,0);
//								
//									if (extendablePointsi==0)
//									{
//										extendpoint=head;
//										bestconnectivepoint = tail;
//									}
//									else
//									{
//										extendpoint=tail;
//										bestconnectivepoint = head;
//									}
//
//									int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
//									Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
//									Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
//							
//
//									extindex=mycr.CatchPointEx(newpt);
//
//									if (extindex>0)
//										extindex=-1; //the order is reverse from back to front of the curve
//
//									//////////////////////////
//								
//									if (extendpoint==head)
//									{
//										connectivevalue[extendablePointsi]=mycr.DistanceBasedConnectiveValue(extindex,((Vision.Point2D)PL[tail]));										
//									
//									}
//									else
//									{
//										connectivevalue[extendablePointsi]=mycr.DistanceBasedConnectiveValue(extindex,((Vision.Point2D)PL[head]));										
//									}
//									connectivevalue[extendablePointsi] = connectivevalue[extendablePointsi] - EdgeMap[head][tail];
//									/////////////////////////////////					
//								}
//								#endregion
//							}														
//							/**
//									* Find the best point to connect to the existing curve? Minimum
//									*/ 	      
//							
//							p1 = (Vision.Point2D)PL[head];
//							p2 = (Vision.Point2D)PL[tail];
//							#region connectable	
//							if ((connectivevalue[0]>0)||(connectivevalue[1]>0))
//							{
//
//								//update the adjacent delaunay vertices for each point
////								((Vision.Point2D)PL[head]).edgeEndPoint.Remove((Vision.Point2D)PL[tail]);
////								((Vision.Point2D)PL[tail]).edgeEndPoint.Remove((Vision.Point2D)PL[head]);
//
//								//An
//								//p2.edgeEndPoint.Remove((Vision.Point2D)PL[extendpoint]);
//								if (p1.CurveID==-1)
//								{
//									p2 = p1;
//								}
//								else
//								{
//									Vision.StCurve cr=CVL.ReadCurve(p1.CurveID);
//									if ((Vision.Point2D)cr.ReadTail() == p1)
//									{p2 = cr.ReadHead();}
//									else
//									
//									{p2 = cr.ReadTail();}								
//								}
//											
//								if (((Vision.Point2D)p2.edgeEndPoint[p2.edgeEndPoint.Count-1])!=p1)
//								{
//
//									p1 = (Vision.Point2D)p2.edgeEndPoint[p2.edgeEndPoint.Count-1]; 
//								}
//								else
//									p1 = (Vision.Point2D)p2.edgeEndPoint[p2.edgeEndPoint.Count-2]; 
//
//								counter = 0;
//
//								//update the curve
//								#region one side is no free
//								if (freedomdegree==1)
//								{
//									((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;
//									Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
//									cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
//									
//									extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
//									if (extindex==0)
//										cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
//									else
//										cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);
//									EdgeStatus[extendpoint][bestconnectivepoint]=1;
//									EdgeStatus[bestconnectivepoint][extendpoint]=1;
//									((int)PointStatus[extendpoint])++;
//									((int)PointStatus[bestconnectivepoint])++;
//									crcount++; /** curve count*/
//														
//								}
//								#endregion one side is no free
//
//								#region both sides are not free
//								if (freedomdegree==2)
//								{
//									//Join the two curve
//							
//									Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[head]).CurveID);
//									Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[tail]).CurveID);
//									int hid=crh.curveID;
//									int tid=crt.curveID;
//									if(hid!=tid)
//									{
//										GCurveID++;
//										for(j=0;j<PL.Count;j++)
//										{
//											if (((Vision.Point2D)PL[j]).CurveID==hid)
//												((Vision.Point2D)PL[j]).CurveID=GCurveID;
//											if (((Vision.Point2D)PL[j]).CurveID==tid)
//												((Vision.Point2D)PL[j]).CurveID=GCurveID;
//
//										}
//										Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
//										cr=CVL.ConnetTwoCurves(ref crh,ref crt);
//										cr.curveID=GCurveID;
//								
//										/* why remove???*/
//										CVL.Remove(crh);
//										CVL.Remove(crt);
//										CVL.AddCurve(cr);						
//									}
//									else
//										/**same curveID so connection will make a closed curve*/
//										crh.Closed=1;							
//
//									EdgeStatus[extendpoint][bestconnectivepoint]=1;
//									EdgeStatus[bestconnectivepoint][extendpoint]=1;
//									((int)PointStatus[extendpoint])++;
//									((int)PointStatus[bestconnectivepoint])++;
//									crcount++;
//				
//									
//								}
//								#endregion both sides are not free
//							}
//							else
//							{							
//								EdgeStatus[head][tail] = 2;
//								EdgeStatus[tail][head] = 2;
//								counter ++;
//
//								if (counter ==0)
//									break;
//								else if (counter==2)
//									break;
//								else 
//								{
//									Vision.StCurve cr=CVL.ReadCurve(p2.CurveID);
//									if ((Vision.Point2D)cr.ReadHead()!=p2)
//									{p2 = cr.ReadHead();}
//									else
//									{p2 = cr.ReadTail();}
//									p1 = (Vision.Point2D)p2.edgeEndPoint[p2.edgeEndPoint.Count-1];
//								}
//									
//							}				
//							#endregion connectable
//						} // if anyside as curves
//					}while(counter<2); //if shortest edge is found
//				}
//
//			}while(!SearchOver);
			#endregion draft1

			#region draft
			bool extend=false;
			//newly added 
			do 
			{
				//add back ignored edge
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{		
						if (EdgeStatus[k][l1]==2)
						{
							EdgeStatus[k][l1]=0;
							EdgeStatus[l1][k]=0;
						}
					}
				}		
				
				extend=false;
				bool edgesearchover=false; // All the adges of edgepriorty are searched over.
				bool edgefilter=false;// Filter of edges in different degree;				
				double d1 = 99999D;
				//find shortest edge
				do
				{
					#region steps control
					if (crcount>=maxcr&&maxcr!=0)
					{
						SearchOver=true;
						break;
					}
					#endregion
					//Step1 find the shortest distance
				

					SearchOver=false;
					head = 0;
					tail=0;
					edgesearchover=true; /* = true -> search again */
					
					#region find the shortest edge				
					for ( k = 0; k < vert.size(); k++)
					{
						for (int l1 = k+1; l1 < vert.size(); l1++)
						{
//							edgefilter=false;
//
//							if((PointStatus[k])<2&&(PointStatus[l1]<2))
//								edgefilter=true; //** point does not lead to intersection situation
					
							/** this is delaunay edge, the point is end point*/
//							if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
							if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0)
							{
								tail = l1;
								head = k;
								d1 = EdgeMap[k][l1]; //the Shortest edge
								edgesearchover=false; //shortest delaunay edge is found
							}		
						}					
					}
					#endregion		

					//if shortest edge found
					if (!edgesearchover)
					{
						// for initialize purpose 
						ExtendablePoints[0]=-1;
						ExtendablePoints[1]=-1;

						//first end point				
						Vision.Point2D pt= (Vision.Point2D)PL[head];										
						ExtendablePoints[0]=pt.CurveID;

						// second end point
						pt= (Vision.Point2D)PL[tail];
						ExtendablePoints[1]=pt.CurveID;

						/** if these are two free points 
						 */  
//						#region free edge
//
//						if (ExtendablePoints[0]+ExtendablePoints[1]==-2)
//						{ 				
//
//							GCurveID++;
//							((Vision.Point2D)PL[head]).CurveID=GCurveID;
//							((Vision.Point2D)PL[tail]).CurveID=GCurveID;
//							Vision.StCurve cr=new Vision.StCurve();
//							cr.AddPoint((Vision.Point2D)PL[head]);
//							cr.AddPoint((Vision.Point2D)PL[tail]);
//							cr.curveID=GCurveID;
//							CVL.AddCurve(cr);
//							EdgeStatus[head][tail]=1;
//							EdgeStatus[tail][head]=1;
//							((int)PointStatus[head])++;
//							((int)PointStatus[tail])++;
//							crcount++; /** the number of edge for iteration view?? */					
//							extend = true;
//						}			
//					
//						#endregion
					

						//Step2 Extend the curve
						//freedom=0 connection
						int extindex=0;		
						int extendpoint=0;
					
						double[] connectivevalue = new double[2];

						if ((ExtendablePoints[0]!=-1)||(ExtendablePoints[1]!=-1))             //if any side has curve, we figure the situation.
						{
							#region clear connective value

							connectivevalue[0]=-100d;  
							connectivevalue[1]=-100d;
							#endregion
						
							freedomdegree=0;
							extindex=0;
							extendpoint=0;
							for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
							{
								#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
								
								if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
								{
									freedomdegree++;

									Vision.Point2D hp=new Vision.Point2D(0,0);
									Vision.Point2D mp=new Vision.Point2D(0,0);
									Vision.Point2D ep=new Vision.Point2D(0,0);
									
									if (extendablePointsi==0)
									{
										extendpoint=head;
										bestconnectivepoint = tail;
									}
									else
									{
										extendpoint=tail;
										bestconnectivepoint = head;
									}

									int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
									Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
									Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
								

									extindex=mycr.CatchPointEx(newpt);

									if (extindex>0)
										extindex=-1; //the order is reverse from back to front of the curve

									//////////////////////////
									
									if (extendpoint==head)
									{
										connectivevalue[extendablePointsi]=mycr.DistanceBasedConnectiveValue(extindex,((Vision.Point2D)PL[tail]));										
										
									}
									else
									{
										connectivevalue[extendablePointsi]=mycr.DistanceBasedConnectiveValue(extindex,((Vision.Point2D)PL[head]));										
									}
									connectivevalue[extendablePointsi] = connectivevalue[extendablePointsi] - EdgeMap[head][tail];
									/////////////////////////////////					
								}
								#endregion
							}														
							/**
								 * Find the best point to connect to the existing curve? Minimum
								 */ 	                			
							if ((connectivevalue[0]>0)||(connectivevalue[1]>0))
							{
								extend = true;
								#region one side is no free
								if (freedomdegree==1)
								{
									((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;
									Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
									extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
									if (extindex==0)
										cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
									else
										cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);
									EdgeStatus[extendpoint][bestconnectivepoint]=1;
									EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                    //((int)PointStatus[extendpoint])++;
                                    //((int)PointStatus[bestconnectivepoint])++;
                                    PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                    PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
									crcount++; /** curve count*/
															
								}
								#endregion one side is no free

								#region both sides are not free
								if (freedomdegree==2)
								{
									//Join the two curve
								
									Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[head]).CurveID);
									Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[tail]).CurveID);
									int hid=crh.curveID;
									int tid=crt.curveID;
									if(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0)
									{
										if(hid!=tid)
										{
											GCurveID++;
											for(j=0;j<PL.Count;j++)
											{
												if (((Vision.Point2D)PL[j]).CurveID==hid)
													((Vision.Point2D)PL[j]).CurveID=GCurveID;
												if (((Vision.Point2D)PL[j]).CurveID==tid)
													((Vision.Point2D)PL[j]).CurveID=GCurveID;

											}
											Vision.StCurve cr=CVL.ConnetTwoCurves2(ref crh,ref crt,(Vision.Point2D)PL[head],(Vision.Point2D)PL[tail]);
											cr.curveID=GCurveID;
										
											/* why remove???*/
											CVL.Remove(crh);
											CVL.Remove(crt);
											CVL.AddCurve(cr);						
										}
										else
											/**same curveID so connection will make a closed curve*/
											crh.Closed=1;							

										EdgeStatus[extendpoint][bestconnectivepoint]=1;
										EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                        //((int)PointStatus[extendpoint])++;
                                        //((int)PointStatus[bestconnectivepoint])++;
                                        PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                        PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
										crcount++;
									}
									#endregion both sides are not free
								}
						
							}
							else
							{
								//marked edge for later reconsideration
								EdgeStatus[head][tail] = 2;
								EdgeStatus[tail][head] = 2;
							}								
						}
					}
				}while(!edgesearchover);

				if (!extend)
					break;

			}while (extend);

			#endregion draft

			return EdgeStatus;
		}

		static int[,] computeDISCURWrapper(float[] coords)
		{
			int i, j;
			int maxcr = 0;	// default value
			Vertex vert = new Vertex();

			double maxabsx = 0.0, maxabsy = 0.0;

			for (i = 0; i < coords.Length/2; i++)
			{
				if (Math.Abs(coords[i*2]) > maxabsx)
					maxabsx = Math.Abs(coords[i*2]);

				if (Math.Abs(coords[i*2 + 1]) > maxabsy)
					maxabsy = Math.Abs(coords[i*2 + 1]);
			}

			double maxabs = maxabsx;

			if (maxabsy > maxabsx)
				maxabs = maxabsy;

			//double factor = int.MaxValue/maxabs;
			double factor = 1024/maxabs;

			for (i = 0; i < coords.Length/2; i++)
			{
				vert.add((int)Math.Floor(coords[i*2]*factor), (int)Math.Floor(coords[i*2 + 1]*factor));
//				Console.WriteLine(string.Format("{0} {1}", (int)Math.Floor(coords[i*2]*factor), (int)Math.Floor(coords[i*2 + 1]*factor)));
			}

			Delaunay delaunay1 = new Delaunay(vert.size());

			int[][] EdgeStatus = computeDISCUR(ref vert, ref delaunay1, maxcr);

// Console.WriteLine("EdgeStatus[0][0]={0}", EdgeStatus[0][0]);
// Console.WriteLine("EdgeStatus[1][1]={0}", EdgeStatus[1][1]);

			int[,] testarray = new int[vert.size(), vert.size()];

			for (j = 0; j < vert.size(); j++)
				for (i = 0; i < vert.size(); i++)
					testarray[j,i] = EdgeStatus[j][i];

			return testarray;
		}

		/**  
		 * Curve reconstruction based on distance.
		 * System.Drawing.Graphics argument
		 * @see DistanceBasedConnectiveValue()
		 */ 		
	    public virtual void  drawDistanceBasedEx(System.Drawing.Graphics g)
		{
			int i, j;
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
			Delaunay delaunay1 = new Delaunay(vert.size());			
			int maxcr=(int)numericUpDown3.Value;

			System.Collections.ArrayList nodes=delaunay1.nodes;

			if (nodes.Count == 1)
				//**each point is a rectangle of size 1*/
				g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
			//	g.DrawEllipse(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
			if (nodes.Count == 2)
				g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, ((Node) nodes[1]).x, ((Node) nodes[1]).y);

			int[][] EdgeStatus = computeDISCUR(ref vert, ref delaunay1, maxcr);


			//draw Delaunay edge
			#region Draw Delaunay edge
			if (drawDEx == true)
			{
				SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Magenta);
				for ( i = 0; i < vert.size(); i++)
				{
					for (j = i + 1; j < vert.size(); j++)
					{
						if (EdgeStatus[i][j]!=-1)
						{
							g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
						}

					}
				}
			}
			#endregion Delaunay edge
			/////			


			#region Draw curves
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
			
			for (i = 0; i < vert.size(); i++)
			{
				for (j = i + 1; j < vert.size(); j++)
				{
					if (EdgeStatus[i][j]==1)
					{
						g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
					}

				}
			}
			#endregion draw curves
		}

		/*******************************************/
		//
		//Consider distance and angle with weight
		// current algorithm: first segment is considered 
		//                    connectivity value of all points attached to delaunay edge
		//
		internal virtual void  drawAngleDistance2Delauney(System.Drawing.Graphics g)
		{
			// * Construct Delauney Trianglation.		
			// * Find the shortest edge.
			double [,] connectivevalue=new double[2,vert.size()];
			int bestconnectivepoint=-1;
			int bestextenalpoint=-1;
			double bestconnectivevalue=-20;
			int freedomdegree=0;
			
			#region calculate the delaunay  trianglation
			int GCurveID=0;
			int maxcr=(int)numericUpDown3.Value;
			int crcount=0;
			
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
			Delaunay delaunay1 = new Delaunay(vert.size());

			/**
			 *  op is not used 
			 * 	Vision.Optimismfilter op=new Vision.Optimismfilter();
			 */

			Vision.StCurveList CVL= new Vision.StCurveList();
			int [] ExtendablePoints={0,0};
			int i=0,j=0,k;
			int head = 0;
			int tail=0;

			/**
			 * Insert all the point into the delaunay array for delaunay triangulation
			 */ 
			for (i = 0; i < vert.size(); i++)
				delaunay1.Insert(vert.get_Renamed(i).X, vert.get_Renamed(i).Y, 0);
			System.Collections.ArrayList nodes=delaunay1.nodes;
			System.Collections.ArrayList edges=delaunay1.edges;
			System.Collections.ArrayList curveedge=new ArrayList();
			System.Collections.ArrayList curvepoint=new ArrayList();
			if (nodes.Count == 1)
				//**each point is a rectangle of size 1*/
				g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
				//g.DrawEllipse(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
			if (nodes.Count == 2)
				g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, ((Node) nodes[1]).x, ((Node) nodes[1]).y);
			#endregion

			#region initialize the data
			/** 	
			 * edgemap contains distance between 2 points if applicable
			 */
			double[][] EdgeMap = new double[vert.size()][];
			/*
			 * edgestatus = -1 -> not delaunay edge
			 * edgestatus = 0 -> delaunay edge
			 * edgestatus = 1 -> connected
			 */
			int [][] EdgeStatus= new int[vert.size()][];
			int [] PointStatus=new int[vert.size()];
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i] = new double[vert.size()];
				EdgeStatus[i]= new int[vert.size()];
				PointStatus[i]=0;
			}
			/**
			 * Initialize all EdgeMap and EdgeSatus to value -1
			 */ 
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i][i]= -1;
				EdgeStatus[i][i]=-1;
				for (j = i + 1; j < vert.size(); j++)
				{
					EdgeMap[i][j]= -1;
					EdgeStatus[i][j]=-1;
					EdgeMap[j][i]= -1;
					EdgeStatus[j][i]=-1;
				}
			}
			
			/**
			 * find 2 points that around the two end points of the Delaunay edge
			 * Calculate the distance between these 2 points
			 */ 
			for (k = 0; k < edges.Count; k++)
			{
				Edge myedge=(Edge)edges[k];
				i=findPt(myedge.p1.x,myedge.p1.y);
				j=findPt(myedge.p2.x,myedge.p2.y);
				double d = vert.get_Renamed(j).X - vert.get_Renamed(i).X;
				double d2 = vert.get_Renamed(j).Y - vert.get_Renamed(i).Y;
				EdgeMap[i][j]=Math.Sqrt( d * d + d2 * d2);
				EdgeStatus[i][j]=0;
				EdgeMap[j][i]= EdgeMap[i][j];
				EdgeStatus[j][i]=0;
			}

			/**
			 * add all the sampling points to pointlist
			 * initialize curveid = -1
			 */ 
			Vision.PointList PL=new Vision.PointList();
			for (i=0 ; i< vert.size(); i++)
			{
				Vision.Point2D pt = new Vision.Point2D(0,0);
				pt.CurveID=-1;
				pt.X= vert.get_Renamed(i).X;
				pt.Y=vert.get_Renamed(i).Y;
				PL.Add(pt);
			}
			#endregion //end region for initialize data

			//Mapping the edge distance. Status=0 means the dalaunay edge.
			bool SearchOver=false;
			int edgepriority=0;  // The priority of edges in some degree.
			int searchStage = 0; //0-> initial search, 1-> include ignored edge
			do
			{
				#region steps control
				if (crcount>=maxcr&&maxcr!=0)
				{
					SearchOver=true;
					break;
				}
				#endregion
				//Step1 find the shortest distance
				//That is:
				//       1: if the freedom degree is 2, we connect these edges firstly.
				//       2: if the freedom degree is 1, we connnect these edges sencondly.				
				
				bool edgesearchover=false; // All the adges of edgepriorty are searched over.
				bool edgefilter=false;// Filter of edges in different degree;
				//bool includeignoredEdge = false; //false -> exempt ignored Delaunay edge from searching				
				double d1 = 99999D;
				SearchOver=false;
				head = 0;
				tail=0;
				edgesearchover=true; /* = true -> search again */
				
				#region find the shortest edge				
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						edgefilter=false;

						if((PointStatus[k])<2&&(PointStatus[l1]<2))
							edgefilter=true; //** point does not lead to intersection situation
					
						/** this is delaunay edge, the point is end point*/
						if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
						{
							tail = l1;
							head = k;
							d1 = EdgeMap[k][l1]; //the Shortest edge
							edgesearchover=false; //shortest delaunay edge is found
						}		
					}					
				}
								
				if (edgesearchover) /** Cannot find the shortest edge??*/
				{
					edgepriority++;

					if (edgepriority>2)
					{
						//SearchOver=true; /** get out, not search again */
						if (searchStage ==0)
						{
							for ( k = 0; k < vert.size(); k++)
							{
								for (int l1 = k+1; l1 < vert.size(); l1++)
								{		
									if (EdgeStatus[k][l1]==2)
									{
										EdgeStatus[k][l1]=0;
										EdgeStatus[l1][k]=0;
									}
								}
							}						 
						}

						
						if (searchStage==2)
							SearchOver=true;
						searchStage++;

					}
				}


				#endregion				
				
					
				/////TO DO : USE DISTANCECOMPARABLE TO IMPROVE RUNNING SPEED.
				if (SearchOver)
				{
					break;  //search over
				}
				
				/** if shortest edge is found */
				if (!edgesearchover)                // Shortest points pair will be connected directly if their degree is 2.  Totally free.				
				{
					SearchOver=false;  /** < continue to search for next shortest edge  */
					
					// for initialize purpose 
					ExtendablePoints[0]=-1;
					ExtendablePoints[1]=-1;

					//first end point				
					Vision.Point2D pt= (Vision.Point2D)PL[head];										
					ExtendablePoints[0]=pt.CurveID;

					// second end point
					pt= (Vision.Point2D)PL[tail];
					ExtendablePoints[1]=pt.CurveID;

					/** if these are two free points 
					 *  check the angle 
					 */  
					#region free edge

					if (ExtendablePoints[0]+ExtendablePoints[1]==-2)
					{ 						
						p=1; //for connecting method 1-> head is currently considered, 2-> tail
						// ... code here
						if (connecting(EdgeStatus, PointStatus, EdgeMap, head, tail, PL))
						{
							GCurveID++;
							((Vision.Point2D)PL[head]).CurveID=GCurveID;
							((Vision.Point2D)PL[tail]).CurveID=GCurveID;
							Vision.StCurve cr=new Vision.StCurve();
							cr.AddPoint((Vision.Point2D)PL[head]);
							cr.AddPoint((Vision.Point2D)PL[tail]);
							cr.curveID=GCurveID;
							CVL.AddCurve(cr);
							EdgeStatus[head][tail]=1;
							EdgeStatus[tail][head]=1;
                            //((int)PointStatus[head])++;
                            //((int)PointStatus[tail])++;
                            PointStatus[head] = ((int)PointStatus[head]) + 1;
                            PointStatus[tail] = ((int)PointStatus[tail]) + 1;
							crcount++; /** the number of edge for iteration view?? */												
						}
						else 
						{					
							EdgeStatus[head][tail]=2; //ignored from searching shortest edge
							EdgeStatus[tail][head]=2; //ignored from searching shortest edge
						}
					}			
					
					#endregion
					

					//Step2 Extend the curve
					//enpoint(s) is/are not free
					#region endpoint(s) is/are not free
					int extindex=0;	
					int extendpoint=0;
					
					if ((ExtendablePoints[0]!=-1)||(ExtendablePoints[1]!=-1))             //if any side has curve, we figure the situation.
					{
						#region clear connective value
						for(i=0;i<vert.size();i++)
						{
							connectivevalue[0,i]=-100d;
							connectivevalue[1,i]=-100d;
						}
						#endregion
						
						freedomdegree=0;

					//	extindex=0;
					//	extendpoint=0;
						for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
						{
							#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
								
							if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
							{
								freedomdegree++;

								Vision.Point2D hp=new Vision.Point2D(0,0);
								Vision.Point2D mp=new Vision.Point2D(0,0);
								Vision.Point2D ep=new Vision.Point2D(0,0);
									
								if (extendablePointsi==0)
									extendpoint=head;
								else
									extendpoint=tail;
									
								int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
								Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
								Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
									
								extindex=mycr.CatchPointEx(newpt);

								//if extindex = -1, the endpoint is not at index 0
								if (extindex>0)								
									extindex=-1;

								if (mycr.Count >=3)
								{
									 mycr = crDetectOutlier(mycr, extindex);
								}
							
								//////////////////////////
								///iterate all the points within a defined area (shortest edge*2)
								for (j = 0; j < vert.size(); j++)
								{
								//	if ((EdgeStatus[extendpoint][j]==0)&&(EdgeMap[extendpoint][j]<=EdgeMap[head][tail]*2)) 
										if ((EdgeStatus[extendpoint][j]==0)) 
									{							
										// calcu the entendable length of each edge.
										connectivevalue[extendablePointsi,j]=mycr.ConnectiveValue(extindex,Convert.ToDouble(vert.get_Renamed(j).X),Convert.ToDouble(vert.get_Renamed(j).Y));
									}
								}
								/////////////////////////////////					
							}
							#endregion
						}

						/**
							 * Find the best point to connect to the existing curve?
							 */ 	                
						#region find the best point
						bestconnectivevalue=-50d;
						bestextenalpoint=-1; /**< 0 for head, 1 for tai?? */
						bestconnectivepoint=-1; 
						for(i=0;i<2;i++)
							for(j=0;j<vert.size();j++)
							{
								if((connectivevalue[i,j]>bestconnectivevalue)&&(PointStatus[j]<2))
								{
									bestconnectivevalue=connectivevalue[i,j];
									bestextenalpoint=i	;
									bestconnectivepoint=j;
								}
							}
						#endregion

						#region Update the curves
						if(bestconnectivevalue<7.5d) /** not be able to connect */ //this is for boundary???
						{
							if (bestextenalpoint==0)
								extendpoint=head;
							else
								extendpoint=tail;

                            PointStatus[extendpoint] = ((int)PointStatus[extendpoint])+1;
						}
						else if(bestconnectivevalue>7.5d)
						{				
							if (bestextenalpoint==0)
								extendpoint=head;
							else
								extendpoint=tail;

							#region one side is no free
							//An 
							//An: defined tempID
							int tempId = 	((Vision.Point2D)PL[bestconnectivepoint]).CurveID;
							//if (freedomdegree==1)
							if (tempId ==-1)
							{
								((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;
								Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
								extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
								if (extindex==0)
									cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
								else
									cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint])+1;
                                PointStatus[bestconnectivepoint]=((int)PointStatus[bestconnectivepoint])+1;
								crcount++; /** curve count*/

								//delaunay edge
								/*for ( k = 0; k < vert.size(); k++)
								{																		
									if (EdgeStatus[bestconnectivepoint][k]==2)
									{
										EdgeStatus[k][bestconnectivepoint]=0;
										EdgeStatus[bestconnectivepoint][k]=0;
									}
								}	*/
							  }
							  #endregion
								
								#region both sides are not free
								//if (freedomdegree==2)
							if (tempId != -1)
								{
									//Join the two curve
									
							//		Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[head]).CurveID);
							//		Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[tail]).CurveID);

									Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
									Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[bestconnectivepoint]).CurveID);

									int hid=crh.curveID;
									int tid=crt.curveID;
									//if ((extendablePointsi==0)&&(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0))
									//fzconnection++;
									if(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0)
									{
										if(hid!=tid)
										{
											GCurveID++;
											for(j=0;j<PL.Count;j++)
											{
												if (((Vision.Point2D)PL[j]).CurveID==hid)
													((Vision.Point2D)PL[j]).CurveID=GCurveID;
												if (((Vision.Point2D)PL[j]).CurveID==tid)
													((Vision.Point2D)PL[j]).CurveID=GCurveID;

											}
											Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
											cr.curveID=GCurveID;							

											/* why remove???*/
											CVL.Remove(crh);
											CVL.Remove(crt);
											CVL.AddCurve(cr);						
										}
										else
											/**same curveID so connection will make a closed curve*/
											crh.Closed=1;							

										EdgeStatus[extendpoint][bestconnectivepoint]=1;
										EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                        //((int)PointStatus[extendpoint])++;
                                        //((int)PointStatus[bestconnectivepoint])++;
                                        PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                        PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;

										crcount++;
									}
								}
								#endregion

						}
						#endregion update the curve
					}						
					#endregion endpoints not free
				}
				
			}while(!SearchOver);

			//Draw curves
			#region Draw curves
			for (i = 0; i < vert.size(); i++)
			{
				for (j = i + 1; j < vert.size(); j++)
				{
					if (EdgeStatus[i][j]==1)
					{
						g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
					}

				}
			}
			#endregion 
			/////						
		}
		

		/// <summary>
		/// Angle + distance: Algorithm 3
		/// first segment is considered, points within defined area are considered
		/// </summary>
		/// <param name="mycurve"></param>
		/// <param name="index"></param>
		/// <returns></returns>
		/// 


		internal virtual void  drawAngleDistance3DelauneyVersion2(System.Drawing.Graphics g)
		{
			// * Construct Delauney Trianglation.		
			// * Find the shortest edge.
			double [,] connectivevalue=new double[2,vert.size()];
			int bestconnectivepoint=-1;
			int bestextenalpoint=-1;
			double bestconnectivevalue=-20;
			int freedomdegree=0;
			double range=1.849; //1.8; 
			#region calculate the delaunay  trianglation
			int GCurveID=0;
			int maxcr=(int)numericUpDown3.Value;
			int crcount=0;
			
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
			Delaunay delaunay1 = new Delaunay(vert.size());

			/**
			 *  op is not used 
			 * 	Vision.Optimismfilter op=new Vision.Optimismfilter();
			 */

			Vision.StCurveList CVL= new Vision.StCurveList();
			int [] ExtendablePoints={0,0};
			int i=0,j=0,k;
			int head = 0;
			int tail=0;

			/**
			 * Insert all the point into the delaunay array for delaunay triangulation
			 */ 
			for (i = 0; i < vert.size(); i++)
				delaunay1.Insert(vert.get_Renamed(i).X, vert.get_Renamed(i).Y, 0);


			System.Collections.ArrayList nodes=delaunay1.nodes;
			System.Collections.ArrayList edges=delaunay1.edges;
			System.Collections.ArrayList curveedge=new ArrayList();
			System.Collections.ArrayList curvepoint=new ArrayList();
			if (nodes.Count == 1)
				//**each point is a rectangle of size 1*/				
				g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
				//g.DrawEllipse(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
			if (nodes.Count == 2)
				g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, ((Node) nodes[1]).x, ((Node) nodes[1]).y);
			#endregion

			#region initialize the data
			/** 	
			 * edgemap contains distance between 2 points if applicable
			 */
			double[][] EdgeMap = new double[vert.size()][];
			/*
			 * edgestatus = -1 -> not delaunay edge
			 * edgestatus = 0 -> delaunay edge
			 * edgestatus = 1 -> connected
			 */
			int [][] EdgeStatus= new int[vert.size()][];
			int [] PointStatus=new int[vert.size()];
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i] = new double[vert.size()];
				EdgeStatus[i]= new int[vert.size()];
				PointStatus[i]=0;
			}
			/**
			 * Initialize all EdgeMap and EdgeSatus to value -1
			 */ 
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i][i]= -1;
				EdgeStatus[i][i]=-1;
				for (j = i + 1; j < vert.size(); j++)
				{
					EdgeMap[i][j]= -1;
					EdgeStatus[i][j]=-1;
					EdgeMap[j][i]= -1;
					EdgeStatus[j][i]=-1;
				}
			}
			
			/**
			 * find 2 points that around the two end points of the Delaunay edge
			 * Calculate the distance between these 2 points
			 */ 
			for (k = 0; k < edges.Count; k++)
			{
				Edge myedge=(Edge)edges[k];
				i=findPt(myedge.p1.x,myedge.p1.y);
				j=findPt(myedge.p2.x,myedge.p2.y);
				double d = vert.get_Renamed(j).X - vert.get_Renamed(i).X;
				double d2 = vert.get_Renamed(j).Y - vert.get_Renamed(i).Y;
				EdgeMap[i][j]=Math.Sqrt( d * d + d2 * d2);
				EdgeStatus[i][j]=0;
				EdgeMap[j][i]= EdgeMap[i][j];
				EdgeStatus[j][i]=0;
			}

			/**
			 * add all the sampling points to pointlist
			 * initialize curveid = -1
			 */ 
			Vision.PointList PL=new Vision.PointList();
			for (i=0 ; i< vert.size(); i++)
			{
				Vision.Point2D pt = new Vision.Point2D(0,0);
				pt.CurveID=-1;
				pt.X= vert.get_Renamed(i).X;
				pt.Y=vert.get_Renamed(i).Y;
				PL.Add(pt);
			}
			#endregion //end region for initialize data

			//Mapping the edge distance. Status=0 means the dalaunay edge.
			bool SearchOver=false;
		//	int edgepriority=0;  // The priority of edges in some degree.
		//	int searchStage = 0; //0-> initial search, 1-> include ignored edge
			do
			{
				#region steps control
				if (crcount>=maxcr&&maxcr!=0)
				{
					SearchOver=true;
					break;
				}
				#endregion

				bool edgesearchover=false; // All the adges of edgepriorty are searched over.
				bool edgefilter=false;// Filter of edges in different degree;	
				double d1 = 99999D;
				SearchOver=false;
				head = 0;
				tail=0;
				edgesearchover=true; /* = true -> search again */
				
				#region find the shortest edge				
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						edgefilter=false;

						if((PointStatus[k])<2&&(PointStatus[l1]<2))
							edgefilter=true; //** point does not lead to intersection situation
					
						/** this is delaunay edge, the point is end point*/
						if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
						{
							tail = l1;
							head = k;
							d1 = EdgeMap[k][l1]; //the Shortest edge
							edgesearchover=false; //shortest delaunay edge is found
						}		
					}					
				}
								
				if (edgesearchover) /** Cannot find the shortest edge??*/
				{
					SearchOver=true; /** get out, not search again */
//					edgepriority++;
//
//					if (edgepriority>2)
//					{
//						SearchOver=true; /** get out, not search again */
//						if (searchStage ==0)
//						{
//							for ( k = 0; k < vert.size(); k++)
//							{
//								for (int l1 = k+1; l1 < vert.size(); l1++)
//								{		
//									if (EdgeStatus[k][l1]==2)
//									{
//										EdgeStatus[k][l1]=0;
//										EdgeStatus[l1][k]=0;
//									}
//								}
//							}						 
//						}
						
//						if (searchStage==2)
//							SearchOver=true;
//						
//						searchStage++;

//					}
				}


				#endregion				
												
				if (SearchOver)
				{
					break;  //search over
				}
				
				/** if shortest edge is found */
				if (!edgesearchover)                // Shortest points pair will be connected directly if their degree is 2.  Totally free.				
				{
					SearchOver=false;  /** < continue to search for next shortest edge  */
					
					// for initialize purpose 
					ExtendablePoints[0]=-1;
					ExtendablePoints[1]=-1;

					//first end point				
					Vision.Point2D pt= (Vision.Point2D)PL[head];										
					ExtendablePoints[0]=pt.CurveID;

					// second end point
					pt= (Vision.Point2D)PL[tail];
					ExtendablePoints[1]=pt.CurveID;

					/** if these are two free points 
					 *  check the angle 
					 */  
					#region free edge
					if (ExtendablePoints[0]+ExtendablePoints[1]==-2)
					{ 						
						p=1; //for connecting method 1-> head is currently considered, 2-> tail
						// ... code here
						if (connecting(EdgeStatus, PointStatus, EdgeMap, head, tail, PL))
						{
							GCurveID++;
							((Vision.Point2D)PL[head]).CurveID=GCurveID;
							((Vision.Point2D)PL[tail]).CurveID=GCurveID;
							Vision.StCurve cr=new Vision.StCurve();
							cr.AddPoint((Vision.Point2D)PL[head]);
							cr.AddPoint((Vision.Point2D)PL[tail]);
							cr.curveID=GCurveID;
							CVL.AddCurve(cr);
							EdgeStatus[head][tail]=1;
							EdgeStatus[tail][head]=1;
                            //((int)PointStatus[head])++;
                            //((int)PointStatus[tail])++;
                            PointStatus[head] = ((int)PointStatus[head]) + 1;
                            PointStatus[tail] = ((int)PointStatus[tail]) + 1;
							crcount++; /** the number of edge for iteration view?? */												
						}
						else 
						{					
							EdgeStatus[head][tail]=2; //ignored from searching shortest edge
							EdgeStatus[tail][head]=2; //ignored from searching shortest edge
						}
					}			
					
					#endregion
					

					//Step2 Extend the curve
					//enpoint(s) is/are not free
					#region endpoint(s) is/are not free
					int extindex=0;	
					int extendpoint=0;
					
					if ((ExtendablePoints[0]!=-1)||(ExtendablePoints[1]!=-1))             //if any side has curve, we figure the situation.
					{
						#region clear connective value
						for(i=0;i<vert.size();i++)
						{
							connectivevalue[0,i]=-100d;
							connectivevalue[1,i]=-100d;
						}
						#endregion
						
						freedomdegree=0;

						for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
						{
							#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
								
							if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
							{
								freedomdegree++;

								Vision.Point2D hp=new Vision.Point2D(0,0);
								Vision.Point2D mp=new Vision.Point2D(0,0);
								Vision.Point2D ep=new Vision.Point2D(0,0);
									
								if (extendablePointsi==0)
									extendpoint=head;
								else
									extendpoint=tail;
									
								int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
								Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
								Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
								Vision.StCurve curve = new Vision.StCurve();	

								curve = (Vision.StCurve)mycr.Clone();
								extindex=curve.CatchPointEx(newpt);

								//if extindex = -1, the endpoint is not at index 0
								if (extindex>0)								
									extindex=-1;

								if (mycr.Count >=3)
								{
									//mycr = crDetectOutlier(mycr, extindex);
									curve = crDetectOutlier(curve, extindex);
								}
						
								double angletemp = 0;
								double areatemp = 0; //define the maximum distance a point can connect to a curve								
								Vision.Point2D second_point; //the point next to endpoint in the curve

								if (extindex==0)
									second_point = (Vision.Point2D)curve[1];
								else
									second_point = (Vision.Point2D)curve[curve.Count-2]; 

								areatemp = curve.DistanceMean()*range;								

								//////////////////////////
								///iterate all the points within a defined area 
								///
											
							//	SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Orange);
													
								for (j = 0; j < vert.size(); j++)
								{
									Vision.Point2D point;
									
								   //consider all the points in the area	
									if (((EdgeStatus[extendpoint][j]==0)||(EdgeStatus[extendpoint][j]==2))&&(PointStatus[j]<2))
									{
										point = (Vision.Point2D)PL[j];
										angletemp = CalAngle(point,newpt, second_point);										

										//	if ((EdgeStatus[extendpoint][j]==0)&&(EdgeMap[extendpoint][j]<=EdgeMap[head][tail]*2)) 
										//if ((EdgeMap[extendpoint][j]<areatemp)&&(60<angletemp)&&(angletemp<300))
										if ((EdgeMap[extendpoint][j]<areatemp)&&(45<angletemp)&&(angletemp<315))

										//checking

									//	if ((EdgeMap[extendpoint][j]<areatemp))
										{							
										//	g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(extendpoint).X, vert.get_Renamed(extendpoint).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
											// calcu the entendable length of each edge.
											connectivevalue[extendablePointsi,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j], (double)numericUpDown4.Value);
										}
									}
			
								}
								/////////////////////////////////					
							}
							#endregion
						}

						/**
							 * Find the best point to connect to the existing curve?
							 */ 	

							#region find the best point
							bestconnectivevalue=-50d;
							bestextenalpoint=-1; /**< 0 for head, 1 for tai?? */
							bestconnectivepoint=-1; 
							for(i=0;i<2;i++)
								for(j=0;j<vert.size();j++)
								{
									if((connectivevalue[i,j]>bestconnectivevalue)&&(PointStatus[j]<2))
									{
										bestconnectivevalue=connectivevalue[i,j];
										bestextenalpoint=i;
										bestconnectivepoint=j;
									}
								}
							#endregion

						#region Update the curves
						if (bestconnectivevalue<0)
						{																				
							EdgeStatus[head][tail]=2;
							EdgeStatus[tail][head]=2;							
						}else 
						if(bestconnectivevalue>0)
						{				
							if (bestextenalpoint==0)
								extendpoint=head;
							else
								extendpoint=tail;

							#region one side is no free

							//An: defined tempID
							int tempId = 	((Vision.Point2D)PL[bestconnectivepoint]).CurveID;
							//An: freedomdegree may not be used here
//							if (freedomdegree==1)
							if (tempId == -1)
							{
								((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;
								Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
								extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
								if (extindex==0)
									cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
								else
									cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
								crcount++; /** curve count*/

							}
							#endregion
								
							#region both sides are not free
					//		if (freedomdegree==2)
							if (tempId!=-1)
							{
								//Join the two curve
									
								Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
								Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[bestconnectivepoint]).CurveID);
								int hid=crh.curveID;
								int tid=crt.curveID;
								
								//if ((extendablePointsi==0)&&(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0))
								//fzconnection++;
							//	if(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0)
							//	{
									if(hid!=tid)
									{
										GCurveID++;
										for(j=0;j<PL.Count;j++)
										{
											if (((Vision.Point2D)PL[j]).CurveID==hid)
												((Vision.Point2D)PL[j]).CurveID=GCurveID;
											if (((Vision.Point2D)PL[j]).CurveID==tid)
												((Vision.Point2D)PL[j]).CurveID=GCurveID;
										}
										Vision.StCurve cr=CVL.ConnetTwoCurves2(ref crh,ref crt, (Vision.Point2D)PL[extendpoint], (Vision.Point2D)PL[bestconnectivepoint]);
										//Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
										cr.curveID=GCurveID;							

										/* why remove???*/
										CVL.Remove(crh);
										CVL.Remove(crt);
										CVL.AddCurve(cr);						
									}
									else
										/**same curveID so connection will make a closed curve*/
										crh.Closed=1;							

									EdgeStatus[extendpoint][bestconnectivepoint]=1;
									EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                    //((int)PointStatus[extendpoint])++;
                                    //((int)PointStatus[bestconnectivepoint])++;
                                    PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                    PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
									crcount++;
							//	}
							}
							#endregion

						}
						#endregion update the curve
					}						
					#endregion endpoints not free
				}
				
			}while(!SearchOver);

			/////////////////
			/// Reconsider temp removed edge in first step 
			/// 

			#region reconsider removed edge
						
			bool update=false ; 
			if (crcount<maxcr||maxcr==0)
			{
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						if (EdgeStatus[k][l1]==0 && (EdgeStatus[l1][k]==0))
						{
							EdgeStatus[k][l1]=-1;
							EdgeStatus[l1][k]=-1;
							update =true;
						}		
					}					
				}
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						if (EdgeStatus[k][l1]==2 && (EdgeStatus[l1][k]==2))
						{
							EdgeStatus[k][l1]=0;
							EdgeStatus[l1][k]=0;
							update =true;
						}		
					}					
				}
			}

			if (update)
			{
				do 
				{
					bool edgefilter; 
					double d1 = 99999D;
					SearchOver = true;

					#region steps control
					if (crcount>=maxcr&&maxcr!=0)
					{
						SearchOver=true;
						break;
					}
					#endregion


					//find the shortest edge
					for ( k = 0; k < vert.size(); k++)
					{
						for (int l1 = k+1; l1 < vert.size(); l1++)
						{
							edgefilter=false;

							if((PointStatus[k])<2&&(PointStatus[l1]<2))
								edgefilter=true; //** point does not lead to intersection situation
					
							/** this is delaunay edge, the point is end point*/
							if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
							{
								tail = l1;
								head = k;
								d1 = EdgeMap[k][l1]; //the Shortest edge
								SearchOver=false; //shortest delaunay edge is found
							}		
						}					
					}

					//reconsidered removed edges
					if (SearchOver)
						break;

					//clear connectivity value
					for(i=0;i<vert.size();i++)
					{
						connectivevalue[0,i]=-100d;
						connectivevalue[1,i]=-100d;
					}

					//check best point 
					// for initialize purpose 
					ExtendablePoints[0]=-1;
					ExtendablePoints[1]=-1;

					//first end point				
					Vision.Point2D pt= (Vision.Point2D)PL[head];										
					ExtendablePoints[0]=pt.CurveID;

					// second end point
					pt= (Vision.Point2D)PL[tail];
					ExtendablePoints[1]=pt.CurveID;

					int extindex=0;	
					int extendpoint=0;

					freedomdegree=0;

					if (ExtendablePoints[0]+ExtendablePoints[1]==-2)
					{ 		
						//reconsider
						p=1;
						if (connecting(EdgeStatus, PointStatus, EdgeMap, head, tail, PL))
						{
							GCurveID++;
							((Vision.Point2D)PL[head]).CurveID=GCurveID;
							((Vision.Point2D)PL[tail]).CurveID=GCurveID;
							Vision.StCurve cr=new Vision.StCurve();
							cr.AddPoint((Vision.Point2D)PL[head]);
							cr.AddPoint((Vision.Point2D)PL[tail]);
							cr.curveID=GCurveID;
							CVL.AddCurve(cr);
							EdgeStatus[head][tail]=1;
							EdgeStatus[tail][head]=1;
                            //((int)PointStatus[head])++;
                            //((int)PointStatus[tail])++;
                            PointStatus[head] = ((int)PointStatus[head]) + 1;
                            PointStatus[tail] = ((int)PointStatus[tail]) + 1;
							crcount++; /** the number of edge for iteration view?? */												
						}
						else 
						{					
							EdgeStatus[head][tail]=2; //ignored from searching shortest edge
							EdgeStatus[tail][head]=2; //ignored from searching shortest edge
						}
					}
					else
					{
						for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
						{
							#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
						
		
							if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
							{
								freedomdegree++;

								Vision.Point2D hp=new Vision.Point2D(0,0);
								Vision.Point2D mp=new Vision.Point2D(0,0);
								Vision.Point2D ep=new Vision.Point2D(0,0);
									
								if (extendablePointsi==0)
									extendpoint=head;
								else
									extendpoint=tail;
									
								int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
								Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
								Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
								Vision.StCurve curve = new Vision.StCurve();	

								curve = (Vision.StCurve)mycr.Clone();
								extindex=curve.CatchPointEx(newpt);

								//if extindex = -1, the endpoint is not at index 0
								if (extindex>0)								
									extindex=-1;

								if (mycr.Count >=3)
								{
									//mycr = crDetectOutlier(mycr, extindex);
									curve = crDetectOutlier(curve, extindex);
								}
						
								double angletemp = 0;
								double areatemp = 0; //define the maximum distance a point can connect to a curve								
								Vision.Point2D second_point; //the point next to endpoint in the curve

								if (extindex==0)
									second_point = (Vision.Point2D)curve[1];
								else
									second_point = (Vision.Point2D)curve[curve.Count-2]; 

								areatemp = curve.DistanceMean()*range;								

								//////////////////////////
								///iterate all the points within a defined area 
								for (j = 0; j < vert.size(); j++)
								{
									Vision.Point2D point;
									
									//	if ((EdgeStatus[extendpoint][j]==0)&&(PointStatus[j]<2))
									if (((EdgeStatus[extendpoint][j]==0)||(EdgeStatus[extendpoint][j]==2))&&(PointStatus[j]<2))
									{
										point = (Vision.Point2D)PL[j];
										angletemp = CalAngle(point,newpt, second_point);										

										//	if ((EdgeStatus[extendpoint][j]==0)&&(EdgeMap[extendpoint][j]<=EdgeMap[head][tail]*2)) 
										//if ((EdgeMap[extendpoint][j]<areatemp)&&(60<angletemp)&&(angletemp<300))
										if ((EdgeMap[extendpoint][j]<areatemp)&&(45<angletemp)&&(angletemp<315))
										//if ((EdgeMap[extendpoint][j]<areatemp))
										{															
											// calcu the entendable length of each edge.
											//connectivevalue[extendablePointsi,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j]);
											connectivevalue[extendablePointsi,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j], (double)numericUpDown4.Value);
										}
									}
			
								}
								/////////////////////////////////					
							}
							#endregion
						}

						/**
									 * Find the best point to connect to the existing curve?
									 */ 	

						#region find the best point
						bestconnectivevalue=-50d;
						bestextenalpoint=-1; /**< 0 for head, 1 for tai?? */
						bestconnectivepoint=-1; 
						for(i=0;i<2;i++)
							for(j=0;j<vert.size();j++)
							{
								if((connectivevalue[i,j]>bestconnectivevalue)&&(PointStatus[j]<2))
								{
									bestconnectivevalue=connectivevalue[i,j];
									bestextenalpoint=i;
									bestconnectivepoint=j;
								}
							}
						#endregion

						#region Update the curves
						if (bestconnectivevalue<0)
						{																				
							EdgeStatus[head][tail]=2;
							EdgeStatus[tail][head]=2;							
						}
						else if(bestconnectivevalue>0)
						{				
							if (bestextenalpoint==0)
								extendpoint=head;
							else
								extendpoint=tail;

							#region one side is no free

							//An: defined tempID
							int tempId = 	((Vision.Point2D)PL[bestconnectivepoint]).CurveID;
				
							if (tempId == -1)
							{
								((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;								
								Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
								extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
								if (extindex==0)
									cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
								else
									cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
								crcount++; /** curve count*/

							}
							#endregion one side is no free
								
							#region both sides are not free
							if (tempId!=-1)
							{
								//Join the two curve
									
								Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
								Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[bestconnectivepoint]).CurveID);								
								int hid=crh.curveID;
								int tid=crt.curveID;
								
								if(hid!=tid)
								{
									GCurveID++;
									for(j=0;j<PL.Count;j++)
									{
										if (((Vision.Point2D)PL[j]).CurveID==hid)
											((Vision.Point2D)PL[j]).CurveID=GCurveID;
										if (((Vision.Point2D)PL[j]).CurveID==tid)
											((Vision.Point2D)PL[j]).CurveID=GCurveID;
									}
									Vision.StCurve cr=CVL.ConnetTwoCurves2(ref crh,ref crt, (Vision.Point2D)PL[extendpoint], (Vision.Point2D)PL[bestconnectivepoint]);
							//		Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
									cr.curveID=GCurveID;							

									/* why remove???*/
									CVL.Remove(crh);
									CVL.Remove(crt);
									CVL.AddCurve(cr);						
								}
								else
									/**same curveID so connection will make a closed curve*/
									crh.Closed=1;							

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
								//crcount++;
								//	}
							}
							#endregion

							
					
							//int new_extend_point = extendpoint;
							if (CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID).Closed!=1)
							{
								#region repeating the update
								do 
								{

									update = false;


									//iterate endpoint of the current curve
									for (k=0; k<2; k++)
									{
										//		Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[new_extend_point]).CurveID);
										Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										Vision.Point2D endpoint;
									
										//clear connectivity value
										for(i=0;i<vert.size();i++)
										{
											connectivevalue[0,i]=-100d;
											connectivevalue[1,i]=-100d;
										}

										if (k==0)
										{																		
											endpoint = crh.ReadHead(); //read headpoint of the curve													
											//				head = PL.IndexOf(endpoint, 0, PL.Count);
											//				extendpoint = head;
											extendpoint = PL.IndexOf(endpoint, 0, PL.Count); 
										}
										else
										{ 										
											endpoint = crh.ReadTail();  //read tailpoint of the curve
											//				tail = PL.IndexOf(endpoint, 0, PL.Count);
											//				extendpoint = tail;
											extendpoint = PL.IndexOf(endpoint, 0, PL.Count);
										}
									
										//extendpoint = endpoint.;
										//int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
										int mycurveID=endpoint.CurveID; //find the curve
										//Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
										Vision.Point2D newpt=endpoint;  //find the point
										Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
										Vision.StCurve curve = new Vision.StCurve();	

										curve = (Vision.StCurve)mycr.Clone();
										extindex=curve.CatchPointEx(newpt);

										//if extindex = -1, the endpoint is not at index 0
										if (extindex>0)								
											extindex=-1;

										if (mycr.Count >=3)
										{
											//mycr = crDetectOutlier(mycr, extindex);
											curve = crDetectOutlier(curve, extindex);
										}
						
										double angletemp = 0;
										double areatemp = 0; //define the maximum distance a point can connect to a curve								
										Vision.Point2D second_point; //the point next to endpoint in the curve

										if (extindex==0)
											second_point = (Vision.Point2D)curve[1];
										else
											second_point = (Vision.Point2D)curve[curve.Count-2]; 

										areatemp = curve.DistanceMean()*range;								

										//////////////////////////
										///iterate all the points within a defined area 
										///


										for (j = 0; j < vert.size(); j++)
										{
											Vision.Point2D point;
									
											//		if ((EdgeStatus[extendpoint][j]==0)&&(PointStatus[j]<2))
											if (((EdgeStatus[extendpoint][j]==0)||(EdgeStatus[extendpoint][j]==2))&&(PointStatus[j]<2))
											{
												point = (Vision.Point2D)PL[j];
												angletemp = CalAngle(point,newpt, second_point);										

												//	if ((EdgeStatus[extendpoint][j]==0)&&(EdgeMap[extendpoint][j]<=EdgeMap[head][tail]*2)) 
												//	if ((EdgeMap[extendpoint][j]<areatemp)&&(60<angletemp)&&(angletemp<300))
												if ((EdgeMap[extendpoint][j]<areatemp)&&(45<angletemp)&&(angletemp<315))
													//if ((EdgeMap[extendpoint][j]<areatemp))
												{															
													// calcu the extendable length of each edge.
													//	connectivevalue[k,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j]);
													//	connectivevalue[0,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j]);
													connectivevalue[0,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j], (double)numericUpDown4.Value);
												}
											}
			
										}



										//find the best point to connect
										#region find the best point
										bestconnectivevalue=-50d;
										bestextenalpoint=-1; /**< 0 for head, 1 for tai?? */
										bestconnectivepoint=-1; 
										//	for(i=0;i<2;i++)
										for(j=0;j<vert.size();j++)
										{
											//			if((connectivevalue[i,j]>bestconnectivevalue)&&(PointStatus[j]<2))
											if((connectivevalue[0,j]>bestconnectivevalue)&&(PointStatus[j]<2))
											{
												bestconnectivevalue=connectivevalue[0,j];
												bestextenalpoint=0;
												bestconnectivepoint=j;
											}
										}
										#endregion find the best point

										#region Update the curves

										if(bestconnectivevalue>0)
										{				
											update = true;

											#region one side is no free
											//An: defined tempID
											tempId = 	((Vision.Point2D)PL[bestconnectivepoint]).CurveID;
				

											//An: freedomdegree may not be used here
											//							if (freedomdegree==1)
											if (tempId == -1)
											{
												((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;								
												Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
												extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
												if (extindex==0)
													cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
												else
													cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);

												EdgeStatus[extendpoint][bestconnectivepoint]=1;
												EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                                //((int)PointStatus[extendpoint])++;
                                                //((int)PointStatus[bestconnectivepoint])++;
                                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
												//	crcount++; /** curve count*/

											}
											#endregion oneside is no free
								
											#region both sides are not free
											//		if (freedomdegree==2)
											if (tempId!=-1)
											{
												//Join the two curve
									
												crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
												Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[bestconnectivepoint]).CurveID);								
												int hid=crh.curveID;
												int tid=crt.curveID;
								
												if(hid!=tid)
												{
													GCurveID++;
													for(j=0;j<PL.Count;j++)
													{
														if (((Vision.Point2D)PL[j]).CurveID==hid)
															((Vision.Point2D)PL[j]).CurveID=GCurveID;
														if (((Vision.Point2D)PL[j]).CurveID==tid)
															((Vision.Point2D)PL[j]).CurveID=GCurveID;
													}
													Vision.StCurve cr=CVL.ConnetTwoCurves2(ref crh,ref crt,(Vision.Point2D)PL[extendpoint], (Vision.Point2D)PL[bestconnectivepoint]);
													//		Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
													cr.curveID=GCurveID;							

													/* why remove???*/
													CVL.Remove(crh);
													CVL.Remove(crt);
													CVL.AddCurve(cr);						
												}
												else
													/**same curveID so connection will make a closed curve*/
													crh.Closed=1;							

												EdgeStatus[extendpoint][bestconnectivepoint]=1;
												EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                                //((int)PointStatus[extendpoint])++;
                                                //((int)PointStatus[bestconnectivepoint])++;
                                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
												//	crcount++;
												//	}
										
											}
											#endregion both sides are not free
										}
										#endregion update the curves
									}//endfor 
								}while(update);
								#endregion repeating the update
							}
						}
						#endregion update the curves 
					} //end else-if not free points
				}while (!SearchOver);
			}//endif update
	
			#endregion reconsider removed edge

			//Draw curves
			#region Draw curves

			for (i = 0; i < vert.size(); i++)
			{
				//remove all the delaunay edge for interior point
				if (PointStatus[i]==2)
				{
					for (int t=0; t<vert.size(); t++)						
					{
						if (EdgeStatus[i][t]==0)
						{
							EdgeStatus[i][t] = -1;
							EdgeStatus[t][i] = -1;
						}
					}
				}
			}

			for (i = 0; i < vert.size(); i++)
			{

				for (j = i + 1; j < vert.size(); j++)
				{
//					if (EdgeStatus[i][j]==0)
//					{
//						SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Magenta);
//						g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
//					}



					if (EdgeStatus[i][j]==1)
					{
						SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
						g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
					}

//					if (EdgeStatus[i][j]==2)
//					{
//						SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Blue);
//						g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
//					}
//					SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
					
				}
			}
			#endregion 
			/////						
		}
		
		//DetectOutlier in a curve (points in the curve>=3)
		internal virtual void  drawAngleDistance3DelauneyVersion1(System.Drawing.Graphics g)
		{
			// * Construct Delauney Trianglation.		
			// * Find the shortest edge.
			double [,] connectivevalue=new double[2,vert.size()];
			int bestconnectivepoint=-1;
			int bestextenalpoint=-1;
			double bestconnectivevalue=-20;
			int freedomdegree=0;
			double range=1.849; //1.8; 
			#region calculate the delaunay  trianglation
			int GCurveID=0;
			int maxcr=(int)numericUpDown3.Value;
			int crcount=0;
			
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
			Delaunay delaunay1 = new Delaunay(vert.size());

			/**
			 *  op is not used 
			 * 	Vision.Optimismfilter op=new Vision.Optimismfilter();
			 */

			Vision.StCurveList CVL= new Vision.StCurveList();
			int [] ExtendablePoints={0,0};
			int i=0,j=0,k;
			int head = 0;
			int tail=0;

			/**
			 * Insert all the point into the delaunay array for delaunay triangulation
			 */ 
			for (i = 0; i < vert.size(); i++)
				delaunay1.Insert(vert.get_Renamed(i).X, vert.get_Renamed(i).Y, 0);


			System.Collections.ArrayList nodes=delaunay1.nodes;
			System.Collections.ArrayList edges=delaunay1.edges;
			System.Collections.ArrayList curveedge=new ArrayList();
			System.Collections.ArrayList curvepoint=new ArrayList();
			if (nodes.Count == 1)
				//**each point is a rectangle of size 1*/				
				g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
			//g.DrawEllipse(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
			if (nodes.Count == 2)
				g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, ((Node) nodes[1]).x, ((Node) nodes[1]).y);
			#endregion

			#region initialize the data
			/** 	
			 * edgemap contains distance between 2 points if applicable
			 */
			double[][] EdgeMap = new double[vert.size()][];
			/*
			 * edgestatus = -1 -> not delaunay edge
			 * edgestatus = 0 -> delaunay edge
			 * edgestatus = 1 -> connected
			 */
			int [][] EdgeStatus= new int[vert.size()][];
			int [] PointStatus=new int[vert.size()];
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i] = new double[vert.size()];
				EdgeStatus[i]= new int[vert.size()];
				PointStatus[i]=0;
			}
			/**
			 * Initialize all EdgeMap and EdgeSatus to value -1
			 */ 
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i][i]= -1;
				EdgeStatus[i][i]=-1;
				for (j = i + 1; j < vert.size(); j++)
				{
					EdgeMap[i][j]= -1;
					EdgeStatus[i][j]=-1;
					EdgeMap[j][i]= -1;
					EdgeStatus[j][i]=-1;
				}
			}
			
			/**
			 * find 2 points that around the two end points of the Delaunay edge
			 * Calculate the distance between these 2 points
			 */ 
			for (k = 0; k < edges.Count; k++)
			{
				Edge myedge=(Edge)edges[k];
				i=findPt(myedge.p1.x,myedge.p1.y);
				j=findPt(myedge.p2.x,myedge.p2.y);
				double d = vert.get_Renamed(j).X - vert.get_Renamed(i).X;
				double d2 = vert.get_Renamed(j).Y - vert.get_Renamed(i).Y;
				EdgeMap[i][j]=Math.Sqrt( d * d + d2 * d2);
				EdgeStatus[i][j]=0;
				EdgeMap[j][i]= EdgeMap[i][j];
				EdgeStatus[j][i]=0;
			}

			/**
			 * add all the sampling points to pointlist
			 * initialize curveid = -1
			 */ 
			Vision.PointList PL=new Vision.PointList();
			for (i=0 ; i< vert.size(); i++)
			{
				Vision.Point2D pt = new Vision.Point2D(0,0);
				pt.CurveID=-1;
				pt.X= vert.get_Renamed(i).X;
				pt.Y=vert.get_Renamed(i).Y;
				PL.Add(pt);
			}
			#endregion //end region for initialize data

			//Mapping the edge distance. Status=0 means the dalaunay edge.
			bool SearchOver=false;
			//	int edgepriority=0;  // The priority of edges in some degree.
			//	int searchStage = 0; //0-> initial search, 1-> include ignored edge
			do
			{
				#region steps control
				if (crcount>=maxcr&&maxcr!=0)
				{
					SearchOver=true;
					break;
				}
				#endregion

				bool edgesearchover=false; // All the adges of edgepriorty are searched over.
				bool edgefilter=false;// Filter of edges in different degree;	
				double d1 = 99999D;
				SearchOver=false;
				head = 0;
				tail=0;
				edgesearchover=true; /* = true -> search again */
				
				#region find the shortest edge				
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						edgefilter=false;

						if((PointStatus[k])<2&&(PointStatus[l1]<2))
							edgefilter=true; //** point does not lead to intersection situation
					
						/** this is delaunay edge, the point is end point*/
						if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
						{
							tail = l1;
							head = k;
							d1 = EdgeMap[k][l1]; //the Shortest edge
							edgesearchover=false; //shortest delaunay edge is found
						}		
					}					
				}
								
				if (edgesearchover) /** Cannot find the shortest edge??*/
				{
					SearchOver=true; /** get out, not search again */
				}


				#endregion				
												
				if (SearchOver)
				{
					break;  //search over
				}
				
				/** if shortest edge is found */
				if (!edgesearchover)                // Shortest points pair will be connected directly if their degree is 2.  Totally free.				
				{
					SearchOver=false;  /** < continue to search for next shortest edge  */
					
					// for initialize purpose 
					ExtendablePoints[0]=-1;
					ExtendablePoints[1]=-1;

					//first end point				
					Vision.Point2D pt= (Vision.Point2D)PL[head];										
					ExtendablePoints[0]=pt.CurveID;

					// second end point
					pt= (Vision.Point2D)PL[tail];
					ExtendablePoints[1]=pt.CurveID;

					/** if these are two free points 
					 *  check the angle 
					 */  
					#region free edge
					if (ExtendablePoints[0]+ExtendablePoints[1]==-2)
					{ 						
						p=1; //for connecting method 1-> head is currently considered, 2-> tail
						// ... code here
						if (connecting(EdgeStatus, PointStatus, EdgeMap, head, tail, PL))
						{
							GCurveID++;
							((Vision.Point2D)PL[head]).CurveID=GCurveID;
							((Vision.Point2D)PL[tail]).CurveID=GCurveID;
							Vision.StCurve cr=new Vision.StCurve();
							cr.AddPoint((Vision.Point2D)PL[head]);
							cr.AddPoint((Vision.Point2D)PL[tail]);
							cr.curveID=GCurveID;
							CVL.AddCurve(cr);
							EdgeStatus[head][tail]=1;
							EdgeStatus[tail][head]=1;
                            //((int)PointStatus[head])++;
                            //((int)PointStatus[tail])++;
                            PointStatus[head] = ((int)PointStatus[head]) + 1;
                            PointStatus[tail] = ((int)PointStatus[tail]) + 1;
							crcount++; /** the number of edge for iteration view?? */												
						}
						else 
						{					
							EdgeStatus[head][tail]=2; //ignored from searching shortest edge
							EdgeStatus[tail][head]=2; //ignored from searching shortest edge
						}
					}			
					
					#endregion
					

					//Step2 Extend the curve
					//enpoint(s) is/are not free
					#region endpoint(s) is/are not free
					int extindex=0;	
					int extendpoint=0;
					
					if ((ExtendablePoints[0]!=-1)||(ExtendablePoints[1]!=-1))             //if any side has curve, we figure the situation.
					{
						#region clear connective value
						for(i=0;i<vert.size();i++)
						{
							connectivevalue[0,i]=-100d;
							connectivevalue[1,i]=-100d;
						}
						#endregion
						
						freedomdegree=0;

						for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
						{
							#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
								
							if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
							{
								freedomdegree++;

								Vision.Point2D hp=new Vision.Point2D(0,0);
								Vision.Point2D mp=new Vision.Point2D(0,0);
								Vision.Point2D ep=new Vision.Point2D(0,0);
									
								if (extendablePointsi==0)
									extendpoint=head;
								else
									extendpoint=tail;
									
								int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
								Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
								Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
								Vision.StCurve curve = new Vision.StCurve();	

								curve = (Vision.StCurve)mycr.Clone();
								extindex=curve.CatchPointEx(newpt);

								//if extindex = -1, the endpoint is not at index 0
								if (extindex>0)								
									extindex=-1;

								if (mycr.Count >=3)
								{
									//mycr = crDetectOutlier(mycr, extindex);
									curve = crDetectOutlier(curve, extindex);
								}
						
								double angletemp = 0;
								double areatemp = 0; //define the maximum distance a point can connect to a curve								
								Vision.Point2D second_point; //the point next to endpoint in the curve

								if (extindex==0)
									second_point = (Vision.Point2D)curve[1];
								else
									second_point = (Vision.Point2D)curve[curve.Count-2]; 

								areatemp = curve.DistanceMean()*range;								

								//////////////////////////
								///iterate all the points within a defined area 
								///
																														
								for (j = 0; j < vert.size(); j++)
								{
									Vision.Point2D point;
									
									//consider all the points in the area	
									if (((EdgeStatus[extendpoint][j]==0)||(EdgeStatus[extendpoint][j]==2))&&(PointStatus[j]<2))
									{
										point = (Vision.Point2D)PL[j];
										angletemp = CalAngle(point,newpt, second_point);										

										//	if ((EdgeStatus[extendpoint][j]==0)&&(EdgeMap[extendpoint][j]<=EdgeMap[head][tail]*2)) 
										//if ((EdgeMap[extendpoint][j]<areatemp)&&(60<angletemp)&&(angletemp<300))
										if ((EdgeMap[extendpoint][j]<areatemp)&&(45<angletemp)&&(angletemp<315))

											//checking

											//	if ((EdgeMap[extendpoint][j]<areatemp))
										{							
											//	g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(extendpoint).X, vert.get_Renamed(extendpoint).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
											// calcu the entendable length of each edge.
											connectivevalue[extendablePointsi,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j], (double)numericUpDown4.Value);
										}
									}
			
								}
								/////////////////////////////////					
							}
							#endregion
						}

						/**
							 * Find the best point to connect to the existing curve?
							 */ 	

						#region find the best point
						bestconnectivevalue=-50d;
						bestextenalpoint=-1; /**< 0 for head, 1 for tai?? */
						bestconnectivepoint=-1; 
						for(i=0;i<2;i++)
							for(j=0;j<vert.size();j++)
							{
								if((connectivevalue[i,j]>bestconnectivevalue)&&(PointStatus[j]<2))
								{
									bestconnectivevalue=connectivevalue[i,j];
									bestextenalpoint=i;
									bestconnectivepoint=j;
								}
							}
						#endregion

						#region Update the curves
						if (bestconnectivevalue<0)
						{																				
							EdgeStatus[head][tail]=2;
							EdgeStatus[tail][head]=2;							
						}
						else 
							if(bestconnectivevalue>0)
						{				
							if (bestextenalpoint==0)
								extendpoint=head;
							else
								extendpoint=tail;

							#region one side is no free

							//An: defined tempID
							int tempId = 	((Vision.Point2D)PL[bestconnectivepoint]).CurveID;
							//An: freedomdegree may not be used here
							//							if (freedomdegree==1)
							if (tempId == -1)
							{
								((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;
								Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
								extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
								if (extindex==0)
									cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
								else
									cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
								crcount++; /** curve count*/

							}
							#endregion
								
							#region both sides are not free						
							if (tempId!=-1)
							{
								//Join the two curve
									
								Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
								Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[bestconnectivepoint]).CurveID);
								int hid=crh.curveID;
								int tid=crt.curveID;
								
								if(hid!=tid)
								{
									GCurveID++;
									for(j=0;j<PL.Count;j++)
									{
										if (((Vision.Point2D)PL[j]).CurveID==hid)
											((Vision.Point2D)PL[j]).CurveID=GCurveID;
										if (((Vision.Point2D)PL[j]).CurveID==tid)
											((Vision.Point2D)PL[j]).CurveID=GCurveID;
									}
									Vision.StCurve cr=CVL.ConnetTwoCurves2(ref crh,ref crt, (Vision.Point2D)PL[extendpoint], (Vision.Point2D)PL[bestconnectivepoint]);
									cr.curveID=GCurveID;							

									/* why remove???*/
									CVL.Remove(crh);
									CVL.Remove(crt);
									CVL.AddCurve(cr);						
								}
								else
									/**same curveID so connection will make a closed curve*/
									crh.Closed=1;							

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
								crcount++;
								//	}
							}
							#endregion

							#region explore the curve
						
							Vision.StCurve curve=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
							Vision.Point2D firstendpoint;
							Vision.Point2D secondendpoint;																	
							firstendpoint = curve.ReadHead(); //read headpoint of the curve													
							
							extendpoint = PL.IndexOf(firstendpoint, 0, PL.Count);							

							double distance = Double.MaxValue;

							for (k=0; k<vert.size(); k++)
							{
								if ((EdgeMap[k][extendpoint]<distance)&&(EdgeStatus[k][extendpoint]==2))
								{
									distance = EdgeMap[k][extendpoint];
									head = k;
									tail = extendpoint;
								}
							}
													
							secondendpoint = curve.ReadTail();  //read tailpoint of the curve
							extendpoint = PL.IndexOf(firstendpoint, 0, PL.Count);
												
							for (k=0; k<vert.size(); k++)
							{
								if ((EdgeMap[k][extendpoint]<distance)&&(EdgeStatus[k][extendpoint]==2))
								{
									distance = EdgeMap[k][extendpoint];
									head = k;
									tail = extendpoint;
								}
							}

							if (distance!=Double.MaxValue)
							{
									EdgeStatus[head][tail]=0;	
									EdgeStatus[tail][head]=0;	
							}
							#endregion explore curve
						}
						#endregion update the curve
					}						
					#endregion endpoints not free
				}
				
			}while(!SearchOver);

			//Draw curves
			#region Draw curves

			for (i = 0; i < vert.size(); i++)
			{
				//remove all the delaunay edge for interior point
				if (PointStatus[i]==2)
				{
					for (int t=0; t<vert.size(); t++)						
					{
						if (EdgeStatus[i][t]==0)
						{
							EdgeStatus[i][t] = -1;
							EdgeStatus[t][i] = -1;
						}
					}
				}
			}

			for (i = 0; i < vert.size(); i++)
			{

				for (j = i + 1; j < vert.size(); j++)
				{
					//					if (EdgeStatus[i][j]==0)
					//					{
					//						SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Magenta);
					//						g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
					//					}



					if (EdgeStatus[i][j]==1)
					{
						SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
						g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
					}

					//					if (EdgeStatus[i][j]==2)
					//					{
					//						SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Blue);
					//						g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
					//					}
					//					SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
					
				}
			}
			#endregion 
			/////						
		}
		
		static int[][] computeVICUR(ref Vertex vert, ref Delaunay delaunay1, int maxcr, double smoothness)
		{
			// * Construct Delauney Trianglation.		
			// * Find the shortest edge.
			int p = 1;
			double [,] connectivevalue=new double[2,vert.size()];
			int bestconnectivepoint=-1;
			int bestextenalpoint=-1;
			double bestconnectivevalue=-20;
			int freedomdegree=0;
			double range=1.849; //1.8; 
			#region calculate the delaunay  trianglation
			int GCurveID=0;
			int crcount=0;
			
			/**
			 *  op is not used 
			 * 	Vision.Optimismfilter op=new Vision.Optimismfilter();
			 */

			Vision.StCurveList CVL= new Vision.StCurveList();
			int [] ExtendablePoints={0,0};
			int i=0,j=0,k;
			int head = 0;
			int tail=0;

			/**
			 * Insert all the point into the delaunay array for delaunay triangulation
			 */ 
			for (i = 0; i < vert.size(); i++)
				delaunay1.Insert(vert.get_Renamed(i).X, vert.get_Renamed(i).Y, 0);


			System.Collections.ArrayList edges=delaunay1.edges;
			System.Collections.ArrayList curveedge=new ArrayList();
			System.Collections.ArrayList curvepoint=new ArrayList();
			#endregion

			#region initialize the data
			/** 	
			 * edgemap contains distance between 2 points if applicable
			 */
			double[][] EdgeMap = new double[vert.size()][];
			/*
			 * edgestatus = -1 -> not delaunay edge
			 * edgestatus = 0 -> delaunay edge
			 * edgestatus = 1 -> connected
			 */
			int [][] EdgeStatus= new int[vert.size()][];
			int [] PointStatus=new int[vert.size()];
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i] = new double[vert.size()];
				EdgeStatus[i]= new int[vert.size()];
				PointStatus[i]=0;
			}
			/**
			 * Initialize all EdgeMap and EdgeSatus to value -1
			 */ 
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i][i]= -1;
				EdgeStatus[i][i]=-1;
				for (j = i + 1; j < vert.size(); j++)
				{
					EdgeMap[i][j]= -1;
					EdgeStatus[i][j]=-1;
					EdgeMap[j][i]= -1;
					EdgeStatus[j][i]=-1;
				}
			}
			
			/**
			 * find 2 points that around the two end points of the Delaunay edge
			 * Calculate the distance between these 2 points
			 */ 
			for (k = 0; k < edges.Count; k++)
			{
				Edge myedge=(Edge)edges[k];
				i=findPt(ref vert,myedge.p1.x,myedge.p1.y);
				j=findPt(ref vert,myedge.p2.x,myedge.p2.y);
				double d = vert.get_Renamed(j).X - vert.get_Renamed(i).X;
				double d2 = vert.get_Renamed(j).Y - vert.get_Renamed(i).Y;
				EdgeMap[i][j]=Math.Sqrt( d * d + d2 * d2);
				EdgeStatus[i][j]=0;
				EdgeMap[j][i]= EdgeMap[i][j];
				EdgeStatus[j][i]=0;
			}

			/**
			 * add all the sampling points to pointlist
			 * initialize curveid = -1

			 */ 
			Vision.PointList PL=new Vision.PointList();
			for (i=0 ; i< vert.size(); i++)
			{
				Vision.Point2D pt = new Vision.Point2D(0,0);
				pt.CurveID=-1;
				pt.X= vert.get_Renamed(i).X;
				pt.Y=vert.get_Renamed(i).Y;
				PL.Add(pt);
			}
			#endregion //end region for initialize data

			//Mapping the edge distance. Status=0 means the dalaunay edge.
			bool SearchOver=false;
			//	int edgepriority=0;  // The priority of edges in some degree.
			//	int searchStage = 0; //0-> initial search, 1-> include ignored edge
			do
			{
				#region steps control
				if (crcount>=maxcr&&maxcr!=0)
				{
					SearchOver=true;
					break;
				}
				#endregion

				bool edgesearchover=false; // All the adges of edgepriorty are searched over.
				bool edgefilter=false;// Filter of edges in different degree;	
				double d1 = 99999D;
				SearchOver=false;
				head = 0;
				tail=0;
				edgesearchover=true; /* = true -> search again */
				
				#region find the shortest edge				
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						edgefilter=false;

						if((PointStatus[k])<2&&(PointStatus[l1]<2))
							edgefilter=true; //** point does not lead to intersection situation
					
						/** this is delaunay edge, the point is end point*/
						if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
						{
							tail = l1;
							head = k;
							d1 = EdgeMap[k][l1]; //the Shortest edge
							edgesearchover=false; //shortest delaunay edge is found
						}		
					}					
				}
								
				if (edgesearchover) /** Cannot find the shortest edge??*/
				{
					SearchOver=true; /** get out, not search again */
				}


				#endregion				
												
				if (SearchOver)
				{
					break;  //search over
				}
				
				/** if shortest edge is found */
				if (!edgesearchover)                // Shortest points pair will be connected directly if their degree is 2.  Totally free.				
				{
					SearchOver=false;  /** < continue to search for next shortest edge  */
					
					// for initialize purpose 
					ExtendablePoints[0]=-1;
					ExtendablePoints[1]=-1;

					//first end point				
					Vision.Point2D pt= (Vision.Point2D)PL[head];										
					ExtendablePoints[0]=pt.CurveID;

					// second end point
					pt= (Vision.Point2D)PL[tail];
					ExtendablePoints[1]=pt.CurveID;

					/** if these are two free points 
					 *  check the angle 
					 */  
					#region free edge
					if (ExtendablePoints[0]+ExtendablePoints[1]==-2)
					{ 						
						p=1; //for connecting method 1-> head is currently considered, 2-> tail
						// ... code here
						if (connecting(ref vert, EdgeStatus, PointStatus, EdgeMap, head, tail, PL, p))
						{
							GCurveID++;
							((Vision.Point2D)PL[head]).CurveID=GCurveID;
							((Vision.Point2D)PL[tail]).CurveID=GCurveID;
							Vision.StCurve cr=new Vision.StCurve();
							cr.AddPoint((Vision.Point2D)PL[head]);
							cr.AddPoint((Vision.Point2D)PL[tail]);
							cr.curveID=GCurveID;
							CVL.AddCurve(cr);
							EdgeStatus[head][tail]=1;
							EdgeStatus[tail][head]=1;
                            //((int)PointStatus[head])++;
                            //((int)PointStatus[tail])++;
                            PointStatus[head] = ((int)PointStatus[head]) + 1;
                            PointStatus[tail] = ((int)PointStatus[tail]) + 1;
							crcount++; /** the number of edge for iteration view?? */												
						}
						else 
						{					
							EdgeStatus[head][tail]=2; //ignored from searching shortest edge
							EdgeStatus[tail][head]=2; //ignored from searching shortest edge
						}
					}			
					
					#endregion
					

					//Step2 Extend the curve
					//enpoint(s) is/are not free
					#region endpoint(s) is/are not free
					int extindex=0;	
					int extendpoint=0;
					
					if ((ExtendablePoints[0]!=-1)||(ExtendablePoints[1]!=-1))             //if any side has curve, we figure the situation.
					{
						#region clear connective value
						for(i=0;i<vert.size();i++)
						{
							connectivevalue[0,i]=-100d;
							connectivevalue[1,i]=-100d;
						}
						#endregion
						
						freedomdegree=0;

						for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
						{
							#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
								
							if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
							{
								freedomdegree++;

								Vision.Point2D hp=new Vision.Point2D(0,0);
								Vision.Point2D mp=new Vision.Point2D(0,0);
								Vision.Point2D ep=new Vision.Point2D(0,0);
									
								if (extendablePointsi==0)
									extendpoint=head;
								else
									extendpoint=tail;
									
								int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
								Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
								Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
								Vision.StCurve curve = new Vision.StCurve();	

								curve = (Vision.StCurve)mycr.Clone();
								extindex=curve.CatchPointEx(newpt);

								//if extindex = -1, the endpoint is not at index 0
								if (extindex>0)								
									extindex=-1;

								if (mycr.Count >=3)
								{
									//mycr = crDetectOutlier(mycr, extindex);
									curve = crDetectOutlier(curve, extindex);
								}
						
								double angletemp = 0;
								double areatemp = 0; //define the maximum distance a point can connect to a curve								
								Vision.Point2D second_point; //the point next to endpoint in the curve

								if (extindex==0)
									second_point = (Vision.Point2D)curve[1];
								else
									second_point = (Vision.Point2D)curve[curve.Count-2]; 

								areatemp = curve.DistanceMean()*range;								

								//////////////////////////
								///iterate all the points within a defined area 
								///
											
								//	SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Orange);
													
								for (j = 0; j < vert.size(); j++)
								{
									Vision.Point2D point;
									
									//consider all the points in the area	
									if (((EdgeStatus[extendpoint][j]==0)||(EdgeStatus[extendpoint][j]==2))&&(PointStatus[j]<2))
									{
										point = (Vision.Point2D)PL[j];
										angletemp = CalAngle(point,newpt, second_point);										

										//	if ((EdgeStatus[extendpoint][j]==0)&&(EdgeMap[extendpoint][j]<=EdgeMap[head][tail]*2)) 
										//if ((EdgeMap[extendpoint][j]<areatemp)&&(60<angletemp)&&(angletemp<300))
										if ((EdgeMap[extendpoint][j]<areatemp)&&(45<angletemp)&&(angletemp<315))

											//checking

											//	if ((EdgeMap[extendpoint][j]<areatemp))
										{							
											//	g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(extendpoint).X, vert.get_Renamed(extendpoint).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
											// calcu the entendable length of each edge.
											connectivevalue[extendablePointsi,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j], smoothness);
										}
									}
			
								}
								/////////////////////////////////					
							}
							#endregion
						}

						/**
							 * Find the best point to connect to the existing curve?
							 */ 	

						#region find the best point
						bestconnectivevalue=-50d;
						bestextenalpoint=-1; /**< 0 for head, 1 for tai?? */
						bestconnectivepoint=-1; 
						for(i=0;i<2;i++)
							for(j=0;j<vert.size();j++)
							{
								if((connectivevalue[i,j]>bestconnectivevalue)&&(PointStatus[j]<2))
								{
									bestconnectivevalue=connectivevalue[i,j];
									bestextenalpoint=i;
									bestconnectivepoint=j;
								}
							}
						#endregion

						#region Update the curves
						if (bestconnectivevalue<0)
						{																				
							EdgeStatus[head][tail]=2;
							EdgeStatus[tail][head]=2;							
						}
						else 
							if(bestconnectivevalue>0)
						{				
							if (bestextenalpoint==0)
								extendpoint=head;
							else
								extendpoint=tail;

							#region one side is no free

							//An: defined tempID
							int tempId = 	((Vision.Point2D)PL[bestconnectivepoint]).CurveID;
							//An: freedomdegree may not be used here
							//							if (freedomdegree==1)
							if (tempId == -1)
							{
								((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;
								Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
								extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
								if (extindex==0)
									cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
								else
									cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
								crcount++; /** curve count*/

							}
							#endregion
								
							#region both sides are not free
							//		if (freedomdegree==2)
							if (tempId!=-1)
							{
								//Join the two curve
									
								Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
								Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[bestconnectivepoint]).CurveID);
								int hid=crh.curveID;
								int tid=crt.curveID;
								
								//if ((extendablePointsi==0)&&(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0))
								//fzconnection++;
								//	if(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0)
								//	{
								if(hid!=tid)
								{
									GCurveID++;
									for(j=0;j<PL.Count;j++)
									{
										if (((Vision.Point2D)PL[j]).CurveID==hid)

											((Vision.Point2D)PL[j]).CurveID=GCurveID;
										if (((Vision.Point2D)PL[j]).CurveID==tid)
											((Vision.Point2D)PL[j]).CurveID=GCurveID;
									}
									Vision.StCurve cr=CVL.ConnetTwoCurves2(ref crh,ref crt, (Vision.Point2D)PL[extendpoint], (Vision.Point2D)PL[bestconnectivepoint]);
									//Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
									cr.curveID=GCurveID;							

									/* why remove???*/
									CVL.Remove(crh);
									CVL.Remove(crt);
									CVL.AddCurve(cr);						
								}
								else
									/**same curveID so connection will make a closed curve*/
									crh.Closed=1;							

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
								crcount++;
								//	}
							}
							#endregion

						}
						#endregion update the curve
					}						
					#endregion endpoints not free
				}
				
			}while(!SearchOver);

			/////////////////
			/// Reconsider temp removed edge in first step 
			/// 

			#region reconsider removed edge
						
			bool update=false ; 
			if (crcount<maxcr||maxcr==0)
			{
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						if (EdgeStatus[k][l1]==0 && (EdgeStatus[l1][k]==0))
						{
							EdgeStatus[k][l1]=-1;
							EdgeStatus[l1][k]=-1;
							update =true;
						}		
					}					
				}
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						if (EdgeStatus[k][l1]==2 && (EdgeStatus[l1][k]==2))
						{
							EdgeStatus[k][l1]=0;
							EdgeStatus[l1][k]=0;
							update =true;
						}		
					}					
				}
			}

			if (update)
			{
				do 
				{
					bool edgefilter; 
					double d1 = 99999D;
					SearchOver = true;

					#region steps control
					if (crcount>=maxcr&&maxcr!=0)
					{
						SearchOver=true;
						break;
					}
					#endregion


					//find the shortest edge
					for ( k = 0; k < vert.size(); k++)
					{
						for (int l1 = k+1; l1 < vert.size(); l1++)
						{
							edgefilter=false;

							if((PointStatus[k])<2&&(PointStatus[l1]<2))
								edgefilter=true; //** point does not lead to intersection situation
					
							/** this is delaunay edge, the point is end point*/
							if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
							{
								tail = l1;
								head = k;
								d1 = EdgeMap[k][l1]; //the Shortest edge
								SearchOver=false; //shortest delaunay edge is found
							}		
						}					
					}

					//reconsidered removed edges
					if (SearchOver)
						break;

					//clear connectivity value
					for(i=0;i<vert.size();i++)
					{
						connectivevalue[0,i]=-100d;
						connectivevalue[1,i]=-100d;
					}

					//check best point 
					// for initialize purpose 
					ExtendablePoints[0]=-1;
					ExtendablePoints[1]=-1;

					//first end point				
					Vision.Point2D pt= (Vision.Point2D)PL[head];										
					ExtendablePoints[0]=pt.CurveID;

					// second end point
					pt= (Vision.Point2D)PL[tail];
					ExtendablePoints[1]=pt.CurveID;

					int extindex=0;	
					int extendpoint=0;

					freedomdegree=0;

					if (ExtendablePoints[0]+ExtendablePoints[1]==-2)
					{ 		
						//reconsider
						p=1;
						if (connecting(ref vert, EdgeStatus, PointStatus, EdgeMap, head, tail, PL, p))
						{
							GCurveID++;
							((Vision.Point2D)PL[head]).CurveID=GCurveID;
							((Vision.Point2D)PL[tail]).CurveID=GCurveID;
							Vision.StCurve cr=new Vision.StCurve();
							cr.AddPoint((Vision.Point2D)PL[head]);
							cr.AddPoint((Vision.Point2D)PL[tail]);
							cr.curveID=GCurveID;
							CVL.AddCurve(cr);
							EdgeStatus[head][tail]=1;
							EdgeStatus[tail][head]=1;
                            //((int)PointStatus[head])++;
                            //((int)PointStatus[tail])++;
                            PointStatus[head] = ((int)PointStatus[head]) + 1;
                            PointStatus[tail] = ((int)PointStatus[tail]) + 1;
							crcount++; /** the number of edge for iteration view?? */												
						}
						else 
						{					
							EdgeStatus[head][tail]=2; //ignored from searching shortest edge
							EdgeStatus[tail][head]=2; //ignored from searching shortest edge
						}
					}
					else
					{
						for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
						{
							#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
						
		
							if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
							{
								freedomdegree++;

								Vision.Point2D hp=new Vision.Point2D(0,0);
								Vision.Point2D mp=new Vision.Point2D(0,0);
								Vision.Point2D ep=new Vision.Point2D(0,0);
									
								if (extendablePointsi==0)
									extendpoint=head;
								else
									extendpoint=tail;
									
								int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
								Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
								Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
								Vision.StCurve curve = new Vision.StCurve();	

								curve = (Vision.StCurve)mycr.Clone();
								extindex=curve.CatchPointEx(newpt);

								//if extindex = -1, the endpoint is not at index 0
								if (extindex>0)								
									extindex=-1;

								if (mycr.Count >=3)
								{
									//mycr = crDetectOutlier(mycr, extindex);
									curve = crDetectOutlier(curve, extindex);
								}
						
								double angletemp = 0;
								double areatemp = 0; //define the maximum distance a point can connect to a curve								
								Vision.Point2D second_point; //the point next to endpoint in the curve

								if (extindex==0)
									second_point = (Vision.Point2D)curve[1];
								else
									second_point = (Vision.Point2D)curve[curve.Count-2]; 

								areatemp = curve.DistanceMean()*range;								

								//////////////////////////
								///iterate all the points within a defined area 
								for (j = 0; j < vert.size(); j++)
								{
									Vision.Point2D point;
									
									//	if ((EdgeStatus[extendpoint][j]==0)&&(PointStatus[j]<2))
									if (((EdgeStatus[extendpoint][j]==0)||(EdgeStatus[extendpoint][j]==2))&&(PointStatus[j]<2))
									{
										point = (Vision.Point2D)PL[j];
										angletemp = CalAngle(point,newpt, second_point);										

										//	if ((EdgeStatus[extendpoint][j]==0)&&(EdgeMap[extendpoint][j]<=EdgeMap[head][tail]*2)) 
										//if ((EdgeMap[extendpoint][j]<areatemp)&&(60<angletemp)&&(angletemp<300))
										if ((EdgeMap[extendpoint][j]<areatemp)&&(45<angletemp)&&(angletemp<315))
											//if ((EdgeMap[extendpoint][j]<areatemp))
										{															
											// calcu the entendable length of each edge.
											//connectivevalue[extendablePointsi,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j]);
											connectivevalue[extendablePointsi,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j], smoothness);
										}
									}
			
								}
								/////////////////////////////////					
							}
							#endregion
						}

						/**
									 * Find the best point to connect to the existing curve?
									 */ 	

						#region find the best point
						bestconnectivevalue=-50d;
						bestextenalpoint=-1; /**< 0 for head, 1 for tai?? */
						bestconnectivepoint=-1; 
						for(i=0;i<2;i++)
							for(j=0;j<vert.size();j++)
							{
								if((connectivevalue[i,j]>bestconnectivevalue)&&(PointStatus[j]<2))
								{
									bestconnectivevalue=connectivevalue[i,j];
									bestextenalpoint=i;
									bestconnectivepoint=j;
								}
							}
						#endregion

						#region Update the curves
						if (bestconnectivevalue<0)
						{																				
							EdgeStatus[head][tail]=2;
							EdgeStatus[tail][head]=2;							
						}
						else if(bestconnectivevalue>0)
						{				
							if (bestextenalpoint==0)
								extendpoint=head;
							else
								extendpoint=tail;

							#region one side is no free

							//An: defined tempID
							int tempId = 	((Vision.Point2D)PL[bestconnectivepoint]).CurveID;
				
							if (tempId == -1)
							{
								((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;								
								Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
								extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
								if (extindex==0)
									cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
								else
									cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
								crcount++; /** curve count*/

							}
							#endregion one side is no free
								
							#region both sides are not free
							if (tempId!=-1)
							{
								//Join the two curve
									
								Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
								Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[bestconnectivepoint]).CurveID);								
								int hid=crh.curveID;
								int tid=crt.curveID;
								
								if(hid!=tid)
								{
									GCurveID++;
									for(j=0;j<PL.Count;j++)
									{
										if (((Vision.Point2D)PL[j]).CurveID==hid)
											((Vision.Point2D)PL[j]).CurveID=GCurveID;
										if (((Vision.Point2D)PL[j]).CurveID==tid)
											((Vision.Point2D)PL[j]).CurveID=GCurveID;
									}
									Vision.StCurve cr=CVL.ConnetTwoCurves2(ref crh,ref crt, (Vision.Point2D)PL[extendpoint], (Vision.Point2D)PL[bestconnectivepoint]);
									//		Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
									cr.curveID=GCurveID;							

									/* why remove???*/
									CVL.Remove(crh);
									CVL.Remove(crt);
									CVL.AddCurve(cr);						
								}
								else
									/**same curveID so connection will make a closed curve*/
									crh.Closed=1;							

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
								//crcount++;
								//	}
							}
							#endregion

							
					
							//int new_extend_point = extendpoint;
							if (CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID).Closed!=1)
							{
								#region repeating the update
								do 
								{

									update = false;

									Vision.Point2D firstendpoint;
									Vision.Point2D secondendpoint;


									Vision.StCurve crh =CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
									//for initialization purpose
									Vision.Point2D endpoint = crh.ReadHead(); //just for initialize purpose
									
									//iterate endpoint of the current curve
									for (k=0; k<2; k++)
									{																													
										if (k==1)
										{
											//if the endpoint in k=0 is not being connected
											// choose the endpoint on the other side
											if (endpoint == crh.ReadHead())
											{
												endpoint = crh.ReadTail();
											}
											else{endpoint = crh.ReadHead();}
											
										}
										else
											//choose the endpoint having shortest Delaunay edge
											// to consider first
										{											
											firstendpoint= crh.ReadHead(); //read headpoint of the curve													
											extendpoint = PL.IndexOf(firstendpoint, 0, PL.Count); 
											double distance = Double.MaxValue;

											for (k=0; k<vert.size(); k++)
											{
												if ((EdgeMap[k][extendpoint]<distance)&&(EdgeStatus[k][extendpoint]==0))
												{
													distance = EdgeMap[k][extendpoint];
													endpoint = firstendpoint;
												}
											}
													
											secondendpoint = crh.ReadTail();  //read tailpoint of the curve
											extendpoint = PL.IndexOf(secondendpoint, 0, PL.Count);
												
											for (k=0; k<vert.size(); k++)
											{
												if ((EdgeMap[k][extendpoint]<distance)&&(EdgeStatus[k][extendpoint]==0))
												{
													distance = EdgeMap[k][extendpoint];
													endpoint = secondendpoint;
												}
											}
										}
											//clear connectivity value
											for(i=0;i<vert.size();i++)
											{
												connectivevalue[0,i]=-100d;
												connectivevalue[1,i]=-100d;
											}

										
										//extendpoint = endpoint.;
										//int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
										int mycurveID=endpoint.CurveID; //find the curve
										//Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
										Vision.Point2D newpt=endpoint;  //find the point
										Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
										Vision.StCurve curve = new Vision.StCurve();	

										curve = (Vision.StCurve)mycr.Clone();
										extindex=curve.CatchPointEx(newpt);

										//if extindex = -1, the endpoint is not at index 0
										if (extindex>0)								
											extindex=-1;

										if (mycr.Count >=3)
										{
											//mycr = crDetectOutlier(mycr, extindex);
											curve = crDetectOutlier(curve, extindex);
										}
						
										double angletemp = 0;
										double areatemp = 0; //define the maximum distance a point can connect to a curve								
										Vision.Point2D second_point; //the point next to endpoint in the curve

										if (extindex==0)
											second_point = (Vision.Point2D)curve[1];
										else
											second_point = (Vision.Point2D)curve[curve.Count-2]; 

										areatemp = curve.DistanceMean()*range;								

										//////////////////////////
										///iterate all the points within a defined area 
										///


										for (j = 0; j < vert.size(); j++)
										{
											Vision.Point2D point;
									
											//		if ((EdgeStatus[extendpoint][j]==0)&&(PointStatus[j]<2))
											if (((EdgeStatus[extendpoint][j]==0)||(EdgeStatus[extendpoint][j]==2))&&(PointStatus[j]<2))
											{
												point = (Vision.Point2D)PL[j];
												angletemp = CalAngle(point,newpt, second_point);										

												//	if ((EdgeStatus[extendpoint][j]==0)&&(EdgeMap[extendpoint][j]<=EdgeMap[head][tail]*2)) 
												//	if ((EdgeMap[extendpoint][j]<areatemp)&&(60<angletemp)&&(angletemp<300))
												if ((EdgeMap[extendpoint][j]<areatemp)&&(45<angletemp)&&(angletemp<315))
													//if ((EdgeMap[extendpoint][j]<areatemp))
												{															
													// calcu the extendable length of each edge.
													//	connectivevalue[k,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j]);
													//	connectivevalue[0,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j]);
													connectivevalue[0,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j], smoothness);
												}
											}
			
										}



										//find the best point to connect
										#region find the best point
										bestconnectivevalue=-50d;
										bestextenalpoint=-1; /**< 0 for head, 1 for tai?? */
										bestconnectivepoint=-1; 
										//	for(i=0;i<2;i++)
										for(j=0;j<vert.size();j++)
										{
											//			if((connectivevalue[i,j]>bestconnectivevalue)&&(PointStatus[j]<2))
											if((connectivevalue[0,j]>bestconnectivevalue)&&(PointStatus[j]<2))
											{
												bestconnectivevalue=connectivevalue[0,j];
												bestextenalpoint=0;
												bestconnectivepoint=j;
											}
										}
										#endregion find the best point

										#region Update the curves

										if(bestconnectivevalue>0)
										{				
											update = true;

											#region one side is no free
											//An: defined tempID
											tempId = 	((Vision.Point2D)PL[bestconnectivepoint]).CurveID;
				

											//An: freedomdegree may not be used here
											//							if (freedomdegree==1)
											if (tempId == -1)
											{
												((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;								
												Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
												extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
												if (extindex==0)
													cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
												else
													cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);

												EdgeStatus[extendpoint][bestconnectivepoint]=1;
												EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                                //((int)PointStatus[extendpoint])++;
                                                //((int)PointStatus[bestconnectivepoint])++;
                                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
												//	crcount++; /** curve count*/

											}
											#endregion oneside is no free
								
											#region both sides are not free
											//		if (freedomdegree==2)
											if (tempId!=-1)
											{
												//Join the two curve
									
												crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
												Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[bestconnectivepoint]).CurveID);								
												int hid=crh.curveID;
												int tid=crt.curveID;
								
												if(hid!=tid)
												{
													GCurveID++;
													for(j=0;j<PL.Count;j++)
													{
														if (((Vision.Point2D)PL[j]).CurveID==hid)
															((Vision.Point2D)PL[j]).CurveID=GCurveID;
														if (((Vision.Point2D)PL[j]).CurveID==tid)
															((Vision.Point2D)PL[j]).CurveID=GCurveID;
													}
													Vision.StCurve cr=CVL.ConnetTwoCurves2(ref crh,ref crt,(Vision.Point2D)PL[extendpoint], (Vision.Point2D)PL[bestconnectivepoint]);
													//		Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
													cr.curveID=GCurveID;							

													/* why remove???*/
													CVL.Remove(crh);
													CVL.Remove(crt);
													CVL.AddCurve(cr);						
												}
												else
													/**same curveID so connection will make a closed curve*/
													crh.Closed=1;							

												EdgeStatus[extendpoint][bestconnectivepoint]=1;
												EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                                //((int)PointStatus[extendpoint])++;
                                                //((int)PointStatus[bestconnectivepoint])++;
                                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
												//	crcount++;
												//	}
										
											}
											#endregion both sides are not free
										}
										#endregion update the curves
											
										if (update==true)
										{
											break;
										}
									}//endfor 
								}while(update);
								#endregion repeating the update
							}
						}
						#endregion update the curves 
					} //end else-if not free points
				}while (!SearchOver);
			}//endif update
	
			#endregion reconsider removed edge

			//Draw curves
			#region Draw curves

			for (i = 0; i < vert.size(); i++)
			{
				//remove all the delaunay edge for interior point
				if (PointStatus[i]==2)
				{
					for (int t=0; t<vert.size(); t++)						
					{
						if (EdgeStatus[i][t]==0)
						{
							EdgeStatus[i][t] = -1;
							EdgeStatus[t][i] = -1;
						}
					}
				}
			}

// Console.WriteLine("EdgeStatus[0][0]={0}", EdgeStatus[0][0]);
// Console.WriteLine("EdgeStatus[1][1]={0}", EdgeStatus[1][1]);

			return EdgeStatus;
		}

		static int[,] computeVICURWrapper(float[] coords)
		{
			int i, j;
			int maxcr = 0;	// default value
			double smoothness = 0.8;	// default value
			Vertex vert = new Vertex();

			double maxabsx = 0.0, maxabsy = 0.0;

			for (i = 0; i < coords.Length/2; i++)
			{
				if (Math.Abs(coords[i*2]) > maxabsx)
					maxabsx = Math.Abs(coords[i*2]);

				if (Math.Abs(coords[i*2 + 1]) > maxabsy)
					maxabsy = Math.Abs(coords[i*2 + 1]);
			}

			double maxabs = maxabsx;

			if (maxabsy > maxabsx)
				maxabs = maxabsy;

			//double factor = int.MaxValue/maxabs;
			double factor = 1024/maxabs;

			for (i = 0; i < coords.Length/2; i++)
			{
				vert.add((int)Math.Floor(coords[i*2]*factor), (int)Math.Floor(coords[i*2 + 1]*factor));
//				Console.WriteLine(string.Format("{0} {1}", (int)Math.Floor(coords[i*2]*factor), (int)Math.Floor(coords[i*2 + 1]*factor)));
			}

			Delaunay delaunay1 = new Delaunay(vert.size());

			int[][] EdgeStatus = computeVICUR(ref vert, ref delaunay1, maxcr, smoothness);

// Console.WriteLine("EdgeStatus[0][0]={0}", EdgeStatus[0][0]);
// Console.WriteLine("EdgeStatus[1][1]={0}", EdgeStatus[1][1]);

			int[,] testarray = new int[vert.size(), vert.size()];

			for (j = 0; j < vert.size(); j++)
				for (i = 0; i < vert.size(); i++)
					testarray[j,i] = EdgeStatus[j][i];

			return testarray;
		}

		internal virtual void  drawAngleDistance3Delauney(System.Drawing.Graphics g)
		{
			Delaunay delaunay1 = new Delaunay(vert.size());
			System.Collections.ArrayList nodes=delaunay1.nodes;
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
			if (nodes.Count == 1)
				//**each point is a rectangle of size 1*/				
				g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
			//g.DrawEllipse(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
			if (nodes.Count == 2)
				g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, ((Node) nodes[1]).x, ((Node) nodes[1]).y);

			int maxcr=(int)numericUpDown3.Value;
			double smoothness = (double)numericUpDown4.Value;

// Console.WriteLine(string.Format("maxcr={0}, smoothness={1}", maxcr, smoothness));

			int[][] EdgeStatus = computeVICUR(ref vert, ref delaunay1, maxcr, smoothness);

			int i, j;

			for (i = 0; i < vert.size(); i++)
			{

				for (j = i + 1; j < vert.size(); j++)
				{
					//					if (EdgeStatus[i][j]==0)
					//					{
					//						SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Magenta);
					//						g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
					//					}



					if (EdgeStatus[i][j]==1)
					{
						SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
						Pen myPen = SupportClass.GraphicsManager.manager.GetPen(g);
						//line size
					    //myPen.Width = 2;
                        // SupportClass.GraphicsManager.manager.SetBrush(g, System.Drawing.SolidBrush);
						g.DrawLine(myPen, vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
						
					}

					//					if (EdgeStatus[i][j]==2)
					//					{
					//						SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Blue);
					//						g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
					//					}
					//					SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
					
				}
			}
			#endregion 
			/////						
		}
		
		private static Vision.StCurve crDetectOutlier(Vision.StCurve mycurve, int index)
		{
			double temp=0;
			ArrayList angletemp = new ArrayList();

			((Vision.Point2D)mycurve[0]).Angle = -1;
			((Vision.Point2D)mycurve[mycurve.Count-1]).Angle = -1;

			//if endpoint is the first index
			if (index==0)
			{

//				if (mycurve.Count > 10)
//				{
//					mycurve.RemoveRange(10,mycurve.Count-10);
//				}
				//compute the angle
				for (int k=1; k<mycurve.Count-1; k++)
				{
					temp = CalAngle((Vision.Point2D)mycurve[k-1], (Vision.Point2D)mycurve[k], (Vision.Point2D)mycurve[k+1]);
					((Vision.Point2D)mycurve[k]).Angle = temp;
				}

				//remove all angle outside [135, 225]
				for (int k=1; k<mycurve.Count-1; k++) //exclude the endpoint
				{
					
					if ((((Vision.Point2D)mycurve[k]).Angle >225)||(((Vision.Point2D)mycurve[k]).Angle <135))
					{
						mycurve.RemoveRange(k+1, mycurve.Count-k-1);	
						break;
					}
				}
				
				if (mycurve.Count>=6)  //at least 3 angles
				{
					//detect outlier
					//add all the angles into the angle array
					for (int k=1; k<mycurve.Count-1; k++) //exclude the endpoint
					{
						angletemp.Add(((Vision.Point2D)mycurve[k]).Angle);
					}				
					angletemp = DetectOutlier(angletemp);
				
					//remove all the outliers
					for (int k=0; k<angletemp.Count; k++)
					{
						if (((Vision.Point2D)mycurve[k+1]).Angle != (double)angletemp[k])
						{
							mycurve.RemoveRange(k+1, mycurve.Count-k-1);
							break;
						}
					}

				}
							
			}
			else 
			{
				
//				if (mycurve.Count > 10)
//				{
//					mycurve.RemoveRange(0,mycurve.Count-10);
//				}
				//compute the angle
				for (int k=mycurve.Count-1; k>1; k--)
				{
					temp = CalAngle((Vision.Point2D)mycurve[k], (Vision.Point2D)mycurve[k-1], (Vision.Point2D)mycurve[k-2]);
					((Vision.Point2D)mycurve[k-1]).Angle = temp;
				}

				//remove all angle outside [135, 225]
				for (int k=mycurve.Count-2; k>0; k--) //exclude the endpoint
				{
					
					if ((((Vision.Point2D)mycurve[k]).Angle >225)||(((Vision.Point2D)mycurve[k]).Angle <135))
					{
						//mycurve.RemoveRange(k-1, k);	
						mycurve.RemoveRange(0, k);
						break;
					}
				}

				//detect outlier
				if (mycurve.Count>=6)
				{
					//add all the angles into the angle array
					for (int k=mycurve.Count-2; k>0; k--) //exclude the endpoint
					{
						angletemp.Add(((Vision.Point2D)mycurve[k]).Angle);
					}				
					angletemp = DetectOutlier(angletemp);

					//remove all the outliers
					for (int k=0; k<angletemp.Count; k++)
					{
						if (((Vision.Point2D)mycurve[mycurve.Count-k-2]).Angle != (double)angletemp[k])
						{
							//mycurve.RemoveRange(mycurve.Count-k-3,mycurve.Count-k-2);
							//mycurve.RemoveRange(0,mycurve.Count-k-2);
							mycurve.RemoveRange(0,mycurve.Count-k-2);
							break;
						}
					}

				}
				
			}

			return mycurve;
				
		}
		

		//Formula obtained from Experiment		
		internal virtual void  drawExperiment(System.Drawing.Graphics g)
		{
			// * Construct Delauney Trianglation.		
			// * Find the shortest edge.
			double [,] connectivevalue=new double[2,vert.size()];
			int bestconnectivepoint=-1;
			int bestextenalpoint=-1;
			double bestconnectivevalue=-20;
			int freedomdegree=0;
			
			#region calculate the delaunay  trianglation
			int GCurveID=0;
			int maxcr=(int)numericUpDown3.Value;
			int crcount=0;
			
			SupportClass.GraphicsManager.manager.SetColor(g, System.Drawing.Color.Black);
			Delaunay delaunay1 = new Delaunay(vert.size());

			Vision.StCurveList CVL= new Vision.StCurveList();
			int [] ExtendablePoints={0,0};
			int i=0,j=0,k;
			int head = 0;
			int tail=0;

			/**
			 * Insert all the point into the delaunay array for delaunay triangulation
			 */ 
			for (i = 0; i < vert.size(); i++)
				delaunay1.Insert(vert.get_Renamed(i).X, vert.get_Renamed(i).Y, 0);
			System.Collections.ArrayList nodes=delaunay1.nodes;
			System.Collections.ArrayList edges=delaunay1.edges;
			System.Collections.ArrayList curveedge=new ArrayList();
			System.Collections.ArrayList curvepoint=new ArrayList();
			if (nodes.Count == 1)
				//**each point is a rectangle of size 1*/
				g.DrawRectangle(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
				//g.DrawEllipse(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, 1, 1);
			if (nodes.Count == 2)
				g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), ((Node) nodes[0]).x, ((Node) nodes[0]).y, ((Node) nodes[1]).x, ((Node) nodes[1]).y);
			#endregion

			#region initialize the data
			/** 	
			 * edgemap contains distance between 2 points if applicable
			 */
			double[][] EdgeMap = new double[vert.size()][];
			/*
			 * edgestatus = -1 -> not delaunay edge
			 * edgestatus = 0 -> delaunay edge
			 * edgestatus = 1 -> connected
			 */
			int [][] EdgeStatus= new int[vert.size()][];
			int [] PointStatus=new int[vert.size()];
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i] = new double[vert.size()];
				EdgeStatus[i]= new int[vert.size()];
				PointStatus[i]=0;
			}
			/**
			 * Initialize all EdgeMap and EdgeSatus to value -1
			 */ 
			for (i = 0; i < vert.size(); i++)
			{
				EdgeMap[i][i]= -1;
				EdgeStatus[i][i]=-1;
				for (j = i + 1; j < vert.size(); j++)
				{
					EdgeMap[i][j]= -1;
					EdgeStatus[i][j]=-1;
					EdgeMap[j][i]= -1;
					EdgeStatus[j][i]=-1;
				}
			}
			
			/**
			 * find 2 points that around the two end points of the Delaunay edge
			 * Calculate the distance between these 2 points
			 */ 
			for (k = 0; k < edges.Count; k++)
			{
				Edge myedge=(Edge)edges[k];
				i=findPt(myedge.p1.x,myedge.p1.y);
				j=findPt(myedge.p2.x,myedge.p2.y);
				double d = vert.get_Renamed(j).X - vert.get_Renamed(i).X;
				double d2 = vert.get_Renamed(j).Y - vert.get_Renamed(i).Y;
				EdgeMap[i][j]=Math.Sqrt( d * d + d2 * d2);
				EdgeStatus[i][j]=0;
				EdgeMap[j][i]= EdgeMap[i][j];
				EdgeStatus[j][i]=0;
			}

			/**
			 * add all the sampling points to pointlist
			 * initialize curveid = -1
			 */ 
			Vision.PointList PL=new Vision.PointList();
			for (i=0 ; i< vert.size(); i++)
			{
				Vision.Point2D pt = new Vision.Point2D(0,0);
				pt.CurveID=-1;
				pt.X= vert.get_Renamed(i).X;
				pt.Y=vert.get_Renamed(i).Y;
				PL.Add(pt);
			}
			#endregion //end region for initialize data

			//Mapping the edge distance. Status=0 means the dalaunay edge.
			bool SearchOver=false;
			int edgepriority=0;  // The priority of edges in some degree.
			int searchStage = 0; //0-> initial search, 1-> include ignored edge
			do
			{
				#region steps control
				if (crcount>=maxcr&&maxcr!=0)
				{
					SearchOver=true;
					break;
				}
				#endregion

				bool edgesearchover=false; // All the adges of edgepriorty are searched over.
				bool edgefilter=false;// Filter of edges in different degree;	
				double d1 = 99999D;
				SearchOver=false;
				head = 0;
				tail=0;
				edgesearchover=true; /* = true -> search again */
				
				#region find the shortest edge				
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						edgefilter=false;

						if((PointStatus[k])<2&&(PointStatus[l1]<2))
							edgefilter=true; //** point does not lead to intersection situation
					
						/** this is delaunay edge, the point is end point*/
						if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
						{
							tail = l1;
							head = k;
							d1 = EdgeMap[k][l1]; //the Shortest edge
							edgesearchover=false; //shortest delaunay edge is found
						}		
					}					
				}
								
				if (edgesearchover) /** Cannot find the shortest edge??*/
				{
					//		edgepriority++;

					///		if (edgepriority>2)
					//		{
					SearchOver=true; /** get out, not search again */
					//	if (searchStage ==0)
					//	{
//					for ( k = 0; k < vert.size(); k++)
//					{
//						for (int l1 = k+1; l1 < vert.size(); l1++)
//						{		
//							if (EdgeStatus[k][l1]==2)
//							{
//								EdgeStatus[k][l1]=0;
//								EdgeStatus[l1][k]=0;
//							}
//						}
//					}						 
					//}
						
					//			if (searchStage==2)
					//				SearchOver=true;
						
					//			searchStage++;

					//	}
				}
				#endregion				
				
					
				/////TO DO : USE DISTANCECOMPARABLE TO IMPROVE RUNNING SPEED.
				if (SearchOver)
				{
					break;  //search over
				}
				
				/** if shortest edge is found */
				if (!edgesearchover)                // Shortest points pair will be connected directly if their degree is 2.  Totally free.				
				{
					SearchOver=false;  /** < continue to search for next shortest edge  */
					
					// for initialize purpose 
					ExtendablePoints[0]=-1;
					ExtendablePoints[1]=-1;

					//first end point				
					Vision.Point2D pt= (Vision.Point2D)PL[head];										
					ExtendablePoints[0]=pt.CurveID;

					// second end point
					pt= (Vision.Point2D)PL[tail];
					ExtendablePoints[1]=pt.CurveID;

					/** if these are two free points 
					 *  check the angle 
					 */  
					#region free edge
					if (ExtendablePoints[0]+ExtendablePoints[1]==-2)
					{ 						
						p=1; //for connecting method 1-> head is currently considered, 2-> tail
						// ... code here
						if (connecting(EdgeStatus, PointStatus, EdgeMap, head, tail, PL))
						{
							GCurveID++;
							((Vision.Point2D)PL[head]).CurveID=GCurveID;
							((Vision.Point2D)PL[tail]).CurveID=GCurveID;
							Vision.StCurve cr=new Vision.StCurve();
							cr.AddPoint((Vision.Point2D)PL[head]);
							cr.AddPoint((Vision.Point2D)PL[tail]);
							cr.curveID=GCurveID;
							CVL.AddCurve(cr);
							EdgeStatus[head][tail]=1;
							EdgeStatus[tail][head]=1;
                            //((int)PointStatus[head])++;
                            //((int)PointStatus[tail])++;
                            PointStatus[head] = ((int)PointStatus[head]) + 1;
                            PointStatus[tail] = ((int)PointStatus[tail]) + 1;
							crcount++; /** the number of edge for iteration view?? */												
						}
						else 
						{					
							EdgeStatus[head][tail]=2; //ignored from searching shortest edge
							EdgeStatus[tail][head]=2; //ignored from searching shortest edge
						}
					}			
					
					#endregion
					

					//Step2 Extend the curve
					//enpoint(s) is/are not free
					#region endpoint(s) is/are not free
					int extindex=0;	
					int extendpoint=0;
					
					if ((ExtendablePoints[0]!=-1)||(ExtendablePoints[1]!=-1))             //if any side has curve, we figure the situation.
					{
						#region clear connective value
						for(i=0;i<vert.size();i++)
						{
							connectivevalue[0,i]=-100d;
							connectivevalue[1,i]=-100d;
						}
						#endregion
						
						freedomdegree=0;

						//	extindex=0;
						//	extendpoint=0;
						//int[] count = new int[2];
						for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
						{
							#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
								
							if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
							{
								freedomdegree++;

								Vision.Point2D hp=new Vision.Point2D(0,0);
								Vision.Point2D mp=new Vision.Point2D(0,0);
								Vision.Point2D ep=new Vision.Point2D(0,0);
									
								if (extendablePointsi==0)
									extendpoint=head;
								else
									extendpoint=tail;
									
								int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
								Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
								Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
								Vision.StCurve curve = new Vision.StCurve();	

								curve = (Vision.StCurve)mycr.Clone();
								extindex=curve.CatchPointEx(newpt);

								//if extindex = -1, the endpoint is not at index 0
								if (extindex>0)								
									extindex=-1;

								if (mycr.Count >=3)
								{
									//mycr = crDetectOutlier(mycr, extindex);
									curve = crDetectOutlier(curve, extindex);
								}
						
								double angletemp = 0;
								double areatemp = 0; //define the maximum distance a point can connect to a curve								
								Vision.Point2D second_point; //the point next to endpoint in the curve

								if (extindex==0)
									second_point = (Vision.Point2D)curve[1];
								else
									second_point = (Vision.Point2D)curve[curve.Count-2]; 

//								Function YData=m1+m2*sin(x1*PI/2)+m3*(x2+m4)*cos(((x3-x1)*m5)^2*PI/2)+m6*sin(x3*m7*PI/2);

//								m1         	-0.461542421850068
//								m2         	1.37240240004903
//								m3         	0.0486207650351494
//								m4         	12.3477500018619
//								m5         	-22.9044375771555
//								m6         	0.575410411637972
//								m7         	1.71006056587997
//
//								x1 = ref_angle/180
//								x2 = std_dis / (ave_dis * 0.4931)
//								x3 = angle_mean / 180

//								double m1 =	-0.461542421850068;
//								double m2 =  	1.37240240004903;
//								double m3 =        	0.0486207650351494;
//								double m4 =        	12.3477500018619;
//								double m5 =        	-22.9044375771555;
//								double m6 =        	0.575410411637972;
//								double m7 =        	1.71006056587997;
//
//								double x1 = 0;
//								double x2 = curve.StdDistance() / (curve.DistanceMean() * 0.4931);
//								double x3 = curve.AngleMean() / 180;
								int range = 2;
								areatemp = curve.DistanceMean()*range;		
								//////////////////////////
								///iterate all the points within a defined area (shortest edge*2)
								for (j = 0; j < vert.size(); j++)
								{
									Vision.Point2D point;
									
									if (((EdgeStatus[extendpoint][j]==0)||(EdgeStatus[extendpoint][j]==2))&&(PointStatus[j]<2))
									{
										point = (Vision.Point2D)PL[j];
										angletemp = CalAngle(point,newpt, second_point);										
									//	x1 = angletemp/180;
																				
//										areatemp = ((x3-x1)*m5)*((x3-x1)*m5)*Math.PI/2;
//										areatemp = m3*(x2+m4)*Math.Cos(areatemp);
//										areatemp = areatemp  + m1 + m2*Math.Sin(x1*Math.PI/2);
//										areatemp = areatemp + m6*Math.Sin(x3*m7*Math.PI/2);
																					
									//	areatemp = areatemp*curve.DistanceMean();
										//	if ((EdgeStatus[extendpoint][j]==0)&&(EdgeMap[extendpoint][j]<=EdgeMap[head][tail]*2)) 
										//	if ((EdgeMap[extendpoint][j]<areatemp)&&(60<angletemp)&&(angletemp<300))
										if ((EdgeMap[extendpoint][j]<areatemp)&&(45<=angletemp)&&(angletemp<=315))										
										{															
											// calcu the entendable length of each edge.
											//connectivevalue[extendablePointsi,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j], (double)numericUpDown4.Value);
											connectivevalue[extendablePointsi,j]=curve.experiment(angletemp,EdgeMap[extendpoint][j] );
										}
									}
			
								}
								/////////////////////////////////					
							}
							#endregion
						}

						/**
							 * Find the best point to connect to the existing curve?
							 */ 	

						#region find the best point
						bestconnectivevalue=-50d;
						bestextenalpoint=-1; /**< 0 for head, 1 for tai?? */
						bestconnectivepoint=-1; 
						for(i=0;i<2;i++)
							for(j=0;j<vert.size();j++)
							{
								if((connectivevalue[i,j]>bestconnectivevalue)&&(PointStatus[j]<2))
								{
									bestconnectivevalue=connectivevalue[i,j];
									bestextenalpoint=i;
									bestconnectivepoint=j;
								}
							}
						#endregion

						#region Update the curves
						if (bestconnectivevalue<0)
						{																				
							EdgeStatus[head][tail]=2;
							EdgeStatus[tail][head]=2;							
						}
						else 
							if(bestconnectivevalue>0)
						{				
							if (bestextenalpoint==0)
								extendpoint=head;
							else
								extendpoint=tail;

							#region one side is no free

							//An: defined tempID
							int tempId = 	((Vision.Point2D)PL[bestconnectivepoint]).CurveID;
							//An: freedomdegree may not be used here
							//							if (freedomdegree==1)
							if (tempId == -1)
							{
								((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;
								Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
								extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
								if (extindex==0)
									cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
								else
									cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
								crcount++; /** curve count*/

							}
							#endregion
								
							#region both sides are not free
							//		if (freedomdegree==2)
							if (tempId!=-1)
							{
								//Join the two curve
									
								Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
								Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[bestconnectivepoint]).CurveID);
								int hid=crh.curveID;
								int tid=crt.curveID;
								
								//if ((extendablePointsi==0)&&(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0))
								//fzconnection++;
								//	if(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0)
								//	{
								if(hid!=tid)
								{
									GCurveID++;
									for(j=0;j<PL.Count;j++)
									{
										if (((Vision.Point2D)PL[j]).CurveID==hid)
											((Vision.Point2D)PL[j]).CurveID=GCurveID;
										if (((Vision.Point2D)PL[j]).CurveID==tid)
											((Vision.Point2D)PL[j]).CurveID=GCurveID;
									}
									Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
									cr.curveID=GCurveID;							

									/* why remove???*/
									CVL.Remove(crh);
									CVL.Remove(crt);
									CVL.AddCurve(cr);						
								}
								else
									/**same curveID so connection will make a closed curve*/
									crh.Closed=1;							

								EdgeStatus[extendpoint][bestconnectivepoint]=1;
								EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                //((int)PointStatus[extendpoint])++;
                                //((int)PointStatus[bestconnectivepoint])++;
                                PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
								crcount++;
								//	}
							}
							#endregion

						}
						#endregion update the curve
					}						
					#endregion endpoints not free
				}
				
			}while(!SearchOver);


			#region reconsider removed edge
						
			for ( k = 0; k < vert.size(); k++)
			{
				for (int l1 = k+1; l1 < vert.size(); l1++)
				{
					if (EdgeStatus[k][l1]==2 && (EdgeStatus[l1][k]==2))
					{
						EdgeStatus[k][l1]=0;
						EdgeStatus[l1][k]=0;
					}		
				}					
			}

			do 
			{
				bool edgefilter; 
				double d1 = 99999D;
				SearchOver = true;

				#region steps control
				if (crcount>=maxcr&&maxcr!=0)
				{
					SearchOver=true;
					break;
				}
				#endregion


				//find the shortest edge
				for ( k = 0; k < vert.size(); k++)
				{
					for (int l1 = k+1; l1 < vert.size(); l1++)
					{
						edgefilter=false;

						if((PointStatus[k])<2&&(PointStatus[l1]<2))
							edgefilter=true; //** point does not lead to intersection situation
					
						/** this is delaunay edge, the point is end point*/
						if (EdgeMap[k][l1] < d1&&EdgeStatus[k][l1]==0&&edgefilter)
						{
							tail = l1;
							head = k;
							d1 = EdgeMap[k][l1]; //the Shortest edge
							SearchOver=false; //shortest delaunay edge is found
						}		
					}					
				}

				if (SearchOver)
					break;

				//clear connectivity value
				for(i=0;i<vert.size();i++)
				{
					connectivevalue[0,i]=-100d;
					connectivevalue[1,i]=-100d;
				}

				//check best point 
				// for initialize purpose 
				ExtendablePoints[0]=-1;
				ExtendablePoints[1]=-1;

				//first end point				
				Vision.Point2D pt= (Vision.Point2D)PL[head];										
				ExtendablePoints[0]=pt.CurveID;

				// second end point
				pt= (Vision.Point2D)PL[tail];
				ExtendablePoints[1]=pt.CurveID;

				int extindex=0;	
				int extendpoint=0;

				freedomdegree=0;

				for(int extendablePointsi=0;extendablePointsi<2;extendablePointsi++) //iterate both sides
				{
					#region  Deal with non-free-edge. If it is nonfree point to find the best connection and connection value. head is 0
								
					if (ExtendablePoints[extendablePointsi]!=-1)//Deal with the curve end point  . find the extentional point.
					{
						freedomdegree++;

						Vision.Point2D hp=new Vision.Point2D(0,0);
						Vision.Point2D mp=new Vision.Point2D(0,0);
						Vision.Point2D ep=new Vision.Point2D(0,0);
									
						if (extendablePointsi==0)
							extendpoint=head;
						else
							extendpoint=tail;
									
						int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
						Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
						Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
						Vision.StCurve curve = new Vision.StCurve();	

						curve = (Vision.StCurve)mycr.Clone();
						extindex=curve.CatchPointEx(newpt);

						//if extindex = -1, the endpoint is not at index 0
						if (extindex>0)								
							extindex=-1;

						if (mycr.Count >=3)
						{
							//mycr = crDetectOutlier(mycr, extindex);
							curve = crDetectOutlier(curve, extindex);
						}
						
						double angletemp = 0;
						double areatemp = 0; //define the maximum distance a point can connect to a curve								
						Vision.Point2D second_point; //the point next to endpoint in the curve

						if (extindex==0)
							second_point = (Vision.Point2D)curve[1];
						else
							second_point = (Vision.Point2D)curve[curve.Count-2]; 

						//								Function YData=m1+m2*sin(x1*PI/2)+m3*(x2+m4)*cos(((x3-x1)*m5)^2*PI/2)+m6*sin(x3*m7*PI/2);

						//								m1         	-0.461542421850068
						//								m2         	1.37240240004903
						//								m3         	0.0486207650351494
						//								m4         	12.3477500018619
						//								m5         	-22.9044375771555
						//								m6         	0.575410411637972
						//								m7         	1.71006056587997
						//
						//								x1 = ref_angle/180
						//								x2 = std_dis / (ave_dis * 0.4931)
						//								x3 = angle_mean / 180

						//								double m1 =	-0.461542421850068;
						//								double m2 =  	1.37240240004903;
						//								double m3 =        	0.0486207650351494;
						//								double m4 =        	12.3477500018619;
						//								double m5 =        	-22.9044375771555;
						//								double m6 =        	0.575410411637972;
						//								double m7 =        	1.71006056587997;
						//
						//								double x1 = 0;
						//								double x2 = curve.StdDistance() / (curve.DistanceMean() * 0.4931);
						//								double x3 = curve.AngleMean() / 180;
						int range = 2;
						areatemp = curve.DistanceMean()*range;		
						//////////////////////////
						///iterate all the points within a defined area (shortest edge*2)
						for (j = 0; j < vert.size(); j++)
						{
							Vision.Point2D point;
									
							if (((EdgeStatus[extendpoint][j]==0)||(EdgeStatus[extendpoint][j]==2))&&(PointStatus[j]<2))
							{
								point = (Vision.Point2D)PL[j];
								angletemp = CalAngle(point,newpt, second_point);										
								//	x1 = angletemp/180;
																				
								//										areatemp = ((x3-x1)*m5)*((x3-x1)*m5)*Math.PI/2;
								//										areatemp = m3*(x2+m4)*Math.Cos(areatemp);
								//										areatemp = areatemp  + m1 + m2*Math.Sin(x1*Math.PI/2);
								//										areatemp = areatemp + m6*Math.Sin(x3*m7*Math.PI/2);
																					
								//	areatemp = areatemp*curve.DistanceMean();
								//	if ((EdgeStatus[extendpoint][j]==0)&&(EdgeMap[extendpoint][j]<=EdgeMap[head][tail]*2)) 
								//	if ((EdgeMap[extendpoint][j]<areatemp)&&(60<angletemp)&&(angletemp<300))
								if ((EdgeMap[extendpoint][j]<areatemp)&&(45<=angletemp)&&(angletemp<=315))										
								{															
									// calcu the entendable length of each edge.
									//connectivevalue[extendablePointsi,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j], (double)numericUpDown4.Value);
									connectivevalue[extendablePointsi,j]=curve.experiment(angletemp,EdgeMap[extendpoint][j] );
								}
							}
			
						}
						/////////////////////////////////					
					}
					#endregion
				}

				/**
							 * Find the best point to connect to the existing curve?
							 */ 	

				#region find the best point
				bestconnectivevalue=-50d;
				bestextenalpoint=-1; /**< 0 for head, 1 for tai?? */
				bestconnectivepoint=-1; 
				for(i=0;i<2;i++)
					for(j=0;j<vert.size();j++)
					{
						if((connectivevalue[i,j]>bestconnectivevalue)&&(PointStatus[j]<2))
						{
							bestconnectivevalue=connectivevalue[i,j];
							bestextenalpoint=i;
							bestconnectivepoint=j;
						}
					}
				#endregion

				#region Update the curves
				if (bestconnectivevalue<0)
				{																				
					EdgeStatus[head][tail]=2;
					EdgeStatus[tail][head]=2;							
				}
				else if(bestconnectivevalue>0)
				{				
					if (bestextenalpoint==0)
						extendpoint=head;
					else
						extendpoint=tail;

					#region one side is no free

					//An: defined tempID
					int tempId = 	((Vision.Point2D)PL[bestconnectivepoint]).CurveID;
				

					//An: freedomdegree may not be used here
					//							if (freedomdegree==1)
					if (tempId == -1)
					{
						((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;								
						Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
						extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
						if (extindex==0)
							cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
						else
							cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);

						EdgeStatus[extendpoint][bestconnectivepoint]=1;
						EdgeStatus[bestconnectivepoint][extendpoint]=1;
                        //((int)PointStatus[extendpoint])++;
                        //((int)PointStatus[bestconnectivepoint])++;
                        PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                        PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
						crcount++; /** curve count*/

					}
					#endregion one side is no free
								
					#region both sides are not free
					//		if (freedomdegree==2)
					if (tempId!=-1)
					{
						//Join the two curve
									
						Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
						Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[bestconnectivepoint]).CurveID);								
						int hid=crh.curveID;
						int tid=crt.curveID;
								
						//if ((extendablePointsi==0)&&(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0))
						//fzconnection++;
						//	if(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0)
						//	{
						if(hid!=tid)
						{
							GCurveID++;
							for(j=0;j<PL.Count;j++)
							{
								if (((Vision.Point2D)PL[j]).CurveID==hid)
									((Vision.Point2D)PL[j]).CurveID=GCurveID;
								if (((Vision.Point2D)PL[j]).CurveID==tid)
									((Vision.Point2D)PL[j]).CurveID=GCurveID;
							}
							Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
							cr.curveID=GCurveID;							

							/* why remove???*/
							CVL.Remove(crh);
							CVL.Remove(crt);
							CVL.AddCurve(cr);						
						}
						else
							/**same curveID so connection will make a closed curve*/
							crh.Closed=1;							

						EdgeStatus[extendpoint][bestconnectivepoint]=1;
						EdgeStatus[bestconnectivepoint][extendpoint]=1;
                        //((int)PointStatus[extendpoint])++;
                        //((int)PointStatus[bestconnectivepoint])++;
                        PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                        PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
						crcount++;
						//	}
					}
					#endregion

							
					bool update;
					//int new_extend_point = extendpoint;
					#region repeating the update
					do 
					{
						//clear connectivity value
						for(i=0;i<vert.size();i++)
						{
							connectivevalue[0,i]=-100d;
							connectivevalue[1,i]=-100d;
						}
						update = false;


						//iterate endpoint of the current curve
						for (k=0; k<2; k++)
						{
							//		Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[new_extend_point]).CurveID);
							Vision.StCurve crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
							Vision.Point2D endpoint;
									
							if (k==0)										
								endpoint = crh.ReadHead(); //read headpoint								
							else 										
								endpoint = crh.ReadTail();  //read tailpoint
									
							//extendpoint = endpoint.;
							//int mycurveID=((Vision.Point2D)PL[extendpoint]).CurveID; //find the curve
							int mycurveID=endpoint.CurveID; //find the curve
							//Vision.Point2D newpt=(Vision.Point2D)PL[extendpoint];  //find the point
							Vision.Point2D newpt=endpoint;  //find the point
							Vision.StCurve mycr=CVL.ReadCurve(mycurveID);    //read the curve
							Vision.StCurve curve = new Vision.StCurve();	

							curve = (Vision.StCurve)mycr.Clone();
							extindex=curve.CatchPointEx(newpt);

							//if extindex = -1, the endpoint is not at index 0
							if (extindex>0)								
								extindex=-1;

							if (mycr.Count >=3)
							{
								//mycr = crDetectOutlier(mycr, extindex);
								curve = crDetectOutlier(curve, extindex);
							}
						
							double angletemp = 0;
							double areatemp = 0; //define the maximum distance a point can connect to a curve								
							Vision.Point2D second_point; //the point next to endpoint in the curve

							if (extindex==0)
								second_point = (Vision.Point2D)curve[1];
							else
								second_point = (Vision.Point2D)curve[curve.Count-2]; 

							
							double m1 =	-0.461542421850068;
							double m2 =  	1.37240240004903;
							double m3 =        	0.0486207650351494;
							double m4 =        	12.3477500018619;
							double m5 =        	-22.9044375771555;
							double m6 =        	0.575410411637972;
							double m7 =        	1.71006056587997;

							double x1 = 0;
							double x2 = curve.StdDistance() / (curve.DistanceMean() * 0.4931);
							double x3 = curve.AngleMean() / 180;

								
							//////////////////////////
							///iterate all the points within a defined area (shortest edge*2)
							for (j = 0; j < vert.size(); j++)
							{
								Vision.Point2D point;
									
								if (((EdgeStatus[extendpoint][j]==0)||(EdgeStatus[extendpoint][j]==2))&&(PointStatus[j]<2))
								{
									point = (Vision.Point2D)PL[j];
									angletemp = CalAngle(point,newpt, second_point);										
									x1 = angletemp/180;
																				
									areatemp = ((x3-x1)*m5)*((x3-x1)*m5)*Math.PI/2;
									areatemp = m3*(x2+m4)*Math.Cos(areatemp);
									areatemp = areatemp  + m1 + m2*Math.Sin(x1*Math.PI/2);
									areatemp = areatemp + m6*Math.Sin(x3*m7*Math.PI/2);
																					
									areatemp = areatemp*curve.DistanceMean();
									//	if ((EdgeStatus[extendpoint][j]==0)&&(EdgeMap[extendpoint][j]<=EdgeMap[head][tail]*2)) 
									//	if ((EdgeMap[extendpoint][j]<areatemp)&&(60<angletemp)&&(angletemp<300))
									if ((EdgeMap[extendpoint][j]<areatemp)&&(90<=angletemp)&&(angletemp<=270))										
									{					
										
										// calcu the entendable length of each edge.
										connectivevalue[k,j]=curve.DSconnectivalue(angletemp,EdgeMap[extendpoint][j],(double)numericUpDown4.Value);
									}
								}
			
							}		

							//find the best point to connect
							#region find the best point
							bestconnectivevalue=-50d;
							bestextenalpoint=-1; /**< 0 for head, 1 for tai?? */
							bestconnectivepoint=-1; 
							for(i=0;i<2;i++)
								for(j=0;j<vert.size();j++)
								{
									if((connectivevalue[i,j]>bestconnectivevalue)&&(PointStatus[j]<2))
									{
										bestconnectivevalue=connectivevalue[i,j];
										bestextenalpoint=i;
										bestconnectivepoint=j;
									}
								}
							#endregion find the best point

							#region Update the curves

							if(bestconnectivevalue>0)
							{				
								update = true;
								if (bestextenalpoint==0)
									extendpoint=head;
								else
									extendpoint=tail;

								#region one side is no free
								//An: defined tempID
								tempId = 	((Vision.Point2D)PL[bestconnectivepoint]).CurveID;
				

								//An: freedomdegree may not be used here
								//							if (freedomdegree==1)
								if (tempId == -1)
								{
									((Vision.Point2D)PL[bestconnectivepoint]).CurveID=((Vision.Point2D)PL[extendpoint]).CurveID;								
									Vision.StCurve cr=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
										
									extindex=cr.CatchPointEx((Vision.Point2D)PL[extendpoint]);
									if (extindex==0)
										cr.Insert(0,(Vision.Point2D)PL[bestconnectivepoint]);
									else
										cr.AddPoint((Vision.Point2D)PL[bestconnectivepoint]);

									EdgeStatus[extendpoint][bestconnectivepoint]=1;
									EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                    //((int)PointStatus[extendpoint])++;
                                    //((int)PointStatus[bestconnectivepoint])++;
                                    PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                    PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
									crcount++; /** curve count*/

								}
								#endregion oneside is no free
								
								#region both sides are not free
								//		if (freedomdegree==2)
								if (tempId!=-1)
								{
									//Join the two curve
									
									crh=CVL.ReadCurve(((Vision.Point2D)PL[extendpoint]).CurveID);
									Vision.StCurve crt=CVL.ReadCurve(((Vision.Point2D)PL[bestconnectivepoint]).CurveID);								
									int hid=crh.curveID;
									int tid=crt.curveID;
								
									//if ((extendablePointsi==0)&&(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0))
									//fzconnection++;
									//	if(EdgeStatus[head][tail]==0||EdgeStatus[tail][head]==0)
									//	{
									if(hid!=tid)
									{
										GCurveID++;
										for(j=0;j<PL.Count;j++)
										{
											if (((Vision.Point2D)PL[j]).CurveID==hid)
												((Vision.Point2D)PL[j]).CurveID=GCurveID;
											if (((Vision.Point2D)PL[j]).CurveID==tid)
												((Vision.Point2D)PL[j]).CurveID=GCurveID;
										}
										Vision.StCurve cr=CVL.ConnetTwoCurves(ref crh,ref crt);
										cr.curveID=GCurveID;							

										/* why remove???*/
										CVL.Remove(crh);
										CVL.Remove(crt);
										CVL.AddCurve(cr);						
									}
									else
										/**same curveID so connection will make a closed curve*/
										crh.Closed=1;							

									EdgeStatus[extendpoint][bestconnectivepoint]=1;
									EdgeStatus[bestconnectivepoint][extendpoint]=1;
                                    //((int)PointStatus[extendpoint])++;
                                    //((int)PointStatus[bestconnectivepoint])++;
                                    PointStatus[extendpoint] = ((int)PointStatus[extendpoint]) + 1;
                                    PointStatus[bestconnectivepoint] = ((int)PointStatus[bestconnectivepoint]) + 1;
									crcount++;
									//	}
										
								}
								#endregion both sides are not free
							}
							#endregion update the curves
						}//endfor 
					}while(update);

					#endregion repeating the update

				}
				#endregion update the curves 
			
			}while (!SearchOver);
			#endregion reconsider removed edge



			//Draw curves
			#region Draw curves
			for (i = 0; i < vert.size(); i++)
			{
				for (j = i + 1; j < vert.size(); j++)
				{
					if (EdgeStatus[i][j]==1)
					{
						g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), vert.get_Renamed(i).X, vert.get_Renamed(i).Y, vert.get_Renamed(j).X, vert.get_Renamed(j).Y);
					}

				}
			}
			#endregion 
			/////						
		}
		
		internal virtual void  drawAll()
		{
			buffGr.FillRegion(new System.Drawing.SolidBrush(SupportClass.GraphicsManager.manager.GetBackColor(buffGr)), new System.Drawing.Region(new System.Drawing.Rectangle(0, 0, Size.Width, Size.Height)));
			paint(buffGr);
			Refresh();
		}

		
		private void checkBoxPoint_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			drawAll();
		}

		private void buttonAdd_Click(object sender, System.EventArgs e)
		{
			numericUpDown3.Value=numericUpDown3.Value+1;
		}

		private void numericUpDown3_ValueChanged(object sender, System.EventArgs e)
		{
		drawAll();
		}

		private void buttonReset_Click(object sender, System.EventArgs e)
		{
			numericUpDown3.Value=0;
		}

		private void buttonGoldVariance_Click(object sender, System.EventArgs e)
		{
			double k=1.618;
			System.Collections.ArrayList seg=new ArrayList();
			int i=0;
			double j=1;
			Vision.Optimismfilter op =new Vision.Optimismfilter();
			
			for(i=0;i<40;i++)
			{
				j=j*k;
				seg.Add(j);
				//listBox1.Items.Add(j.ToString());
				listBox1.Items.Add("---"+op.Variance(seg).ToString());
				//listBox1.Items.Add("------"+op.ExternalLength(seg,j,j*k).ToString());
				//listBox1.Items.Add(op.ExternalLength(seg,j,j*k).ToString());
			}
			
		}

		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			/////////////////
			// Create OpenFileDialog
			OpenFileDialog opnDlg = new OpenFileDialog();
			opnDlg.InitialDirectory="G:/";
			// Set a filter for images
			opnDlg.Filter = "Background files|*.*";

			opnDlg.Title = "Open a picture";
			opnDlg.ShowHelp = true;

			if(opnDlg.ShowDialog() == DialogResult.OK)
			{
				// Read current selected file name
				curFileName = opnDlg.FileName;

				// Creat the Image object using
				// Image.FromFile;
				try
				{
					bgpicture= new Bitmap(curFileName);
				}
				catch(Exception exp)
				{
					MessageBox.Show(exp.Message);
				}
			}
			drawAll();
			Refresh();
			
			/////////////////
		}

		private void radioButton1_CheckedChanged(object sender, System.EventArgs e)
		{
			if(radioButton1.Checked)
			pointcolor=System.Drawing.Color.Black;
			drawAll();
			Refresh();
		}

		private void radioButton2_CheckedChanged(object sender, System.EventArgs e)
		{
			if(radioButton2.Checked)
				pointcolor=System.Drawing.Color.Red;
			drawAll();
			Refresh();
		}

		private void checkBoxAngle_CheckedChanged(object sender, System.EventArgs e)
		{
			if (checkBoxAngle.Checked == true)
			{
				setAngleDistanceDelauney(true);
				return ;
			}
			else
			{
				setAngleDistanceDelauney(false);
				return ;
			}
		}

		private void checkBoxAngleDistance2_CheckedChanged(object sender, System.EventArgs e)
		{
			if (checkBoxAngleDistance2.Checked == true)
			{
				setAngleDistance2Delauney(true);
				return ;
			}
			else
			{
				setAngleDistance2Delauney(false);
				return ;
			}		
		}

		private void checkboxDistance_CheckedChanged(object sender, System.EventArgs e)
		{
			if (checkboxDISCUR.Checked == true)
			{
				setDistanceBased(true);
				return ;
			}
			else
			{
				setDistanceBased(false);
				return ;
			}		
		}

		private void checkBox4_CheckedChanged(object sender, System.EventArgs e)
		{
			if (checkbox_DelEx.Checked == true)
			{
				drawDEx = true;
				//Refresh();
			}
			else 
				drawDEx = false;

		}

		private void checkBox4_CheckedChanged_1(object sender, System.EventArgs e)
		{
			///
			backgroundpicture=checkBox4.Checked;
			drawAll();
			Refresh();
			
		}

						
		private bool calAngle=false; 
		private void chboxAngle_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chboxAngle.Checked==true)
				calAngle = true;

			else
				calAngle = false;

		}

		private void checkBoxAngleDistance3_CheckedChanged(object sender, System.EventArgs e)
		{
			if (checkBoxVICUR.Checked==true)		
			{
				setAngleDistance3Delauney(true);
				return ;
			}
			else
			{
				setAngleDistance3Delauney(false);
				return ;
			}	
		}

		private void chkboxExperiment_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkboxExperiment.Checked==true)		
			{
				 setExperiment(true);
				return ;
			}
			else
			{
				setExperiment(false);
				return ;
			}	
		}
		private void chkboxInter_CheckedChanged(object sender, System.EventArgs e)
		{
		
			if ((chkboxInter.Checked==true)&&(checkBoxbeta.Checked==true))
			{
					setDistanceBasedNew(true);
					setInterStep(true);				

			}
			else if (checkBoxbeta.Checked==true)
			{				
				setInterStep(false);	
				setDistanceBasedNew(true);
			}
			else 
			{
				setInterStep(false);	
				setDistanceBasedNew(false);
			
			}

		}

		private void numericUpDown4_ValueChanged(object sender, System.EventArgs e)
		{
            if (this.checkBoxVICUR.Checked == false)
                this.checkBoxVICUR.Checked = true;
			setAngleDistance3Delauney(true);
			return ;
		}

		private void radioBtnPoint_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.radioBtnCountPoint.Checked)
			{
				if (this.vert.size()!=0)
                this.txtBxPoints.Text = this.vert.size().ToString();
                     
			}
			else if (this.radioBtnGenPoint.Checked)
			{

			}
		}

        private void menuItem2_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void menuItemImport_Click(object sender, EventArgs e)
        {
            // Create OpenFileDialog
				OpenFileDialog opnDlg = new OpenFileDialog();
				//opnDlg.InitialDirectory="G:/";
				// Set a filter for images
				opnDlg.Filter = "Txt files|*.txt";

				opnDlg.Title = "TxtFileViewer: Import text File";
				opnDlg.ShowHelp = true;

                if (opnDlg.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        StreamReader FileStreamReader = File.OpenText(opnDlg.FileName);
                        vert.clear();
                        while (FileStreamReader.Peek() != -1)
                        {
                            string[] words;
                            words = FileStreamReader.ReadLine().Split(',');
                            vert.add(Convert.ToInt32(words[0]), Convert.ToInt32(words[1]));

                        }
                        FileStreamReader.Close();

                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show(exp.Message);
                    }
                    
                }

                drawAll();
                Refresh();
          
        }

        private void menuItemExport_Click(object sender, EventArgs e)
        {
            //Save point file for Gathan program
            // Create OpenFileDialog
            SaveFileDialog savDlg = new SaveFileDialog();

            // Set a filter for images
            savDlg.Filter = "TXT file|*.txt";

            savDlg.Title = "TxtFileViewer: Export File";
            savDlg.ShowHelp = true;

            if (savDlg.ShowDialog() == DialogResult.OK)
            {
                // Read current selected file name
                curFileName = savDlg.FileName;

                try
                {

                    if (File.Exists(curFileName))
                    {
                        Console.WriteLine("File Created!");
                    }
                    else
                    {
                        Console.WriteLine("File Not Created!");
                    }

                    //Pass the filepath and filename to the StreamWriter Constructor
                    StreamWriter sw = new StreamWriter(curFileName);                 

                    for (int i = 0; i < vert.size(); i++)
                    {
                        System.Drawing.Point point;
                        point = vert.get_Renamed(i);

                        //sw.WriteLine(i + " " + String.Format("{0:00.000000}", point.X) + "," + String.Format("{0:00.000000}", point.Y));
                        sw.WriteLine(point.X + "," + point.Y);
                    }


                    //Close the file
                    sw.Close();

                }
                catch (Exception exp)
                {
                    MessageBox.Show(exp.Message);
                }
                finally
                {
                    Console.WriteLine("Executing finally block.");
                }
            }
        }

        private void menuItem4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("The program implements algorithms described in the following papers: \n" +
            "1) Y. Zeng, T.A. Nguyen, B. Yan and S. Li (2008), A distance-based parameter free curve "+ 
            "reconstruction algorithm, Computer-Aided Design, Vol.40, No. 2, 210-222. \n\n" +
                "2) T.A. Nguyen and Y. Zeng (2008), VICUR: A human-vision-based algorithm for curve reconstruction,"+ 
                "Robotics and Computer Integrated Manufacturing, Vol.24, No. 6, 824-834. \n\n"+
                "Contributors to the program: \n 1) Shuren Li \n 2) Thanh An Nguyen");
        }

	}
}
