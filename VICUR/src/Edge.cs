// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   Delaunay.java
using System;

class Edge
{
	virtual internal Edge NextE
	{
		set
		{
			nextE = value;
		}
		
	}
	virtual internal Edge NextH
	{
		set
		{
			nextH = value;
		}
		
	}
	virtual internal Triangle Tri
	{
		set
		{
			inT = value;
		}
		
	}
	virtual internal Edge InvE
	{
		set
		{
			invE = value;
		}
		
	}
	
	public Edge(Node node, Node node1)
	{
		Update(node, node1);
	}
	
	public virtual void  Update(Node node, Node node1)
	{
		p1 = node;
		p2 = node1;
		Setabc();
		AsIndex();
	}
	
	internal virtual Edge MakeSymm()
	{
		Edge edge = new Edge(p2, p1);
		LinkSymm(edge);
		return edge;
	}
	
	internal virtual void  LinkSymm(Edge edge)
	{
		invE = edge;
		if (edge != null)
			edge.invE = this;
	}
	
	//check if node is on the line. 
	public virtual int onSide(Node node)
	{
		double d = a * (double) node.x + b * (double) node.y + c;
		if (d > 0.0D)
			return 1;
		return d >= 0.0D?0:- 1;
	}
	
	//Construct an equation through p1, p2
	internal virtual void  Setabc()
	{
		a = p2.y - p1.y;
		b = p1.x - p2.x;
		c = p2.x * p1.y - p1.x * p2.y;
	}
	
	internal virtual void  AsIndex()
	{
		p1.anEdge = this;
	}
	
	internal virtual Edge MostLeft()
	{
		Edge edge;
		Edge edge1;
		for (edge1 = this; (edge = edge1.nextE.nextE.invE) != null && edge != this; edge1 = edge)
			;
		return edge1.nextE.nextE;
	}
	
	internal virtual Edge MostRight()
	{
		Edge edge;
		Edge edge1;
		for (edge1 = this; edge1.invE != null && (edge = edge1.invE.nextE) != this; edge1 = edge)
			;
		return edge1;
	}
	
	public virtual void  Draw(System.Drawing.Graphics g)
	{
		g.DrawLine(SupportClass.GraphicsManager.manager.GetPen(g), p1.x, p1.y, p2.x, p2.y);
	}
	
	internal Node p1;
	internal Node p2;
	internal Edge invE;
	internal Edge nextE;
	internal Edge nextH;
	internal Triangle inT;
	internal double a;
	internal double b;
	internal double c;
}