# VICUR
VICUR curve reconstruction algorithm (C#) based on human perception

A C# program developed in 2007 mimics how human connect points to form curves. The program works based on Gestalt principles of proximity and continuation .

Paper:

Nguyen, T. A., & Zeng, Y. (2008). VICUR: A human-vision-based algorithm for curve reconstruction. Robotics and Computer-Integrated Manufacturing, 24(6), 824-834.
Zeng, Y., Nguyen, T. A., Yan, B., & Li, S. (2008). A distance-based parameter free algorithm for curve reconstruction. Computer-Aided Design, 40(2), 210-222.
