#include <cstdlib>
#include <cmath>
#include <cstdio>
#include <ctime>
#include <cstring>


#define FLOAT_MAX_VALUE 999999999.9f

#define MAX_VERTICES 10000

#define C_BASE 16.0
#define C_EXP 1.5

#define MULTIPLE_COMPONENTS 1



typedef struct {
	float x,y;
	unsigned char degree;
	unsigned short indirectPos;
} tPoint;


tPoint V[MAX_VERTICES];

unsigned int numUnusedVertices;	//an index smaller than this number in the "unusedVertices" array indicates an "unused" vertex
unsigned short unusedVertices[MAX_VERTICES];

bool taken[MAX_VERTICES*MAX_VERTICES];

unsigned int numVertices;
unsigned int numEdgesCreated;
unsigned int maxDegree;
unsigned int numComponents;


inline float distanceSq(const unsigned int p, const unsigned int q) {
	const float x = V[p].x - V[q].x;
	const float y = V[p].y - V[q].y;
	return x*x+y*y;
}

inline void markVertex(const unsigned int i) {
	if (V[i].indirectPos < numUnusedVertices) {
		--numUnusedVertices;

		const unsigned short newPos = V[i].indirectPos;	//swap vertex to the end to mark it as "used"
		unusedVertices[newPos] = unusedVertices[numUnusedVertices];
		V[unusedVertices[newPos]].indirectPos = newPos;
		
		V[i].indirectPos = numVertices;	//"mark" the vertex as used
	}
}

void takeEdge(const unsigned int p, const unsigned int q) {
	taken[p*numVertices+q] = true;	//put edge into adjacency matrix
	taken[q*numVertices+p] = true;

	++numEdgesCreated;

	if (++V[p].degree > maxDegree)
		maxDegree = V[p].degree;
	if (++V[q].degree > maxDegree)
		maxDegree = V[q].degree;

	markVertex(p);
	markVertex(q);
}

inline float lineAngle(const unsigned int from, const unsigned int to, const unsigned int p) {
	const float x1 = V[to].x - V[from].x;
	const float y1 = V[to].y - V[from].y;
	const float x2 = V[p].x - V[to].x;
	const float y2 = V[p].y - V[to].y;

	return (float)acos( (x1 * x2 + y1 * y2) / sqrt((x1 * x1 + y1 * y1) * (x2 * x2 + y2 * y2)) );
}


void processEdge(const unsigned int p, const unsigned int q) {
	float weightSq;
	float minWeightSq = FLOAT_MAX_VALUE;
	unsigned int r;

	for (unsigned int i=0; i<numVertices; ++i)
		if (q != i) {
			weightSq = (float)pow(C_BASE*C_BASE, pow( fabs( lineAngle(p, q, i) ), C_EXP)) * distanceSq(q,i);

			if (weightSq < minWeightSq) {
				r = i;
				minWeightSq = weightSq;
			}
		}

	
		if ( !taken[q*numVertices+r] ) {
		takeEdge(q,r);
		processEdge(q,r);
//			processEdge(r,q);	//not necessary (only helps [but not always] for non-manifolds)
	}
}

void reconstructLenz() {
	//start with shortest edge
	float minWeight, distSq;
	unsigned int p, q;

	do {	//do until all components are found
		minWeight = FLOAT_MAX_VALUE;
		for (unsigned int i = 0; i < numUnusedVertices; ++i)
			for (unsigned int j = i+1; j < numUnusedVertices; ++j) {
				distSq = distanceSq( unusedVertices[i], unusedVertices[j] );
				if ( distSq < minWeight) {
					p = unusedVertices[i];
					q = unusedVertices[j];
					minWeight = distSq;
				}
			}

		++numComponents;
		takeEdge(p,q);
		processEdge(p,q);
		processEdge(q,p);
	} while ( MULTIPLE_COMPONENTS && (numUnusedVertices >= 2) );
}


void experiment() {
	memset(taken, 0, numVertices*numVertices*sizeof(taken[0]));

	//generate random point set
	for (unsigned int i=0;i<numVertices;++i) {
		//V[i].x = rand()/(float)RAND_MAX;
		//V[i].y = rand()/(float)RAND_MAX;
		V[i].degree = 0;
		V[i].indirectPos = i;
		unusedVertices[i] = i;
	}

	//start reconstruction and count #edges
	numUnusedVertices = numVertices;
	numEdgesCreated = 0;
	maxDegree = 0;
	numComponents = 0;

	reconstructLenz();
}


