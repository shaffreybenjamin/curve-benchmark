#!/bin/bash

evalname=Self-Intersecting
#_alglist="lenz optimaltransport peel"
_alglist="lenz peel optimaltransport"
#_alglist="lenz connect2d stretchdenoise"
_filelist="Figure1.txt Figure9.txt Figure10.txt Figure14a.txt Figure14b.txt Figure14c.txt Figure14d.txt Figure14e.txt Figure16.txt Figure19b.txt LFig5out.txt LFig6dout.txt nm1out.txt nm2out.txt nm3out.txt nm4out.txt nm5out.txt nm6out.txt nm7out.txt nm8out.txt nm9out.txt nm10out.txt OTFig1.txt OTFig14.txt OTFig18a.txt OTFig18g.txt OTFig18k.txt OTFig19b.txt"

#_filelist="Figure1.txt Figure9.txt Figure10.txt Figure14a.txt Figure14b.txt Figure14c.txt Figure14d.txt Figure14e.txt Figure16.txt Figure19b.txt OTFig1.txt OTFig14.txt OTFig18a.txt OTFig18g.txt OTFig18k.txt OTFig19b.txt"
#_filelist="LFig6dout.txt"
#_filelist="nm2out.txt"
env alglist="${_alglist}" filelist="${_filelist}" ./run-evalsub.sh ${evalname} ../testdata/Self-Intersecting yes

gnuplot -c self-intersecting.plot


