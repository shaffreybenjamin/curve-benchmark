import java.util.Date;

int resizefactor=5; //Edit the image resizing value to avoid hanging pixels
String path = "./InvertHS-SOD"; //Root directory of input images
  
String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list();
    return names;
  } else {
    return null;
  }
}
void setup() {
  size(100,100);
  String[] filenames = listFileNames(path);
  File[] files = listFiles(path);
  println("In total "+files.length+" files");
  for (int i = 0; i < files.length; i++) {
    File f = files[i];    
    int UserFileNameStringLength = f.getName().length();
    String extension = f.getName().substring(UserFileNameStringLength-3);
    if(f.getName().endsWith("jpg")||f.getName().endsWith("png")||f.getName().endsWith("bmp")
      ||f.getName().endsWith("JPG")||f.getName().endsWith("PNG")||f.getName().endsWith("BMP"))
    {
      PrintWriter output=createWriter("./TextFiles/"+f.getName()+".txt");
      PrintWriter output1=createWriter("./EdgeFiles/"+f.getName()+".txt");
      PImage im=loadImage(path+"/"+f.getName());
      im.resize(im.width*resizefactor,im.height*resizefactor);
      PImage im1=createImage(im.width,im.height,RGB);
      for(int j=0;j<im.width;j++)
        for(int k=0;k<im.height;k++)
          im1.set(j,k,color(255));
      for(int j=0;j<im.width;j++)
        for(int k=0;k<im.height;k++)
          if(red(im.get(j,k))>50)
            im.set(j,k,color(255));
          else
            im.set(j,k,color(0));
      for(int j=1;j<im.width-1;j++)
        for(int k=1;k<im.height-1;k++){
          if(im.get(j,k)==color(255)){
            if(im.get(j-1,k-1)==color(0)||im.get(j-1,k)==color(0)||im.get(j,k-1)==color(0)||
              im.get(j+1,k-1)==color(0)||im.get(j-1,k+1)==color(0)||im.get(j+1,k)==color(0)||
              im.get(j,k+1)==color(0)||im.get(j+1,k+1)==color(0)){
                output.println(j+" "+k);
          im1.set(j,k,color(0));
        }
        else
        im1.set(j,k,color(255));
      }
    }
    for(int j=0;j<im.width;j++)
      for(int k=0;k<im.height;k++)
        if(im.get(j,k)==color(0))
          if(j==0||k==0||j==im.width-1||k==im.height-1){ 
            output.println(j+" "+k);
            im1.set(j,k,color(255));
          }
          PImage im3=im1;
          for(int j=1;j<im3.width-1;j++)
            for(int k=1;k<im3.height-1;k++){
              if(im3.get(j,k)==color(0)){
                if(im3.get(j-1,k)==color(0)){
                  output1.println((j-1)+" "+k+" "+j+" "+k);
                  im3.set(i,j,color(255));
                }
        if(im3.get(j,k-1)==color(0)){
          output1.println((j)+" "+(k-1)+" "+j+" "+k);
          im3.set(i,j,color(255));
        }
        if(im3.get(j+1,k)==color(0)){
          output1.println((j+1)+" "+k+" "+j+" "+k);
          im3.set(i,j,color(255));
        }
        if(im3.get(j,k+1)==color(0)){
          output1.println((j)+" "+(k+1)+" "+j+" "+k);
          im3.set(i,j,color(255));
        }
      }
    }
    im1.save("./Output/"+f.getName());
    output.flush();
    output.close();
    output1.flush();
    output1.close();
    im1.save("./Output/"+f.getName());
    }
  }
  exit();
}