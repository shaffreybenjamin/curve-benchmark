// motif window for displaying graphics (header file)
// $Id: motif_window.h,v 1.17 2004/09/21 20:34:05 wenger Exp $

#ifndef _MOTIF_WINDOW_H
#define _MOTIF_WINDOW_H

// include motif definition files
#include <Xm/CascadeB.h>
#include <Xm/DialogS.h>
#include <Xm/FileSB.h>
#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/Label.h>
#include <Xm/MainW.h>
#include <Xm/PanedW.h>
#include <Xm/PushB.h>
#include <Xm/RowColumn.h>
#include <Xm/Scale.h>
#include <Xm/ScrolledW.h>
#include <Xm/Text.h>
#include <Xm/TextF.h>
#include <Xm/ToggleB.h>

// include OpenGL definition files
#include <X11/keysym.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>
#include <GL/GLwDrawA.h>           // draw area widget

// function types
typedef void (*SIMPLE_FUNC_PTR)();
typedef void (*FILE_FUNC_PTR)(char *filename);
typedef void (*WIDGET_FUNC_PTR)(Widget w);


// classes
class MOTIF_WINDOW;
class MOTIF_MENU;
class MOTIF_RADIOBOX;
class MOTIF_DIALOG;
class MOTIF_FILE_DIALOG;

const int MOTIF_WINDOW_NUM_BUTTONS = 3;

class MOTIF_WINDOW
{ 
 protected:
      Widget toplevel;
      Widget mainw;
      Widget menubar;
      Widget form;                    // form for work area
      Widget glxarea_frame;           // drawing area frame
      Widget glxarea;                 // OpenGL drawing area
      Widget msgw;                    // scrolled text widget for messages
      XmTextPosition msgw_position;   // cursor position in msgw

      Display *dpy;                   // X display
      XVisualInfo *vi;
      XtAppContext app;

      GLXContext   cx;                // OpenGL context
      GLboolean    doubleBuffer;

      void Init_Win(const char *title, const int width, const int height,
		    String * fallbackResources); 
                                      // initialize window
      void Init_Clipwin(const int left, const int bot,
			const int right, const int top);
                                      // initialize clipping window
      void Init_Callbacks();          // initialize callbacks

// callbacks and functions used in callbacks
      void (*buttonpressCB[MOTIF_WINDOW_NUM_BUTTONS])(int x, int y);
                                      // button press callbacks
      void (*buttonreleaseCB[MOTIF_WINDOW_NUM_BUTTONS])(int x, int y);
                                      // button press callbacks
      void (*keypressCB)(KeySym key);           // key press callback
      void (*keyreleaseCB)(KeySym key);         // key release callback
      void (*resizeCB)(int width, int height);  // resize callback

 public: 
      MOTIF_WINDOW(const char *title,
		   const int width, const int height); // constructor
      MOTIF_WINDOW(const char *title,
		   const int width, const int height,
		   String * fallbackResources); // constructor
      void Realize();                 // "realize" (display) the X-window
      void Loop();                    // enter mainoop
      void AddCallback(String callback_name, XtCallbackProc callback,
		       XtPointer data = NULL);
                                      // add callback to the graphics area
      void AddCallback(String callback_name, SIMPLE_FUNC_PTR callback);
                                      // add callback to the graphics area
      Widget AddMenu(MOTIF_MENU & menu, char * menu_name); 
                                      // add a new menu to the menubar
      Widget AddMenu(MOTIF_RADIOBOX & radio_box, char * radiobox_name); 
                                      // add a new menu of type radio box
      Widget AddTearOffMenu(MOTIF_MENU & menu, char * menu_name); 
                             // add a new tear off menu to the menubar
      Widget AddTearOffMenu(MOTIF_RADIOBOX & radio_box, char * radiobox_name); 
                             // add a new tear off menu of type radio box
      Widget AddDialog(MOTIF_DIALOG & dialog, char * dialog_name,
		       WidgetClass control_area_class = 
		       xmRowColumnWidgetClass);
      Widget AddDialog(MOTIF_FILE_DIALOG & dialog, char * dialog_name);
                                      // add a file dialog window
      Widget AddMsg(const int num_rows);// add an output message area
      void OutMsg(char * msg);        // output msg to the message area
      void SetButtonPressCB(const int button_index, 
			    void (*callback)(int, int));
                                      // set button press callback
      void SetButtonReleaseCB(const int button_index, 
			      void (*callback)(int, int));
                                      // set button release callback
      void SetKeyPressCB(void (*callback)(KeySym));
                                      // set key press callback
      void SetKeyReleaseCB(void (*callback)(KeySym));
                                      // set key release callback
      void SetResizeCB(void (*callback)(int, int));
                                      // set resize callback
      int NumButtons() const          // return # of mouse buttons
        { return(MOTIF_WINDOW_NUM_BUTTONS); };
      int Width() const;              // return width of drawing area
      int Height() const;             // return height of drawing area
      void Bell(int percent = 0) const      // ring keyboard bell
	{ XBell(dpy, percent); };
      void ErrorMsg(char *error_msg) const; // output error message & halt
      void WarningMsg(char *error_msg) const; // output warning message
      void UpdateDisplay() const      // update display
	{ XmUpdateDisplay(Toplevel()); };

      ~MOTIF_WINDOW();                // destructor

// coordinate transformations
      void Xwin2GLwin(const int x_Xwin, const int y_Xwin,
		      int & x_GLwin, int & y_GLwin) const
            // map X-window coordinates to GL window coordinates
            // X-window's - upper left origin; GL window - lower left origin
	{ x_GLwin = x_Xwin; y_GLwin = Height() - y_Xwin; };
      void GLwin2Obj(const int x_GLwin, const int y_GLwin,
		     double & x_obj, double & y_obj) const;
                                     // map GL window to object coordinates

// invoke callback and related functions
      void PressButton(int button_index, int x, int y);
                                      // call button press callback
      void ReleaseButton(int button_index, int x, int y);          
                                      // call button release callback
      void PressKey(KeySym key);      // call key press callback
      void ReleaseKey(KeySym key);    // call key release callback
      void Resize(int width, int height);  // call resize callback

// return widgets and other Xt objects
      Widget Toplevel() const {return(toplevel);};
      Widget DrawAreaWidget() const {return(glxarea);};

// error handler (mainly for widget creation/initialization errors)
// errors related existing widgets trigger calls to ErrorMsg() or WarningMsg()
      class ERROR {

      public:
	char * msg1;                 // error message 1 (main error message)
	char * msg2;                 // error message 2 (hints on cause)
	int num;                     // error number

	ERROR(char * errmsg1, char * errmsg2, int errnum)
	  { msg1 = errmsg1; msg2 = errmsg2; num = errnum; };
      };
};

typedef MOTIF_WINDOW * MOTIF_WINDOW_PTR;

class MOTIF_MENU {

  friend class MOTIF_WINDOW;

  protected :

    Widget button;
    Widget pane;

  public:

    MOTIF_MENU()
      { button = pane = NULL; };

    Widget Add(char * btn_name, SIMPLE_FUNC_PTR callback);
                                     // add push button and callback to menu
    Widget Add(char * btn_name, XtCallbackProc callback, 
	       XtPointer data = NULL);
                                     // add push button and callback to menu
    Widget AddToggle(char * btn_name, bool state = false);
                                     // add toggle button to menu
    Widget AddToggle(char * btn_name, WIDGET_FUNC_PTR callback,
		     bool state = false);
                                     // add toggle button and callback to menu
    Widget AddToggle(char * btn_name, XtCallbackProc callback,
		     bool state = false);
                                     // add toggle button and callback to menu
    Widget AddToggle(char * btn_name, XtCallbackProc callback, XtPointer data,
		     bool state = false);
                                     // add toggle button and callback to menu
    Widget AddMenu(MOTIF_MENU & submenu, char * submenu_name);
                                     // add a submenu to this menu
    Widget AddMenu(MOTIF_RADIOBOX & radiobox, char * radiobox_name);
                                     // add a submenu of type radio box
    Widget AddTearOffMenu(MOTIF_MENU & submenu, char * submenu_name);
                              // add a tear off submenu to this menu
    Widget AddTearOffMenu(MOTIF_RADIOBOX & radiobox, char * radiobox_name);
                              // add a tear off submenu of type radio box
    Widget Add(MOTIF_DIALOG & dialog, char * btn_name);
                                     // add button to popup motif dialog
    Widget Add(MOTIF_FILE_DIALOG & dialog, char * btn_name);
                                     // add button to popup motif file dialog
    void SetSensitive(bool state);   // set sensitive
    void ErrorMsg(char *error_msg) const;  // output error message & halt
    void WarningMsg(char *error_msg) const;// output warning message
};

typedef MOTIF_MENU * MOTIF_MENU_PTR;

class MOTIF_RADIOBOX {

  friend class MOTIF_WINDOW;
  friend class MOTIF_MENU;
  friend class MOTIF_DIALOG;

  protected:
    Widget button;
    Widget widget;

  public:
    MOTIF_RADIOBOX()
      { button = widget = NULL; };

    Widget AddToggle(char * btn_name, bool state = false);
                                     // add toggle button to menu
    Widget AddToggle(char * btn_name, WIDGET_FUNC_PTR callback,
		     bool state = false);
                                     // add toggle button and callback to menu
    Widget AddToggle(char * btn_name, XtCallbackProc callback,
		     bool state = false);
                                     // add toggle button and callback to menu
    Widget AddToggle(char * btn_name, XtCallbackProc callback, XtPointer data,
		     bool state = false);
                                     // add toggle button and callback to menu
    void SetSensitive(bool state);   // set sensitive
    void ErrorMsg(char *error_msg) const;  // output error message & halt
    void WarningMsg(char *error_msg) const;// output warning message
};

typedef MOTIF_RADIOBOX * MOTIF_RADIOBOX_PTR;

class MOTIF_DIALOG {

  friend class MOTIF_WINDOW;

  protected:
    Widget shell;
    Widget pane;
    Widget control_area;
    Widget action_area;

    virtual void InitControlArea
      (WidgetClass control_area_class = xmRowColumnWidgetClass);
    virtual void InitActionArea();

  public:
    MOTIF_DIALOG();

    Widget AddAction(char * action_name, SIMPLE_FUNC_PTR callback);
                              // add action button and callback to dialog
    Widget AddAction(char * action_name, XtCallbackProc callback,
		     XtPointer data = NULL);
                              // add action button and callback to dialog
    Widget AddPopdownAction(char * action_name);
                              // add action button and pop down callback
    void Popup();             // pop up dialog
    void Popdown()            // pop down dialog
      {XtPopdown(shell);};
    Widget AddLabel(char * label);          // add a label
    Widget AddHScale(char * label, int min, int max, int value, int decimal);
                                            // add horizontal scale
    Widget Add(MOTIF_RADIOBOX & radio_box); // add a (horizontal) radio box

    // return dialog widgets
    Widget Shell() {return(shell);};
    Widget ActionArea() {return(action_area);};
    Widget ControlArea() {return(control_area);};
    void ErrorMsg(char *error_msg) const;  // output error message & halt
    void WarningMsg(char *error_msg) const;// output warning message
};

typedef MOTIF_DIALOG * MOTIF_DIALOG_PTR;

class MOTIF_FILE_DIALOG {

  friend class MOTIF_WINDOW;

  protected:
    Widget dialogw;     // dialog widget

  public:
    MOTIF_FILE_DIALOG() 
      { dialogw = NULL; };
    void AddCallback(char * action_name, XtCallbackProc callback,
		     XtPointer data = NULL);
                        // add callback to action button
    void AddCallback(char * action_name, FILE_FUNC_PTR callback);
                        // add callback to action button
                        // wraps callback in function to extract file name
    void AddPopdownCallback(char * action_name = XmNcancelCallback);
                        // add pop down callback to action button
    void Popup();       // pop up dialog
    void Popdown()      // pop down dialog
      {XtPopdown(Shell());};
    void ErrorMsg(char *error_msg) const;  // output error message & halt
    void WarningMsg(char *error_msg) const;// output warning message

    // return dialog widgets
    Widget Shell() {return(XtParent(dialogw));};
    Widget Dialog() {return(dialogw);};
};

typedef MOTIF_FILE_DIALOG * MOTIF_FILE_DIALOG_PTR;


// callbacks cannot be member functions
void MOTIF_WINDOW_InputCB(Widget w, XtPointer data, XtPointer callData);
                                     // user input callback
void MOTIF_WINDOW_ResizeCB_wrapper(Widget w, 
				  XtPointer data, XtPointer callData);
                                     // wrapper for resize callback
void MOTIF_DIALOG_PopupCB(Widget w, XtPointer data, XtPointer callData);
                                     // pop up dialog callback
void MOTIF_DIALOG_PopdownCB(Widget w, XtPointer data, XtPointer callData);
                                     // pop down dialog callback
void MOTIF_FILE_DIALOG_PopupCB(Widget w, XtPointer data, XtPointer callData);
                                     // pop up file dialog callback
void MOTIF_FILE_DIALOG_PopdownCB(Widget w, XtPointer data, XtPointer callData);
                                     // pop down file dialog callback
void MOTIF_WINDOW_Default_BpressCB(int x, int y); 
                                     // default button press callback
void MOTIF_WINDOW_Default_BreleaseCB(int x, int y); 
                                     // default button release callback
void MOTIF_WINDOW_Default_KpressCB(KeySym key); 
                                     // default key press callback
void MOTIF_WINDOW_Default_KreleaseCB(KeySym key); 
                                     // default key release callback
void MOTIF_WINDOW_Default_ResizeCB(int x, int y); 
                                     // default resize callback
void MOTIF_MENU_FileCB(Widget w, XtPointer data, XtPointer callData);
                                     // menu button file callback
void MOTIF_FILE_DIALOG_CB_wrapper(Widget w, 
				  XtPointer data, XtPointer callData);
                                     // wrapper for file dialog callback

// convenience utilities for creating/setting widgets

Widget create_vscale(Widget parent, char * label, 
		     int min, int max, int value, int decimal);
                                      // create vertical scale
Widget create_hscale(Widget parent, char * label,
		     int min, int max, int value, int decimal);
                                      // create horizontal scale
void set_label(Widget label_widget, char * label);
                                      // set label
void set_accelerator(Widget button_widget, char * accelerator,
		     char * accelerator_text);
                                      // set menu accelerator

void load_Xfont(Display * display, char * font_name, int & font_base);
void load_Xfont(Display * display, char * font_name, int & font_base, 
		XFontStruct * & fontInfo);
const int ERROR_XFONT_NOT_FOUND = 1110;
const int ERROR_NO_DISPLAY_LIST = 1120;
const int ERROR_MISSING_WIDGET = 1200;

//#endif _MOTIF_WINDOW_H
#endif

